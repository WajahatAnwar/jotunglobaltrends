function createCookie(t, e, o) {
  var i = new Date();
  i.setFullYear(i.getFullYear() + o);
  var n = "; expires=" + i.toUTCString();
  document.cookie = t + "=" + e + n + "; path=/;";
}

function readCookie(t) {
  for (
    var e = t + "=", o = document.cookie.split(";"), i = 0;
    i < o.length;
    i++
  ) {
    for (var n = o[i]; " " == n.charAt(0); ) n = n.substring(1, n.length);
    if (0 == n.indexOf(e)) return n.substring(e.length, n.length);
  }
  return "";
}

function cookieArray(t) {
  var e = readCookie(t);
  return e.split(",");
}

function eraseCookie(t) {
  createCookie(t, "", -1);
}

function checkCookie(t) {
  var e = document.cookie.indexOf(t);
  e == -1 && "history" == t
    ? ($(".js-cookie").addClass("is-active"),
      $("button.js-cookie-close").focus())
    : e == -1 && (void 0, createCookie(t, null, 1));
}

function toggleCookieValue(t, e) {
  var o = cookieArray(t);
  (e = e.toString()),
    "null" === o[0] || "" === o[0]
      ? createCookie("favorites", e, 1)
      : ((valuei = o.indexOf(e)),
        valuei > -1
          ? (o.splice(valuei, 1), createCookie("favorites", o.join(","), 1))
          : (o.push(e), createCookie("favorites", o.join(","), 1)));
}

function checkFavorites(t) {
  for (var e = cookieArray(t), o = 0; o < e.length; o++)
    $("body")
      .find('.js-btn-favorite[data-favorite="' + e[o] + '"]')
      .addClass("is-active");
  updateNav(e, $(".js-favorite-nav__count")), sortFavorites();
}

function updateNav(t, e) {
  "" != t && "null" != t && t.length >= 1 ? e.text(t.length) : e.text("0");
}

function sortFavorites() {
  var t = cookieArray("favorites");
  if (
    ((favorited_images = []),
    (favorited_colors = []),
    (favorited_products = []),
    (void 0 !== jsonFavorites || "" !== jsonFavorites) &&
      "null" != t[0] &&
      "" != t[0])
  )
    for (var e = 0; e < t.length; e++) {
      var o = t[e],
        i = jsonFavorites.type;
      switch (i) {
        case "image":
          favorited_images.push(o);
          break;
        case "color":
          favorited_colors.push(o);
          break;
        case "product":
          favorited_products.push(o);
      }
    }
}

function goBack(t) {
  window.history.length ? history.back(-1) : (void 0, (document.location = t));
}

function appendColors(t, e, o) {
  o ||
    (o = {
      clickable: !0,
      caption: !0,
      helper: !0,
      favorite: !0,
      checkFavorites: !0,
      wrapper: !1
    });
  for (var i = 0; i < t.length; i++) {
    if ("" == t[i] || " " == t[i]) return;
    var n = t[i].toString(),
      s = jsonFavorites[n].result,
      a = s.name,
      r = n,
      l = "",
      c = "",
      d = "",
      h = "",
      u =
        '<i class="material-icons material-icons--first">&#xE87E;</i><i class="material-icons material-icons--second">&#xE87D;</i>';
    o.clickable && (h = "js-color-card"),
      o.caption &&
        (l =
          '<figcaption class="color-card__caption"><p>' +
          a +
          "</p></figcaption>"),
      o.helper &&
        (c =
          '<div class="color-card__helper"><button class="btn--plus" type="button"><span class="btn__text">+</span> <span class="sr-only">' +
          Lang.get("js.color_fullscreen") +
          "</span></button></div>"),
      o.favorite &&
        (d =
          '<button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="' +
          Lang.get("js.add_color_wishlist") +
          '" data-favorite="' +
          r +
          '">' +
          u +
          ' <span class="sr-only">' +
          Lang.get("js.add_color_wishlist") +
          "</span></button>");
    var p =
      '<figure class="color-card ' +
      h +
      " u-bg--" +
      r +
      '" data-color-id="' +
      r +
      '">' +
      l +
      c +
      d +
      "</figure>";
    all_colors.push(a),
      0 == o.wrapper
        ? jQuery(e).append(p)
        : jQuery(e).append('<div class="' + o.wrapper + '">' + p + "</div>"),
      o.checkFavorites && checkFavorites("favorites");
  }
  if (p.badge_02) {
    var f = $.parseHTML(all_colors.join(", ").toUpperCase()),
      m = $(f).text();
    p.badge_02.description = m;
  }
}

function appendProduct(t, e) {
  var o = !1;
  if (t.hasClass("js-btn-plus-override")) {
    o = !0;
    var i = "<h3>" + t.attr("data-title") + "</h3>",
      n = "<p>" + t.attr("data-text") + "</p>",
      s = i + n;
    $(".js-modal-products").append(s);
  }
  for (var a = 0; a < e.length; a++) {
    var s,
      r,
      l = e[a].toString(),
      c = jsonFavorites["product_" + l].result,
      d = c.header,
      h = c.desc,
      u = c.image,
      p = c.tag,
      f =
        '<i class="material-icons material-icons--first">&#xE87E;</i><i class="material-icons material-icons--second">&#xE87D;</i>',
      m =
        '<button class="btn btn--favorite heartBeat btn--favorite btn--favorite-contrast js-btn-favorite" type="button" title="' +
        Lang.get("js.add_product") +
        '" data-favorite="product_' +
        l +
        '">' +
        f +
        '<span class="sr-only">' +
        Lang.get("js.add_product") +
        "</span></button>",
      v =
        '<figure class="product__image"><img class="img-responsive js-product-img" src="' +
        u +
        '">' +
        m +
        "</figure>",
      i = '<h3 class="product__title">' + c.product + "</h3>";
    if (o === !1) {
      var g = '<h3 class="product__header js-product-header">' + d + "</h3>",
        y = '<p class="product__desc js-product-desc">' + h + "</p>",
        n = '<div class="product__text">' + g + y + "</div>",
        b = '<p class="product__tag js-product-tag">' + p + "</p>";
      (s = '<div class="product__body">' + i + v + b + "</div>"),
        (r =
          '<div class="product js-modal-product" data-product="product_' +
          l +
          '">' +
          n +
          s +
          "</div>");
    } else
      (s = '<div class="product__body">' + i + v + "</div>"),
        (r =
          '<div class="product js-modal-product" data-product="product_' +
          l +
          '">' +
          s +
          "</div>");
    $(".js-modal-products").append(r), checkFavorites("favorites");
  }
}

function launchIntoFullscreen(t) {
  t.requestFullscreen
    ? t.requestFullscreen()
    : t.mozRequestFullScreen
      ? t.mozRequestFullScreen()
      : t.webkitRequestFullscreen
        ? t.webkitRequestFullscreen()
        : t.msRequestFullscreen && t.msRequestFullscreen();
}

function exitFullscreen() {
  document.exitFullscreen
    ? document.exitFullscreen()
    : document.mozCancelFullScreen
      ? document.mozCancelFullScreen()
      : document.webkitExitFullscreen && document.webkitExitFullscreen();
}

function toggleResponsiveNav(t) {
  var e = $(".navbar"),
    o = $(".navbar-nav"),
    i = $(".navbar-collapse"),
    n = $(".navbar-header");
  !e.hasClass("is-overflowing") &&
    o.outerWidth(!0) > i.width() - n.width() &&
    (e.addClass("is-overflowing"), e.attr("data-breakpoint", t)),
    e[0].hasAttribute("data-breakpoint") &&
      t > e.attr("data-breakpoint") &&
      e.removeClass("is-overflowing");
}

function selectColor(t) {
  var e = {
      clickable: !0,
      caption: !0,
      helper: !0,
      favorite: !0,
      checkFavorites: !1,
      wrapper: "palettes__color"
    },
    o = jsonFavorites[t].result;
  console.log(o);
  o &&
    ((window.location.hash = t),
    Object.keys(o).forEach(function(i) {
      if (i.includes("group")) {
        var n = i.match(/\d+/)[0];
        o[i] !== "color." + t + ".group.4" &&
        o[i] !== "color." + t + ".group." + n &&
        "" !== o[i]
          ? appendColors(o[i].split(" "), ".js-palette-group-" + n, e)
          : $(".js-palette-group-" + n)
              .parent()
              .addClass("hidden");
      }
    })),
    (e.checkFavorites = !0),
    appendColors($.makeArray(t), ".js-palette-base", e),
    $(".palettes__area--select").css("margin-left", "-100%"),
    $(".palettes__area--select").removeClass("is-active"),
    $(".palettes__area--result").addClass("is-active");
}

function deselectColor() {
  removeHash(),
    $(".palettes__area--select").css("margin-left", ""),
    $(".palettes__area--select").addClass("is-active"),
    $(".palettes__area--result").removeClass("is-active"),
    setTimeout(function() {
      for (
        $(".palettes__section").removeClass("hidden"),
          $(".js-palette-base").html(""),
          i = 1;
        i < 4;
        i++
      )
        $(".js-palette-group-" + i).html("");
    }, 500);
}

function scrollTo(t) {
  return (
    $("html, body").animate(
      {
        scrollTop: t.offset().top
      },
      700
    ),
    !1
  );
}

function removeHash() {
  var t,
    e,
    o = window.location;
  "pushState" in history
    ? history.pushState("", document.title, o.pathname + o.search)
    : ((t = document.body.scrollTop),
      (e = document.body.scrollLeft),
      (o.hash = ""),
      (document.body.scrollTop = t),
      (document.body.scrollLeft = e));
}

function resetPalettes() {
  deselectColor(), scrollTo($("#palettes"));
}

function hashHandler() {
  (window.location.hash && "" != window.location.hash) || resetPalettes();
}

function downloadImageBlob(t, e) {
  var o = $("#modal_save .save"),
    i = new XMLHttpRequest(),
    n = $.parseHTML(t.markup),
    s = $('<div id="temp" style="display: none;"></div>');
  console.log(i);
  console.log(n);
  console.log(s);
  s.html(n), $("body").append(s);
  var a = "<!doctype html><html>" + $("#temp").get(0).innerHTML + "</html>";
  $("#temp").remove(),
    i.open("POST", "https://colours-api-stage.allegro.no/share/image", !0),
    i.setRequestHeader(
      "Content-Type",
      "application/x-www-form-urlencoded; charset=UTF-8"
    ),
    (i.responseType = "blob"),
    (i.onload = function() {
      if ((o.removeClass("is-loading"), 200 === i.status)) {
        var n = new Blob([i.response], {
            type: "image/jpg"
          }),
          s = document.createElement("a");
        (s.href = window.URL.createObjectURL(n)),
          (s.download = "JOTUN-" + t.colorID + ".jpg"),
          document.body.appendChild(s),
          s.click(),
          document.body.removeChild(s),
          e && e();
      } else e && e();
    }),
    void 0,
    i.send("markup=" + encodeURI(a));
}

function closeSaveModal() {
  $("#modal_save").modal("hide");
}

function loadPalette(t) {
  var e = $(".js-palette-base .js-color-card").attr("data-color-id"),
    o = $("#modal_save .save");
  //custom_code
  var original_colour = jsonFavorites[e]["result"]["name"];
  var original_name = jsonFavorites[e]["result"]["hex"];

  var comp_var = jsonFavorites[e]["result"]["group_1"];
  var acce_var = jsonFavorites[e]["result"]["group_2"];
  var whit_var = jsonFavorites[e]["result"]["group_3"];
  console.log(acce_var);
  var temp_comp = comp_var.split(" ");
  var temp_acce = acce_var.split(" ");
  var temp_whit = whit_var.split(" ");

  var comp_name_array = [];
  var comp_color_array = [];

  var acce_name_array = [];
  var acce_color_array = [];

  var whit_name_array = [];
  var whit_color_array = [];

  if (temp_comp.length > 1 || temp_comp["0"] !== "color." + e + ".group.1") {
    for (var i = 0; i < temp_comp.length; i++) {
      var search_in_favorites = temp_comp[i];

      var color_name = jsonFavorites[search_in_favorites]["result"]["name"];
      var color = search_in_favorites;

      comp_name_array.push(color_name);
      comp_color_array.push(color);
    }
  }

  console.log(temp_acce);
  if (temp_acce > 1 || temp_acce["0"] !== "color." + e + ".group.2") {
    for (var i = 0; i < temp_acce.length; i++) {
      var search_in_favorites = temp_acce[i];

      if (search_in_favorites !== "") {
        var color_name = jsonFavorites[search_in_favorites]["result"]["name"];
        var color = search_in_favorites;

        acce_name_array.push(color_name);
        acce_color_array.push(color);
      }
    }
  }
  if (temp_whit.length > 1 || temp_whit["0"] !== "color." + e + ".group.1") {
    for (var i = 0; i < temp_whit.length; i++) {
      var search_in_favorites = temp_whit[i];

      var color_name = jsonFavorites[search_in_favorites]["result"]["name"];
      var color = search_in_favorites;

      whit_name_array.push(color_name);
      whit_color_array.push(color);
    }
  }

  data = {
    comp_color_array: comp_color_array,
    comp_name_array: comp_name_array,
    acce_color_array: acce_color_array,
    acce_name_array: acce_name_array,
    whit_color_array: whit_color_array,
    whit_name_array: whit_name_array,
    original_colour: original_colour,
    original_name: original_name
  };

  o.addClass("is-loading"),
    $.ajax({
      url: "welcome/share/image/" + e,
      data: data
    })
      .done(function(o) {
        t &&
          t({
            markup: o,
            colorID: e
          });
      })
      .error(function() {
        void 0,
          o.removeClass("is-loading"),
          t &&
            t({
              markup: !1
            });
      });
}

function downloadPaletteImage() {
  loadPalette(function(t) {
    // alert(t.markup);
    // alert(t.colorID);
    downloadImageBlob({
      markup: t.markup,
      colorID: t.colorID
    });
  });
}

function printPaletteImage() {
  var t = $("#modal_save .save");
  loadPalette(function(e) {
    e.markup
      ? $.ajax({
          type: "post",
          url: "https://colours-api-stage.allegro.no/share/image/url",
          contentType: "application/x-www-form-urlencoded",
          enctype: "multipart/form-data",
          dataType: "json",
          data: {
            markup: e.markup
          }
        })
          .done(function(e) {
            (window.location = e.url), t.removeClass("is-loading");
          })
          .error(function() {
            t.removeClass("is-loading");
          })
      : t.removeClass("is-loading");
  });
}
if (
  (!(function(t, e) {
    var o = e(t, t.document);
    (t.lazySizes = o),
      "object" == typeof module && module.exports && (module.exports = o);
  })(window, function(t, e) {
    "use strict";
    if (e.getElementsByClassName) {
      var o,
        i = e.documentElement,
        n = t.Date,
        s = t.HTMLPictureElement,
        a = "addEventListener",
        r = "getAttribute",
        l = t[a],
        c = t.setTimeout,
        d = t.requestAnimationFrame || c,
        h = t.requestIdleCallback,
        u = /^picture$/i,
        p = ["load", "error", "lazyincluded", "_lazyloaded"],
        f = {},
        m = Array.prototype.forEach,
        v = function(t, e) {
          return (
            f[e] || (f[e] = new RegExp("(\\s|^)" + e + "(\\s|$)")),
            f[e].test(t[r]("class") || "") && f[e]
          );
        },
        g = function(t, e) {
          v(t, e) ||
            t.setAttribute("class", (t[r]("class") || "").trim() + " " + e);
        },
        y = function(t, e) {
          var o;
          (o = v(t, e)) &&
            t.setAttribute("class", (t[r]("class") || "").replace(o, " "));
        },
        b = function(t, e, o) {
          var i = o ? a : "removeEventListener";
          o && b(t, e),
            p.forEach(function(o) {
              t[i](o, e);
            });
        },
        w = function(t, o, i, n, s) {
          var a = e.createEvent("CustomEvent");
          return a.initCustomEvent(o, !n, !s, i || {}), t.dispatchEvent(a), a;
        },
        C = function(e, i) {
          var n;
          !s && (n = t.picturefill || o.pf)
            ? n({
                reevaluate: !0,
                elements: [e]
              })
            : i && i.src && (e.src = i.src);
        },
        $ = function(t, e) {
          return (getComputedStyle(t, null) || {})[e];
        },
        j = function(t, e, i) {
          for (
            i = i || t.offsetWidth;
            i < o.minSize && e && !t._lazysizesWidth;

          )
            (i = e.offsetWidth), (e = e.parentNode);
          return i;
        },
        k = (function() {
          var t,
            o,
            i = [],
            n = function() {
              var e;
              for (t = !0, o = !1; i.length; )
                (e = i.shift()), e[0].apply(e[1], e[2]);
              t = !1;
            },
            s = function(s) {
              t
                ? s.apply(this, arguments)
                : (i.push([s, this, arguments]),
                  o || ((o = !0), (e.hidden ? c : d)(n)));
            };
          return (s._lsFlush = n), s;
        })(),
        T = function(t, e) {
          return e
            ? function() {
                k(t);
              }
            : function() {
                var e = this,
                  o = arguments;
                k(function() {
                  t.apply(e, o);
                });
              };
        },
        x = function(t) {
          var e,
            o = 0,
            i = 125,
            s = 666,
            a = s,
            r = function() {
              (e = !1), (o = n.now()), t();
            },
            l = h
              ? function() {
                  h(r, {
                    timeout: a
                  }),
                    a !== s && (a = s);
                }
              : T(function() {
                  c(r);
                }, !0);
          return function(t) {
            var s;
            (t = t === !0) && (a = 44),
              e ||
                ((e = !0),
                (s = i - (n.now() - o)),
                0 > s && (s = 0),
                t || (9 > s && h) ? l() : c(l, s));
          };
        },
        E = function(t) {
          var e,
            o,
            i = 99,
            s = function() {
              (e = null), t();
            },
            a = function() {
              var t = n.now() - o;
              i > t ? c(a, i - t) : (h || s)(s);
            };
          return function() {
            (o = n.now()), e || (e = c(a, i));
          };
        },
        S = (function() {
          var s,
            d,
            h,
            p,
            f,
            j,
            S,
            A,
            N,
            I,
            O,
            D,
            F,
            P,
            R,
            z = /^img$/i,
            L = /^iframe$/i,
            W = "onscroll" in t && !/glebot/.test(navigator.userAgent),
            U = 0,
            H = 0,
            B = 0,
            M = -1,
            Q = function(t) {
              B--,
                t && t.target && b(t.target, Q),
                (!t || 0 > B || !t.target) && (B = 0);
            },
            q = function(t, o) {
              var n,
                s = t,
                a =
                  "hidden" == $(e.body, "visibility") ||
                  "hidden" != $(t, "visibility");
              for (
                N -= o, D += o, I -= o, O += o;
                a && (s = s.offsetParent) && s != e.body && s != i;

              )
                (a = ($(s, "opacity") || 1) > 0),
                  a &&
                    "visible" != $(s, "overflow") &&
                    ((n = s.getBoundingClientRect()),
                    (a =
                      O > n.left &&
                      I < n.right &&
                      D > n.top - 1 &&
                      N < n.bottom + 1));
              return a;
            },
            V = function() {
              var t, n, a, l, c, u, p, m, v;
              if ((f = o.loadMode) && 8 > B && (t = s.length)) {
                (n = 0),
                  M++,
                  null == P &&
                    ("expand" in o ||
                      (o.expand =
                        i.clientHeight > 500 && i.clientWidth > 500
                          ? 500
                          : 370),
                    (F = o.expand),
                    (P = F * o.expFactor)),
                  P > H && 1 > B && M > 2 && f > 2 && !e.hidden
                    ? ((H = P), (M = 0))
                    : (H = f > 1 && M > 1 && 6 > B ? F : U);
                for (; t > n; n++)
                  if (s[n] && !s[n]._lazyRace)
                    if (W)
                      if (
                        (((m = s[n][r]("data-expand")) && (u = 1 * m)) ||
                          (u = H),
                        v !== u &&
                          ((S = innerWidth + u * R),
                          (A = innerHeight + u),
                          (p = -1 * u),
                          (v = u)),
                        (a = s[n].getBoundingClientRect()),
                        (D = a.bottom) >= p &&
                          (N = a.top) <= A &&
                          (O = a.right) >= p * R &&
                          (I = a.left) <= S &&
                          (D || O || I || N) &&
                          ((h && 3 > B && !m && (3 > f || 4 > M)) ||
                            q(s[n], u)))
                      ) {
                        if ((et(s[n]), (c = !0), B > 9)) break;
                      } else
                        !c &&
                          h &&
                          !l &&
                          4 > B &&
                          4 > M &&
                          f > 2 &&
                          (d[0] || o.preloadAfterLoad) &&
                          (d[0] ||
                            (!m &&
                              (D ||
                                O ||
                                I ||
                                N ||
                                "auto" != s[n][r](o.sizesAttr)))) &&
                          (l = d[0] || s[n]);
                    else et(s[n]);
                l && !c && et(l);
              }
            },
            Y = x(V),
            K = function(t) {
              g(t.target, o.loadedClass),
                y(t.target, o.loadingClass),
                b(t.target, J);
            },
            G = T(K),
            J = function(t) {
              G({
                target: t.target
              });
            },
            X = function(t, e) {
              try {
                t.contentWindow.location.replace(e);
              } catch (o) {
                t.src = e;
              }
            },
            Z = function(t) {
              var e,
                i,
                n = t[r](o.srcsetAttr);
              (e = o.customMedia[t[r]("data-media") || t[r]("media")]) &&
                t.setAttribute("media", e),
                n && t.setAttribute("srcset", n),
                e &&
                  ((i = t.parentNode),
                  i.insertBefore(t.cloneNode(), t),
                  i.removeChild(t));
            },
            tt = T(function(t, e, i, n, s) {
              var a, l, d, h, f, v;
              (f = w(t, "lazybeforeunveil", e)).defaultPrevented ||
                (n && (i ? g(t, o.autosizesClass) : t.setAttribute("sizes", n)),
                (l = t[r](o.srcsetAttr)),
                (a = t[r](o.srcAttr)),
                s && ((d = t.parentNode), (h = d && u.test(d.nodeName || ""))),
                (v = e.firesLoad || ("src" in t && (l || a || h))),
                (f = {
                  target: t
                }),
                v &&
                  (b(t, Q, !0),
                  clearTimeout(p),
                  (p = c(Q, 2500)),
                  g(t, o.loadingClass),
                  b(t, J, !0)),
                h && m.call(d.getElementsByTagName("source"), Z),
                l
                  ? t.setAttribute("srcset", l)
                  : a && !h && (L.test(t.nodeName) ? X(t, a) : (t.src = a)),
                (l || h) &&
                  C(t, {
                    src: a
                  })),
                k(function() {
                  t._lazyRace && delete t._lazyRace,
                    y(t, o.lazyClass),
                    (!v || t.complete) && (v ? Q(f) : B--, K(f));
                });
            }),
            et = function(t) {
              var e,
                i = z.test(t.nodeName),
                n = i && (t[r](o.sizesAttr) || t[r]("sizes")),
                s = "auto" == n;
              ((!s && h) ||
                !i ||
                (!t.src && !t.srcset) ||
                t.complete ||
                v(t, o.errorClass)) &&
                ((e = w(t, "lazyunveilread").detail),
                s && _.updateElem(t, !0, t.offsetWidth),
                (t._lazyRace = !0),
                B++,
                tt(t, e, s, n, i));
            },
            ot = function() {
              if (!h) {
                if (n.now() - j < 999) return void c(ot, 999);
                var t = E(function() {
                  (o.loadMode = 3), Y();
                });
                (h = !0),
                  (o.loadMode = 3),
                  Y(),
                  l(
                    "scroll",
                    function() {
                      3 == o.loadMode && (o.loadMode = 2), t();
                    },
                    !0
                  );
              }
            };
          return {
            _: function() {
              (j = n.now()),
                (s = e.getElementsByClassName(o.lazyClass)),
                (d = e.getElementsByClassName(
                  o.lazyClass + " " + o.preloadClass
                )),
                (R = o.hFac),
                l("scroll", Y, !0),
                l("resize", Y, !0),
                t.MutationObserver
                  ? new MutationObserver(Y).observe(i, {
                      childList: !0,
                      subtree: !0,
                      attributes: !0
                    })
                  : (i[a]("DOMNodeInserted", Y, !0),
                    i[a]("DOMAttrModified", Y, !0),
                    setInterval(Y, 999)),
                l("hashchange", Y, !0),
                [
                  "focus",
                  "mouseover",
                  "click",
                  "load",
                  "transitionend",
                  "animationend",
                  "webkitAnimationEnd"
                ].forEach(function(t) {
                  e[a](t, Y, !0);
                }),
                /d$|^c/.test(e.readyState)
                  ? ot()
                  : (l("load", ot), e[a]("DOMContentLoaded", Y), c(ot, 2e4)),
                s.length ? V() : Y();
            },
            checkElems: Y,
            unveil: et
          };
        })(),
        _ = (function() {
          var t,
            i = T(function(t, e, o, i) {
              var n, s, a;
              if (
                ((t._lazysizesWidth = i),
                (i += "px"),
                t.setAttribute("sizes", i),
                u.test(e.nodeName || ""))
              )
                for (
                  n = e.getElementsByTagName("source"), s = 0, a = n.length;
                  a > s;
                  s++
                )
                  n[s].setAttribute("sizes", i);
              o.detail.dataAttr || C(t, o.detail);
            }),
            n = function(t, e, o) {
              var n,
                s = t.parentNode;
              s &&
                ((o = j(t, s, o)),
                (n = w(t, "lazybeforesizes", {
                  width: o,
                  dataAttr: !!e
                })),
                n.defaultPrevented ||
                  ((o = n.detail.width),
                  o && o !== t._lazysizesWidth && i(t, s, n, o)));
            },
            s = function() {
              var e,
                o = t.length;
              if (o) for (e = 0; o > e; e++) n(t[e]);
            },
            a = E(s);
          return {
            _: function() {
              (t = e.getElementsByClassName(o.autosizesClass)), l("resize", a);
            },
            checkElems: a,
            updateElem: n
          };
        })(),
        A = function() {
          A.i || ((A.i = !0), _._(), S._());
        };
      return (
        (function() {
          var e,
            i = {
              lazyClass: "lazyload",
              loadedClass: "lazyloaded",
              loadingClass: "lazyloading",
              preloadClass: "lazypreload",
              errorClass: "lazyerror",
              autosizesClass: "lazyautosizes",
              srcAttr: "data-src",
              srcsetAttr: "data-srcset",
              sizesAttr: "data-sizes",
              minSize: 40,
              customMedia: {},
              init: !0,
              expFactor: 1.5,
              hFac: 0.8,
              loadMode: 2
            };
          o = t.lazySizesConfig || t.lazysizesConfig || {};
          for (e in i) e in o || (o[e] = i[e]);
          (t.lazySizesConfig = o),
            c(function() {
              o.init && A();
            });
        })(),
        {
          cfg: o,
          autoSizer: _,
          loader: S,
          init: A,
          uP: C,
          aC: g,
          rC: y,
          hC: v,
          fire: w,
          gW: j,
          rAF: k
        }
      );
    }
  }),
  function() {
    var t,
      e,
      o,
      i,
      n,
      s = function(t, e) {
        return function() {
          return t.apply(e, arguments);
        };
      },
      a =
        [].indexOf ||
        function(t) {
          for (var e = 0, o = this.length; o > e; e++)
            if (e in this && this[e] === t) return e;
          return -1;
        };
    (e = (function() {
      function t() {}
      return (
        (t.prototype.extend = function(t, e) {
          var o, i;
          for (o in e) (i = e[o]), null == t[o] && (t[o] = i);
          return t;
        }),
        (t.prototype.isMobile = function(t) {
          return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
            t
          );
        }),
        (t.prototype.createEvent = function(t, e, o, i) {
          var n;
          return (
            null == e && (e = !1),
            null == o && (o = !1),
            null == i && (i = null),
            null != document.createEvent
              ? ((n = document.createEvent("CustomEvent")),
                n.initCustomEvent(t, e, o, i))
              : null != document.createEventObject
                ? ((n = document.createEventObject()), (n.eventType = t))
                : (n.eventName = t),
            n
          );
        }),
        (t.prototype.emitEvent = function(t, e) {
          return null != t.dispatchEvent
            ? t.dispatchEvent(e)
            : e in (null != t)
              ? t[e]()
              : "on" + e in (null != t)
                ? t["on" + e]()
                : void 0;
        }),
        (t.prototype.addEvent = function(t, e, o) {
          return null != t.addEventListener
            ? t.addEventListener(e, o, !1)
            : null != t.attachEvent
              ? t.attachEvent("on" + e, o)
              : (t[e] = o);
        }),
        (t.prototype.removeEvent = function(t, e, o) {
          return null != t.removeEventListener
            ? t.removeEventListener(e, o, !1)
            : null != t.detachEvent
              ? t.detachEvent("on" + e, o)
              : delete t[e];
        }),
        (t.prototype.innerHeight = function() {
          return "innerHeight" in window
            ? window.innerHeight
            : document.documentElement.clientHeight;
        }),
        t
      );
    })()),
      (o =
        this.WeakMap ||
        this.MozWeakMap ||
        (o = (function() {
          function t() {
            (this.keys = []), (this.values = []);
          }
          return (
            (t.prototype.get = function(t) {
              var e, o, i, n, s;
              for (s = this.keys, e = i = 0, n = s.length; n > i; e = ++i)
                if (((o = s[e]), o === t)) return this.values[e];
            }),
            (t.prototype.set = function(t, e) {
              var o, i, n, s, a;
              for (a = this.keys, o = n = 0, s = a.length; s > n; o = ++n)
                if (((i = a[o]), i === t)) return void (this.values[o] = e);
              return this.keys.push(t), this.values.push(e);
            }),
            t
          );
        })())),
      (t =
        this.MutationObserver ||
        this.WebkitMutationObserver ||
        this.MozMutationObserver ||
        (t = (function() {
          function t() {
            "undefined" != typeof console && null !== console && void 0,
              "undefined" != typeof console && null !== console && void 0;
          }
          return (
            (t.notSupported = !0), (t.prototype.observe = function() {}), t
          );
        })())),
      (i =
        this.getComputedStyle ||
        function(t, e) {
          return (
            (this.getPropertyValue = function(e) {
              var o;
              return (
                "float" === e && (e = "styleFloat"),
                n.test(e) &&
                  e.replace(n, function(t, e) {
                    return e.toUpperCase();
                  }),
                (null != (o = t.currentStyle) ? o[e] : void 0) || null
              );
            }),
            this
          );
        }),
      (n = /(\-([a-z]){1})/g),
      (this.WOW = (function() {
        function n(t) {
          null == t && (t = {}),
            (this.scrollCallback = s(this.scrollCallback, this)),
            (this.scrollHandler = s(this.scrollHandler, this)),
            (this.resetAnimation = s(this.resetAnimation, this)),
            (this.start = s(this.start, this)),
            (this.scrolled = !0),
            (this.config = this.util().extend(t, this.defaults)),
            null != t.scrollContainer &&
              (this.config.scrollContainer = document.querySelector(
                t.scrollContainer
              )),
            (this.animationNameCache = new o()),
            (this.wowEvent = this.util().createEvent(this.config.boxClass));
        }
        return (
          (n.prototype.defaults = {
            boxClass: "wow",
            animateClass: "animated",
            offset: 0,
            mobile: !0,
            live: !0,
            callback: null,
            scrollContainer: null
          }),
          (n.prototype.init = function() {
            var t;
            return (
              (this.element = window.document.documentElement),
              "interactive" === (t = document.readyState) || "complete" === t
                ? this.start()
                : this.util().addEvent(
                    document,
                    "DOMContentLoaded",
                    this.start
                  ),
              (this.finished = [])
            );
          }),
          (n.prototype.start = function() {
            var e, o, i, n;
            if (
              ((this.stopped = !1),
              (this.boxes = function() {
                var t, o, i, n;
                for (
                  i = this.element.querySelectorAll("." + this.config.boxClass),
                    n = [],
                    t = 0,
                    o = i.length;
                  o > t;
                  t++
                )
                  (e = i[t]), n.push(e);
                return n;
              }.call(this)),
              (this.all = function() {
                var t, o, i, n;
                for (i = this.boxes, n = [], t = 0, o = i.length; o > t; t++)
                  (e = i[t]), n.push(e);
                return n;
              }.call(this)),
              this.boxes.length)
            )
              if (this.disabled()) this.resetStyle();
              else
                for (n = this.boxes, o = 0, i = n.length; i > o; o++)
                  (e = n[o]), this.applyStyle(e, !0);
            return (
              this.disabled() ||
                (this.util().addEvent(
                  this.config.scrollContainer || window,
                  "scroll",
                  this.scrollHandler
                ),
                this.util().addEvent(window, "resize", this.scrollHandler),
                (this.interval = setInterval(this.scrollCallback, 50))),
              this.config.live
                ? new t(
                    (function(t) {
                      return function(e) {
                        var o, i, n, s, a;
                        for (a = [], o = 0, i = e.length; i > o; o++)
                          (s = e[o]),
                            a.push(
                              function() {
                                var t, e, o, i;
                                for (
                                  o = s.addedNodes || [],
                                    i = [],
                                    t = 0,
                                    e = o.length;
                                  e > t;
                                  t++
                                )
                                  (n = o[t]), i.push(this.doSync(n));
                                return i;
                              }.call(t)
                            );
                        return a;
                      };
                    })(this)
                  ).observe(document.body, {
                    childList: !0,
                    subtree: !0
                  })
                : void 0
            );
          }),
          (n.prototype.stop = function() {
            return (
              (this.stopped = !0),
              this.util().removeEvent(
                this.config.scrollContainer || window,
                "scroll",
                this.scrollHandler
              ),
              this.util().removeEvent(window, "resize", this.scrollHandler),
              null != this.interval ? clearInterval(this.interval) : void 0
            );
          }),
          (n.prototype.sync = function(e) {
            return t.notSupported ? this.doSync(this.element) : void 0;
          }),
          (n.prototype.doSync = function(t) {
            var e, o, i, n, s;
            if ((null == t && (t = this.element), 1 === t.nodeType)) {
              for (
                t = t.parentNode || t,
                  n = t.querySelectorAll("." + this.config.boxClass),
                  s = [],
                  o = 0,
                  i = n.length;
                i > o;
                o++
              )
                (e = n[o]),
                  a.call(this.all, e) < 0
                    ? (this.boxes.push(e),
                      this.all.push(e),
                      this.stopped || this.disabled()
                        ? this.resetStyle()
                        : this.applyStyle(e, !0),
                      s.push((this.scrolled = !0)))
                    : s.push(void 0);
              return s;
            }
          }),
          (n.prototype.show = function(t) {
            return (
              this.applyStyle(t),
              (t.className = t.className + " " + this.config.animateClass),
              null != this.config.callback && this.config.callback(t),
              this.util().emitEvent(t, this.wowEvent),
              this.util().addEvent(t, "animationend", this.resetAnimation),
              this.util().addEvent(t, "oanimationend", this.resetAnimation),
              this.util().addEvent(
                t,
                "webkitAnimationEnd",
                this.resetAnimation
              ),
              this.util().addEvent(t, "MSAnimationEnd", this.resetAnimation),
              t
            );
          }),
          (n.prototype.applyStyle = function(t, e) {
            var o, i, n;
            return (
              (i = t.getAttribute("data-wow-duration")),
              (o = t.getAttribute("data-wow-delay")),
              (n = t.getAttribute("data-wow-iteration")),
              this.animate(
                (function(s) {
                  return function() {
                    return s.customStyle(t, e, i, o, n);
                  };
                })(this)
              )
            );
          }),
          (n.prototype.animate = (function() {
            return "requestAnimationFrame" in window
              ? function(t) {
                  return window.requestAnimationFrame(t);
                }
              : function(t) {
                  return t();
                };
          })()),
          (n.prototype.resetStyle = function() {
            var t, e, o, i, n;
            for (i = this.boxes, n = [], e = 0, o = i.length; o > e; e++)
              (t = i[e]), n.push((t.style.visibility = "visible"));
            return n;
          }),
          (n.prototype.resetAnimation = function(t) {
            var e;
            return t.type.toLowerCase().indexOf("animationend") >= 0
              ? ((e = t.target || t.srcElement),
                (e.className = e.className
                  .replace(this.config.animateClass, "")
                  .trim()))
              : void 0;
          }),
          (n.prototype.customStyle = function(t, e, o, i, n) {
            return (
              e && this.cacheAnimationName(t),
              (t.style.visibility = e ? "hidden" : "visible"),
              o &&
                this.vendorSet(t.style, {
                  animationDuration: o
                }),
              i &&
                this.vendorSet(t.style, {
                  animationDelay: i
                }),
              n &&
                this.vendorSet(t.style, {
                  animationIterationCount: n
                }),
              this.vendorSet(t.style, {
                animationName: e ? "none" : this.cachedAnimationName(t)
              }),
              t
            );
          }),
          (n.prototype.vendors = ["moz", "webkit"]),
          (n.prototype.vendorSet = function(t, e) {
            var o, i, n, s;
            i = [];
            for (o in e)
              (n = e[o]),
                (t["" + o] = n),
                i.push(
                  function() {
                    var e, i, a, r;
                    for (
                      a = this.vendors, r = [], e = 0, i = a.length;
                      i > e;
                      e++
                    )
                      (s = a[e]),
                        r.push(
                          (t[
                            "" + s + o.charAt(0).toUpperCase() + o.substr(1)
                          ] = n)
                        );
                    return r;
                  }.call(this)
                );
            return i;
          }),
          (n.prototype.vendorCSS = function(t, e) {
            var o, n, s, a, r, l;
            for (
              r = i(t),
                a = r.getPropertyCSSValue(e),
                s = this.vendors,
                o = 0,
                n = s.length;
              n > o;
              o++
            )
              (l = s[o]), (a = a || r.getPropertyCSSValue("-" + l + "-" + e));
            return a;
          }),
          (n.prototype.animationName = function(t) {
            var e;
            try {
              e = this.vendorCSS(t, "animation-name").cssText;
            } catch (o) {
              e = i(t).getPropertyValue("animation-name");
            }
            return "none" === e ? "" : e;
          }),
          (n.prototype.cacheAnimationName = function(t) {
            return this.animationNameCache.set(t, this.animationName(t));
          }),
          (n.prototype.cachedAnimationName = function(t) {
            return this.animationNameCache.get(t);
          }),
          (n.prototype.scrollHandler = function() {
            return (this.scrolled = !0);
          }),
          (n.prototype.scrollCallback = function() {
            var t;
            return !this.scrolled ||
              ((this.scrolled = !1),
              (this.boxes = function() {
                var e, o, i, n;
                for (i = this.boxes, n = [], e = 0, o = i.length; o > e; e++)
                  (t = i[e]),
                    t && (this.isVisible(t) ? this.show(t) : n.push(t));
                return n;
              }.call(this)),
              this.boxes.length || this.config.live)
              ? void 0
              : this.stop();
          }),
          (n.prototype.offsetTop = function(t) {
            for (var e; void 0 === t.offsetTop; ) t = t.parentNode;
            for (e = t.offsetTop; (t = t.offsetParent); ) e += t.offsetTop;
            return e;
          }),
          (n.prototype.isVisible = function(t) {
            var e, o, i, n, s;
            return (
              (o = t.getAttribute("data-wow-offset") || this.config.offset),
              (s =
                (this.config.scrollContainer &&
                  this.config.scrollContainer.scrollTop) ||
                window.pageYOffset),
              (n =
                s +
                Math.min(this.element.clientHeight, this.util().innerHeight()) -
                o),
              (i = this.offsetTop(t)),
              (e = i + t.clientHeight),
              n >= i && e >= s
            );
          }),
          (n.prototype.util = function() {
            return null != this._util ? this._util : (this._util = new e());
          }),
          (n.prototype.disabled = function() {
            return (
              !this.config.mobile && this.util().isMobile(navigator.userAgent)
            );
          }),
          n
        );
      })());
  }.call(this),
  "undefined" == typeof jQuery)
)
  throw new Error("Bootstrap's JavaScript requires jQuery");
+(function(t) {
  "use strict";
  var e = t.fn.jquery.split(" ")[0].split(".");
  if (
    (e[0] < 2 && e[1] < 9) ||
    (1 == e[0] && 9 == e[1] && e[2] < 1) ||
    e[0] > 3
  )
    throw new Error(
      "Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4"
    );
})(jQuery),
  +(function(t) {
    "use strict";

    function e() {
      var t = document.createElement("bootstrap"),
        e = {
          WebkitTransition: "webkitTransitionEnd",
          MozTransition: "transitionend",
          OTransition: "oTransitionEnd otransitionend",
          transition: "transitionend"
        };
      for (var o in e)
        if (void 0 !== t.style[o])
          return {
            end: e[o]
          };
      return !1;
    }
    (t.fn.emulateTransitionEnd = function(e) {
      var o = !1,
        i = this;
      t(this).one("bsTransitionEnd", function() {
        o = !0;
      });
      var n = function() {
        o || t(i).trigger(t.support.transition.end);
      };
      return setTimeout(n, e), this;
    }),
      t(function() {
        (t.support.transition = e()),
          t.support.transition &&
            (t.event.special.bsTransitionEnd = {
              bindType: t.support.transition.end,
              delegateType: t.support.transition.end,
              handle: function(e) {
                if (t(e.target).is(this))
                  return e.handleObj.handler.apply(this, arguments);
              }
            });
      });
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(e) {
      return this.each(function() {
        var o = t(this),
          n = o.data("bs.alert");
        n || o.data("bs.alert", (n = new i(this))),
          "string" == typeof e && n[e].call(o);
      });
    }
    var o = '[data-dismiss="alert"]',
      i = function(e) {
        t(e).on("click", o, this.close);
      };
    (i.VERSION = "3.3.7"),
      (i.TRANSITION_DURATION = 150),
      (i.prototype.close = function(e) {
        function o() {
          a.detach()
            .trigger("closed.bs.alert")
            .remove();
        }
        var n = t(this),
          s = n.attr("data-target");
        s || ((s = n.attr("href")), (s = s && s.replace(/.*(?=#[^\s]*$)/, "")));
        var a = t("#" === s ? [] : s);
        e && e.preventDefault(),
          a.length || (a = n.closest(".alert")),
          a.trigger((e = t.Event("close.bs.alert"))),
          e.isDefaultPrevented() ||
            (a.removeClass("in"),
            t.support.transition && a.hasClass("fade")
              ? a
                  .one("bsTransitionEnd", o)
                  .emulateTransitionEnd(i.TRANSITION_DURATION)
              : o());
      });
    var n = t.fn.alert;
    (t.fn.alert = e),
      (t.fn.alert.Constructor = i),
      (t.fn.alert.noConflict = function() {
        return (t.fn.alert = n), this;
      }),
      t(document).on("click.bs.alert.data-api", o, i.prototype.close);
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(e) {
      return this.each(function() {
        var i = t(this),
          n = i.data("bs.button"),
          s = "object" == typeof e && e;
        n || i.data("bs.button", (n = new o(this, s))),
          "toggle" == e ? n.toggle() : e && n.setState(e);
      });
    }
    var o = function(e, i) {
      (this.$element = t(e)),
        (this.options = t.extend({}, o.DEFAULTS, i)),
        (this.isLoading = !1);
    };
    (o.VERSION = "3.3.7"),
      (o.DEFAULTS = {
        loadingText: "loading..."
      }),
      (o.prototype.setState = function(e) {
        var o = "disabled",
          i = this.$element,
          n = i.is("input") ? "val" : "html",
          s = i.data();
        (e += "Text"),
          null == s.resetText && i.data("resetText", i[n]()),
          setTimeout(
            t.proxy(function() {
              i[n](null == s[e] ? this.options[e] : s[e]),
                "loadingText" == e
                  ? ((this.isLoading = !0),
                    i
                      .addClass(o)
                      .attr(o, o)
                      .prop(o, !0))
                  : this.isLoading &&
                    ((this.isLoading = !1),
                    i
                      .removeClass(o)
                      .removeAttr(o)
                      .prop(o, !1));
            }, this),
            0
          );
      }),
      (o.prototype.toggle = function() {
        var t = !0,
          e = this.$element.closest('[data-toggle="buttons"]');
        if (e.length) {
          var o = this.$element.find("input");
          "radio" == o.prop("type")
            ? (o.prop("checked") && (t = !1),
              e.find(".active").removeClass("active"),
              this.$element.addClass("active"))
            : "checkbox" == o.prop("type") &&
              (o.prop("checked") !== this.$element.hasClass("active") &&
                (t = !1),
              this.$element.toggleClass("active")),
            o.prop("checked", this.$element.hasClass("active")),
            t && o.trigger("change");
        } else
          this.$element.attr("aria-pressed", !this.$element.hasClass("active")),
            this.$element.toggleClass("active");
      });
    var i = t.fn.button;
    (t.fn.button = e),
      (t.fn.button.Constructor = o),
      (t.fn.button.noConflict = function() {
        return (t.fn.button = i), this;
      }),
      t(document)
        .on("click.bs.button.data-api", '[data-toggle^="button"]', function(o) {
          var i = t(o.target).closest(".btn");
          e.call(i, "toggle"),
            t(o.target).is('input[type="radio"], input[type="checkbox"]') ||
              (o.preventDefault(),
              i.is("input,button")
                ? i.trigger("focus")
                : i
                    .find("input:visible,button:visible")
                    .first()
                    .trigger("focus"));
        })
        .on(
          "focus.bs.button.data-api blur.bs.button.data-api",
          '[data-toggle^="button"]',
          function(e) {
            t(e.target)
              .closest(".btn")
              .toggleClass("focus", /^focus(in)?$/.test(e.type));
          }
        );
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(e) {
      return this.each(function() {
        var i = t(this),
          n = i.data("bs.carousel"),
          s = t.extend({}, o.DEFAULTS, i.data(), "object" == typeof e && e),
          a = "string" == typeof e ? e : s.slide;
        n || i.data("bs.carousel", (n = new o(this, s))),
          "number" == typeof e
            ? n.to(e)
            : a
              ? n[a]()
              : s.interval && n.pause().cycle();
      });
    }
    var o = function(e, o) {
      (this.$element = t(e)),
        (this.$indicators = this.$element.find(".carousel-indicators")),
        (this.options = o),
        (this.paused = null),
        (this.sliding = null),
        (this.interval = null),
        (this.$active = null),
        (this.$items = null),
        this.options.keyboard &&
          this.$element.on("keydown.bs.carousel", t.proxy(this.keydown, this)),
        "hover" == this.options.pause &&
          !("ontouchstart" in document.documentElement) &&
          this.$element
            .on("mouseenter.bs.carousel", t.proxy(this.pause, this))
            .on("mouseleave.bs.carousel", t.proxy(this.cycle, this));
    };
    (o.VERSION = "3.3.7"),
      (o.TRANSITION_DURATION = 600),
      (o.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0,
        keyboard: !0
      }),
      (o.prototype.keydown = function(t) {
        if (!/input|textarea/i.test(t.target.tagName)) {
          switch (t.which) {
            case 37:
              this.prev();
              break;
            case 39:
              this.next();
              break;
            default:
              return;
          }
          t.preventDefault();
        }
      }),
      (o.prototype.cycle = function(e) {
        return (
          e || (this.paused = !1),
          this.interval && clearInterval(this.interval),
          this.options.interval &&
            !this.paused &&
            (this.interval = setInterval(
              t.proxy(this.next, this),
              this.options.interval
            )),
          this
        );
      }),
      (o.prototype.getItemIndex = function(t) {
        return (
          (this.$items = t.parent().children(".item")),
          this.$items.index(t || this.$active)
        );
      }),
      (o.prototype.getItemForDirection = function(t, e) {
        var o = this.getItemIndex(e),
          i =
            ("prev" == t && 0 === o) ||
            ("next" == t && o == this.$items.length - 1);
        if (i && !this.options.wrap) return e;
        var n = "prev" == t ? -1 : 1,
          s = (o + n) % this.$items.length;
        return this.$items.eq(s);
      }),
      (o.prototype.to = function(t) {
        var e = this,
          o = this.getItemIndex(
            (this.$active = this.$element.find(".item.active"))
          );
        if (!(t > this.$items.length - 1 || t < 0))
          return this.sliding
            ? this.$element.one("slid.bs.carousel", function() {
                e.to(t);
              })
            : o == t
              ? this.pause().cycle()
              : this.slide(t > o ? "next" : "prev", this.$items.eq(t));
      }),
      (o.prototype.pause = function(e) {
        return (
          e || (this.paused = !0),
          this.$element.find(".next, .prev").length &&
            t.support.transition &&
            (this.$element.trigger(t.support.transition.end), this.cycle(!0)),
          (this.interval = clearInterval(this.interval)),
          this
        );
      }),
      (o.prototype.next = function() {
        if (!this.sliding) return this.slide("next");
      }),
      (o.prototype.prev = function() {
        if (!this.sliding) return this.slide("prev");
      }),
      (o.prototype.slide = function(e, i) {
        var n = this.$element.find(".item.active"),
          s = i || this.getItemForDirection(e, n),
          a = this.interval,
          r = "next" == e ? "left" : "right",
          l = this;
        if (s.hasClass("active")) return (this.sliding = !1);
        var c = s[0],
          d = t.Event("slide.bs.carousel", {
            relatedTarget: c,
            direction: r
          });
        if ((this.$element.trigger(d), !d.isDefaultPrevented())) {
          if (
            ((this.sliding = !0), a && this.pause(), this.$indicators.length)
          ) {
            this.$indicators.find(".active").removeClass("active");
            var h = t(this.$indicators.children()[this.getItemIndex(s)]);
            h && h.addClass("active");
          }
          var u = t.Event("slid.bs.carousel", {
            relatedTarget: c,
            direction: r
          });
          return (
            t.support.transition && this.$element.hasClass("slide")
              ? (s.addClass(e),
                s[0].offsetWidth,
                n.addClass(r),
                s.addClass(r),
                n
                  .one("bsTransitionEnd", function() {
                    s.removeClass([e, r].join(" ")).addClass("active"),
                      n.removeClass(["active", r].join(" ")),
                      (l.sliding = !1),
                      setTimeout(function() {
                        l.$element.trigger(u);
                      }, 0);
                  })
                  .emulateTransitionEnd(o.TRANSITION_DURATION))
              : (n.removeClass("active"),
                s.addClass("active"),
                (this.sliding = !1),
                this.$element.trigger(u)),
            a && this.cycle(),
            this
          );
        }
      });
    var i = t.fn.carousel;
    (t.fn.carousel = e),
      (t.fn.carousel.Constructor = o),
      (t.fn.carousel.noConflict = function() {
        return (t.fn.carousel = i), this;
      });
    var n = function(o) {
      var i,
        n = t(this),
        s = t(
          n.attr("data-target") ||
            ((i = n.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, ""))
        );
      if (s.hasClass("carousel")) {
        var a = t.extend({}, s.data(), n.data()),
          r = n.attr("data-slide-to");
        r && (a.interval = !1),
          e.call(s, a),
          r && s.data("bs.carousel").to(r),
          o.preventDefault();
      }
    };
    t(document)
      .on("click.bs.carousel.data-api", "[data-slide]", n)
      .on("click.bs.carousel.data-api", "[data-slide-to]", n),
      t(window).on("load", function() {
        t('[data-ride="carousel"]').each(function() {
          var o = t(this);
          e.call(o, o.data());
        });
      });
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(e) {
      var o,
        i =
          e.attr("data-target") ||
          ((o = e.attr("href")) && o.replace(/.*(?=#[^\s]+$)/, ""));
      return t(i);
    }

    function o(e) {
      return this.each(function() {
        var o = t(this),
          n = o.data("bs.collapse"),
          s = t.extend({}, i.DEFAULTS, o.data(), "object" == typeof e && e);
        !n && s.toggle && /show|hide/.test(e) && (s.toggle = !1),
          n || o.data("bs.collapse", (n = new i(this, s))),
          "string" == typeof e && n[e]();
      });
    }
    var i = function(e, o) {
      (this.$element = t(e)),
        (this.options = t.extend({}, i.DEFAULTS, o)),
        (this.$trigger = t(
          '[data-toggle="collapse"][href="#' +
            e.id +
            '"],[data-toggle="collapse"][data-target="#' +
            e.id +
            '"]'
        )),
        (this.transitioning = null),
        this.options.parent
          ? (this.$parent = this.getParent())
          : this.addAriaAndCollapsedClass(this.$element, this.$trigger),
        this.options.toggle && this.toggle();
    };
    (i.VERSION = "3.3.7"),
      (i.TRANSITION_DURATION = 350),
      (i.DEFAULTS = {
        toggle: !0
      }),
      (i.prototype.dimension = function() {
        var t = this.$element.hasClass("width");
        return t ? "width" : "height";
      }),
      (i.prototype.show = function() {
        if (!this.transitioning && !this.$element.hasClass("in")) {
          var e,
            n =
              this.$parent &&
              this.$parent.children(".panel").children(".in, .collapsing");
          if (
            !(
              n &&
              n.length &&
              ((e = n.data("bs.collapse")), e && e.transitioning)
            )
          ) {
            var s = t.Event("show.bs.collapse");
            if ((this.$element.trigger(s), !s.isDefaultPrevented())) {
              n &&
                n.length &&
                (o.call(n, "hide"), e || n.data("bs.collapse", null));
              var a = this.dimension();
              this.$element
                .removeClass("collapse")
                .addClass("collapsing")
                [a](0)
                .attr("aria-expanded", !0),
                this.$trigger
                  .removeClass("collapsed")
                  .attr("aria-expanded", !0),
                (this.transitioning = 1);
              var r = function() {
                this.$element
                  .removeClass("collapsing")
                  .addClass("collapse in")
                  [a](""),
                  (this.transitioning = 0),
                  this.$element.trigger("shown.bs.collapse");
              };
              if (!t.support.transition) return r.call(this);
              var l = t.camelCase(["scroll", a].join("-"));
              this.$element
                .one("bsTransitionEnd", t.proxy(r, this))
                .emulateTransitionEnd(i.TRANSITION_DURATION)
                [a](this.$element[0][l]);
            }
          }
        }
      }),
      (i.prototype.hide = function() {
        if (!this.transitioning && this.$element.hasClass("in")) {
          var e = t.Event("hide.bs.collapse");
          if ((this.$element.trigger(e), !e.isDefaultPrevented())) {
            var o = this.dimension();
            this.$element[o](this.$element[o]())[0].offsetHeight,
              this.$element
                .addClass("collapsing")
                .removeClass("collapse in")
                .attr("aria-expanded", !1),
              this.$trigger.addClass("collapsed").attr("aria-expanded", !1),
              (this.transitioning = 1);
            var n = function() {
              (this.transitioning = 0),
                this.$element
                  .removeClass("collapsing")
                  .addClass("collapse")
                  .trigger("hidden.bs.collapse");
            };
            return t.support.transition
              ? void this.$element[o](0)
                  .one("bsTransitionEnd", t.proxy(n, this))
                  .emulateTransitionEnd(i.TRANSITION_DURATION)
              : n.call(this);
          }
        }
      }),
      (i.prototype.toggle = function() {
        this[this.$element.hasClass("in") ? "hide" : "show"]();
      }),
      (i.prototype.getParent = function() {
        return t(this.options.parent)
          .find(
            '[data-toggle="collapse"][data-parent="' +
              this.options.parent +
              '"]'
          )
          .each(
            t.proxy(function(o, i) {
              var n = t(i);
              this.addAriaAndCollapsedClass(e(n), n);
            }, this)
          )
          .end();
      }),
      (i.prototype.addAriaAndCollapsedClass = function(t, e) {
        var o = t.hasClass("in");
        t.attr("aria-expanded", o),
          e.toggleClass("collapsed", !o).attr("aria-expanded", o);
      });
    var n = t.fn.collapse;
    (t.fn.collapse = o),
      (t.fn.collapse.Constructor = i),
      (t.fn.collapse.noConflict = function() {
        return (t.fn.collapse = n), this;
      }),
      t(document).on(
        "click.bs.collapse.data-api",
        '[data-toggle="collapse"]',
        function(i) {
          var n = t(this);
          n.attr("data-target") || i.preventDefault();
          var s = e(n),
            a = s.data("bs.collapse"),
            r = a ? "toggle" : n.data();
          o.call(s, r);
        }
      );
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(e) {
      var o = e.attr("data-target");
      o ||
        ((o = e.attr("href")),
        (o = o && /#[A-Za-z]/.test(o) && o.replace(/.*(?=#[^\s]*$)/, "")));
      var i = o && t(o);
      return i && i.length ? i : e.parent();
    }

    function o(o) {
      (o && 3 === o.which) ||
        (t(n).remove(),
        t(s).each(function() {
          var i = t(this),
            n = e(i),
            s = {
              relatedTarget: this
            };
          n.hasClass("open") &&
            ((o &&
              "click" == o.type &&
              /input|textarea/i.test(o.target.tagName) &&
              t.contains(n[0], o.target)) ||
              (n.trigger((o = t.Event("hide.bs.dropdown", s))),
              o.isDefaultPrevented() ||
                (i.attr("aria-expanded", "false"),
                n
                  .removeClass("open")
                  .trigger(t.Event("hidden.bs.dropdown", s)))));
        }));
    }

    function i(e) {
      return this.each(function() {
        var o = t(this),
          i = o.data("bs.dropdown");
        i || o.data("bs.dropdown", (i = new a(this))),
          "string" == typeof e && i[e].call(o);
      });
    }
    var n = ".dropdown-backdrop",
      s = '[data-toggle="dropdown"]',
      a = function(e) {
        t(e).on("click.bs.dropdown", this.toggle);
      };
    (a.VERSION = "3.3.7"),
      (a.prototype.toggle = function(i) {
        var n = t(this);
        if (!n.is(".disabled, :disabled")) {
          var s = e(n),
            a = s.hasClass("open");
          if ((o(), !a)) {
            "ontouchstart" in document.documentElement &&
              !s.closest(".navbar-nav").length &&
              t(document.createElement("div"))
                .addClass("dropdown-backdrop")
                .insertAfter(t(this))
                .on("click", o);
            var r = {
              relatedTarget: this
            };
            if (
              (s.trigger((i = t.Event("show.bs.dropdown", r))),
              i.isDefaultPrevented())
            )
              return;
            n.trigger("focus").attr("aria-expanded", "true"),
              s.toggleClass("open").trigger(t.Event("shown.bs.dropdown", r));
          }
          return !1;
        }
      }),
      (a.prototype.keydown = function(o) {
        if (
          /(38|40|27|32)/.test(o.which) &&
          !/input|textarea/i.test(o.target.tagName)
        ) {
          var i = t(this);
          if (
            (o.preventDefault(),
            o.stopPropagation(),
            !i.is(".disabled, :disabled"))
          ) {
            var n = e(i),
              a = n.hasClass("open");
            if ((!a && 27 != o.which) || (a && 27 == o.which))
              return (
                27 == o.which && n.find(s).trigger("focus"), i.trigger("click")
              );
            var r = " li:not(.disabled):visible a",
              l = n.find(".dropdown-menu" + r);
            if (l.length) {
              var c = l.index(o.target);
              38 == o.which && c > 0 && c--,
                40 == o.which && c < l.length - 1 && c++,
                ~c || (c = 0),
                l.eq(c).trigger("focus");
            }
          }
        }
      });
    var r = t.fn.dropdown;
    (t.fn.dropdown = i),
      (t.fn.dropdown.Constructor = a),
      (t.fn.dropdown.noConflict = function() {
        return (t.fn.dropdown = r), this;
      }),
      t(document)
        .on("click.bs.dropdown.data-api", o)
        .on("click.bs.dropdown.data-api", ".dropdown form", function(t) {
          t.stopPropagation();
        })
        .on("click.bs.dropdown.data-api", s, a.prototype.toggle)
        .on("keydown.bs.dropdown.data-api", s, a.prototype.keydown)
        .on(
          "keydown.bs.dropdown.data-api",
          ".dropdown-menu",
          a.prototype.keydown
        );
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(e, i) {
      return this.each(function() {
        var n = t(this),
          s = n.data("bs.modal"),
          a = t.extend({}, o.DEFAULTS, n.data(), "object" == typeof e && e);
        s || n.data("bs.modal", (s = new o(this, a))),
          "string" == typeof e ? s[e](i) : a.show && s.show(i);
      });
    }
    var o = function(e, o) {
      (this.options = o),
        (this.$body = t(document.body)),
        (this.$element = t(e)),
        (this.$dialog = this.$element.find(".modal-dialog")),
        (this.$backdrop = null),
        (this.isShown = null),
        (this.originalBodyPad = null),
        (this.scrollbarWidth = 0),
        (this.ignoreBackdropClick = !1),
        this.options.remote &&
          this.$element.find(".modal-content").load(
            this.options.remote,
            t.proxy(function() {
              this.$element.trigger("loaded.bs.modal");
            }, this)
          );
    };
    (o.VERSION = "3.3.7"),
      (o.TRANSITION_DURATION = 300),
      (o.BACKDROP_TRANSITION_DURATION = 150),
      (o.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
      }),
      (o.prototype.toggle = function(t) {
        return this.isShown ? this.hide() : this.show(t);
      }),
      (o.prototype.show = function(e) {
        var i = this,
          n = t.Event("show.bs.modal", {
            relatedTarget: e
          });
        this.$element.trigger(n),
          this.isShown ||
            n.isDefaultPrevented() ||
            ((this.isShown = !0),
            this.checkScrollbar(),
            this.setScrollbar(),
            this.$body.addClass("modal-open"),
            this.escape(),
            this.resize(),
            this.$element.on(
              "click.dismiss.bs.modal",
              '[data-dismiss="modal"]',
              t.proxy(this.hide, this)
            ),
            this.$dialog.on("mousedown.dismiss.bs.modal", function() {
              i.$element.one("mouseup.dismiss.bs.modal", function(e) {
                t(e.target).is(i.$element) && (i.ignoreBackdropClick = !0);
              });
            }),
            this.backdrop(function() {
              var n = t.support.transition && i.$element.hasClass("fade");
              i.$element.parent().length || i.$element.appendTo(i.$body),
                i.$element.show().scrollTop(0),
                i.adjustDialog(),
                n && i.$element[0].offsetWidth,
                i.$element.addClass("in"),
                i.enforceFocus();
              var s = t.Event("shown.bs.modal", {
                relatedTarget: e
              });
              n
                ? i.$dialog
                    .one("bsTransitionEnd", function() {
                      i.$element.trigger("focus").trigger(s);
                    })
                    .emulateTransitionEnd(o.TRANSITION_DURATION)
                : i.$element.trigger("focus").trigger(s);
            }));
      }),
      (o.prototype.hide = function(e) {
        e && e.preventDefault(),
          (e = t.Event("hide.bs.modal")),
          this.$element.trigger(e),
          this.isShown &&
            !e.isDefaultPrevented() &&
            ((this.isShown = !1),
            this.escape(),
            this.resize(),
            t(document).off("focusin.bs.modal"),
            this.$element
              .removeClass("in")
              .off("click.dismiss.bs.modal")
              .off("mouseup.dismiss.bs.modal"),
            this.$dialog.off("mousedown.dismiss.bs.modal"),
            t.support.transition && this.$element.hasClass("fade")
              ? this.$element
                  .one("bsTransitionEnd", t.proxy(this.hideModal, this))
                  .emulateTransitionEnd(o.TRANSITION_DURATION)
              : this.hideModal());
      }),
      (o.prototype.enforceFocus = function() {
        t(document)
          .off("focusin.bs.modal")
          .on(
            "focusin.bs.modal",
            t.proxy(function(t) {
              document === t.target ||
                this.$element[0] === t.target ||
                this.$element.has(t.target).length ||
                this.$element.trigger("focus");
            }, this)
          );
      }),
      (o.prototype.escape = function() {
        this.isShown && this.options.keyboard
          ? this.$element.on(
              "keydown.dismiss.bs.modal",
              t.proxy(function(t) {
                27 == t.which && this.hide();
              }, this)
            )
          : this.isShown || this.$element.off("keydown.dismiss.bs.modal");
      }),
      (o.prototype.resize = function() {
        this.isShown
          ? t(window).on("resize.bs.modal", t.proxy(this.handleUpdate, this))
          : t(window).off("resize.bs.modal");
      }),
      (o.prototype.hideModal = function() {
        var t = this;
        this.$element.hide(),
          this.backdrop(function() {
            t.$body.removeClass("modal-open"),
              t.resetAdjustments(),
              t.resetScrollbar(),
              t.$element.trigger("hidden.bs.modal");
          });
      }),
      (o.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove(), (this.$backdrop = null);
      }),
      (o.prototype.backdrop = function(e) {
        var i = this,
          n = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
          var s = t.support.transition && n;
          if (
            ((this.$backdrop = t(document.createElement("div"))
              .addClass("modal-backdrop " + n)
              .appendTo(this.$body)),
            this.$element.on(
              "click.dismiss.bs.modal",
              t.proxy(function(t) {
                return this.ignoreBackdropClick
                  ? void (this.ignoreBackdropClick = !1)
                  : void (
                      t.target === t.currentTarget &&
                      ("static" == this.options.backdrop
                        ? this.$element[0].focus()
                        : this.hide())
                    );
              }, this)
            ),
            s && this.$backdrop[0].offsetWidth,
            this.$backdrop.addClass("in"),
            !e)
          )
            return;
          s
            ? this.$backdrop
                .one("bsTransitionEnd", e)
                .emulateTransitionEnd(o.BACKDROP_TRANSITION_DURATION)
            : e();
        } else if (!this.isShown && this.$backdrop) {
          this.$backdrop.removeClass("in");
          var a = function() {
            i.removeBackdrop(), e && e();
          };
          t.support.transition && this.$element.hasClass("fade")
            ? this.$backdrop
                .one("bsTransitionEnd", a)
                .emulateTransitionEnd(o.BACKDROP_TRANSITION_DURATION)
            : a();
        } else e && e();
      }),
      (o.prototype.handleUpdate = function() {
        this.adjustDialog();
      }),
      (o.prototype.adjustDialog = function() {
        var t =
          this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
          paddingLeft: !this.bodyIsOverflowing && t ? this.scrollbarWidth : "",
          paddingRight: this.bodyIsOverflowing && !t ? this.scrollbarWidth : ""
        });
      }),
      (o.prototype.resetAdjustments = function() {
        this.$element.css({
          paddingLeft: "",
          paddingRight: ""
        });
      }),
      (o.prototype.checkScrollbar = function() {
        var t = window.innerWidth;
        if (!t) {
          var e = document.documentElement.getBoundingClientRect();
          t = e.right - Math.abs(e.left);
        }
        (this.bodyIsOverflowing = document.body.clientWidth < t),
          (this.scrollbarWidth = this.measureScrollbar());
      }),
      (o.prototype.setScrollbar = function() {
        var t = parseInt(this.$body.css("padding-right") || 0, 10);
        (this.originalBodyPad = document.body.style.paddingRight || ""),
          this.bodyIsOverflowing &&
            this.$body.css("padding-right", t + this.scrollbarWidth);
      }),
      (o.prototype.resetScrollbar = function() {
        this.$body.css("padding-right", this.originalBodyPad);
      }),
      (o.prototype.measureScrollbar = function() {
        var t = document.createElement("div");
        (t.className = "modal-scrollbar-measure"), this.$body.append(t);
        var e = t.offsetWidth - t.clientWidth;
        return this.$body[0].removeChild(t), e;
      });
    var i = t.fn.modal;
    (t.fn.modal = e),
      (t.fn.modal.Constructor = o),
      (t.fn.modal.noConflict = function() {
        return (t.fn.modal = i), this;
      }),
      t(document).on(
        "click.bs.modal.data-api",
        '[data-toggle="modal"]',
        function(o) {
          var i = t(this),
            n = i.attr("href"),
            s = t(
              i.attr("data-target") || (n && n.replace(/.*(?=#[^\s]+$)/, ""))
            ),
            a = s.data("bs.modal")
              ? "toggle"
              : t.extend(
                  {
                    remote: !/#/.test(n) && n
                  },
                  s.data(),
                  i.data()
                );
          i.is("a") && o.preventDefault(),
            s.one("show.bs.modal", function(t) {
              t.isDefaultPrevented() ||
                s.one("hidden.bs.modal", function() {
                  i.is(":visible") && i.trigger("focus");
                });
            }),
            e.call(s, a, this);
        }
      );
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(e) {
      return this.each(function() {
        var i = t(this),
          n = i.data("bs.tooltip"),
          s = "object" == typeof e && e;
        (!n && /destroy|hide/.test(e)) ||
          (n || i.data("bs.tooltip", (n = new o(this, s))),
          "string" == typeof e && n[e]());
      });
    }
    var o = function(t, e) {
      (this.type = null),
        (this.options = null),
        (this.enabled = null),
        (this.timeout = null),
        (this.hoverState = null),
        (this.$element = null),
        (this.inState = null),
        this.init("tooltip", t, e);
    };
    (o.VERSION = "3.3.7"),
      (o.TRANSITION_DURATION = 150),
      (o.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template:
          '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {
          selector: "body",
          padding: 0
        }
      }),
      (o.prototype.init = function(e, o, i) {
        if (
          ((this.enabled = !0),
          (this.type = e),
          (this.$element = t(o)),
          (this.options = this.getOptions(i)),
          (this.$viewport =
            this.options.viewport &&
            t(
              t.isFunction(this.options.viewport)
                ? this.options.viewport.call(this, this.$element)
                : this.options.viewport.selector || this.options.viewport
            )),
          (this.inState = {
            click: !1,
            hover: !1,
            focus: !1
          }),
          this.$element[0] instanceof document.constructor &&
            !this.options.selector)
        )
          throw new Error(
            "`selector` option must be specified when initializing " +
              this.type +
              " on the window.document object!"
          );
        for (var n = this.options.trigger.split(" "), s = n.length; s--; ) {
          var a = n[s];
          if ("click" == a)
            this.$element.on(
              "click." + this.type,
              this.options.selector,
              t.proxy(this.toggle, this)
            );
          else if ("manual" != a) {
            var r = "hover" == a ? "mouseenter" : "focusin",
              l = "hover" == a ? "mouseleave" : "focusout";
            this.$element.on(
              r + "." + this.type,
              this.options.selector,
              t.proxy(this.enter, this)
            ),
              this.$element.on(
                l + "." + this.type,
                this.options.selector,
                t.proxy(this.leave, this)
              );
          }
        }
        this.options.selector
          ? (this._options = t.extend({}, this.options, {
              trigger: "manual",
              selector: ""
            }))
          : this.fixTitle();
      }),
      (o.prototype.getDefaults = function() {
        return o.DEFAULTS;
      }),
      (o.prototype.getOptions = function(e) {
        return (
          (e = t.extend({}, this.getDefaults(), this.$element.data(), e)),
          e.delay &&
            "number" == typeof e.delay &&
            (e.delay = {
              show: e.delay,
              hide: e.delay
            }),
          e
        );
      }),
      (o.prototype.getDelegateOptions = function() {
        var e = {},
          o = this.getDefaults();
        return (
          this._options &&
            t.each(this._options, function(t, i) {
              o[t] != i && (e[t] = i);
            }),
          e
        );
      }),
      (o.prototype.enter = function(e) {
        var o =
          e instanceof this.constructor
            ? e
            : t(e.currentTarget).data("bs." + this.type);
        return (
          o ||
            ((o = new this.constructor(
              e.currentTarget,
              this.getDelegateOptions()
            )),
            t(e.currentTarget).data("bs." + this.type, o)),
          e instanceof t.Event &&
            (o.inState["focusin" == e.type ? "focus" : "hover"] = !0),
          o.tip().hasClass("in") || "in" == o.hoverState
            ? void (o.hoverState = "in")
            : (clearTimeout(o.timeout),
              (o.hoverState = "in"),
              o.options.delay && o.options.delay.show
                ? void (o.timeout = setTimeout(function() {
                    "in" == o.hoverState && o.show();
                  }, o.options.delay.show))
                : o.show())
        );
      }),
      (o.prototype.isInStateTrue = function() {
        for (var t in this.inState) if (this.inState[t]) return !0;
        return !1;
      }),
      (o.prototype.leave = function(e) {
        var o =
          e instanceof this.constructor
            ? e
            : t(e.currentTarget).data("bs." + this.type);
        if (
          (o ||
            ((o = new this.constructor(
              e.currentTarget,
              this.getDelegateOptions()
            )),
            t(e.currentTarget).data("bs." + this.type, o)),
          e instanceof t.Event &&
            (o.inState["focusout" == e.type ? "focus" : "hover"] = !1),
          !o.isInStateTrue())
        )
          return (
            clearTimeout(o.timeout),
            (o.hoverState = "out"),
            o.options.delay && o.options.delay.hide
              ? void (o.timeout = setTimeout(function() {
                  "out" == o.hoverState && o.hide();
                }, o.options.delay.hide))
              : o.hide()
          );
      }),
      (o.prototype.show = function() {
        var e = t.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
          this.$element.trigger(e);
          var i = t.contains(
            this.$element[0].ownerDocument.documentElement,
            this.$element[0]
          );
          if (e.isDefaultPrevented() || !i) return;
          var n = this,
            s = this.tip(),
            a = this.getUID(this.type);
          this.setContent(),
            s.attr("id", a),
            this.$element.attr("aria-describedby", a),
            this.options.animation && s.addClass("fade");
          var r =
              "function" == typeof this.options.placement
                ? this.options.placement.call(this, s[0], this.$element[0])
                : this.options.placement,
            l = /\s?auto?\s?/i,
            c = l.test(r);
          c && (r = r.replace(l, "") || "top"),
            s
              .detach()
              .css({
                top: 0,
                left: 0,
                display: "block"
              })
              .addClass(r)
              .data("bs." + this.type, this),
            this.options.container
              ? s.appendTo(this.options.container)
              : s.insertAfter(this.$element),
            this.$element.trigger("inserted.bs." + this.type);
          var d = this.getPosition(),
            h = s[0].offsetWidth,
            u = s[0].offsetHeight;
          if (c) {
            var p = r,
              f = this.getPosition(this.$viewport);
            (r =
              "bottom" == r && d.bottom + u > f.bottom
                ? "top"
                : "top" == r && d.top - u < f.top
                  ? "bottom"
                  : "right" == r && d.right + h > f.width
                    ? "left"
                    : "left" == r && d.left - h < f.left
                      ? "right"
                      : r),
              s.removeClass(p).addClass(r);
          }
          var m = this.getCalculatedOffset(r, d, h, u);
          this.applyPlacement(m, r);
          var v = function() {
            var t = n.hoverState;
            n.$element.trigger("shown.bs." + n.type),
              (n.hoverState = null),
              "out" == t && n.leave(n);
          };
          t.support.transition && this.$tip.hasClass("fade")
            ? s
                .one("bsTransitionEnd", v)
                .emulateTransitionEnd(o.TRANSITION_DURATION)
            : v();
        }
      }),
      (o.prototype.applyPlacement = function(e, o) {
        var i = this.tip(),
          n = i[0].offsetWidth,
          s = i[0].offsetHeight,
          a = parseInt(i.css("margin-top"), 10),
          r = parseInt(i.css("margin-left"), 10);
        isNaN(a) && (a = 0),
          isNaN(r) && (r = 0),
          (e.top += a),
          (e.left += r),
          t.offset.setOffset(
            i[0],
            t.extend(
              {
                using: function(t) {
                  i.css({
                    top: Math.round(t.top),
                    left: Math.round(t.left)
                  });
                }
              },
              e
            ),
            0
          ),
          i.addClass("in");
        var l = i[0].offsetWidth,
          c = i[0].offsetHeight;
        "top" == o && c != s && (e.top = e.top + s - c);
        var d = this.getViewportAdjustedDelta(o, e, l, c);
        d.left ? (e.left += d.left) : (e.top += d.top);
        var h = /top|bottom/.test(o),
          u = h ? 2 * d.left - n + l : 2 * d.top - s + c,
          p = h ? "offsetWidth" : "offsetHeight";
        i.offset(e), this.replaceArrow(u, i[0][p], h);
      }),
      (o.prototype.replaceArrow = function(t, e, o) {
        this.arrow()
          .css(o ? "left" : "top", 50 * (1 - t / e) + "%")
          .css(o ? "top" : "left", "");
      }),
      (o.prototype.setContent = function() {
        var t = this.tip(),
          e = this.getTitle();
        t.find(".tooltip-inner")[this.options.html ? "html" : "text"](e),
          t.removeClass("fade in top bottom left right");
      }),
      (o.prototype.hide = function(e) {
        function i() {
          "in" != n.hoverState && s.detach(),
            n.$element &&
              n.$element
                .removeAttr("aria-describedby")
                .trigger("hidden.bs." + n.type),
            e && e();
        }
        var n = this,
          s = t(this.$tip),
          a = t.Event("hide.bs." + this.type);
        if ((this.$element.trigger(a), !a.isDefaultPrevented()))
          return (
            s.removeClass("in"),
            t.support.transition && s.hasClass("fade")
              ? s
                  .one("bsTransitionEnd", i)
                  .emulateTransitionEnd(o.TRANSITION_DURATION)
              : i(),
            (this.hoverState = null),
            this
          );
      }),
      (o.prototype.fixTitle = function() {
        var t = this.$element;
        (t.attr("title") || "string" != typeof t.attr("data-original-title")) &&
          t
            .attr("data-original-title", t.attr("title") || "")
            .attr("title", "");
      }),
      (o.prototype.hasContent = function() {
        return this.getTitle();
      }),
      (o.prototype.getPosition = function(e) {
        e = e || this.$element;
        var o = e[0],
          i = "BODY" == o.tagName,
          n = o.getBoundingClientRect();
        null == n.width &&
          (n = t.extend({}, n, {
            width: n.right - n.left,
            height: n.bottom - n.top
          }));
        var s = window.SVGElement && o instanceof window.SVGElement,
          a = i
            ? {
                top: 0,
                left: 0
              }
            : s
              ? null
              : e.offset(),
          r = {
            scroll: i
              ? document.documentElement.scrollTop || document.body.scrollTop
              : e.scrollTop()
          },
          l = i
            ? {
                width: t(window).width(),
                height: t(window).height()
              }
            : null;
        return t.extend({}, n, r, l, a);
      }),
      (o.prototype.getCalculatedOffset = function(t, e, o, i) {
        return "bottom" == t
          ? {
              top: e.top + e.height,
              left: e.left + e.width / 2 - o / 2
            }
          : "top" == t
            ? {
                top: e.top - i,
                left: e.left + e.width / 2 - o / 2
              }
            : "left" == t
              ? {
                  top: e.top + e.height / 2 - i / 2,
                  left: e.left - o
                }
              : {
                  top: e.top + e.height / 2 - i / 2,
                  left: e.left + e.width
                };
      }),
      (o.prototype.getViewportAdjustedDelta = function(t, e, o, i) {
        var n = {
          top: 0,
          left: 0
        };
        if (!this.$viewport) return n;
        var s = (this.options.viewport && this.options.viewport.padding) || 0,
          a = this.getPosition(this.$viewport);
        if (/right|left/.test(t)) {
          var r = e.top - s - a.scroll,
            l = e.top + s - a.scroll + i;
          r < a.top
            ? (n.top = a.top - r)
            : l > a.top + a.height && (n.top = a.top + a.height - l);
        } else {
          var c = e.left - s,
            d = e.left + s + o;
          c < a.left
            ? (n.left = a.left - c)
            : d > a.right && (n.left = a.left + a.width - d);
        }
        return n;
      }),
      (o.prototype.getTitle = function() {
        var t,
          e = this.$element,
          o = this.options;
        return (t =
          e.attr("data-original-title") ||
          ("function" == typeof o.title ? o.title.call(e[0]) : o.title));
      }),
      (o.prototype.getUID = function(t) {
        do t += ~~(1e6 * Math.random());
        while (document.getElementById(t));
        return t;
      }),
      (o.prototype.tip = function() {
        if (
          !this.$tip &&
          ((this.$tip = t(this.options.template)), 1 != this.$tip.length)
        )
          throw new Error(
            this.type +
              " `template` option must consist of exactly 1 top-level element!"
          );
        return this.$tip;
      }),
      (o.prototype.arrow = function() {
        return (this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow"));
      }),
      (o.prototype.enable = function() {
        this.enabled = !0;
      }),
      (o.prototype.disable = function() {
        this.enabled = !1;
      }),
      (o.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled;
      }),
      (o.prototype.toggle = function(e) {
        var o = this;
        e &&
          ((o = t(e.currentTarget).data("bs." + this.type)),
          o ||
            ((o = new this.constructor(
              e.currentTarget,
              this.getDelegateOptions()
            )),
            t(e.currentTarget).data("bs." + this.type, o))),
          e
            ? ((o.inState.click = !o.inState.click),
              o.isInStateTrue() ? o.enter(o) : o.leave(o))
            : o.tip().hasClass("in")
              ? o.leave(o)
              : o.enter(o);
      }),
      (o.prototype.destroy = function() {
        var t = this;
        clearTimeout(this.timeout),
          this.hide(function() {
            t.$element.off("." + t.type).removeData("bs." + t.type),
              t.$tip && t.$tip.detach(),
              (t.$tip = null),
              (t.$arrow = null),
              (t.$viewport = null),
              (t.$element = null);
          });
      });
    var i = t.fn.tooltip;
    (t.fn.tooltip = e),
      (t.fn.tooltip.Constructor = o),
      (t.fn.tooltip.noConflict = function() {
        return (t.fn.tooltip = i), this;
      });
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(e) {
      return this.each(function() {
        var i = t(this),
          n = i.data("bs.popover"),
          s = "object" == typeof e && e;
        (!n && /destroy|hide/.test(e)) ||
          (n || i.data("bs.popover", (n = new o(this, s))),
          "string" == typeof e && n[e]());
      });
    }
    var o = function(t, e) {
      this.init("popover", t, e);
    };
    if (!t.fn.tooltip) throw new Error("Popover requires tooltip.js");
    (o.VERSION = "3.3.7"),
      (o.DEFAULTS = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template:
          '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
      })),
      (o.prototype = t.extend({}, t.fn.tooltip.Constructor.prototype)),
      (o.prototype.constructor = o),
      (o.prototype.getDefaults = function() {
        return o.DEFAULTS;
      }),
      (o.prototype.setContent = function() {
        var t = this.tip(),
          e = this.getTitle(),
          o = this.getContent();
        t.find(".popover-title")[this.options.html ? "html" : "text"](e),
          t
            .find(".popover-content")
            .children()
            .detach()
            .end()
            [
              this.options.html
                ? "string" == typeof o
                  ? "html"
                  : "append"
                : "text"
            ](o),
          t.removeClass("fade top bottom left right in"),
          t.find(".popover-title").html() || t.find(".popover-title").hide();
      }),
      (o.prototype.hasContent = function() {
        return this.getTitle() || this.getContent();
      }),
      (o.prototype.getContent = function() {
        var t = this.$element,
          e = this.options;
        return (
          t.attr("data-content") ||
          ("function" == typeof e.content ? e.content.call(t[0]) : e.content)
        );
      }),
      (o.prototype.arrow = function() {
        return (this.$arrow = this.$arrow || this.tip().find(".arrow"));
      });
    var i = t.fn.popover;
    (t.fn.popover = e),
      (t.fn.popover.Constructor = o),
      (t.fn.popover.noConflict = function() {
        return (t.fn.popover = i), this;
      });
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(o, i) {
      (this.$body = t(document.body)),
        (this.$scrollElement = t(t(o).is(document.body) ? window : o)),
        (this.options = t.extend({}, e.DEFAULTS, i)),
        (this.selector = (this.options.target || "") + " .nav li > a"),
        (this.offsets = []),
        (this.targets = []),
        (this.activeTarget = null),
        (this.scrollHeight = 0),
        this.$scrollElement.on(
          "scroll.bs.scrollspy",
          t.proxy(this.process, this)
        ),
        this.refresh(),
        this.process();
    }

    function o(o) {
      return this.each(function() {
        var i = t(this),
          n = i.data("bs.scrollspy"),
          s = "object" == typeof o && o;
        n || i.data("bs.scrollspy", (n = new e(this, s))),
          "string" == typeof o && n[o]();
      });
    }
    (e.VERSION = "3.3.7"),
      (e.DEFAULTS = {
        offset: 10
      }),
      (e.prototype.getScrollHeight = function() {
        return (
          this.$scrollElement[0].scrollHeight ||
          Math.max(
            this.$body[0].scrollHeight,
            document.documentElement.scrollHeight
          )
        );
      }),
      (e.prototype.refresh = function() {
        var e = this,
          o = "offset",
          i = 0;
        (this.offsets = []),
          (this.targets = []),
          (this.scrollHeight = this.getScrollHeight()),
          t.isWindow(this.$scrollElement[0]) ||
            ((o = "position"), (i = this.$scrollElement.scrollTop())),
          this.$body
            .find(this.selector)
            .map(function() {
              var e = t(this),
                n = e.data("target") || e.attr("href"),
                s = /^#./.test(n) && t(n);
              return (
                (s && s.length && s.is(":visible") && [[s[o]().top + i, n]]) ||
                null
              );
            })
            .sort(function(t, e) {
              return t[0] - e[0];
            })
            .each(function() {
              e.offsets.push(this[0]), e.targets.push(this[1]);
            });
      }),
      (e.prototype.process = function() {
        var t,
          e = this.$scrollElement.scrollTop() + this.options.offset,
          o = this.getScrollHeight(),
          i = this.options.offset + o - this.$scrollElement.height(),
          n = this.offsets,
          s = this.targets,
          a = this.activeTarget;
        if ((this.scrollHeight != o && this.refresh(), e >= i))
          return a != (t = s[s.length - 1]) && this.activate(t);
        if (a && e < n[0]) return (this.activeTarget = null), this.clear();
        for (t = n.length; t--; )
          a != s[t] &&
            e >= n[t] &&
            (void 0 === n[t + 1] || e < n[t + 1]) &&
            this.activate(s[t]);
      }),
      (e.prototype.activate = function(e) {
        (this.activeTarget = e), this.clear();
        var o =
            this.selector +
            '[data-target="' +
            e +
            '"],' +
            this.selector +
            '[href="' +
            e +
            '"]',
          i = t(o)
            .parents("li")
            .addClass("active");
        i.parent(".dropdown-menu").length &&
          (i = i.closest("li.dropdown").addClass("active")),
          i.trigger("activate.bs.scrollspy");
      }),
      (e.prototype.clear = function() {
        t(this.selector)
          .parentsUntil(this.options.target, ".active")
          .removeClass("active");
      });
    var i = t.fn.scrollspy;
    (t.fn.scrollspy = o),
      (t.fn.scrollspy.Constructor = e),
      (t.fn.scrollspy.noConflict = function() {
        return (t.fn.scrollspy = i), this;
      }),
      t(window).on("load.bs.scrollspy.data-api", function() {
        t('[data-spy="scroll"]').each(function() {
          var e = t(this);
          o.call(e, e.data());
        });
      });
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(e) {
      return this.each(function() {
        var i = t(this),
          n = i.data("bs.tab");
        n || i.data("bs.tab", (n = new o(this))),
          "string" == typeof e && n[e]();
      });
    }
    var o = function(e) {
      this.element = t(e);
    };
    (o.VERSION = "3.3.7"),
      (o.TRANSITION_DURATION = 150),
      (o.prototype.show = function() {
        var e = this.element,
          o = e.closest("ul:not(.dropdown-menu)"),
          i = e.data("target");
        if (
          (i ||
            ((i = e.attr("href")), (i = i && i.replace(/.*(?=#[^\s]*$)/, ""))),
          !e.parent("li").hasClass("active"))
        ) {
          var n = o.find(".active:last a"),
            s = t.Event("hide.bs.tab", {
              relatedTarget: e[0]
            }),
            a = t.Event("show.bs.tab", {
              relatedTarget: n[0]
            });
          if (
            (n.trigger(s),
            e.trigger(a),
            !a.isDefaultPrevented() && !s.isDefaultPrevented())
          ) {
            var r = t(i);
            this.activate(e.closest("li"), o),
              this.activate(r, r.parent(), function() {
                n.trigger({
                  type: "hidden.bs.tab",
                  relatedTarget: e[0]
                }),
                  e.trigger({
                    type: "shown.bs.tab",
                    relatedTarget: n[0]
                  });
              });
          }
        }
      }),
      (o.prototype.activate = function(e, i, n) {
        function s() {
          a
            .removeClass("active")
            .find("> .dropdown-menu > .active")
            .removeClass("active")
            .end()
            .find('[data-toggle="tab"]')
            .attr("aria-expanded", !1),
            e
              .addClass("active")
              .find('[data-toggle="tab"]')
              .attr("aria-expanded", !0),
            r ? (e[0].offsetWidth, e.addClass("in")) : e.removeClass("fade"),
            e.parent(".dropdown-menu").length &&
              e
                .closest("li.dropdown")
                .addClass("active")
                .end()
                .find('[data-toggle="tab"]')
                .attr("aria-expanded", !0),
            n && n();
        }
        var a = i.find("> .active"),
          r =
            n &&
            t.support.transition &&
            ((a.length && a.hasClass("fade")) || !!i.find("> .fade").length);
        a.length && r
          ? a
              .one("bsTransitionEnd", s)
              .emulateTransitionEnd(o.TRANSITION_DURATION)
          : s(),
          a.removeClass("in");
      });
    var i = t.fn.tab;
    (t.fn.tab = e),
      (t.fn.tab.Constructor = o),
      (t.fn.tab.noConflict = function() {
        return (t.fn.tab = i), this;
      });
    var n = function(o) {
      o.preventDefault(), e.call(t(this), "show");
    };
    t(document)
      .on("click.bs.tab.data-api", '[data-toggle="tab"]', n)
      .on("click.bs.tab.data-api", '[data-toggle="pill"]', n);
  })(jQuery),
  +(function(t) {
    "use strict";

    function e(e) {
      return this.each(function() {
        var i = t(this),
          n = i.data("bs.affix"),
          s = "object" == typeof e && e;
        n || i.data("bs.affix", (n = new o(this, s))),
          "string" == typeof e && n[e]();
      });
    }
    var o = function(e, i) {
      (this.options = t.extend({}, o.DEFAULTS, i)),
        (this.$target = t(this.options.target)
          .on("scroll.bs.affix.data-api", t.proxy(this.checkPosition, this))
          .on(
            "click.bs.affix.data-api",
            t.proxy(this.checkPositionWithEventLoop, this)
          )),
        (this.$element = t(e)),
        (this.affixed = null),
        (this.unpin = null),
        (this.pinnedOffset = null),
        this.checkPosition();
    };
    (o.VERSION = "3.3.7"),
      (o.RESET = "affix affix-top affix-bottom"),
      (o.DEFAULTS = {
        offset: 0,
        target: window
      }),
      (o.prototype.getState = function(t, e, o, i) {
        var n = this.$target.scrollTop(),
          s = this.$element.offset(),
          a = this.$target.height();
        if (null != o && "top" == this.affixed) return n < o && "top";
        if ("bottom" == this.affixed)
          return null != o
            ? !(n + this.unpin <= s.top) && "bottom"
            : !(n + a <= t - i) && "bottom";
        var r = null == this.affixed,
          l = r ? n : s.top,
          c = r ? a : e;
        return null != o && n <= o
          ? "top"
          : null != i && l + c >= t - i && "bottom";
      }),
      (o.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(o.RESET).addClass("affix");
        var t = this.$target.scrollTop(),
          e = this.$element.offset();
        return (this.pinnedOffset = e.top - t);
      }),
      (o.prototype.checkPositionWithEventLoop = function() {
        setTimeout(t.proxy(this.checkPosition, this), 1);
      }),
      (o.prototype.checkPosition = function() {
        if (this.$element.is(":visible")) {
          var e = this.$element.height(),
            i = this.options.offset,
            n = i.top,
            s = i.bottom,
            a = Math.max(t(document).height(), t(document.body).height());
          "object" != typeof i && (s = n = i),
            "function" == typeof n && (n = i.top(this.$element)),
            "function" == typeof s && (s = i.bottom(this.$element));
          var r = this.getState(a, e, n, s);
          if (this.affixed != r) {
            null != this.unpin && this.$element.css("top", "");
            var l = "affix" + (r ? "-" + r : ""),
              c = t.Event(l + ".bs.affix");
            if ((this.$element.trigger(c), c.isDefaultPrevented())) return;
            (this.affixed = r),
              (this.unpin = "bottom" == r ? this.getPinnedOffset() : null),
              this.$element
                .removeClass(o.RESET)
                .addClass(l)
                .trigger(l.replace("affix", "affixed") + ".bs.affix");
          }
          "bottom" == r &&
            this.$element.offset({
              top: a - e - s
            });
        }
      });
    var i = t.fn.affix;
    (t.fn.affix = e),
      (t.fn.affix.Constructor = o),
      (t.fn.affix.noConflict = function() {
        return (t.fn.affix = i), this;
      }),
      t(window).on("load", function() {
        t('[data-spy="affix"]').each(function() {
          var o = t(this),
            i = o.data();
          (i.offset = i.offset || {}),
            null != i.offsetBottom && (i.offset.bottom = i.offsetBottom),
            null != i.offsetTop && (i.offset.top = i.offsetTop),
            e.call(o, i);
        });
      });
  })(jQuery),
  (function() {
    function t(t, e, o) {
      return t.call.apply(t.bind, arguments);
    }

    function e(t, e, o) {
      if (!t) throw Error();
      if (2 < arguments.length) {
        var i = Array.prototype.slice.call(arguments, 2);
        return function() {
          var o = Array.prototype.slice.call(arguments);
          return Array.prototype.unshift.apply(o, i), t.apply(e, o);
        };
      }
      return function() {
        return t.apply(e, arguments);
      };
    }

    function o(i, n, s) {
      return (
        (o =
          Function.prototype.bind &&
          -1 != Function.prototype.bind.toString().indexOf("native code")
            ? t
            : e),
        o.apply(null, arguments)
      );
    }

    function i(t, e) {
      (this.a = t), (this.o = e || t), (this.c = this.o.document);
    }

    function n(t, e, o, i) {
      if (((e = t.c.createElement(e)), o))
        for (var n in o)
          o.hasOwnProperty(n) &&
            ("style" == n ? (e.style.cssText = o[n]) : e.setAttribute(n, o[n]));
      return i && e.appendChild(t.c.createTextNode(i)), e;
    }

    function s(t, e, o) {
      (t = t.c.getElementsByTagName(e)[0]),
        t || (t = document.documentElement),
        t.insertBefore(o, t.lastChild);
    }

    function a(t) {
      t.parentNode && t.parentNode.removeChild(t);
    }

    function r(t, e, o) {
      (e = e || []), (o = o || []);
      for (var i = t.className.split(/\s+/), n = 0; n < e.length; n += 1) {
        for (var s = !1, a = 0; a < i.length; a += 1)
          if (e[n] === i[a]) {
            s = !0;
            break;
          }
        s || i.push(e[n]);
      }
      for (e = [], n = 0; n < i.length; n += 1) {
        for (s = !1, a = 0; a < o.length; a += 1)
          if (i[n] === o[a]) {
            s = !0;
            break;
          }
        s || e.push(i[n]);
      }
      t.className = e
        .join(" ")
        .replace(/\s+/g, " ")
        .replace(/^\s+|\s+$/, "");
    }

    function l(t, e) {
      for (var o = t.className.split(/\s+/), i = 0, n = o.length; i < n; i++)
        if (o[i] == e) return !0;
      return !1;
    }

    function c(t) {
      return t.o.location.hostname || t.a.location.hostname;
    }

    function d(t, e, o) {
      function i() {
        c && a && r && (c(l), (c = null));
      }
      e = n(t, "link", {
        rel: "stylesheet",
        href: e,
        media: "all"
      });
      var a = !1,
        r = !0,
        l = null,
        c = o || null;
      et
        ? ((e.onload = function() {
            (a = !0), i();
          }),
          (e.onerror = function() {
            (a = !0), (l = Error("Stylesheet failed to load")), i();
          }))
        : setTimeout(function() {
            (a = !0), i();
          }, 0),
        s(t, "head", e);
    }

    function h(t, e, o, i) {
      var s = t.c.getElementsByTagName("head")[0];
      if (s) {
        var a = n(t, "script", {
            src: e
          }),
          r = !1;
        return (
          (a.onload = a.onreadystatechange = function() {
            r ||
              (this.readyState &&
                "loaded" != this.readyState &&
                "complete" != this.readyState) ||
              ((r = !0),
              o && o(null),
              (a.onload = a.onreadystatechange = null),
              "HEAD" == a.parentNode.tagName && s.removeChild(a));
          }),
          s.appendChild(a),
          setTimeout(function() {
            r || ((r = !0), o && o(Error("Script load timeout")));
          }, i || 5e3),
          a
        );
      }
      return null;
    }

    function u() {
      (this.a = 0), (this.c = null);
    }

    function p(t) {
      return (
        t.a++,
        function() {
          t.a--, m(t);
        }
      );
    }

    function f(t, e) {
      (t.c = e), m(t);
    }

    function m(t) {
      0 == t.a && t.c && (t.c(), (t.c = null));
    }

    function v(t) {
      this.a = t || "-";
    }

    function g(t, e) {
      (this.c = t), (this.f = 4), (this.a = "n");
      var o = (e || "n4").match(/^([nio])([1-9])$/i);
      o && ((this.a = o[1]), (this.f = parseInt(o[2], 10)));
    }

    function y(t) {
      return C(t) + " " + (t.f + "00") + " 300px " + b(t.c);
    }

    function b(t) {
      var e = [];
      t = t.split(/,\s*/);
      for (var o = 0; o < t.length; o++) {
        var i = t[o].replace(/['"]/g, "");
        -1 != i.indexOf(" ") || /^\d/.test(i)
          ? e.push("'" + i + "'")
          : e.push(i);
      }
      return e.join(",");
    }

    function w(t) {
      return t.a + t.f;
    }

    function C(t) {
      var e = "normal";
      return "o" === t.a ? (e = "oblique") : "i" === t.a && (e = "italic"), e;
    }

    function $(t) {
      var e = 4,
        o = "n",
        i = null;
      return (
        t &&
          ((i = t.match(/(normal|oblique|italic)/i)) &&
            i[1] &&
            (o = i[1].substr(0, 1).toLowerCase()),
          (i = t.match(/([1-9]00|normal|bold)/i)) &&
            i[1] &&
            (/bold/i.test(i[1])
              ? (e = 7)
              : /[1-9]00/.test(i[1]) && (e = parseInt(i[1].substr(0, 1), 10)))),
        o + e
      );
    }

    function j(t, e) {
      (this.c = t),
        (this.f = t.o.document.documentElement),
        (this.h = e),
        (this.a = new v("-")),
        (this.j = !1 !== e.events),
        (this.g = !1 !== e.classes);
    }

    function k(t) {
      t.g && r(t.f, [t.a.c("wf", "loading")]), x(t, "loading");
    }

    function T(t) {
      if (t.g) {
        var e = l(t.f, t.a.c("wf", "active")),
          o = [],
          i = [t.a.c("wf", "loading")];
        e || o.push(t.a.c("wf", "inactive")), r(t.f, o, i);
      }
      x(t, "inactive");
    }

    function x(t, e, o) {
      t.j && t.h[e] && (o ? t.h[e](o.c, w(o)) : t.h[e]());
    }

    function E() {
      this.c = {};
    }

    function S(t, e, o) {
      var i,
        n = [];
      for (i in e)
        if (e.hasOwnProperty(i)) {
          var s = t.c[i];
          s && n.push(s(e[i], o));
        }
      return n;
    }

    function _(t, e) {
      (this.c = t),
        (this.f = e),
        (this.a = n(
          this.c,
          "span",
          {
            "aria-hidden": "true"
          },
          this.f
        ));
    }

    function A(t) {
      s(t.c, "body", t.a);
    }

    function N(t) {
      return (
        "display:block;position:absolute;top:-9999px;left:-9999px;font-size:300px;width:auto;height:auto;line-height:normal;margin:0;padding:0;font-variant:normal;white-space:nowrap;font-family:" +
        b(t.c) +
        ";" +
        ("font-style:" + C(t) + ";font-weight:" + (t.f + "00") + ";")
      );
    }

    function I(t, e, o, i, n, s) {
      (this.g = t),
        (this.j = e),
        (this.a = i),
        (this.c = o),
        (this.f = n || 3e3),
        (this.h = s || void 0);
    }

    function O(t, e, o, i, n, s, a) {
      (this.v = t),
        (this.B = e),
        (this.c = o),
        (this.a = i),
        (this.s = a || "BESbswy"),
        (this.f = {}),
        (this.w = n || 3e3),
        (this.u = s || null),
        (this.m = this.j = this.h = this.g = null),
        (this.g = new _(this.c, this.s)),
        (this.h = new _(this.c, this.s)),
        (this.j = new _(this.c, this.s)),
        (this.m = new _(this.c, this.s)),
        (t = new g(this.a.c + ",serif", w(this.a))),
        (t = N(t)),
        (this.g.a.style.cssText = t),
        (t = new g(this.a.c + ",sans-serif", w(this.a))),
        (t = N(t)),
        (this.h.a.style.cssText = t),
        (t = new g("serif", w(this.a))),
        (t = N(t)),
        (this.j.a.style.cssText = t),
        (t = new g("sans-serif", w(this.a))),
        (t = N(t)),
        (this.m.a.style.cssText = t),
        A(this.g),
        A(this.h),
        A(this.j),
        A(this.m);
    }

    function D() {
      if (null === it) {
        var t = /AppleWebKit\/([0-9]+)(?:\.([0-9]+))/.exec(
          window.navigator.userAgent
        );
        it =
          !!t &&
          (536 > parseInt(t[1], 10) ||
            (536 === parseInt(t[1], 10) && 11 >= parseInt(t[2], 10)));
      }
      return it;
    }

    function F(t, e, o) {
      for (var i in ot)
        if (ot.hasOwnProperty(i) && e === t.f[ot[i]] && o === t.f[ot[i]])
          return !0;
      return !1;
    }

    function P(t) {
      var e,
        o = t.g.a.offsetWidth,
        i = t.h.a.offsetWidth;
      (e = o === t.f.serif && i === t.f["sans-serif"]) ||
        (e = D() && F(t, o, i)),
        e
          ? tt() - t.A >= t.w
            ? D() && F(t, o, i) && (null === t.u || t.u.hasOwnProperty(t.a.c))
              ? z(t, t.v)
              : z(t, t.B)
            : R(t)
          : z(t, t.v);
    }

    function R(t) {
      setTimeout(
        o(function() {
          P(this);
        }, t),
        50
      );
    }

    function z(t, e) {
      setTimeout(
        o(function() {
          a(this.g.a), a(this.h.a), a(this.j.a), a(this.m.a), e(this.a);
        }, t),
        0
      );
    }

    function L(t, e, o) {
      (this.c = t),
        (this.a = e),
        (this.f = 0),
        (this.m = this.j = !1),
        (this.s = o);
    }

    function W(t) {
      0 == --t.f &&
        t.j &&
        (t.m
          ? ((t = t.a),
            t.g &&
              r(
                t.f,
                [t.a.c("wf", "active")],
                [t.a.c("wf", "loading"), t.a.c("wf", "inactive")]
              ),
            x(t, "active"))
          : T(t.a));
    }

    function U(t) {
      (this.j = t), (this.a = new E()), (this.h = 0), (this.f = this.g = !0);
    }

    function H(t, e, i, n, s) {
      var a = 0 == --t.h;
      (t.f || t.g) &&
        setTimeout(function() {
          var t = s || null,
            l = n || null || {};
          if (0 === i.length && a) T(e.a);
          else {
            (e.f += i.length), a && (e.j = a);
            var c,
              d = [];
            for (c = 0; c < i.length; c++) {
              var h = i[c],
                u = l[h.c],
                p = e.a,
                f = h;
              if (
                (p.g && r(p.f, [p.a.c("wf", f.c, w(f).toString(), "loading")]),
                x(p, "fontloading", f),
                (p = null),
                null === nt)
              )
                if (window.FontFace) {
                  var f = /Gecko.*Firefox\/(\d+)/.exec(
                      window.navigator.userAgent
                    ),
                    m =
                      /OS X.*Version\/10\..*Safari/.exec(
                        window.navigator.userAgent
                      ) && /Apple/.exec(window.navigator.vendor);
                  nt = f ? 42 < parseInt(f[1], 10) : !m;
                } else nt = !1;
              (p = nt
                ? new I(o(e.g, e), o(e.h, e), e.c, h, e.s, u)
                : new O(o(e.g, e), o(e.h, e), e.c, h, e.s, t, u)),
                d.push(p);
            }
            for (c = 0; c < d.length; c++) d[c].start();
          }
        }, 0);
    }

    function B(t, e, o) {
      var i = [],
        n = o.timeout;
      k(e);
      var i = S(t.a, o, t.c),
        s = new L(t.c, e, n);
      for (t.h = i.length, e = 0, o = i.length; e < o; e++)
        i[e].load(function(e, o, i) {
          H(t, s, e, o, i);
        });
    }

    function M(t, e) {
      (this.c = t), (this.a = e);
    }

    function Q(t, e) {
      (this.c = t), (this.a = e);
    }

    function q(t, e) {
      t ? (this.c = t) : (this.c = st),
        (this.a = []),
        (this.f = []),
        (this.g = e || "");
    }

    function V(t, e) {
      for (var o = e.length, i = 0; i < o; i++) {
        var n = e[i].split(":");
        3 == n.length && t.f.push(n.pop());
        var s = "";
        2 == n.length && "" != n[1] && (s = ":"), t.a.push(n.join(s));
      }
    }

    function Y(t) {
      if (0 == t.a.length) throw Error("No fonts to load!");
      if (-1 != t.c.indexOf("kit=")) return t.c;
      for (var e = t.a.length, o = [], i = 0; i < e; i++)
        o.push(t.a[i].replace(/ /g, "+"));
      return (
        (e = t.c + "?family=" + o.join("%7C")),
        0 < t.f.length && (e += "&subset=" + t.f.join(",")),
        0 < t.g.length && (e += "&text=" + encodeURIComponent(t.g)),
        e
      );
    }

    function K(t) {
      (this.f = t), (this.a = []), (this.c = {});
    }

    function G(t) {
      for (var e = t.f.length, o = 0; o < e; o++) {
        var i = t.f[o].split(":"),
          n = i[0].replace(/\+/g, " "),
          s = ["n4"];
        if (2 <= i.length) {
          var a,
            r = i[1];
          if (((a = []), r))
            for (var r = r.split(","), l = r.length, c = 0; c < l; c++) {
              var d;
              if (((d = r[c]), d.match(/^[\w-]+$/))) {
                var h = ct.exec(d.toLowerCase());
                if (null == h) d = "";
                else {
                  if (
                    ((d = h[2]),
                    (d = null == d || "" == d ? "n" : lt[d]),
                    (h = h[1]),
                    null == h || "" == h)
                  )
                    h = "4";
                  else
                    var u = rt[h],
                      h = u ? u : isNaN(h) ? "4" : h.substr(0, 1);
                  d = [d, h].join("");
                }
              } else d = "";
              d && a.push(d);
            }
          0 < a.length && (s = a),
            3 == i.length &&
              ((i = i[2]),
              (a = []),
              (i = i ? i.split(",") : a),
              0 < i.length && (i = at[i[0]]) && (t.c[n] = i));
        }
        for (
          t.c[n] || ((i = at[n]) && (t.c[n] = i)), i = 0;
          i < s.length;
          i += 1
        )
          t.a.push(new g(n, s[i]));
      }
    }

    function J(t, e) {
      (this.c = t), (this.a = e);
    }

    function X(t, e) {
      (this.c = t), (this.a = e);
    }

    function Z(t, e) {
      (this.c = t), (this.f = e), (this.a = []);
    }
    var tt =
        Date.now ||
        function() {
          return +new Date();
        },
      et = !!window.FontFace;
    (v.prototype.c = function(t) {
      for (var e = [], o = 0; o < arguments.length; o++)
        e.push(arguments[o].replace(/[\W_]+/g, "").toLowerCase());
      return e.join(this.a);
    }),
      (I.prototype.start = function() {
        var t = this.c.o.document,
          e = this,
          o = tt(),
          i = new Promise(function(i, n) {
            function s() {
              tt() - o >= e.f
                ? n()
                : t.fonts.load(y(e.a), e.h).then(
                    function(t) {
                      1 <= t.length ? i() : setTimeout(s, 25);
                    },
                    function() {
                      n();
                    }
                  );
            }
            s();
          }),
          n = null,
          s = new Promise(function(t, o) {
            n = setTimeout(o, e.f);
          });
        Promise.race([s, i]).then(
          function() {
            n && (clearTimeout(n), (n = null)), e.g(e.a);
          },
          function() {
            e.j(e.a);
          }
        );
      });
    var ot = {
        D: "serif",
        C: "sans-serif"
      },
      it = null;
    O.prototype.start = function() {
      (this.f.serif = this.j.a.offsetWidth),
        (this.f["sans-serif"] = this.m.a.offsetWidth),
        (this.A = tt()),
        P(this);
    };
    var nt = null;
    (L.prototype.g = function(t) {
      var e = this.a;
      e.g &&
        r(
          e.f,
          [e.a.c("wf", t.c, w(t).toString(), "active")],
          [
            e.a.c("wf", t.c, w(t).toString(), "loading"),
            e.a.c("wf", t.c, w(t).toString(), "inactive")
          ]
        ),
        x(e, "fontactive", t),
        (this.m = !0),
        W(this);
    }),
      (L.prototype.h = function(t) {
        var e = this.a;
        if (e.g) {
          var o = l(e.f, e.a.c("wf", t.c, w(t).toString(), "active")),
            i = [],
            n = [e.a.c("wf", t.c, w(t).toString(), "loading")];
          o || i.push(e.a.c("wf", t.c, w(t).toString(), "inactive")),
            r(e.f, i, n);
        }
        x(e, "fontinactive", t), W(this);
      }),
      (U.prototype.load = function(t) {
        (this.c = new i(this.j, t.context || this.j)),
          (this.g = !1 !== t.events),
          (this.f = !1 !== t.classes),
          B(this, new j(this.c, t), t);
      }),
      (M.prototype.load = function(t) {
        function e() {
          if (s["__mti_fntLst" + i]) {
            var o,
              n = s["__mti_fntLst" + i](),
              a = [];
            if (n)
              for (var r = 0; r < n.length; r++) {
                var l = n[r].fontfamily;
                void 0 != n[r].fontStyle && void 0 != n[r].fontWeight
                  ? ((o = n[r].fontStyle + n[r].fontWeight),
                    a.push(new g(l, o)))
                  : a.push(new g(l));
              }
            t(a);
          } else
            setTimeout(function() {
              e();
            }, 50);
        }
        var o = this,
          i = o.a.projectId,
          n = o.a.version;
        if (i) {
          var s = o.c.o;
          h(
            this.c,
            (o.a.api || "https://fast.fonts.net/jsapi") +
              "/" +
              i +
              ".js" +
              (n ? "?v=" + n : ""),
            function(n) {
              n
                ? t([])
                : ((s["__MonotypeConfiguration__" + i] = function() {
                    return o.a;
                  }),
                  e());
            }
          ).id = "__MonotypeAPIScript__" + i;
        } else t([]);
      }),
      (Q.prototype.load = function(t) {
        var e,
          o,
          i = this.a.urls || [],
          n = this.a.families || [],
          s = this.a.testStrings || {},
          a = new u();
        for (e = 0, o = i.length; e < o; e++) d(this.c, i[e], p(a));
        var r = [];
        for (e = 0, o = n.length; e < o; e++)
          if (((i = n[e].split(":")), i[1]))
            for (var l = i[1].split(","), c = 0; c < l.length; c += 1)
              r.push(new g(i[0], l[c]));
          else r.push(new g(i[0]));
        f(a, function() {
          t(r, s);
        });
      });
    var st = "https://fonts.googleapis.com/css",
      at = {
        latin: "BESbswy",
        "latin-ext": "çöüğş",
        cyrillic: "йяЖ",
        greek: "αβΣ",
        khmer: "កខគ",
        Hanuman: "កខគ"
      },
      rt = {
        thin: "1",
        extralight: "2",
        "extra-light": "2",
        ultralight: "2",
        "ultra-light": "2",
        light: "3",
        regular: "4",
        book: "4",
        medium: "5",
        "semi-bold": "6",
        semibold: "6",
        "demi-bold": "6",
        demibold: "6",
        bold: "7",
        "extra-bold": "8",
        extrabold: "8",
        "ultra-bold": "8",
        ultrabold: "8",
        black: "9",
        heavy: "9",
        l: "3",
        r: "4",
        b: "7"
      },
      lt = {
        i: "i",
        italic: "i",
        n: "n",
        normal: "n"
      },
      ct = /^(thin|(?:(?:extra|ultra)-?)?light|regular|book|medium|(?:(?:semi|demi|extra|ultra)-?)?bold|black|heavy|l|r|b|[1-9]00)?(n|i|normal|italic)?$/,
      dt = {
        Arimo: !0,
        Cousine: !0,
        Tinos: !0
      };
    (J.prototype.load = function(t) {
      var e = new u(),
        o = this.c,
        i = new q(this.a.api, this.a.text),
        n = this.a.families;
      V(i, n);
      var s = new K(n);
      G(s),
        d(o, Y(i), p(e)),
        f(e, function() {
          t(s.a, s.c, dt);
        });
    }),
      (X.prototype.load = function(t) {
        var e = this.a.id,
          o = this.c.o;
        e
          ? h(
              this.c,
              (this.a.api || "https://use.typekit.net") + "/" + e + ".js",
              function(e) {
                if (e) t([]);
                else if (o.Typekit && o.Typekit.config && o.Typekit.config.fn) {
                  e = o.Typekit.config.fn;
                  for (var i = [], n = 0; n < e.length; n += 2)
                    for (var s = e[n], a = e[n + 1], r = 0; r < a.length; r++)
                      i.push(new g(s, a[r]));
                  try {
                    o.Typekit.load({
                      events: !1,
                      classes: !1,
                      async: !0
                    });
                  } catch (l) {}
                  t(i);
                }
              },
              2e3
            )
          : t([]);
      }),
      (Z.prototype.load = function(t) {
        var e = this.f.id,
          o = this.c.o,
          i = this;
        e
          ? (o.__webfontfontdeckmodule__ || (o.__webfontfontdeckmodule__ = {}),
            (o.__webfontfontdeckmodule__[e] = function(e, o) {
              for (var n = 0, s = o.fonts.length; n < s; ++n) {
                var a = o.fonts[n];
                i.a.push(
                  new g(
                    a.name,
                    $("font-weight:" + a.weight + ";font-style:" + a.style)
                  )
                );
              }
              t(i.a);
            }),
            h(
              this.c,
              (this.f.api || "https://f.fontdeck.com/s/css/js/") +
                c(this.c) +
                "/" +
                e +
                ".js",
              function(e) {
                e && t([]);
              }
            ))
          : t([]);
      });
    var ht = new U(window);
    (ht.a.c.custom = function(t, e) {
      return new Q(e, t);
    }),
      (ht.a.c.fontdeck = function(t, e) {
        return new Z(e, t);
      }),
      (ht.a.c.monotype = function(t, e) {
        return new M(e, t);
      }),
      (ht.a.c.typekit = function(t, e) {
        return new X(e, t);
      }),
      (ht.a.c.google = function(t, e) {
        return new J(e, t);
      });
    var ut = {
      load: o(ht.load, ht)
    };
    "function" == typeof define && define.amd
      ? define(function() {
          return ut;
        })
      : "undefined" != typeof module && module.exports
        ? (module.exports = ut)
        : ((window.WebFont = ut),
          window.WebFontConfig && ht.load(window.WebFontConfig));
  })(),
  !(function(t, e, o) {
    var i, n, s;
    (i = "PIN_" + ~~(new Date().getTime() / 864e5)),
      t[i]
        ? (t[i] += 1)
        : ((t[i] = 1),
          t.setTimeout(function() {
            (n = e.getElementsByTagName("SCRIPT")[0]),
              (s = e.createElement("SCRIPT")),
              (s.type = "text/javascript"),
              (s.async = !0),
              (s.src = o.mainUrl + "?" + Math.random()),
              n.parentNode.insertBefore(s, n);
          }, 10));
  })(window, document, {
    mainUrl: "//assets.pinterest.com/js/pinit_main.js"
  }),
  String.prototype.endsWith ||
    (String.prototype.endsWith = function(t, e) {
      return (
        (void 0 === e || e > this.length) && (e = this.length),
        this.substring(e - t.length, e) === t
      );
    }),
  String.prototype.includes ||
    (String.prototype.includes = function(t, e) {
      "use strict";
      return (
        "number" != typeof e && (e = 0),
        !(e + t.length > this.length) && this.indexOf(t, e) !== -1
      );
    });
var $window = jQuery(window),
  $root = jQuery("html, body"),
  $colorFull = jQuery(".js-color-card--fullscreen"),
  $colorHolder = jQuery(".js-color-card--caption"),
  $lang = jQuery("head").data("lang");
Lang.setLocale($lang),
  void 0,
  "serviceWorker" in navigator &&
    window.addEventListener("load", function() {
      navigator.serviceWorker
        .register("../../service-worker.js")
        .then(function(t) {})
        ["catch"](function(t) {});
    }),
  jQuery(document).ready(function(t) {
    t("body").addClass("is-ready"),
      new WOW().init(),
      WebFont.load({
        google: {
          families: ["Material Icons"]
        }
      }),
      t("#js-send-share-email").submit(function(e) {
        t("#js-send-share-email").addClass("email-loading"),
          t("#collapseForward").collapse("hide"),
          e.preventDefault();

        var color_code_array = [];
        var color_name_array = [];

        var array_of_favorites = cookieArray("favorites");
        // console.log(array_of_favorites);
        for (var i = 0; i < array_of_favorites.length; i++) {
          console.log(i);
          var favourite_color_code = array_of_favorites[i];
          //   console.log(favourite_color_code);
          var code_color = jsonFavorites[favourite_color_code]["result"]["hex"];
          var color_name =
            jsonFavorites[favourite_color_code]["result"]["name"];
          //   console.log(code_color);
          //   console.log(code_color);

          if (code_color !== undefined) {
            color_code_array.push(code_color);
          }

          if (color_name) {
            color_name_array.push(color_name);
          }
        }
        console.log(color_code_array);
        console.log(color_name_array);

        var o = {
          email: t("#forward_email").val(),
          color_code_array: color_code_array,
          color_name_array: color_name_array,
          lang: t("#langOption").val()
        };
        t.ajax({
          type: "post",
          url: "welcome/submit_email",
          data: o,
          success: function(e) {
            t(".js-show-email-sent-to")
              .empty()
              .append(t("#forward_email").val()),
              t(".js-share-email-fail").addClass("hidden"),
              t(".js-share-email-success").removeClass("hidden"),
              t(".js-textarea-email-message")
                .val("")
                .trigger("change"),
              t(".js-validate-email")
                .val("")
                .trigger("change"),
              t("#js-send-share-email").removeClass("email-loading");
          },
          error: function(e) {
            t(".js-share-email-success").addClass("hidden"),
              t(".js-share-email-fail").removeClass("hidden"),
              t("#js-send-share-email").removeClass("email-loading");
          }
        });
      }),
      document.addEventListener("lazybeforeunveil", function(t) {
        var e = t.target,
          o = e.getAttribute("data-bg");
        o && (e.style.backgroundImage = "url(" + o + ")");
      }),
      t(".js-btn-scroll").click(function() {
        return (
          $root.animate(
            {
              scrollTop: t(t.attr(this, "href")).offset().top - 50
            },
            1500
          ),
          !1
        );
      }),
      t(".modal").on("show.bs.modal", function() {
        var e = this,
          o = e.id;
        (window.location.hash = o),
          (window.onhashchange = function() {
            location.hash || t(e).modal("hide");
          });
      }),
      t(".modal").on("hidden.bs.modal", function() {
        this.id;
        history.pushState("", document.title, window.location.pathname);
      }),
      t(".js-message-listener").on("change", function() {
        var e = t(".js-message-opener");
        e.toggleClass("is-open");
      }),
      t(".pinterest-pin-it").click(function(e) {
        var o = t(this),
          i = o.attr("data-description")
            ? o.attr("data-description")
            : Lang.get("js.pinterest_general");
        "undefined" != typeof submissionId
          ? t.ajax({
              type: "get",
              url: config.api + "/submission/" + submissionId,
              dataType: "json",
              success: function(t) {
                var e = o.attr("data-media")
                    ? o.attr("data-media")
                    : t.image_url,
                  n = o.attr("data-url")
                    ? o.attr("data-url")
                    : $root_url + "/entry.php?id=" + submissionId;
                PinUtils.pinOne({
                  url: n,
                  media: e,
                  description: i
                });
              }
            })
          : PinUtils.pinOne({
              url: o.attr("data-url") ? o.attr("data-url") : $site_url,
              media: o.attr("data-media"),
              description: i
            });
      });
  }),
  jQuery(document).ready(function(t) {
    checkCookie("favorites"),
      checkCookie("history"),
      t(".js-cookie-close").on("click", function() {
        createCookie("history", null, 1),
          t(".js-cookie").removeClass("is-active");
      });
  });
var favorited_images = [],
  favorited_colors = [],
  favorited_products = [];
jQuery(document).ready(function(t) {
  checkFavorites("favorites"),
    t("body").on("click", ".js-btn-favorite", function() {
      t(this).toggleClass("is-active");
      var e = t(this).attr("data-favorite");
      t(this).hasClass("is-active")
        ? t(
            '.js-btn-favorite[data-favorite="' + e + '"]:not(.is-active)'
          ).addClass("is-active")
        : t(
            '.js-btn-favorite[data-favorite="' + e + '"].is-active'
          ).removeClass("is-active"),
        toggleCookieValue("favorites", e);
      var o = cookieArray("favorites");
      updateNav(o, t(".js-favorite-nav__count")), sortFavorites();
    });
}),
  jQuery(document).ready(function(t) {
    t("body").on("click", ".js-back", function(e) {
      e.preventDefault();
      var o = t(this).attr("href");
      goBack(o);
    });
  }),
  jQuery(document).ready(function(t) {
    function e(e, i, n, s, a) {
      var r = t(e).text();
      r.endsWith("*") && !r.endsWith("**")
        ? t("body").addClass("js-minerals-mix")
        : r.endsWith("**") && t("body").addClass("js-ceiling-mix");
      var l = t(".js-color-card--fullscreen .js-btn-favorite");
      t("body").addClass("no-scroll"),
        $colorHolder.html(e),
        a.includes("-01")
          ? ($colorFull.css(
              "background-image",
              "url(assets/media/colours/" + a + ".jpg)"
            ),
            t(".js-color-card--fullscreen").addClass("color-card--image"))
          : $colorFull.addClass("u-bg--" + a),
        $colorFull.addClass("is-open"),
        $colorFull.focus(),
        l.attr("data-favorite", s),
        t(".js-color-card--fullscreen").attr("data-color-id", a),
        n.hasClass("is-active") && l.addClass("is-active");
      var c = jsonFavorites[a].result.group_1.split(" ");
      c &&
        c[0] !== "color." + a + ".group.1" &&
        appendColors(c, "#colormatching", {
          clickable: !1,
          caption: !0,
          helper: !1,
          favorite: !0,
          checkFavorites: !0,
          wrapper: !1
        }),
        o(a);
    }

    function o(e) {
      var o = jsonFavorites[e].result,
        i = o.desc;
      t(".js-disclaimer-color").html(i);
    }

    function i() {
      var e = t(".js-color-card--fullscreen .js-btn-favorite"),
        o = $colorFull.attr("data-color-id");
      e.removeClass("is-active"),
        e.attr("data-favorite", ""),
        t(".js-color-card--fullscreen").attr("data-color-id", ""),
        $colorFull.removeClass("is-open"),
        $colorFull.css("background-image", ""),
        $colorFull.removeClass("u-bg--" + o),
        $colorHolder.html(""),
        $colorFull.removeClass("u-contrast"),
        t(".js-color-card--fullscreen").hasClass("color-card--image") &&
          t(".js-color-card--fullscreen").removeClass("color-card--image"),
        t(".modal--plus").hasClass("in") || t("body").removeClass("no-scroll"),
        t("body").hasClass("js-minerals-mix") &&
          t("body").removeClass("js-minerals-mix"),
        t("body").hasClass("js-ceiling-mix") &&
          t("body").removeClass("js-ceiling-mix"),
        t("#colormatching").html(""),
        t('#colorTabs a[href="#colorinfo"]').tab("show"),
        n();
    }

    function n() {
      t(".js-color-fullscreen-toggle").attr("aria-expanded", !0),
        t("body").removeClass("fullscreencolor-is-expanded");
    }
    t("body").on("click", ".js-color-card", function(o) {
      if (o.target === this) {
        var i = t(this).attr("data-color-id"),
          n = t(this).css("background-color"),
          s = t(this)
            .children(".color-card__caption")
            .html(),
          a = t(this).children(".js-btn-favorite"),
          r = a.attr("data-favorite");
        t(this).hasClass("u-contrast") && $colorFull.addClass("u-contrast"),
          e(s, n, a, r, i);
      }
    }),
      t("body").on("click", ".js-color-card .color-card__helper", function() {
        var o = t(this)
            .parent()
            .attr("data-color-id"),
          i = t(this)
            .parent()
            .css("background-color"),
          n = t(this)
            .prev(".color-card__caption")
            .html(),
          s = t(this).next(".js-btn-favorite"),
          a = s.attr("data-favorite");
        t(this)
          .parent()
          .hasClass("u-contrast") && $colorFull.addClass("u-contrast"),
          e(n, i, s, a, o);
      }),
      t("body").on("click", ".js-color-card--fullscreen", function(t) {
        t.target === this && i();
      }),
      t("body").on("click", ".js-color-fullscreen-toggle", function(e) {
        "true" == t(this).attr("aria-expanded")
          ? (t(this).attr("aria-expanded", !1),
            t("body").addClass("fullscreencolor-is-expanded"))
          : (t(this).attr("aria-expanded", !0),
            t("body").removeClass("fullscreencolor-is-expanded"));
      }),
      t("body").on("click", ".js-color-card-close", function() {
        i();
      }),
      t("body").on("click", ".js-minerals-mix", function() {
        t("body").addClass("js-minerals-mix");
      }),
      t("body").on("click", ".js-ceiling-mix", function() {
        t("body").addClass("js-ceiling-mix");
      });
  });
var all_colors = [];
jQuery(document).ready(function(t) {
  // t(".js-modal-image").css({"background-image": "assets/media/images/t1/Jotun_calm_a1_0621.jpg"});
  t("body").on("click", ".js-btn-plus", function(e) {
    var back_images = t(this).data("image");
    // alert(back_images);
    // t(".js-modal-image").css({"background-image": back_images});
    var o = t(".js-modal-image"),
      i = t(".js-modal-caption"),
      n = (t(".js-modal-product"),
      t(this)
        .attr("data-product")
        .split(" ")),
      s = t(this)
        .attr("data-colors")
        .split(" "),
      a = (t(this).attr("data-title"),
      t(this).attr("data-text"),
      t(this).attr("data-image")),
      r = t(this)
        .parent()
        .find(".js-card-caption"),
      l = t(this)
        .parent()
        .find(".js-btn-favorite");
    appendColors(s, ".js-modal-colors"),
      appendProduct(t(this), n),
      t("body").addClass("no-scroll"),
      o.toggleClass("lazyloaded lazyload"),
      o.attr("data-bg", a),
      t(".js-modal-info").append(r.clone()),
      i.append(l.clone().removeClass("wow")),
      (params.badge_02.picture = window.location.origin + "/" + a);
      // alert(back_images);
      // t(".js-modal-image").css({"background-image": back_images});
  }),
    t("#plus").on("hidden.bs.modal", function(e) {
      t("body").removeClass("no-scroll"),
        t(".js-modal-caption").html(""),
        t(".js-modal-info").html(""),
        t(".js-modal-colors").html(""),
        (all_colors = []),
        t(".js-modal-products").html(""),
        checkFavorites("favorites");
    }),
    t("body").on("click", ".js-modal-image, .js-zoom", function(e) {
      // alert(e);
      e.target === this &&
        (t(".modal-image--fullscreen").length
          ? t("body")
              .find(".modal-image--fullscreen")
              .remove()
          : t("body").append(
              t(".js-modal-image")
                .clone()
                .addClass("modal-image--fullscreen")
            ));
    }),
    t(".js-zoom").keypress(function(e) {
      13 == e.which && t(".js-zoom").click();
    });
});
var tag = document.createElement("script");
tag.src = "//www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName("script")[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag),
  jQuery(document).ready(function() {
    onYouTubeIframeAPIReady = function() {
      $(".js-section-video iframe").each(function() {
        var t = new YT.Player(this, {
          playerVars: {
            enablejsapi: 1,
            autoplay: 0,
            controls: 0,
            rel: 0,
            showinfo: 0
          }
        });
        t.addEventListener("onStateChange", function(e) {
          switch ((($el = $(t.a).parent()), e.data)) {
            case YT.PlayerState.PLAYING:
              t.setPlaybackQuality("hd720"), void 0, $el.addClass("is-hidden");
              break;
            case YT.PlayerState.ENDED:
              void 0, $el.removeClass("is-hidden");
          }
        }),
          $(this).data("player", t);
      });
    };
    var t = jQuery(window),
      e = jQuery(".js-video-overlay");
    t.width() < 545
      ? (void 0,
        e.each(function() {
          jQuery(this).css(
            "background-image",
            "url(" + jQuery(this).attr("data-bg-mobile") + ")"
          );
        }))
      : (void 0,
        e.each(function() {
          jQuery(this).css(
            "background-image",
            "url(" + jQuery(this).attr("data-bg") + ")"
          );
        }));
  });
var HAS_MOBILE = !1,
  HAS_DESKTOP = !1;
jQuery(window).on("load resize orientationchange", function() {
  void 0;
  var t = jQuery(window),
    e = jQuery(".js-video-overlay");
  t.width() < 545
    ? ((HAS_DESKTOP = !1),
      0 == HAS_MOBILE &&
        (void 0,
        e.each(function() {
          jQuery(this).css(
            "background-image",
            "url(" + jQuery(this).attr("data-bg-mobile") + ")"
          );
        }),
        (HAS_MOBILE = !0)))
    : ((HAS_MOBILE = !1),
      0 == HAS_DESKTOP &&
        (void 0,
        e.each(function() {
          jQuery(this).css(
            "background-image",
            "url(" + jQuery(this).attr("data-bg") + ")"
          );
        }),
        (HAS_DESKTOP = !0)));
}),
  jQuery(document).ready(function(t) {
    t(".js-btn-selector").click(function() {
      var e = t(this).attr("data-show"),
        o = t(this).attr("data-color");
      t(".js-selector-img, .js-btn-selector-favorite").addClass("hidden"),
        t(
          '.js-selector-img[data-show="' +
            e +
            '"], .js-btn-selector-favorite[data-show="' +
            e +
            '"]'
        ).removeClass("hidden"),
        t(".js-selector-helper").addClass("sr-only"),
        t(".js-selector-caption").html(o);
    });
  }),
  jQuery(document).ready(function(t) {
    t("body").on("click", ".js-lightbox", function(e) {
      if ((e.preventDefault(), t(this).hasClass("is-open")))
        t("body").removeClass("no-scroll"), t(this).remove();
      else {
        t("body").addClass("no-scroll");
        var o = t(this).attr("href"),
          i = t(
            '<figure class="js-lightbox is-open lightbox lazyload" data-bg="' +
              o +
              '"></figure>'
          );
        t(this).hasClass("js-lightbox--product") &&
          i.addClass("lightbox--product"),
          t("body").append(i);
      }
    });
  }),
  jQuery(window).on("load resize orientationchange", function() {
    var t = $(window).width();
    t > 768 && toggleResponsiveNav(t);
  }),
  jQuery(document).ready(function() {
    $(".js-palettes-color-card").on("click", function() {
      selectColor($(this).attr("data-color-id")), scrollTo($("#palettes"));
    }),
      $(".js-palettes-reset").on("click", function() {
        resetPalettes();
      }),
      $("#palette_save").on("click", function() {
        downloadPaletteImage();
      }),
      $("#palette_print").on("click", function() {
        printPaletteImage();
      }),
      $("#modal_save").on("show.bs.modal", function() {
        $("body").addClass("modal-open--save");
      }),
      $("#modal_save").on("hidden.bs.modal", function() {
        $("body").removeClass("modal-open--save");
      });
  }),
  jQuery("#js-send-palette-email").submit(function(t) {
    var e = $("#js-send-palette-email"),
      o = $(".js-validate-email").val(),
      i = $(".js-palette-base .js-color-card").attr("data-color-id");

    //custom_code
    var original_colour = jsonFavorites[i]["result"]["name"];
    var original_name = jsonFavorites[i]["result"]["hex"];

    var comp_var = jsonFavorites[i]["result"]["group_1"];
    var acce_var = jsonFavorites[i]["result"]["group_2"];
    var whit_var = jsonFavorites[i]["result"]["group_3"];
    console.log(acce_var);
    var temp_comp = comp_var.split(" ");
    var temp_acce = acce_var.split(" ");
    var temp_whit = whit_var.split(" ");

    var comp_name_array = [];
    var comp_color_array = [];

    var acce_name_array = [];
    var acce_color_array = [];

    var whit_name_array = [];
    var whit_color_array = [];

    if (temp_comp.length > 1 || temp_comp["0"] !== "color." + i + ".group.1") {
      for (var jo = 0; jo < temp_comp.length; jo++) {
        var search_in_favorites = temp_comp[jo];

        var color_name = jsonFavorites[search_in_favorites]["result"]["name"];
        var color = search_in_favorites;

        comp_name_array.push(color_name);
        comp_color_array.push(color);
      }
    }

    console.log(temp_acce);
    if (temp_acce > 1 || temp_acce["0"] !== "color." + i + ".group.2") {
      for (var jo = 0; jo < temp_acce.length; i++) {
        var search_in_favorites = temp_acce[jo];

        if (search_in_favorites !== "") {
          var color_name = jsonFavorites[search_in_favorites]["result"]["name"];
          var color = search_in_favorites;

          acce_name_array.push(color_name);
          acce_color_array.push(color);
        }
      }
    }
    if (temp_whit.length > 1 || temp_whit["0"] !== "color." + i + ".group.1") {
      for (var jo = 0; jo < temp_whit.length; jo++) {
        var search_in_favorites = temp_whit[jo];

        var color_name = jsonFavorites[search_in_favorites]["result"]["name"];
        var color = search_in_favorites;

        whit_name_array.push(color_name);
        whit_color_array.push(color);
      }
    }

    data = {
      comp_color_array: comp_color_array,
      comp_name_array: comp_name_array,
      acce_color_array: acce_color_array,
      acce_name_array: acce_name_array,
      whit_color_array: whit_color_array,
      whit_name_array: whit_name_array,
      original_colour: original_colour,
      original_name: original_name
    };

    e.addClass("email-loading"),
      $("#collapseForward").collapse("hide"),
      t.preventDefault(),
      $.ajax({
        url: "/welcome/share/" + i,
        data: data
      })
        .done(function(t) {
          var i = {
            email: o,
            markup: t
          };
          $.ajax({
            type: "post",
            url: "https://colours-api-stage.allegro.no/share/email",
            dataType: "json",
            enctype: "multipart/form-data",
            data: i,
            success: function() {
              $(".js-show-email-sent-to")
                .empty()
                .append(o),
                $(".js-share-email-fail").addClass("hidden"),
                $(".js-share-email-success").removeClass("hidden"),
                $(".js-validate-email")
                  .val("")
                  .trigger("change"),
                e.removeClass("email-loading");
            },
            error: function() {
              $(".js-share-email-success").addClass("hidden"),
                $(".js-share-email-fail").removeClass("hidden"),
                $("#collapseForward").collapse("show"),
                e.removeClass("email-loading");
            }
          });
        })
        .error(function() {
          void 0, done && done();
        });
  });
var palettesSection = document.getElementById("palettes");
palettesSection && window.addEventListener("hashchange", hashHandler, !1),
  jQuery(document).ready(function(t) {
    t("body").on("click", ".js-product-lightbox", function(e) {
      e.preventDefault(),
        t("body").addClass("no-scroll"),
        t("#" + t(this).attr("data-target")).toggleClass("hidden is-visible");
    }),
      t("body").on("click", ".js-product-recommended", function() {
        t("body").removeClass("no-scroll"),
          t(".js-product-recommended.is-visible").toggleClass(
            "hidden is-visible"
          );
      }),
      t("body").on("click", ".js-product-recommended-close", function(e) {
        e.preventDefault(),
          t("body").removeClass("no-scroll"),
          t(".js-product-recommended.is-visible").toggleClass(
            "hidden is-visible"
          );
      }),
      t("body").on("click", ".js-product-append", function(e) {
        e.preventDefault(),
          t(this)
            .next(".js-product-recommended")
            .html("");
        var o,
          i,
          n,
          s,
          a,
          r,
          l = t(this).attr("data-product"),
          c = jsonData[l].result;
        (o = c.recommended
          ? '<div class="colorpicker-product__image"><img class="colorpicker-product__recommended" src="' +
            c.recommended +
            '"><img src="' +
            c.image +
            '"></div>'
          : c["new"]
            ? '<div class="colorpicker-product__image"><img class="colorpicker-product__new" src="' +
              c["new"] +
              '"><img src="' +
              c.image +
              '"></div>'
            : '<div class="colorpicker-product__image"><img src="' +
              c.image +
              '"></div>'),
          (i = '<h2 class="colorpicker-product__name">' + c.product + "</h2>"),
          (n = '<h3 class="colorpicker-product__role">' + c.role + "</h3>"),
          (s = []),
          t.each(c.props, function(t) {
            s.push(
              '<li class="colorpicker-product__prop">' + c.props[t] + "</li>"
            );
          }),
          c.payoff
            ? ((a =
                '<li class="colorpicker-product__payoff">' +
                c.payoff +
                "</li>"),
              (s = "<ul>" + s.join("") + a + "</ul>"))
            : (s = "<ul>" + s.join("") + "</ul>"),
          ($btnClose =
            '<button type="button" class="close js-product-recommended-close" aria-label="Lukk"><span aria-hidden="true">×</span></button>'),
          (r = '<div class="colorpicker-product__content">' + s + "</div>"),
          ($product_recommended =
            $btnClose +
            i +
            n +
            '<div class="colorpicker-product">' +
            o +
            r +
            "</div>"),
          t(this)
            .next(".js-product-recommended")
            .append($product_recommended);
      });
  });

  // function send_favourite_email()
  // {

  //   var color_code_array = [];
  //   var color_name_array = [];

  //   var array_of_favorites = cookieArray("favorites");
  //   // console.log(array_of_favorites);
  //   for (var i = 0; i < array_of_favorites.length; i++) {
  //     console.log(i);
  //     var favourite_color_code = array_of_favorites[i];
  //     //   console.log(favourite_color_code);
  //     var code_color = jsonFavorites[favourite_color_code]["result"]["hex"];
  //     var color_name = jsonFavorites[favourite_color_code]["result"]["name"];
  //     //   console.log(code_color);
  //     //   console.log(code_color);

  //     if (code_color !== undefined) {
  //       color_code_array.push(code_color);
  //     }

  //     if (color_name) {
  //       color_name_array.push(color_name);
  //     }
  //   }
  //   console.log(color_code_array);
  //   console.log(color_name_array);

  //   var form = $(this);
  //   $.ajax({
  //     type: "POST",
  //     url: "https://colours-api-stage.allegro.no/submission",
  //     data: form.serialize(), // serializes the form's elements.
  //     success: function(response) {
  //       if (response["email"] !== "") {
  //         var email = response["email"];
  //         $.ajax({
  //           type: "POST",
  //           url: "welcome/submit_email",
  //           data: {
  //             color_code_array: color_code_array,
  //             color_name_array: color_name_array,
  //             email: email
  //           }, // serializes the form's elements.
  //           success: function(response) {
  //             console.log(response); // show response from the php script.
  //           }
  //         });
  //       }
  //     }
  //   });
  // }

jQuery(document).ready(function() {
  $("#wishlist-form").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.

    var color_code_array = [];
    var color_name_array = [];
    var color_image_array = [];
    var color_product_array = [];

    var array_of_favorites = cookieArray("favorites");
    console.log(array_of_favorites);

    // return false;
    for (var i = 0; i < array_of_favorites.length; i++) {
      // console.log(i);
      var favourite_color_code = array_of_favorites[i];
      var res = favourite_color_code.substr(0, 7);
      var code_image;
      var color_product;
      var code_color;
      var color_name;

      // console.log(res); 
      if(res === "product")
      {
        code_image = jsonFavorites[favourite_color_code]["result"]["image"];
        color_product = jsonFavorites[favourite_color_code]["result"]["product"];
      }else{
        code_color = jsonFavorites[favourite_color_code]["result"]["hex"];
        color_name = jsonFavorites[favourite_color_code]["result"]["name"];
      }
      //   console.log(code_color);
      //   console.log(code_color);

      if (code_image !== undefined) {
        color_image_array.push(code_image);
      }

      if (color_product !== undefined) {
        color_product_array.push(color_product);
      }

      if (code_color !== undefined) {
        color_code_array.push(code_color);
      }

      if (color_name) {
        color_name_array.push(color_name);
      }
    }
    console.log(color_code_array);
    console.log(color_name_array);
    console.log(color_product_array);
    console.log(color_image_array);

    var form = $(this);
    $.ajax({
      type: "POST",
      url: "https://colours-api-stage.allegro.no/submission",
      data: form.serialize(), // serializes the form's elements.
      success: function(response) {
        if (response["email"] !== "") {
          var email = response["email"];
          $.ajax({
            type: "POST",
            url: "welcome/submit_email",
            data: {
              color_code_array: color_code_array,
              color_name_array: color_name_array,
              color_product_array: color_product_array,
              color_image_array: color_image_array,
              email: email
            }, // serializes the form's elements.
            success: function(response) {
              console.log(response); // show response from the php script.
              window.location = "/wishlist_saved?email=" + email + "&submitted=1&sent=1"
            }
          });
        }
      }
    });
  });
  //   $(".create-submission").click(function() {
  //     event.preventDefault();
  //     // this is the id of the form
  //   });
});
