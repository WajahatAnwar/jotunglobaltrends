function toggleValidationFeedback(t, e) {
    t && e.val().length > 0 ? (e.removeClass("is-valid"),
        e.parent().removeClass("has-valid has-empty"),
        e.hasClass("is-error") ||
        (e.parent().addClass("has-error"), e.addClass("is-error"))) : e.val().length <= 0 ? (e.parent().removeClass("has-valid has-error"),
        e.removeClass("is-valid is-error")) : (e.parent().removeClass("has-error has-empty"),
        e.removeClass("is-error"),
        e.hasClass("is-valid") ||
        (e.addClass("is-valid"), e.parent().addClass("has-valid")));
}

function resetSlides() {
    var t = $(".owl-item:not(.cloned)");
    t.each(function(t) {
        0 === t &&
            ($(this).addClass("active"), slider.trigger("to.owl.carousel", t)),
            $(this)
            .find(".js-select-item")
            .attr("data-slide-id", t);
    });
}

function handleSlider() {
    function t() {
        slider.removeClass("owl-carousel owl-theme"), slider.owlCarousel("destroy");
    }

    function e() {
        slider.addClass("owl-carousel owl-theme"),
            slider.owlCarousel({
                items: 1,
                nav: !1,
                loop: !0,
                dots: !1,
                stagePadding: 30
            }),
            slider.on("remove.owl.carousel", function(e) {
                var s = $(".owl-item:not(.cloned)");
                s.length <= 1 ? t() : resetSlides();
            });
    }
    slider = $(".drawer__row--images");
    var s = $(".drawer > .container");
    "none" == s.css("max-width") &&
        slider.children().length > 1 &&
        !slider.hasClass("owl-carousel") ? e() : "none" !== s.css("max-width") && slider.hasClass("owl-carousel") && t();
}

function checkBreakpoint() {
    var t = $(".drawer > .container");
    if (
        "none" == t.css("max-width") ||
        "576px" == t.css("max-width") ||
        "720px" == t.css("max-width") ||
        "940px" == t.css("max-width") ||
        "1364px" == t.css("max-width") ||
        "1500px" == t.css("max-width") ||
        "1600px" == t.css("max-width") ||
        "1800px" == t.css("max-width")
    ) {
        var e = t.css("max-width");
        if (e !== last_breakpoint) return (last_breakpoint = e), !0;
    }
    return !1;
}

function checkTextOverflow() {
    checkBreakpoint() &&
        $(".drawer__item--image").each(function() {
            var t = $(this),
                e = t.find(".drawer__text"),
                s = t.find(".drawer__text-inner");
            s.width() + e.find(".drawer__readall").outerWidth() > e.innerWidth() ? (e.css("padding-right", e.find(".drawer__readall").outerWidth()),
                e.addClass("is-overflowing")) : e.hasClass("is-overflowing") && e.removeClass("is-overflowing");
        });
}

function buildFavorites(t, e) {
    var s = !0;
    if ((t && "object" == typeof t && t.images) || t.colors || t.products)
        (all_images = t.images), (all_colors = t.colors), (all_products = t.products), (s = !1),
        t.search ||
        (t.sent && t.email ? ($(".js-alert-email").text(t.email),
                $(".js-email-alert-container").removeClass("hidden"),
                setTimeout(function() {
                    $(".js-email-alert-container").fadeOut("normal", function() {
                        $(this).remove();
                    });
                }, 6e3)) : t.email &&
            ($(".js-success-alert-container").removeClass("hidden"),
                setTimeout(function() {
                    $(".js-success-alert-container").fadeOut("normal", function() {
                        $(this).remove();
                    });
                }, 6e3)));
    else
        for (var i = 0; i < t.length; i++) {
            var n = t[i];
            if (void 0 != jsonFavorites[n]) {
                var n = t[i],
                    o = jsonFavorites[n].type;
                switch (o) {
                    case "image":
                        all_images.push(n);
                        break;
                    case "color":
                        all_colors.push(n);
                        break;
                    case "product":
                        all_products.push(n);
                }
            }
        }
    $(".js-btn-step-2").removeClass("hidden"),
        $(".js-visible-no-favorites").addClass("hidden"),
        $(".js-visible-favorites").removeClass("hidden");
    var a,
        r,
        l,
        h,
        c,
        d = $(".js-favorited-images"),
        p = $(".js-favorited-colors"),
        u = $(".js-favorited-products");
    if ("" != all_images && "" !== all_images[0] && all_images.length > 0) {
        var g =
            "<h4>" +
            Lang.get("js.no_liked_images") +
            "</h4><p>" +
            Lang.get("js.like_favorites") +
            '</p><a href="/" class="btn btn--line"><span class="btn__text">' +
            Lang.get("js.to_global") +
            "</span></a>";
        for (
            d.html(
                '<div class="section section--items js-section-items"><h2>' +
                Lang.get("js.images") +
                '</h2><span class="alert alert-info">' +
                g +
                '</span><div class="drawer__row drawer__row--images"></div></div>'
            ),
            d = $(".js-favorited-images .drawer__row--images"),
            i = 0; i < all_images.length; i++
        ) {
            var m = jsonFavorites[all_images[i]].result,
                f = m.image,
                _ = m.text,
                v = all_images[i];
            (a =
                '<figcaption class="grid-caption js-image-caption sr-only">' +
                _ +
                "</figcaption>"), ($icons =
                '<i class="material-icons material-icons--first">&#xE87E;</i><i class="material-icons material-icons--second">&#xE87D;</i>'),
            s
                ? ($removeBtn =
                    '<button type="button" class="js-btn-favorite js-btn-remove btn--remove" data-favorite="' +
                    v +
                    '" aria-label="' +
                    Lang.get("js.remove_favorite") +
                    '"><i class="material-icons">&#xE5CD;</i></button>') : ($removeBtn = ""), ($figure =
                    '<a href="' +
                    f +
                    '" class="js-lightbox"><figure class="background-image lazyload" data-bg="' +
                    f +
                    '">' +
                    a +
                    "</figure></a>"), ($drawerReadall =
                    '<button type="button" class="drawer__readall js-readall" data-active-text="' +
                    Lang.get("js.read_all_close") +
                    '" data-default-text="' +
                    Lang.get("js.read_all") +
                    '" aria-expanded="false" aria-controls="drawerText' +
                    i +
                    '">' +
                    Lang.get("js.read_all") +
                    "</button>"), ($drawerText =
                    '<div class="drawer__text is-collapsed" id="drawerText' +
                    i +
                    '"><span class="drawer__text-inner">' +
                    _ +
                    "</span>" +
                    $drawerReadall +
                    "</div>"), (h =
                    '<div class="drawer__background">' +
                    $removeBtn +
                    $figure +
                    $drawerText +
                    "</div>"), (c =
                    '<div class="drawer__item drawer__item--image js-select-item" data-type="image" data-slide-id="' +
                    i +
                    '">' +
                    h +
                    "</div>"),
                d.append(c);
        }
    }
    if ("" != all_colors && "" !== all_colors[0] && all_colors.length > 0) {
        p.append(
            "<h2>" +
            Lang.get("js.colors") +
            '</h2><div class="section section--items js-section-items"><div class="drawer__row drawer__row--colors"></div></div>'
        ), (p = $(".js-favorited-colors .drawer__row--colors"));
        var y = [],
            w = [],
            x = [],
            b = [];
        for (i = 0; i < all_colors.length; i++) {
            var C = jsonFavorites[all_colors[i]].result,
                j = (C["class"], C.name),
                z = all_colors[i],
                k = C.theme;
            if (
                (($icons =
                        '<i class="material-icons material-icons--first">&#xE87E;</i><i class="material-icons material-icons--second">&#xE87D;</i>'),
                    s ? ($removeBtn =
                        '<button type="button" class="js-btn-favorite js-btn-remove btn--remove" data-favorite="' +
                        z +
                        '" aria-label="' +
                        Lang.get("js.remove_favorite") +
                        '"><i class="material-icons">&#xE5CD;</i></button>') : ($removeBtn = ""), ($helper =
                        '<div class="color-card__helper"><button class="btn--plus" type="button"><span class="btn__text">+</span> <span class="sr-only">' +
                        Lang.get("js.color_fullscreen") +
                        "</span></button></div>"), (r =
                        '<figure class="color-card js-color-card u-bg--' +
                        z +
                        '" data-color-id="' +
                        z +
                        '">' +
                        $helper +
                        $removeBtn +
                        '<div class="color-card__caption"><p>' +
                        j +
                        "</p></div></figure>"), (c =
                        '<div class="drawer__item drawer__item--color js-select-item" data-type="color">' +
                        r +
                        "</div></div>"),
                    void 0 !== k && null !== k)
            )
                switch (k) {
                    case Lang.get("js.t1"):
                        y.push(c);
                        break;
                    case Lang.get("js.t2"):
                        w.push(c);
                        break;
                    case Lang.get("js.t3"):
                        x.push(c);
                } else b.push(c);
        }
        "" != y && "" !== y[0] && y.length > 0 && p.append(y.join("")),
            "" != w && "" !== w[0] && w.length > 0 && p.append(w.join("")),
            "" != x && "" !== x[0] && x.length > 0 && p.append(x.join("")),
            "" != b && "" !== b[0] && b.length > 0 && p.append(b.join(""));
    }
    if ("" != all_products && "" !== all_products[0] && all_products.length > 0) {
        var T = $(".js-banner-product"),
            E =
            "<h4>" +
            Lang.get("js.no_liked_products") +
            "</h4><p>" +
            Lang.get("js.find_products") +
            '</p><a href="/colorpicker" class="btn btn--line"><span class="btn__text">' +
            Lang.get("js.to_colorpicker") +
            "</span></a>";
        for (
            u.html(
                '<div class="section section--items js-section-items"><h2>' +
                Lang.get("js.products") +
                '</h2><span class="alert alert-info">' +
                E +
                '</span><div class="drawer__row drawer__row--products"></div></div>'
            ),
            u = $(".js-favorited-products .drawer__row--products"),
            i = 0; i < all_products.length; i++
        ) {
            var h,
                D,
                L,
                P,
                S,
                M,
                O = jsonFavorites[all_products[i]].result,
                B = O.image,
                I = O.product,
                W = all_products[i];
            (h = O.recommended ? '<div class="colorpicker-product__image"><img class="colorpicker-product__recommended" src="' +
                O.recommended +
                '"><img src="' +
                O.image +
                '"></div>' : O["new"] ? '<div class="colorpicker-product__image"><img class="colorpicker-product__new" src="' +
                O["new"] +
                '"><img src="' +
                O.image +
                '"></div>' : '<div class="colorpicker-product__image"><img src="' +
                O.image +
                '"></div>'), (D = '<h2 class="colorpicker-product__name">' + O.product + "</h2>"), (L = '<h3 class="colorpicker-product__role">' + O.role + "</h3>"), (P = []),
            $.each(O.props, function(t) {
                    " " != O.props[t] &&
                        P.push(
                            '<li class="colorpicker-product__prop">' + O.props[t] + "</li>"
                        );
                }),
                O.payoff && " " !== O.payoff ? ((S =
                    '<li class="colorpicker-product__payoff">' + O.payoff + "</li>"), (P = "<ul>" + P.join("") + S + "</ul>")) : (P = "<ul>" + P.join("") + "</ul>"), ($btnClose =
                    '<button type="button" class="close js-product-recommended-close" aria-label="Lukk"><span aria-hidden="true">Ã—</span></button>'), (M = '<div class="colorpicker-product__content">' + P + "</div>"), ($product_recommended =
                    '<div class="js-product-recommended colorpicker__lightbox hidden" id="lightbox' +
                    i +
                    '">' +
                    $btnClose +
                    D +
                    L +
                    '<div class="colorpicker-product">' +
                    h +
                    M +
                    "</div></div>"), ($icons =
                    '<i class="material-icons material-icons--first">&#xE87E;</i><i class="material-icons material-icons--second">&#xE87D;</i>'),
                s ? ($removeBtn =
                    '<button type="button" class="js-btn-favorite js-btn-remove btn--remove" data-favorite="' +
                    W +
                    '" aria-label="' +
                    Lang.get("js.remove_favorite") +
                    '"><i class="material-icons">&#xE5CD;</i></button>') : ($removeBtn = ""), ($drawerText =
                    '<div class="drawer__text"><h3 class="product__title">' +
                    I +
                    '</h3><p class="product__tag">' +
                    O.role +
                    "</p></div>"), (l =
                    '<div class="drawer__product">' +
                    $removeBtn +
                    '<a href="' +
                    B +
                    '" class="drawer__product-wrapper js-product-lightbox" data-target="lightbox' +
                    i +
                    '"><img class="drawer__image lazyload" src="" data-src="' +
                    B +
                    '"></a>' +
                    $drawerText +
                    "</div>"), (c =
                    '<div class="drawer__item drawer__item--product js-select-item" data-type="product">' +
                    l +
                    $product_recommended +
                    "</div>"),
                u.append(c);
        }
        u.append(T);
    }
    $(".js-select").attr("aria-busy", !1),
        $(window).on("load resize orientationchange", function() {
            checkTextOverflow(), handleSlider();
        }),
        e && e();
}!(function(t, e, s, i) {
    function n(e, s) {
        (this.settings = null), (this.options = t.extend({}, n.Defaults, s)), (this.$element = t(e)), (this._handlers = {}), (this._plugins = {}), (this._supress = {}), (this._current = null), (this._speed = null), (this._coordinates = []), (this._breakpoint = null), (this._width = null), (this._items = []), (this._clones = []), (this._mergers = []), (this._widths = []), (this._invalidated = {}), (this._pipe = []), (this._drag = {
            time: null,
            target: null,
            pointer: null,
            stage: {
                start: null,
                current: null
            },
            direction: null
        }), (this._states = {
            current: {},
            tags: {
                initializing: ["busy"],
                animating: ["busy"],
                dragging: ["interacting"]
            }
        }),
        t.each(
                ["onResize", "onThrottledResize"],
                t.proxy(function(e, s) {
                    this._handlers[s] = t.proxy(this[s], this);
                }, this)
            ),
            t.each(
                n.Plugins,
                t.proxy(function(t, e) {
                    this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this);
                }, this)
            ),
            t.each(
                n.Workers,
                t.proxy(function(e, s) {
                    this._pipe.push({
                        filter: s.filter,
                        run: t.proxy(s.run, this)
                    });
                }, this)
            ),
            this.setup(),
            this.initialize();
    }
    (n.Defaults = {
        items: 3,
        loop: !1,
        center: !1,
        rewind: !1,
        checkVisibility: !0,
        mouseDrag: !0,
        touchDrag: !0,
        pullDrag: !0,
        freeDrag: !1,
        margin: 0,
        stagePadding: 0,
        merge: !1,
        mergeFit: !0,
        autoWidth: !1,
        startPosition: 0,
        rtl: !1,
        smartSpeed: 250,
        fluidSpeed: !1,
        dragEndSpeed: !1,
        responsive: {},
        responsiveRefreshRate: 200,
        responsiveBaseElement: e,
        fallbackEasing: "swing",
        slideTransition: "",
        info: !1,
        nestedItemSelector: !1,
        itemElement: "div",
        stageElement: "div",
        refreshClass: "owl-refresh",
        loadedClass: "owl-loaded",
        loadingClass: "owl-loading",
        rtlClass: "owl-rtl",
        responsiveClass: "owl-responsive",
        dragClass: "owl-drag",
        itemClass: "owl-item",
        stageClass: "owl-stage",
        stageOuterClass: "owl-stage-outer",
        grabClass: "owl-grab"
    }), (n.Width = {
        Default: "default",
        Inner: "inner",
        Outer: "outer"
    }), (n.Type = {
        Event: "event",
        State: "state"
    }), (n.Plugins = {}), (n.Workers = [{
        filter: ["width", "settings"],
        run: function() {
            this._width = this.$element.width();
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            t.current = this._items && this._items[this.relative(this._current)];
        }
    }, {
        filter: ["items", "settings"],
        run: function() {
            this.$stage.children(".cloned").remove();
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            var e = this.settings.margin || "",
                s = !this.settings.autoWidth,
                i = this.settings.rtl,
                n = {
                    width: "auto",
                    "margin-left": i ? e : "",
                    "margin-right": i ? "" : e
                };
            !s && this.$stage.children().css(n), (t.css = n);
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            var e =
                (this.width() / this.settings.items).toFixed(3) -
                this.settings.margin,
                s = null,
                i = this._items.length,
                n = !this.settings.autoWidth,
                o = [];
            for (t.items = {
                    merge: !1,
                    width: e
                }; i--;)
                (s = this._mergers[i]), (s =
                    (this.settings.mergeFit && Math.min(s, this.settings.items)) ||
                    s), (t.items.merge = s > 1 || t.items.merge), (o[i] = n ? e * s : this._items[i].width());
            this._widths = o;
        }
    }, {
        filter: ["items", "settings"],
        run: function() {
            var e = [],
                s = this._items,
                i = this.settings,
                n = Math.max(2 * i.items, 4),
                o = 2 * Math.ceil(s.length / 2),
                a = i.loop && s.length ? (i.rewind ? n : Math.max(n, o)) : 0,
                r = "",
                l = "";
            for (a /= 2; a > 0;)
                e.push(this.normalize(e.length / 2, !0)), (r += s[e[e.length - 1]][0].outerHTML),
                e.push(this.normalize(s.length - 1 - (e.length - 1) / 2, !0)), (l = s[e[e.length - 1]][0].outerHTML + l), (a -= 1);
            (this._clones = e),
            t(r)
                .addClass("cloned")
                .appendTo(this.$stage),
                t(l)
                .addClass("cloned")
                .prependTo(this.$stage);
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function() {
            for (
                var t = this.settings.rtl ? 1 : -1,
                    e = this._clones.length + this._items.length,
                    s = -1,
                    i = 0,
                    n = 0,
                    o = [];
                ++s < e;

            )
                (i = o[s - 1] || 0), (n = this._widths[this.relative(s)] + this.settings.margin),
                o.push(i + n * t);
            this._coordinates = o;
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function() {
            var t = this.settings.stagePadding,
                e = this._coordinates,
                s = {
                    width: Math.ceil(Math.abs(e[e.length - 1])) + 2 * t,
                    "padding-left": t || "",
                    "padding-right": t || ""
                };
            this.$stage.css(s);
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            var e = this._coordinates.length,
                s = !this.settings.autoWidth,
                i = this.$stage.children();
            if (s && t.items.merge)
                for (; e--;)
                    (t.css.width = this._widths[this.relative(e)]),
                    i.eq(e).css(t.css);
            else s && ((t.css.width = t.items.width), i.css(t.css));
        }
    }, {
        filter: ["items"],
        run: function() {
            this._coordinates.length < 1 && this.$stage.removeAttr("style");
        }
    }, {
        filter: ["width", "items", "settings"],
        run: function(t) {
            (t.current = t.current ? this.$stage.children().index(t.current) : 0), (t.current = Math.max(
                this.minimum(),
                Math.min(this.maximum(), t.current)
            )),
            this.reset(t.current);
        }
    }, {
        filter: ["position"],
        run: function() {
            this.animate(this.coordinates(this._current));
        }
    }, {
        filter: ["width", "position", "items", "settings"],
        run: function() {
            var t,
                e,
                s,
                i,
                n = this.settings.rtl ? 1 : -1,
                o = 2 * this.settings.stagePadding,
                a = this.coordinates(this.current()) + o,
                r = a + this.width() * n,
                l = [];
            for (s = 0, i = this._coordinates.length; s < i; s++)
                (t = this._coordinates[s - 1] || 0), (e = Math.abs(this._coordinates[s]) + o * n), ((this.op(t, "<=", a) && this.op(t, ">", r)) ||
                    (this.op(e, "<", a) && this.op(e, ">", r))) &&
                l.push(s);
            this.$stage.children(".active").removeClass("active"),
                this.$stage
                .children(":eq(" + l.join("), :eq(") + ")")
                .addClass("active"),
                this.$stage.children(".center").removeClass("center"),
                this.settings.center &&
                this.$stage
                .children()
                .eq(this.current())
                .addClass("center");
        }
    }]), (n.prototype.initializeStage = function() {
        (this.$stage = this.$element.find("." + this.settings.stageClass)),
        this.$stage.length ||
            (this.$element.addClass(this.options.loadingClass), (this.$stage = t("<" + this.settings.stageElement + ">", {
                    class: this.settings.stageClass
                }).wrap(t("<div/>", {
                    class: this.settings.stageOuterClass
                }))),
                this.$element.append(this.$stage.parent()));
    }), (n.prototype.initializeItems = function() {
        var e = this.$element.find(".owl-item");
        return e.length ? ((this._items = e.get().map(function(e) {
                return t(e);
            })), (this._mergers = this._items.map(function() {
                return 1;
            })),
            void this.refresh()) : (this.replace(this.$element.children().not(this.$stage.parent())),
            this.isVisible() ? this.refresh() : this.invalidate("width"),
            this.$element
            .removeClass(this.options.loadingClass)
            .addClass(this.options.loadedClass),
            void 0);
    }), (n.prototype.initialize = function() {
        if (
            (this.enter("initializing"),
                this.trigger("initialize"),
                this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl),
                this.settings.autoWidth && !this.is("pre-loading"))
        ) {
            var t, e, s;
            (t = this.$element.find("img")), (e = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : i), (s = this.$element.children(e).width()),
            t.length && s <= 0 && this.preloadAutoWidthImages(t);
        }
        this.initializeStage(),
            this.initializeItems(),
            this.registerEventHandlers(),
            this.leave("initializing"),
            this.trigger("initialized");
    }), (n.prototype.isVisible = function() {
        return !this.settings.checkVisibility || this.$element.is(":visible");
    }), (n.prototype.setup = function() {
        var e = this.viewport(),
            s = this.options.responsive,
            i = -1,
            n = null;
        s
            ? (t.each(s, function(t) {
                    t <= e && t > i && (i = Number(t));
                }), (n = t.extend({}, this.options, s[i])),
                "function" == typeof n.stagePadding &&
                (n.stagePadding = n.stagePadding()),
                delete n.responsive,
                n.responsiveClass &&
                this.$element.attr(
                    "class",
                    this.$element
                    .attr("class")
                    .replace(
                        new RegExp(
                            "(" + this.options.responsiveClass + "-)\\S+\\s",
                            "g"
                        ),
                        "$1" + i
                    )
                )) : (n = t.extend({}, this.options)),
            this.trigger("change", {
                property: {
                    name: "settings",
                    value: n
                }
            }), (this._breakpoint = i), (this.settings = n),
            this.invalidate("settings"),
            this.trigger("changed", {
                property: {
                    name: "settings",
                    value: this.settings
                }
            });
    }), (n.prototype.optionsLogic = function() {
        this.settings.autoWidth &&
            ((this.settings.stagePadding = !1), (this.settings.merge = !1));
    }), (n.prototype.prepare = function(e) {
        var s = this.trigger("prepare", {
            content: e
        });
        return (
            s.data ||
            (s.data = t("<" + this.settings.itemElement + "/>")
                .addClass(this.options.itemClass)
                .append(e)),
            this.trigger("prepared", {
                content: s.data
            }),
            s.data
        );
    }), (n.prototype.update = function() {
        for (
            var e = 0,
                s = this._pipe.length,
                i = t.proxy(function(t) {
                    return this[t];
                }, this._invalidated),
                n = {}; e < s;

        )
            (this._invalidated.all || t.grep(this._pipe[e].filter, i).length > 0) &&
            this._pipe[e].run(n),
            e++;
        (this._invalidated = {}), !this.is("valid") && this.enter("valid");
    }), (n.prototype.width = function(t) {
        switch ((t = t || n.Width.Default)) {
            case n.Width.Inner:
            case n.Width.Outer:
                return this._width;
            default:
                return (
                    this._width - 2 * this.settings.stagePadding + this.settings.margin
                );
        }
    }), (n.prototype.refresh = function() {
        this.enter("refreshing"),
            this.trigger("refresh"),
            this.setup(),
            this.optionsLogic(),
            this.$element.addClass(this.options.refreshClass),
            this.update(),
            this.$element.removeClass(this.options.refreshClass),
            this.leave("refreshing"),
            this.trigger("refreshed");
    }), (n.prototype.onThrottledResize = function() {
        e.clearTimeout(this.resizeTimer), (this.resizeTimer = e.setTimeout(
            this._handlers.onResize,
            this.settings.responsiveRefreshRate
        ));
    }), (n.prototype.onResize = function() {
        return (!!this._items.length &&
            this._width !== this.$element.width() &&
            !!this.isVisible() &&
            (this.enter("resizing"),
                this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"),
                    this.refresh(),
                    this.leave("resizing"),
                    void this.trigger("resized")))
        );
    }), (n.prototype.registerEventHandlers = function() {
        t.support.transition &&
            this.$stage.on(
                t.support.transition.end + ".owl.core",
                t.proxy(this.onTransitionEnd, this)
            ), !1 !== this.settings.responsive &&
            this.on(e, "resize", this._handlers.onThrottledResize),
            this.settings.mouseDrag &&
            (this.$element.addClass(this.options.dragClass),
                this.$stage.on("mousedown.owl.core", t.proxy(this.onDragStart, this)),
                this.$stage.on("dragstart.owl.core selectstart.owl.core", function() {
                    return !1;
                })),
            this.settings.touchDrag &&
            (this.$stage.on(
                    "touchstart.owl.core",
                    t.proxy(this.onDragStart, this)
                ),
                this.$stage.on(
                    "touchcancel.owl.core",
                    t.proxy(this.onDragEnd, this)
                ));
    }), (n.prototype.onDragStart = function(e) {
        var i = null;
        3 !== e.which &&
            (t.support.transform ? ((i = this.$stage
                    .css("transform")
                    .replace(/.*\(|\)| /g, "")
                    .split(",")), (i = {
                    x: i[16 === i.length ? 12 : 4],
                    y: i[16 === i.length ? 13 : 5]
                })) : ((i = this.$stage.position()), (i = {
                    x: this.settings.rtl ? i.left +
                        this.$stage.width() -
                        this.width() +
                        this.settings.margin : i.left,
                    y: i.top
                })),
                this.is("animating") &&
                (t.support.transform ? this.animate(i.x) : this.$stage.stop(),
                    this.invalidate("position")),
                this.$element.toggleClass(
                    this.options.grabClass,
                    "mousedown" === e.type
                ),
                this.speed(0), (this._drag.time = new Date().getTime()), (this._drag.target = t(e.target)), (this._drag.stage.start = i), (this._drag.stage.current = i), (this._drag.pointer = this.pointer(e)),
                t(s).on(
                    "mouseup.owl.core touchend.owl.core",
                    t.proxy(this.onDragEnd, this)
                ),
                t(s).one(
                    "mousemove.owl.core touchmove.owl.core",
                    t.proxy(function(e) {
                        var i = this.difference(this._drag.pointer, this.pointer(e));
                        t(s).on(
                                "mousemove.owl.core touchmove.owl.core",
                                t.proxy(this.onDragMove, this)
                            ), (Math.abs(i.x) < Math.abs(i.y) && this.is("valid")) ||
                            (e.preventDefault(),
                                this.enter("dragging"),
                                this.trigger("drag"));
                    }, this)
                ));
    }), (n.prototype.onDragMove = function(t) {
        var e = null,
            s = null,
            i = null,
            n = this.difference(this._drag.pointer, this.pointer(t)),
            o = this.difference(this._drag.stage.start, n);
        this.is("dragging") &&
            (t.preventDefault(),
                this.settings.loop ? ((e = this.coordinates(this.minimum())), (s = this.coordinates(this.maximum() + 1) - e), (o.x = ((((o.x - e) % s) + s) % s) + e)) : ((e = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum())), (s = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum())), (i = this.settings.pullDrag ? (-1 * n.x) / 5 : 0), (o.x = Math.max(Math.min(o.x, e + i), s + i))), (this._drag.stage.current = o),
                this.animate(o.x));
    }), (n.prototype.onDragEnd = function(e) {
        var i = this.difference(this._drag.pointer, this.pointer(e)),
            n = this._drag.stage.current,
            o = (i.x > 0) ^ this.settings.rtl ? "left" : "right";
        t(s).off(".owl.core"),
            this.$element.removeClass(this.options.grabClass), ((0 !== i.x && this.is("dragging")) || !this.is("valid")) &&
            (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed),
                this.current(this.closest(n.x, 0 !== i.x ? o : this._drag.direction)),
                this.invalidate("position"),
                this.update(), (this._drag.direction = o), (Math.abs(i.x) > 3 || new Date().getTime() - this._drag.time > 300) &&
                this._drag.target.one("click.owl.core", function() {
                    return !1;
                })),
            this.is("dragging") &&
            (this.leave("dragging"), this.trigger("dragged"));
    }), (n.prototype.closest = function(e, s) {
        var n = -1,
            o = 30,
            a = this.width(),
            r = this.coordinates();
        return (
            this.settings.freeDrag ||
            t.each(
                r,
                t.proxy(function(t, l) {
                    return (
                        "left" === s && e > l - o && e < l + o ? (n = t) : "right" === s && e > l - a - o && e < l - a + o ? (n = t + 1) : this.op(e, "<", l) &&
                        this.op(e, ">", r[t + 1] !== i ? r[t + 1] : l - a) &&
                        (n = "left" === s ? t + 1 : t), -1 === n
                    );
                }, this)
            ),
            this.settings.loop ||
            (this.op(e, ">", r[this.minimum()]) ? (n = e = this.minimum()) : this.op(e, "<", r[this.maximum()]) && (n = e = this.maximum())),
            n
        );
    }), (n.prototype.animate = function(e) {
        var s = this.speed() > 0;
        this.is("animating") && this.onTransitionEnd(),
            s && (this.enter("animating"), this.trigger("translate")),
            t.support.transform3d && t.support.transition ? this.$stage.css({
                transform: "translate3d(" + e + "px,0px,0px)",
                transition: this.speed() / 1e3 +
                    "s" +
                    (this.settings.slideTransition ? " " + this.settings.slideTransition : "")
            }) : s ? this.$stage.animate({
                    left: e + "px"
                },
                this.speed(),
                this.settings.fallbackEasing,
                t.proxy(this.onTransitionEnd, this)
            ) : this.$stage.css({
                left: e + "px"
            });
    }), (n.prototype.is = function(t) {
        return this._states.current[t] && this._states.current[t] > 0;
    }), (n.prototype.current = function(t) {
        if (t === i) return this._current;
        if (0 === this._items.length) return i;
        if (((t = this.normalize(t)), this._current !== t)) {
            var e = this.trigger("change", {
                property: {
                    name: "position",
                    value: t
                }
            });
            e.data !== i && (t = this.normalize(e.data)), (this._current = t),
                this.invalidate("position"),
                this.trigger("changed", {
                    property: {
                        name: "position",
                        value: this._current
                    }
                });
        }
        return this._current;
    }), (n.prototype.invalidate = function(e) {
        return (
            "string" === t.type(e) &&
            ((this._invalidated[e] = !0),
                this.is("valid") && this.leave("valid")),
            t.map(this._invalidated, function(t, e) {
                return e;
            })
        );
    }), (n.prototype.reset = function(t) {
        (t = this.normalize(t)) !== i &&
            ((this._speed = 0), (this._current = t),
                this.suppress(["translate", "translated"]),
                this.animate(this.coordinates(t)),
                this.release(["translate", "translated"]));
    }), (n.prototype.normalize = function(t, e) {
        var s = this._items.length,
            n = e ? 0 : this._clones.length;
        return (!this.isNumeric(t) || s < 1 ? (t = i) : (t < 0 || t >= s + n) &&
            (t = ((((t - n / 2) % s) + s) % s) + n / 2),
            t
        );
    }), (n.prototype.relative = function(t) {
        return (t -= this._clones.length / 2), this.normalize(t, !0);
    }), (n.prototype.maximum = function(t) {
        var e,
            s,
            i,
            n = this.settings,
            o = this._coordinates.length;
        if (n.loop) o = this._clones.length / 2 + this._items.length - 1;
        else if (n.autoWidth || n.merge) {
            if ((e = this._items.length))
                for (
                    s = this._items[--e].width(), i = this.$element.width(); e-- && !((s += this._items[e].width() + this.settings.margin) > i);

                );
            o = e + 1;
        } else
            o = n.center ? this._items.length - 1 : this._items.length - n.items;
        return t && (o -= this._clones.length / 2), Math.max(o, 0);
    }), (n.prototype.minimum = function(t) {
        return t ? 0 : this._clones.length / 2;
    }), (n.prototype.items = function(t) {
        return t === i ? this._items.slice() : ((t = this.normalize(t, !0)), this._items[t]);
    }), (n.prototype.mergers = function(t) {
        return t === i ? this._mergers.slice() : ((t = this.normalize(t, !0)), this._mergers[t]);
    }), (n.prototype.clones = function(e) {
        var s = this._clones.length / 2,
            n = s + this._items.length,
            o = function(t) {
                return t % 2 == 0 ? n + t / 2 : s - (t + 1) / 2;
            };
        return e === i ? t.map(this._clones, function(t, e) {
            return o(e);
        }) : t.map(this._clones, function(t, s) {
            return t === e ? o(s) : null;
        });
    }), (n.prototype.speed = function(t) {
        return t !== i && (this._speed = t), this._speed;
    }), (n.prototype.coordinates = function(e) {
        var s,
            n = 1,
            o = e - 1;
        return e === i ? t.map(
            this._coordinates,
            t.proxy(function(t, e) {
                return this.coordinates(e);
            }, this)
        ) : (this.settings.center ? (this.settings.rtl && ((n = -1), (o = e + 1)), (s = this._coordinates[e]), (s += ((this.width() - s + (this._coordinates[o] || 0)) / 2) * n)) : (s = this._coordinates[o] || 0), (s = Math.ceil(s)));
    }), (n.prototype.duration = function(t, e, s) {
        return 0 === s ? 0 : Math.min(Math.max(Math.abs(e - t), 1), 6) *
            Math.abs(s || this.settings.smartSpeed);
    }), (n.prototype.to = function(t, e) {
        var s = this.current(),
            i = null,
            n = t - this.relative(s),
            o = (n > 0) - (n < 0),
            a = this._items.length,
            r = this.minimum(),
            l = this.maximum();
        this.settings.loop ? (!this.settings.rewind && Math.abs(n) > a / 2 && (n += -1 * o * a), (t = s + n), (i = ((((t - r) % a) + a) % a) + r) !== t &&
                i - n <= l &&
                i - n > 0 &&
                ((s = i - n), (t = i), this.reset(s))) : this.settings.rewind ? ((l += 1), (t = ((t % l) + l) % l)) : (t = Math.max(r, Math.min(l, t))),
            this.speed(this.duration(s, t, e)),
            this.current(t),
            this.isVisible() && this.update();
    }), (n.prototype.next = function(t) {
        (t = t || !1), this.to(this.relative(this.current()) + 1, t);
    }), (n.prototype.prev = function(t) {
        (t = t || !1), this.to(this.relative(this.current()) - 1, t);
    }), (n.prototype.onTransitionEnd = function(t) {
        return (
            (t === i ||
                (t.stopPropagation(), (t.target || t.srcElement || t.originalTarget) ===
                    this.$stage.get(0))) &&
            (this.leave("animating"), void this.trigger("translated"))
        );
    }), (n.prototype.viewport = function() {
        var i;
        return (
            this.options.responsiveBaseElement !== e ? (i = t(this.options.responsiveBaseElement).width()) : e.innerWidth ? (i = e.innerWidth) : s.documentElement && s.documentElement.clientWidth ? (i = s.documentElement.clientWidth) : void 0,
            i
        );
    }), (n.prototype.replace = function(e) {
        this.$stage.empty(), (this._items = []),
            e && (e = e instanceof jQuery ? e : t(e)),
            this.settings.nestedItemSelector &&
            (e = e.find("." + this.settings.nestedItemSelector)),
            e
            .filter(function() {
                return 1 === this.nodeType;
            })
            .each(
                t.proxy(function(t, e) {
                    (e = this.prepare(e)),
                    this.$stage.append(e),
                        this._items.push(e),
                        this._mergers.push(
                            1 *
                            e
                            .find("[data-merge]")
                            .addBack("[data-merge]")
                            .attr("data-merge") || 1
                        );
                }, this)
            ),
            this.reset(
                this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0
            ),
            this.invalidate("items");
    }), (n.prototype.add = function(e, s) {
        var n = this.relative(this._current);
        (s = s === i ? this._items.length : this.normalize(s, !0)), (e = e instanceof jQuery ? e : t(e)),
        this.trigger("add", {
                content: e,
                position: s
            }), (e = this.prepare(e)),
            0 === this._items.length || s === this._items.length ? (0 === this._items.length && this.$stage.append(e),
                0 !== this._items.length && this._items[s - 1].after(e),
                this._items.push(e),
                this._mergers.push(
                    1 *
                    e
                    .find("[data-merge]")
                    .addBack("[data-merge]")
                    .attr("data-merge") || 1
                )) : (this._items[s].before(e),
                this._items.splice(s, 0, e),
                this._mergers.splice(
                    s,
                    0,
                    1 *
                    e
                    .find("[data-merge]")
                    .addBack("[data-merge]")
                    .attr("data-merge") || 1
                )),
            this._items[n] && this.reset(this._items[n].index()),
            this.invalidate("items"),
            this.trigger("added", {
                content: e,
                position: s
            });
    }), (n.prototype.remove = function(t) {
        (t = this.normalize(t, !0)) !== i &&
            (this.trigger("remove", {
                    content: this._items[t],
                    position: t
                }),
                this._items[t].remove(),
                this._items.splice(t, 1),
                this._mergers.splice(t, 1),
                this.invalidate("items"),
                this.trigger("removed", {
                    content: null,
                    position: t
                }));
    }), (n.prototype.preloadAutoWidthImages = function(e) {
        e.each(
            t.proxy(function(e, s) {
                this.enter("pre-loading"), (s = t(s)),
                    t(new Image())
                    .one(
                        "load",
                        t.proxy(function(t) {
                            s.attr("src", t.target.src),
                                s.css("opacity", 1),
                                this.leave("pre-loading"), !this.is("pre-loading") &&
                                !this.is("initializing") &&
                                this.refresh();
                        }, this)
                    )
                    .attr(
                        "src",
                        s.attr("src") || s.attr("data-src") || s.attr("data-src-retina")
                    );
            }, this)
        );
    }), (n.prototype.destroy = function() {
        this.$element.off(".owl.core"),
            this.$stage.off(".owl.core"),
            t(s).off(".owl.core"), !1 !== this.settings.responsive &&
            (e.clearTimeout(this.resizeTimer),
                this.off(e, "resize", this._handlers.onThrottledResize));
        for (var i in this._plugins) this._plugins[i].destroy();
        this.$stage.children(".cloned").remove(),
            this.$stage.unwrap(),
            this.$stage
            .children()
            .contents()
            .unwrap(),
            this.$stage.children().unwrap(),
            this.$stage.remove(),
            this.$element
            .removeClass(this.options.refreshClass)
            .removeClass(this.options.loadingClass)
            .removeClass(this.options.loadedClass)
            .removeClass(this.options.rtlClass)
            .removeClass(this.options.dragClass)
            .removeClass(this.options.grabClass)
            .attr(
                "class",
                this.$element
                .attr("class")
                .replace(
                    new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"),
                    ""
                )
            )
            .removeData("owl.carousel");
    }), (n.prototype.op = function(t, e, s) {
        var i = this.settings.rtl;
        switch (e) {
            case "<":
                return i ? t > s : t < s;
            case ">":
                return i ? t < s : t > s;
            case ">=":
                return i ? t <= s : t >= s;
            case "<=":
                return i ? t >= s : t <= s;
        }
    }), (n.prototype.on = function(t, e, s, i) {
        t.addEventListener ? t.addEventListener(e, s, i) : t.attachEvent && t.attachEvent("on" + e, s);
    }), (n.prototype.off = function(t, e, s, i) {
        t.removeEventListener ? t.removeEventListener(e, s, i) : t.detachEvent && t.detachEvent("on" + e, s);
    }), (n.prototype.trigger = function(e, s, i, o, a) {
        var r = {
                item: {
                    count: this._items.length,
                    index: this.current()
                }
            },
            l = t.camelCase(
                t
                .grep(["on", e, i], function(t) {
                    return t;
                })
                .join("-")
                .toLowerCase()
            ),
            h = t.Event(
                [e, "owl", i || "carousel"].join(".").toLowerCase(),
                t.extend({
                    relatedTarget: this
                }, r, s)
            );
        return (
            this._supress[e] ||
            (t.each(this._plugins, function(t, e) {
                    e.onTrigger && e.onTrigger(h);
                }),
                this.register({
                    type: n.Type.Event,
                    name: e
                }),
                this.$element.trigger(h),
                this.settings &&
                "function" == typeof this.settings[l] &&
                this.settings[l].call(this, h)),
            h
        );
    }), (n.prototype.enter = function(e) {
        t.each(
            [e].concat(this._states.tags[e] || []),
            t.proxy(function(t, e) {
                this._states.current[e] === i && (this._states.current[e] = 0),
                    this._states.current[e]++;
            }, this)
        );
    }), (n.prototype.leave = function(e) {
        t.each(
            [e].concat(this._states.tags[e] || []),
            t.proxy(function(t, e) {
                this._states.current[e]--;
            }, this)
        );
    }), (n.prototype.register = function(e) {
        if (e.type === n.Type.Event) {
            if (
                (t.event.special[e.name] || (t.event.special[e.name] = {}), !t.event.special[e.name].owl)
            ) {
                var s = t.event.special[e.name]._default;
                (t.event.special[e.name]._default = function(t) {
                    return !s ||
                        !s.apply ||
                        (t.namespace && -1 !== t.namespace.indexOf("owl")) ? t.namespace && t.namespace.indexOf("owl") > -1 : s.apply(this, arguments);
                }), (t.event.special[e.name].owl = !0);
            }
        } else
            e.type === n.Type.State &&
            (this._states.tags[e.name] ? (this._states.tags[e.name] = this._states.tags[e.name].concat(
                e.tags
            )) : (this._states.tags[e.name] = e.tags), (this._states.tags[e.name] = t.grep(
                this._states.tags[e.name],
                t.proxy(function(s, i) {
                    return t.inArray(s, this._states.tags[e.name]) === i;
                }, this)
            )));
    }), (n.prototype.suppress = function(e) {
        t.each(
            e,
            t.proxy(function(t, e) {
                this._supress[e] = !0;
            }, this)
        );
    }), (n.prototype.release = function(e) {
        t.each(
            e,
            t.proxy(function(t, e) {
                delete this._supress[e];
            }, this)
        );
    }), (n.prototype.pointer = function(t) {
        var s = {
            x: null,
            y: null
        };
        return (
            (t = t.originalEvent || t || e.event), (t =
                t.touches && t.touches.length ? t.touches[0] : t.changedTouches && t.changedTouches.length ? t.changedTouches[0] : t),
            t.pageX ? ((s.x = t.pageX), (s.y = t.pageY)) : ((s.x = t.clientX), (s.y = t.clientY)),
            s
        );
    }), (n.prototype.isNumeric = function(t) {
        return !isNaN(parseFloat(t));
    }), (n.prototype.difference = function(t, e) {
        return {
            x: t.x - e.x,
            y: t.y - e.y
        };
    }), (t.fn.owlCarousel = function(e) {
        var s = Array.prototype.slice.call(arguments, 1);
        return this.each(function() {
            var i = t(this),
                o = i.data("owl.carousel");
            o ||
                ((o = new n(this, "object" == typeof e && e)),
                    i.data("owl.carousel", o),
                    t.each(
                        [
                            "next",
                            "prev",
                            "to",
                            "destroy",
                            "refresh",
                            "replace",
                            "add",
                            "remove"
                        ],
                        function(e, s) {
                            o.register({
                                    type: n.Type.Event,
                                    name: s
                                }),
                                o.$element.on(
                                    s + ".owl.carousel.core",
                                    t.proxy(function(t) {
                                        t.namespace &&
                                            t.relatedTarget !== this &&
                                            (this.suppress([s]),
                                                o[s].apply(this, [].slice.call(arguments, 1)),
                                                this.release([s]));
                                    }, o)
                                );
                        }
                    )),
                "string" == typeof e && "_" !== e.charAt(0) && o[e].apply(o, s);
        });
    }), (t.fn.owlCarousel.Constructor = n);
})(window.Zepto || window.jQuery, window, document), (function(t, e, s, i) {
    var n = function(e) {
        (this._core = e), (this._interval = null), (this._visible = null), (this._handlers = {
            "initialized.owl.carousel": t.proxy(function(t) {
                t.namespace && this._core.settings.autoRefresh && this.watch();
            }, this)
        }), (this._core.options = t.extend({}, n.Defaults, this._core.options)),
        this._core.$element.on(this._handlers);
    };
    (n.Defaults = {
        autoRefresh: !0,
        autoRefreshInterval: 500
    }), (n.prototype.watch = function() {
        this._interval ||
            ((this._visible = this._core.isVisible()), (this._interval = e.setInterval(
                t.proxy(this.refresh, this),
                this._core.settings.autoRefreshInterval
            )));
    }), (n.prototype.refresh = function() {
        this._core.isVisible() !== this._visible &&
            ((this._visible = !this._visible),
                this._core.$element.toggleClass("owl-hidden", !this._visible),
                this._visible &&
                this._core.invalidate("width") &&
                this._core.refresh());
    }), (n.prototype.destroy = function() {
        var t, s;
        e.clearInterval(this._interval);
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (s in Object.getOwnPropertyNames(this))
            "function" != typeof this[s] && (this[s] = null);
    }), (t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = n);
})(window.Zepto || window.jQuery, window, document), (function(t, e, s, i) {
    var n = function(e) {
        (this._core = e), (this._loaded = []), (this._handlers = {
            "initialized.owl.carousel change.owl.carousel resized.owl.carousel": t.proxy(
                function(e) {
                    if (
                        e.namespace &&
                        this._core.settings &&
                        this._core.settings.lazyLoad &&
                        ((e.property && "position" == e.property.name) ||
                            "initialized" == e.type)
                    ) {
                        var s = this._core.settings,
                            n = (s.center && Math.ceil(s.items / 2)) || s.items,
                            o = (s.center && -1 * n) || 0,
                            a =
                            (e.property && e.property.value !== i ? e.property.value : this._core.current()) + o,
                            r = this._core.clones().length,
                            l = t.proxy(function(t, e) {
                                this.load(e);
                            }, this);
                        for (
                            s.lazyLoadEager > 0 &&
                            ((n += s.lazyLoadEager),
                                s.loop && ((a -= s.lazyLoadEager), n++)); o++ < n;

                        )
                            this.load(r / 2 + this._core.relative(a)),
                            r && t.each(this._core.clones(this._core.relative(a)), l),
                            a++;
                    }
                },
                this
            )
        }), (this._core.options = t.extend({}, n.Defaults, this._core.options)),
        this._core.$element.on(this._handlers);
    };
    (n.Defaults = {
        lazyLoad: !1,
        lazyLoadEager: 0
    }), (n.prototype.load = function(s) {
        var i = this._core.$stage.children().eq(s),
            n = i && i.find(".owl-lazy");
        !n ||
            t.inArray(i.get(0), this._loaded) > -1 ||
            (n.each(
                    t.proxy(function(s, i) {
                        var n,
                            o = t(i),
                            a =
                            (e.devicePixelRatio > 1 && o.attr("data-src-retina")) ||
                            o.attr("data-src") ||
                            o.attr("data-srcset");
                        this._core.trigger("load", {
                                element: o,
                                url: a
                            }, "lazy"),
                            o.is("img") ? o
                            .one(
                                "load.owl.lazy",
                                t.proxy(function() {
                                    o.css("opacity", 1),
                                        this._core.trigger(
                                            "loaded", {
                                                element: o,
                                                url: a
                                            },
                                            "lazy"
                                        );
                                }, this)
                            )
                            .attr("src", a) : o.is("source") ? o
                            .one(
                                "load.owl.lazy",
                                t.proxy(function() {
                                    this._core.trigger(
                                        "loaded", {
                                            element: o,
                                            url: a
                                        },
                                        "lazy"
                                    );
                                }, this)
                            )
                            .attr("srcset", a) : ((n = new Image()), (n.onload = t.proxy(function() {
                                o.css({
                                        "background-image": 'url("' + a + '")',
                                        opacity: "1"
                                    }),
                                    this._core.trigger(
                                        "loaded", {
                                            element: o,
                                            url: a
                                        },
                                        "lazy"
                                    );
                            }, this)), (n.src = a));
                    }, this)
                ),
                this._loaded.push(i.get(0)));
    }), (n.prototype.destroy = function() {
        var t, e;
        for (t in this.handlers) this._core.$element.off(t, this.handlers[t]);
        for (e in Object.getOwnPropertyNames(this))
            "function" != typeof this[e] && (this[e] = null);
    }), (t.fn.owlCarousel.Constructor.Plugins.Lazy = n);
})(window.Zepto || window.jQuery, window, document), (function(t, e, s, i) {
    var n = function(s) {
        (this._core = s), (this._previousHeight = null), (this._handlers = {
            "initialized.owl.carousel refreshed.owl.carousel": t.proxy(function(
                    t
                ) {
                    t.namespace && this._core.settings.autoHeight && this.update();
                },
                this),
            "changed.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    this._core.settings.autoHeight &&
                    "position" === t.property.name &&
                    this.update();
            }, this),
            "loaded.owl.lazy": t.proxy(function(t) {
                t.namespace &&
                    this._core.settings.autoHeight &&
                    t.element.closest("." + this._core.settings.itemClass).index() ===
                    this._core.current() &&
                    this.update();
            }, this)
        }), (this._core.options = t.extend({}, n.Defaults, this._core.options)),
        this._core.$element.on(this._handlers), (this._intervalId = null);
        var i = this;
        t(e).on("load", function() {
                i._core.settings.autoHeight && i.update();
            }),
            t(e).resize(function() {
                i._core.settings.autoHeight &&
                    (null != i._intervalId && clearTimeout(i._intervalId), (i._intervalId = setTimeout(function() {
                        i.update();
                    }, 250)));
            });
    };
    (n.Defaults = {
        autoHeight: !1,
        autoHeightClass: "owl-height"
    }), (n.prototype.update = function() {
        var e = this._core._current,
            s = e + this._core.settings.items,
            i = this._core.settings.lazyLoad,
            n = this._core.$stage
            .children()
            .toArray()
            .slice(e, s),
            o = [],
            a = 0;
        t.each(n, function(e, s) {
                o.push(t(s).height());
            }), (a = Math.max.apply(null, o)),
            a <= 1 && i && this._previousHeight && (a = this._previousHeight), (this._previousHeight = a),
            this._core.$stage
            .parent()
            .height(a)
            .addClass(this._core.settings.autoHeightClass);
    }), (n.prototype.destroy = function() {
        var t, e;
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this))
            "function" != typeof this[e] && (this[e] = null);
    }), (t.fn.owlCarousel.Constructor.Plugins.AutoHeight = n);
})(window.Zepto || window.jQuery, window, document), (function(t, e, s, i) {
    var n = function(e) {
        (this._core = e), (this._videos = {}), (this._playing = null), (this._handlers = {
            "initialized.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    this._core.register({
                        type: "state",
                        name: "playing",
                        tags: ["interacting"]
                    });
            }, this),
            "resize.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    this._core.settings.video &&
                    this.isInFullScreen() &&
                    t.preventDefault();
            }, this),
            "refreshed.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    this._core.is("resizing") &&
                    this._core.$stage.find(".cloned .owl-video-frame").remove();
            }, this),
            "changed.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    "position" === t.property.name &&
                    this._playing &&
                    this.stop();
            }, this),
            "prepared.owl.carousel": t.proxy(function(e) {
                if (e.namespace) {
                    var s = t(e.content).find(".owl-video");
                    s.length &&
                        (s.css("display", "none"), this.fetch(s, t(e.content)));
                }
            }, this)
        }), (this._core.options = t.extend({}, n.Defaults, this._core.options)),
        this._core.$element.on(this._handlers),
            this._core.$element.on(
                "click.owl.video",
                ".owl-video-play-icon",
                t.proxy(function(t) {
                    this.play(t);
                }, this)
            );
    };
    (n.Defaults = {
        video: !1,
        videoHeight: !1,
        videoWidth: !1
    }), (n.prototype.fetch = function(t, e) {
        var s = (function() {
                return t.attr("data-vimeo-id") ? "vimeo" : t.attr("data-vzaar-id") ? "vzaar" : "youtube";
            })(),
            i =
            t.attr("data-vimeo-id") ||
            t.attr("data-youtube-id") ||
            t.attr("data-vzaar-id"),
            n = t.attr("data-width") || this._core.settings.videoWidth,
            o = t.attr("data-height") || this._core.settings.videoHeight,
            a = t.attr("href");
        if (!a) throw new Error("Missing video URL.");
        if (
            ((i = a.match(
                    /(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com|be\-nocookie\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/
                )),
                i[3].indexOf("youtu") > -1)
        )
            s = "youtube";
        else if (i[3].indexOf("vimeo") > -1) s = "vimeo";
        else {
            if (!(i[3].indexOf("vzaar") > -1))
                throw new Error("Video URL not supported.");
            s = "vzaar";
        }
        (i = i[6]), (this._videos[a] = {
            type: s,
            id: i,
            width: n,
            height: o
        }),
        e.attr("data-video", a),
            this.thumbnail(t, this._videos[a]);
    }), (n.prototype.thumbnail = function(e, s) {
        var i,
            n,
            o,
            a =
            s.width && s.height ? "width:" + s.width + "px;height:" + s.height + "px;" : "",
            r = e.find("img"),
            l = "src",
            h = "",
            c = this._core.settings,
            d = function(s) {
                (n = '<div class="owl-video-play-icon"></div>'), (i = c.lazyLoad ? t("<div/>", {
                    class: "owl-video-tn " + h,
                    srcType: s
                }) : t("<div/>", {
                    class: "owl-video-tn",
                    style: "opacity:1;background-image:url(" + s + ")"
                })),
                e.after(i),
                    e.after(n);
            };
        return (
            e.wrap(t("<div/>", {
                class: "owl-video-wrapper",
                style: a
            })),
            this._core.settings.lazyLoad && ((l = "data-src"), (h = "owl-lazy")),
            r.length ? (d(r.attr(l)), r.remove(), !1) : void("youtube" === s.type ? ((o = "//img.youtube.com/vi/" + s.id + "/hqdefault.jpg"),
                    d(o)) : "vimeo" === s.type ? t.ajax({
                    type: "GET",
                    url: "//vimeo.com/api/v2/video/" + s.id + ".json",
                    jsonp: "callback",
                    dataType: "jsonp",
                    success: function(t) {
                        (o = t[0].thumbnail_large), d(o);
                    }
                }) : "vzaar" === s.type &&
                t.ajax({
                    type: "GET",
                    url: "//vzaar.com/api/videos/" + s.id + ".json",
                    jsonp: "callback",
                    dataType: "jsonp",
                    success: function(t) {
                        (o = t.framegrab_url), d(o);
                    }
                }))
        );
    }), (n.prototype.stop = function() {
        this._core.trigger("stop", null, "video"),
            this._playing.find(".owl-video-frame").remove(),
            this._playing.removeClass("owl-video-playing"), (this._playing = null),
            this._core.leave("playing"),
            this._core.trigger("stopped", null, "video");
    }), (n.prototype.play = function(e) {
        var s,
            i = t(e.target),
            n = i.closest("." + this._core.settings.itemClass),
            o = this._videos[n.attr("data-video")],
            a = o.width || "100%",
            r = o.height || this._core.$stage.height();
        this._playing ||
            (this._core.enter("playing"),
                this._core.trigger("play", null, "video"), (n = this._core.items(this._core.relative(n.index()))),
                this._core.reset(n.index()), (s = t(
                    '<iframe frameborder="0" allowfullscreen mozallowfullscreen webkitAllowFullScreen ></iframe>'
                )),
                s.attr("height", r),
                s.attr("width", a),
                "youtube" === o.type ? s.attr(
                    "src",
                    "//www.youtube.com/embed/" +
                    o.id +
                    "?autoplay=1&rel=0&v=" +
                    o.id
                ) : "vimeo" === o.type ? s.attr(
                    "src",
                    "//player.vimeo.com/video/" + o.id + "?autoplay=1"
                ) : "vzaar" === o.type &&
                s.attr(
                    "src",
                    "//view.vzaar.com/" + o.id + "/player?autoplay=true"
                ),
                t(s)
                .wrap('<div class="owl-video-frame" />')
                .insertAfter(n.find(".owl-video")), (this._playing = n.addClass("owl-video-playing")));
    }), (n.prototype.isInFullScreen = function() {
        var e =
            s.fullscreenElement ||
            s.mozFullScreenElement ||
            s.webkitFullscreenElement;
        return (
            e &&
            t(e)
            .parent()
            .hasClass("owl-video-frame")
        );
    }), (n.prototype.destroy = function() {
        var t, e;
        this._core.$element.off("click.owl.video");
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this))
            "function" != typeof this[e] && (this[e] = null);
    }), (t.fn.owlCarousel.Constructor.Plugins.Video = n);
})(window.Zepto || window.jQuery, window, document), (function(t, e, s, i) {
    var n = function(e) {
        (this.core = e), (this.core.options = t.extend({}, n.Defaults, this.core.options)), (this.swapping = !0), (this.previous = i), (this.next = i), (this.handlers = {
            "change.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    "position" == t.property.name &&
                    ((this.previous = this.core.current()), (this.next = t.property.value));
            }, this),
            "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": t.proxy(
                function(t) {
                    t.namespace && (this.swapping = "translated" == t.type);
                },
                this
            ),
            "translate.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    this.swapping &&
                    (this.core.options.animateOut || this.core.options.animateIn) &&
                    this.swap();
            }, this)
        }),
        this.core.$element.on(this.handlers);
    };
    (n.Defaults = {
        animateOut: !1,
        animateIn: !1
    }), (n.prototype.swap = function() {
        if (
            1 === this.core.settings.items &&
            t.support.animation &&
            t.support.transition
        ) {
            this.core.speed(0);
            var e,
                s = t.proxy(this.clear, this),
                i = this.core.$stage.children().eq(this.previous),
                n = this.core.$stage.children().eq(this.next),
                o = this.core.settings.animateIn,
                a = this.core.settings.animateOut;
            this.core.current() !== this.previous &&
                (a &&
                    ((e =
                            this.core.coordinates(this.previous) -
                            this.core.coordinates(this.next)),
                        i
                        .one(t.support.animation.end, s)
                        .css({
                            left: e + "px"
                        })
                        .addClass("animated owl-animated-out")
                        .addClass(a)),
                    o &&
                    n
                    .one(t.support.animation.end, s)
                    .addClass("animated owl-animated-in")
                    .addClass(o));
        }
    }), (n.prototype.clear = function(e) {
        t(e.target)
            .css({
                left: ""
            })
            .removeClass("animated owl-animated-out owl-animated-in")
            .removeClass(this.core.settings.animateIn)
            .removeClass(this.core.settings.animateOut),
            this.core.onTransitionEnd();
    }), (n.prototype.destroy = function() {
        var t, e;
        for (t in this.handlers) this.core.$element.off(t, this.handlers[t]);
        for (e in Object.getOwnPropertyNames(this))
            "function" != typeof this[e] && (this[e] = null);
    }), (t.fn.owlCarousel.Constructor.Plugins.Animate = n);
})(window.Zepto || window.jQuery, window, document), (function(t, e, s, i) {
    var n = function(e) {
        (this._core = e), (this._call = null), (this._time = 0), (this._timeout = 0), (this._paused = !0), (this._handlers = {
            "changed.owl.carousel": t.proxy(function(t) {
                t.namespace && "settings" === t.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : t.namespace &&
                    "position" === t.property.name &&
                    this._paused &&
                    (this._time = 0);
            }, this),
            "initialized.owl.carousel": t.proxy(function(t) {
                t.namespace && this._core.settings.autoplay && this.play();
            }, this),
            "play.owl.autoplay": t.proxy(function(t, e, s) {
                t.namespace && this.play(e, s);
            }, this),
            "stop.owl.autoplay": t.proxy(function(t) {
                t.namespace && this.stop();
            }, this),
            "mouseover.owl.autoplay": t.proxy(function() {
                this._core.settings.autoplayHoverPause &&
                    this._core.is("rotating") &&
                    this.pause();
            }, this),
            "mouseleave.owl.autoplay": t.proxy(function() {
                this._core.settings.autoplayHoverPause &&
                    this._core.is("rotating") &&
                    this.play();
            }, this),
            "touchstart.owl.core": t.proxy(function() {
                this._core.settings.autoplayHoverPause &&
                    this._core.is("rotating") &&
                    this.pause();
            }, this),
            "touchend.owl.core": t.proxy(function() {
                this._core.settings.autoplayHoverPause && this.play();
            }, this)
        }),
        this._core.$element.on(this._handlers), (this._core.options = t.extend({}, n.Defaults, this._core.options));
    };
    (n.Defaults = {
        autoplay: !1,
        autoplayTimeout: 5e3,
        autoplayHoverPause: !1,
        autoplaySpeed: !1
    }), (n.prototype._next = function(i) {
        (this._call = e.setTimeout(
            t.proxy(this._next, this, i),
            this._timeout * (Math.round(this.read() / this._timeout) + 1) -
            this.read()
        )),
        this._core.is("interacting") ||
            s.hidden ||
            this._core.next(i || this._core.settings.autoplaySpeed);
    }), (n.prototype.read = function() {
        return new Date().getTime() - this._time;
    }), (n.prototype.play = function(s, i) {
        var n;
        this._core.is("rotating") || this._core.enter("rotating"), (s = s || this._core.settings.autoplayTimeout), (n = Math.min(this._time % (this._timeout || s), s)),
            this._paused ? ((this._time = this.read()), (this._paused = !1)) : e.clearTimeout(this._call), (this._time += (this.read() % s) - n), (this._timeout = s), (this._call = e.setTimeout(t.proxy(this._next, this, i), s - n));
    }), (n.prototype.stop = function() {
        this._core.is("rotating") &&
            ((this._time = 0), (this._paused = !0),
                e.clearTimeout(this._call),
                this._core.leave("rotating"));
    }), (n.prototype.pause = function() {
        this._core.is("rotating") &&
            !this._paused &&
            ((this._time = this.read()), (this._paused = !0),
                e.clearTimeout(this._call));
    }), (n.prototype.destroy = function() {
        var t, e;
        this.stop();
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this))
            "function" != typeof this[e] && (this[e] = null);
    }), (t.fn.owlCarousel.Constructor.Plugins.autoplay = n);
})(window.Zepto || window.jQuery, window, document), (function(t, e, s, i) {
    "use strict";
    var n = function(e) {
        (this._core = e), (this._initialized = !1), (this._pages = []), (this._controls = {}), (this._templates = []), (this.$element = this._core.$element), (this._overrides = {
            next: this._core.next,
            prev: this._core.prev,
            to: this._core.to
        }), (this._handlers = {
            "prepared.owl.carousel": t.proxy(function(e) {
                e.namespace &&
                    this._core.settings.dotsData &&
                    this._templates.push(
                        '<div class="' +
                        this._core.settings.dotClass +
                        '">' +
                        t(e.content)
                        .find("[data-dot]")
                        .addBack("[data-dot]")
                        .attr("data-dot") +
                        "</div>"
                    );
            }, this),
            "added.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    this._core.settings.dotsData &&
                    this._templates.splice(t.position, 0, this._templates.pop());
            }, this),
            "remove.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    this._core.settings.dotsData &&
                    this._templates.splice(t.position, 1);
            }, this),
            "changed.owl.carousel": t.proxy(function(t) {
                t.namespace && "position" == t.property.name && this.draw();
            }, this),
            "initialized.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    !this._initialized &&
                    (this._core.trigger("initialize", null, "navigation"),
                        this.initialize(),
                        this.update(),
                        this.draw(), (this._initialized = !0),
                        this._core.trigger("initialized", null, "navigation"));
            }, this),
            "refreshed.owl.carousel": t.proxy(function(t) {
                t.namespace &&
                    this._initialized &&
                    (this._core.trigger("refresh", null, "navigation"),
                        this.update(),
                        this.draw(),
                        this._core.trigger("refreshed", null, "navigation"));
            }, this)
        }), (this._core.options = t.extend({}, n.Defaults, this._core.options)),
        this.$element.on(this._handlers);
    };
    (n.Defaults = {
        nav: !1,
        navText: [
            '<span aria-label="Previous">&#x2039;</span>',
            '<span aria-label="Next">&#x203a;</span>'
        ],
        navSpeed: !1,
        navElement: 'button type="button" role="presentation"',
        navContainer: !1,
        navContainerClass: "owl-nav",
        navClass: ["owl-prev", "owl-next"],
        slideBy: 1,
        dotClass: "owl-dot",
        dotsClass: "owl-dots",
        dots: !0,
        dotsEach: !1,
        dotsData: !1,
        dotsSpeed: !1,
        dotsContainer: !1
    }), (n.prototype.initialize = function() {
        var e,
            s = this._core.settings;
        (this._controls.$relative = (s.navContainer ? t(s.navContainer) : t("<div>")
            .addClass(s.navContainerClass)
            .appendTo(this.$element)
        ).addClass("disabled")), (this._controls.$previous = t("<" + s.navElement + ">")
            .addClass(s.navClass[0])
            .html(s.navText[0])
            .prependTo(this._controls.$relative)
            .on(
                "click",
                t.proxy(function(t) {
                    this.prev(s.navSpeed);
                }, this)
            )), (this._controls.$next = t("<" + s.navElement + ">")
            .addClass(s.navClass[1])
            .html(s.navText[1])
            .appendTo(this._controls.$relative)
            .on(
                "click",
                t.proxy(function(t) {
                    this.next(s.navSpeed);
                }, this)
            )),
        s.dotsData ||
            (this._templates = [
                t('<button role="button">')
                .addClass(s.dotClass)
                .append(t("<span>"))
                .prop("outerHTML")
            ]), (this._controls.$absolute = (s.dotsContainer ? t(s.dotsContainer) : t("<div>")
                .addClass(s.dotsClass)
                .appendTo(this.$element)
            ).addClass("disabled")),
            this._controls.$absolute.on(
                "click",
                "button",
                t.proxy(function(e) {
                    var i = t(e.target)
                        .parent()
                        .is(this._controls.$absolute) ? t(e.target).index() : t(e.target)
                        .parent()
                        .index();
                    e.preventDefault(), this.to(i, s.dotsSpeed);
                }, this)
            );
        for (e in this._overrides) this._core[e] = t.proxy(this[e], this);
    }), (n.prototype.destroy = function() {
        var t, e, s, i, n;
        n = this._core.settings;
        for (t in this._handlers) this.$element.off(t, this._handlers[t]);
        for (e in this._controls)
            "$relative" === e && n.navContainer ? this._controls[e].html("") : this._controls[e].remove();
        for (i in this.overides) this._core[i] = this._overrides[i];
        for (s in Object.getOwnPropertyNames(this))
            "function" != typeof this[s] && (this[s] = null);
    }), (n.prototype.update = function() {
        var t,
            e,
            s,
            i = this._core.clones().length / 2,
            n = i + this._core.items().length,
            o = this._core.maximum(!0),
            a = this._core.settings,
            r = a.center || a.autoWidth || a.dotsData ? 1 : a.dotsEach || a.items;
        if (
            ("page" !== a.slideBy && (a.slideBy = Math.min(a.slideBy, a.items)),
                a.dots || "page" == a.slideBy)
        )
            for (this._pages = [], t = i, e = 0, s = 0; t < n; t++) {
                if (e >= r || 0 === e) {
                    if (
                        (this._pages.push({
                                start: Math.min(o, t - i),
                                end: t - i + r - 1
                            }),
                            Math.min(o, t - i) === o)
                    )
                        break;
                    (e = 0), ++s;
                }
                e += this._core.mergers(this._core.relative(t));
            }
    }), (n.prototype.draw = function() {
        var e,
            s = this._core.settings,
            i = this._core.items().length <= s.items,
            n = this._core.relative(this._core.current()),
            o = s.loop || s.rewind;
        this._controls.$relative.toggleClass("disabled", !s.nav || i),
            s.nav &&
            (this._controls.$previous.toggleClass(
                    "disabled", !o && n <= this._core.minimum(!0)
                ),
                this._controls.$next.toggleClass(
                    "disabled", !o && n >= this._core.maximum(!0)
                )),
            this._controls.$absolute.toggleClass("disabled", !s.dots || i),
            s.dots &&
            ((e =
                    this._pages.length - this._controls.$absolute.children().length),
                s.dotsData && 0 !== e ? this._controls.$absolute.html(this._templates.join("")) : e > 0 ? this._controls.$absolute.append(
                    new Array(e + 1).join(this._templates[0])
                ) : e < 0 &&
                this._controls.$absolute
                .children()
                .slice(e)
                .remove(),
                this._controls.$absolute.find(".active").removeClass("active"),
                this._controls.$absolute
                .children()
                .eq(t.inArray(this.current(), this._pages))
                .addClass("active"));
    }), (n.prototype.onTrigger = function(e) {
        var s = this._core.settings;
        e.page = {
            index: t.inArray(this.current(), this._pages),
            count: this._pages.length,
            size: s &&
                (s.center || s.autoWidth || s.dotsData ? 1 : s.dotsEach || s.items)
        };
    }), (n.prototype.current = function() {
        var e = this._core.relative(this._core.current());
        return t
            .grep(
                this._pages,
                t.proxy(function(t, s) {
                    return t.start <= e && t.end >= e;
                }, this)
            )
            .pop();
    }), (n.prototype.getPosition = function(e) {
        var s,
            i,
            n = this._core.settings;
        return (
            "page" == n.slideBy ? ((s = t.inArray(this.current(), this._pages)), (i = this._pages.length),
                e ? ++s : --s, (s = this._pages[((s % i) + i) % i].start)) : ((s = this._core.relative(this._core.current())), (i = this._core.items().length),
                e ? (s += n.slideBy) : (s -= n.slideBy)),
            s
        );
    }), (n.prototype.next = function(e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!0), e);
    }), (n.prototype.prev = function(e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!1), e);
    }), (n.prototype.to = function(e, s, i) {
        var n;
        !i && this._pages.length ? ((n = this._pages.length),
            t.proxy(this._overrides.to, this._core)(
                this._pages[((e % n) + n) % n].start,
                s
            )) : t.proxy(this._overrides.to, this._core)(e, s);
    }), (t.fn.owlCarousel.Constructor.Plugins.Navigation = n);
})(window.Zepto || window.jQuery, window, document), (function(t, e, s, i) {
    "use strict";
    var n = function(s) {
        (this._core = s), (this._hashes = {}), (this.$element = this._core.$element), (this._handlers = {
            "initialized.owl.carousel": t.proxy(function(s) {
                s.namespace &&
                    "URLHash" === this._core.settings.startPosition &&
                    t(e).trigger("hashchange.owl.navigation");
            }, this),
            "prepared.owl.carousel": t.proxy(function(e) {
                if (e.namespace) {
                    var s = t(e.content)
                        .find("[data-hash]")
                        .addBack("[data-hash]")
                        .attr("data-hash");
                    if (!s) return;
                    this._hashes[s] = e.content;
                }
            }, this),
            "changed.owl.carousel": t.proxy(function(s) {
                if (s.namespace && "position" === s.property.name) {
                    var i = this._core.items(
                            this._core.relative(this._core.current())
                        ),
                        n = t
                        .map(this._hashes, function(t, e) {
                            return t === i ? e : null;
                        })
                        .join();
                    if (!n || e.location.hash.slice(1) === n) return;
                    e.location.hash = n;
                }
            }, this)
        }), (this._core.options = t.extend({}, n.Defaults, this._core.options)),
        this.$element.on(this._handlers),
            t(e).on(
                "hashchange.owl.navigation",
                t.proxy(function(t) {
                    var s = e.location.hash.substring(1),
                        n = this._core.$stage.children(),
                        o = this._hashes[s] && n.index(this._hashes[s]);
                    o !== i &&
                        o !== this._core.current() &&
                        this._core.to(this._core.relative(o), !1, !0);
                }, this)
            );
    };
    (n.Defaults = {
        URLhashListener: !1
    }), (n.prototype.destroy = function() {
        var s, i;
        t(e).off("hashchange.owl.navigation");
        for (s in this._handlers) this._core.$element.off(s, this._handlers[s]);
        for (i in Object.getOwnPropertyNames(this))
            "function" != typeof this[i] && (this[i] = null);
    }), (t.fn.owlCarousel.Constructor.Plugins.Hash = n);
})(window.Zepto || window.jQuery, window, document), (function(t, e, s, i) {
    function n(e, s) {
        var n = !1,
            o = e.charAt(0).toUpperCase() + e.slice(1);
        return (
            t.each((e + " " + r.join(o + " ") + o).split(" "), function(t, e) {
                if (a[e] !== i) return (n = !s || e), !1;
            }),
            n
        );
    }

    function o(t) {
        return n(t, !0);
    }
    var a = t("<support>").get(0).style,
        r = "Webkit Moz O ms".split(" "),
        l = {
            transition: {
                end: {
                    WebkitTransition: "webkitTransitionEnd",
                    MozTransition: "transitionend",
                    OTransition: "oTransitionEnd",
                    transition: "transitionend"
                }
            },
            animation: {
                end: {
                    WebkitAnimation: "webkitAnimationEnd",
                    MozAnimation: "animationend",
                    OAnimation: "oAnimationEnd",
                    animation: "animationend"
                }
            }
        },
        h = {
            csstransforms: function() {
                return !!n("transform");
            },
            csstransforms3d: function() {
                return !!n("perspective");
            },
            csstransitions: function() {
                return !!n("transition");
            },
            cssanimations: function() {
                return !!n("animation");
            }
        };
    h.csstransitions() &&
        ((t.support.transition = new String(o("transition"))), (t.support.transition.end = l.transition.end[t.support.transition])),
        h.cssanimations() &&
        ((t.support.animation = new String(o("animation"))), (t.support.animation.end = l.animation.end[t.support.animation])),
        h.csstransforms() &&
        ((t.support.transform = new String(o("transform"))), (t.support.transform3d = h.csstransforms3d()));
})(window.Zepto || window.jQuery, window, document), (function(t) {
    t.fn.extend({
        donetyping: function(e, s) {
            s = s || 500;
            var i,
                n = function(t) {
                    i && ((i = null), e.call(t));
                };
            return this.each(function(e, o) {
                var a = t(o);
                a.is(":input") &&
                    a
                    .on(
                        "propertychange change click keyup keypress input paste",
                        function(t) {
                            ("keyup" == t.type && 8 != t.keyCode) ||
                            (i && clearTimeout(i), (i = setTimeout(function() {
                                n(o);
                            }, s)));
                        }
                    )
                    .on("blur", function() {
                        n(o);
                    });
            });
        }
    });
})(jQuery),
jQuery(document).ready(function(t) {
    t(".js-validate-email").donetyping(function() {
            var e = /^([^@]+)@([^@]+)\.([a-zA-Z]{2,6})$/i;
            toggleValidationFeedback(!e.test(t(this).val()), t(this));
        }),
        t(".js-validate-name").donetyping(function() {
            var e = /.{2,}/i;
            toggleValidationFeedback(!e.test(t(this).val()), t(this));
        }),
        t(".js-btn-finish").on("click", function() {
            var e = t(this)
                .prev()
                .prev();
            e.hasClass("has-valid") ||
                (e.hasClass("has-error") ? e.focus() : (e.addClass("has-error has-empty"), e.focus()));
        });
});
var slider, removed_item;
jQuery(document).ready(function(t) {
    function e(e) {
        var s = t(e).parents(".js-section-items"),
            i = t(e).parents(".js-select-item");
        removed_item = i.clone();
        var n = t(e).attr("data-favorite"),
            o =
            '<button type="button" class="btn--transparent btn--undo js-btn-undo" data-favorite="' +
            n +
            '"><i class="material-icons">&#xE166;</i> ' +
            Lang.get("js.undo") +
            "</button>";
        s.find(".js-btn-undo").length ? s.find(".js-btn-undo").attr("data-favorite", n) : s.append(o),
            i.closest(".owl-item").length ? slider.trigger("remove.owl.carousel", [
                parseInt(i.attr("data-slide-id"))
            ]) : i.remove();
        var a = t('.js-select-item[data-type="' + i.attr("data-type") + '"]');
        a.length < 1 ? s.addClass("is-empty") : s.hasClass("is-empty") && s.removeClass("is-empty");
    }

    function s(e) {
        var s = t(e).attr("data-favorite"),
            i = t(e).parents(".js-section-items");
        t(".owl-carousel").length && "image" == removed_item.attr("data-type") ? (slider.trigger("add.owl.carousel", [removed_item]), resetSlides()) : i.find(".drawer__row").append(removed_item),
            toggleCookieValue("favorites", s);
        var n = cookieArray("favorites");
        updateNav(n, t(".js-favorite-nav__count")),
            sortFavorites(),
            i.hasClass("is-empty") && i.removeClass("is-empty"),
            t(e).remove();
    }
    t("body").on("click", ".js-btn-remove", function() {
            e(this);
        }),
        t("body").on("click", ".js-btn-undo", function() {
            s(this);
        });
});
var all_images = [],
    all_colors = [],
    all_products = [],
    last_breakpoint = null;
jQuery(document).ready(function(t) {
        t("body").on("click", ".js-readall", function() {
            var e = t(this),
                s = e.parent();
            s.hasClass("is-collapsed") ? (s.removeClass("is-collapsed"),
                e.attr("aria-expanded", !0),
                e.text(e.attr("data-active-text"))) : (s.addClass("is-collapsed"),
                e.attr("aria-expanded", !1),
                e.text(e.attr("data-default-text")));
        });
    }),
    jQuery(document).ready(function(t) {
        var e = cookieArray("favorites"),
            s = !1;
        null == e || (1 === e.length && "" == e[0]) ? void 0 : e.length && (void 0, (s = !0)),
            s && buildFavorites(e);
    }),
    jQuery(document).ready(function(t) {
        function e(e) {
            void 0,
            t.ajax({
                type: "post",
                url: "https://colours-api-stage.allegro.no/submission/search",
                data: {
                    email: e
                },
                dataType: "json",
                success: function(t) {
                    void 0, s(t.wishlists);
                },
                error: function() {
                    void 0, s([]);
                }
            });
        }

        function s(e) {
            var s = "";
            e.length ? (window.location =
                    "/wishlist-finished?email=" + e[0].email + "&search=1") : (s =
                    '<div class="search-result__item text-center"><p>' +
                    Lang.get("js.no_results") +
                    '</p><div class="btn-row text-center"><a class="btn btn--line" href="/">' +
                    Lang.get("js.to_global") +
                    "</a></div></div>"),
                t("#search-result-submission").html(s);
        }
        var i = ("http://" + window.location.hostname, t("#signup_email"));
        t("#create-submission").click(function(e) {
                if ((e.preventDefault(), i.hasClass("is-valid"))) {
                    t(this).attr("data-images", all_images),
                        t(this).attr("data-products", all_products),
                        t(this).attr("data-colors", all_colors);
                    var s = t(this),
                        n = {};
                    (n = {
                        email: i.val(),
                        images: s.data("images"),
                        products: s.data("products"),
                        colors: s.data("colors"),
                        name: t("#signup_name").val(),
                        newsletter: t("#signup_newsletter").is(":checked") ? 1 : 0,
                        mail: t("#mailCheckBox").is(":checked") ? 1 : 0,
                        lang: t("#langOption").val()
                    }),
                    t.ajax({
                        type: "post",
                        url: "https://colours-api-stage.allegro.no/submission",
                        dataType: "json",
                        enctype: "multipart/form-data",
                        data: n,
                        success: function(t) {
                            send_favourite_email();
                            //   n.mail
                            //     ? (window.location =
                            //         "/wishlist-finished?email=" +
                            //         t.email +
                            //         "&submitted=" +
                            //         t.updated +
                            //         "&sent=1")
                            //     : (window.location =
                            //         "/wishlist-finished?email=" +
                            //         t.email +
                            //         "&submitted=" +
                            //         t.updated);
                        }
                    });
                } else
                    i.addClass("has-error"),
                    i.focus(),
                    clearTimeout(e.checkmarkTimeout), (e.checkmarkTimeout = setTimeout(function() {
                        i.removeClass("has-error");
                    }, 1e3));
            }),
            t("#update-submission").submit(function(e) {
                e.preventDefault();
                var s = t("#submissionId").val(),
                    i = {
                        first_name: t("#user_firstname").val(),
                        last_name: t("#user_lastname").val(),
                        email: t("#signup_email").val(),
                        newsletter: t("#newsletter_signup").is(":checked") ? 1 : 0,
                        mail: t("#mailCheckBox").is(":checked") ? 1 : 0,
                        lang: t("#newsletter_language").val()
                    };
                t.ajax({
                    type: "post",
                    url: "https://colours-api-stage.allegro.no/submission/" + s,
                    dataType: "json",
                    enctype: "multipart/form-data",
                    data: i,
                    success: function(t) {
                        i.mail ? (window.location =
                            "/wishlist-finished?email=" +
                            t.email +
                            "&submitted=" +
                            t.updated +
                            "&sent=1") : (window.location =
                            "/wishlist-finished?email=" +
                            t.email +
                            "&submitted=" +
                            t.updated);
                    }
                });
            });
        var n = t("#search-submission");
        n.length &&
            n.submit(function(t) {
                t.preventDefault();
                var s = n.find("input").val();
                void 0, e(s);
            });
    });