<?php

class Adaptive_model extends CI_Model {
		
	public function __construct() {
		parent::__construct();

	}

	function get_instructor_info()
	{
		$query = $this->db->query('SELECT
			`users`.`userName`,
			`users`.`userType`,
			`users`.`userEmail`,
			`users`.`userPhoneNo`,
			`users`.`country`,
			`educatorEarning`.`courseID`,
			`educatorEarning`.`authorID`,
			sum(`educatorEarning`.`earnings`) as "to_pay",
			`educatorEarning`.`adminCommission`
			FROM
			`users`
			JOIN `educatorEarning`
			ON `users`.`userID` = `educatorEarning`.`authorID`
			WHERE
			users.userType = "instructor"

			GROUP BY users.userID');

		// $query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function insert_paid_user($data)
	{
			$this->db->insert('users_paid', $data);
			$last_user_id = $this->db->insert_id();

			return $last_user_id;
	}

	function update_paid_user($user_id, $data)
	{
		$where = array (
						'paidID' => $user_id
					);
		$this->db->update('users_paid', $data, $where);
	}

}

