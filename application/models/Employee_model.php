<?php

class Employee_model extends CI_Model {

	public function __construct() {
		parent::__construct();

	}



	function add_employee($data_arr , $user_id)
	{
		$this->db->select('userCompanyName , userEmail');
		$where = array (
						// 'userCompanyName' => $data['userCompanyName'],
						'userEmail' => $data_arr['userEmail'],
					);
		$this->db->where($where);
		$query = $this->db->get('users');

				 if($query->num_rows() > 0) {
					$row = $query->result_array();
				  return false;
				} else {
					$this->db->insert('users', $data_arr);
					$last_user_id = $this->db->insert_id();
					
					return true;

				}

	}
	function update_employee($data , $employee_id, $file_names) {

	$where = array (
						'userID' => $employee_id
					);
		$this->db->where($where);
		$result = $this->db->update('users', $data);
		
			$data2   = array(
                    'userID' => $employee_id,
                    'userMetaKey' => 'file_name',
                    'userMetaValue' => $file_names
            );
			$where2 = array (
				'userID' => $employee_id
			);
			if(!empty($file_names)){
				 $updatess = $this->db->update('users_meta' , $data2 , $where2);
			}
		
		if($result) {

		return true;
		} else {

		return false;
		}
	}

	function add_schedule($data)
	{
		$query = $this->db->insert('employee_courses', $data);
		if($query)
		{
			return $data;
		}
	}

	function get_employee($params = array())
	{
		$company_id = $this->session->userdata('userID');
		$this->db->select('*');
			$where = array (
						'userType' => 'employee',
						'companyID' => $company_id,
						'userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function get_all_employee($company_id)
	{

		$this->db->select('*');
		$where = "userType='employee' AND companyID='".$company_id."'";
			// $where = array (
						// 'userType' => 'employee',
						// 'companyID' => $company_id,
					// );
		$this->db->where($where);
		$this->db->from('users');

		$query = $this->db->get();
		// return $company_id;
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}





	function get_employee_assign()
	{
		$this->db->select('courses.coursesName,courses.coursesID');
		$where = array (
						'coursesDeleted' => '0'
					);
		$this->db->where($where);
		$this->db->group_by('courses.coursesID');
		$this->db->from('courses');
		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function get_employee_assignned()
	{
		$this->db->select('users.userID,courses.coursesName,courses.coursesID');
		$this->db->join('employee_courses', 'users.userID = employee_courses.employeeID','inner');
		$this->db->join('courses', 'employee_courses.coursesID = courses.coursesID','inner');
		$where = array (
						'userDeleted' => '0'
					);
		$this->db->where($where);
		// $this->db->group_by('courses.coursesID');
		$this->db->from('users');
		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}






	function get_courses()
	{
		$this->db->select('*');
		$this->db->from('courses');
		$this->db->order_by('coursesID','desc');

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function get_schedule($params = array())
	{
		$this->db->select('*');
		$this->db->from('employee_courses');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function check_schedules($employee_id,$course_id,$courseDate)
	{
		$this->db->select('*');
		$where = array (
						'employeeID' => $employee_id,
						'coursesID' => $course_id,
					);
		$this->db->where($where);
		$this->db->from('employee_courses');
		$query = $this->db->get();

		$row = $query->result_array();
		
		$UpdateData = array(
			'courseDate' => $courseDate
		);
		
        $update = $this->db->update('employee_courses' , $UpdateData , $where);

		return $row;

	}
	function get_employee_by_id($company_id)
	{
		$this->db->select('*');
		$this->db->join('users_meta', 'users.userID = users_meta.userID','left');
		$where = array (
						'users.userID' => $company_id,
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('users.userID','desc');

			$this->db->limit(1);


		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function get_profile_info($id){

		$this->db->select('*');
		$where = array (
						'userID' => $id,
					);
		$this->db->where($where);

		$query = $this->db->get('users');
		$row = $query->result_array();

		return $row;
	}
	function update_profile_info($id,$pass){


	$UpdateData = array(

				'userName' => $this->input->post('user_name'),
				'userEmail' => $this->input->post('email'),
				'userPassword' => $pass,
				'modifiedDate' => date('Y-m-d H:i:s')
			);
			// echo $id;
		$where = array (
						'userID' => $id
					);
          $update = $this->db->update('users' , $UpdateData , $where);

		// echo $update;
		  if($update)
			{
				// return $pass;
				return $update;
			}
	}



	function delete_employee($employee_id)
	{
			$this->db->where(array("userID" => $employee_id));
			$this->db->set('userDeleted', '1');
            $this->db->update('users');
			 return true;
	}
function search_company($company_id , $params = array())
	{
		$user_id = $this->session->userdata('userID');
		$this->db->select('*');
			$where = array (
						'companyID' => $user_id,
						'userID' => $company_id,
						'userType' => 'employee',
						'userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

		    /**
    * get client users data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function employee_report()
	{
		$user_id = $this->session->userdata('userID');

				return $query = $this->db->query("SELECT
										users.userName,
										users.userEmail,
										users.userPhoneNo
										 FROM `users`
										 WHERE  `userType` = 'employee' AND `userDeleted` = '0' AND `companyID` = '".$user_id."'
										");



	}
	function full_report()
	{
				 $query = $this->db->query("SELECT
										courses.coursesID,courses.coursesName
										 FROM `courses`
										");
									return	$query->result_array();
									// return	$query->result_array();
	}
	function special_report($case_query)
	{
				$query = $this->db->query("SELECT
								users.userName,
								users.userEmail,
								$case_query
								FROM
								users
								INNER JOIN employee_courses ON users.userID = employee_courses.employeeID
								INNER JOIN courses ON courses.coursesID = employee_courses.coursesID
								INNER JOIN quizess_report ON courses.coursesID = quizess_report.courseId AND quizess_report.userId = users.userID
								INNER JOIN quizzes ON quizess_report.quizId = quizzes.quizID
								
								group by users.username"
								);
				return	$query->result_array();
	}

	function get_company_phone($phone_no)
	{

	$user_id = $this->session->userdata('userID');
	
	$query = $this->db->query("SELECT *
								FROM `users`
								WHERE userPhoneNo like '" .$phone_no. "%' AND companyID ='".$user_id."' OR userName like '" .$phone_no. "%' AND companyID ='".$user_id."'
								ORDER BY userPhoneNo
								LIMIT 0,10");

	$skillarryhold = '';
	 if($query->num_rows() > 0){
			$rows     = $query->result();
	foreach ($rows as $row)
	{

			$cashbackComission = 0;
			$charityComission = 0;

			$skillarryhold .= ' <ul id="name-list">
			<li class="phone_click" onClick="selectname('.$cashbackComission.','.$charityComission.','.$row->userPhoneNo.','.$row->userID.');"><span class="">
			<i class="fa fa-phone" style="/*background: #e5e5e5;*/ padding: 12px 27px 12px 12px;margin-top: 0;" ></i>
			</span><span class="">'.$row->userName.'  - '.$row->userPhoneNo.'</span></li></ul>';

		}
				return $skillarryhold;

} else {

	return "Not Found";
}
}
function get_membership($company_id)
{
	$this->db->select('membershipID');

		$where = array (
						// 'userCompanyName' => $data['userCompanyName'],
						'userID' => $company_id
					);
		$this->db->where($where);
		$query = $this->db->get('users');
		$row = $query->result_array();
		return $row;

	}
	function get_members_id($user_id)
	{
		$this->db->select('membershipID');
		$where = array (
						// 'userCompanyName' => $data['userCompanyName'],
						'userID' => $user_id
					);
		$this->db->where($where);
		$query = $this->db->get('users');
		$row = $query->result_array();
		return $row;
	}
	function get_employee_limit($for_employee)
	{
		$this->db->select('employeeLimit');
		$where = array (
				// 'userCompanyName' => $data['userCompanyName'],
				'membershipID' => $for_employee
			);
		$this->db->where($where);
		$query = $this->db->get('membership');
		$row = $query->result_array();
		return $row;
	}
	function count_employee($company_id)
	{

		$this->db->select('*');
		$where = "userType='employee' AND companyID='".$company_id."'";
			// $where = array (
						// 'userType' => 'employee',
						// 'companyID' => $company_id,
					// );
		$this->db->where($where);
		$this->db->from('users');

		$query = $this->db->get();
		// return $company_id;
		return ($query->num_rows() > 0)?$query->num_rows():FALSE;
	}
	function filter_by_status($status,$params = array())
	{
		$company_id = $this->session->userdata('userID');
		$this->db->select('*');
			$where = array (
						'userType' => 'employee',
						'userStatus' => $status,
						'companyID' => $company_id,
						'userDeleted' => '0'
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
    
    
    /**
    * get show membership package  data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function get_employee_phone($phone_no)
	{

			$clientID = $this->session->userdata('userID');
            $query = $this->db->query("SELECT *
                    FROM `users`
                    WHERE userName like '" .$phone_no. "%' AND userDeleted = 0 AND userType='employee' AND companyID='".$clientID."'
                    ORDER BY userID
                    LIMIT 0,10");
            $skillarryhold = '';
            if($query->num_rows() > 0){
                
			    $rows     = $query->result();
                foreach ($rows as $row)
                {
                    $cashbackComission = 0;
                    $charityComission = 0;

                    $skillarryhold .= ' <ul id="name-list">
                    <li class="phone_click" onClick="selectname('.$row->userID.');"><span class=""><i class="fa fa-phone" style="/*background: #e5e5e5;*/ padding: 12px 27px 12px 12px;margin-top: 0;" ></i></span><span class="">'.$row->userName.'</span></li></ul>';
                }
                return $skillarryhold;
                
            }
    }
    
    function search_employee($company_id , $params = array())
	{
		$this->db->select('*');
			$where = array (
						'userID' => $company_id,
						'userType' => 'employee',
						'userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}


    
    function search_employee_assignned()
	{
		$this->db->select('courses.coursesName,courses.coursesID');
		$where = array (
						'coursesDeleted' => '0'
					);
		$this->db->where($where);
		$this->db->group_by('courses.coursesID');
		$this->db->from('courses');
		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function search_employee_assign($user_id)
	{
		$this->db->select('users.userID,courses.coursesName,courses.coursesID');
		$this->db->join('employee_courses', 'users.userID = employee_courses.employeeID','inner');
		$this->db->join('courses', 'employee_courses.coursesID = courses.coursesID','inner');
		$where = array (
                        'userID' => $user_id,
						'userDeleted' => '0'
					);
		$this->db->where($where);
		// $this->db->group_by('courses.coursesID');
		$this->db->from('users');
		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

}

