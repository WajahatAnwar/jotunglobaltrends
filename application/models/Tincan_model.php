<?php

class Tincan_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

    }
    
    function add_info($user_id,$course_id,$gained_score,$total_score,$mini,$type)
    {

        $data = array(
                    'userID' => $user_id,
                    'courseID' => $course_id,
                    'intGainedScore' => $gained_score,
                    'intTotalScore' => $total_score,
                    'intMinScore' => $mini,
                    'intQuizStatus' => $type,
                    'createdDate' => date('Y-m-d'),
                    'modifiedDate' => date('Y-m-d H:i:s')
                );
        if(!empty($user_id) && !empty($course_id))
        {
            $this->db->select('*');
            $where = array(
                'userID' => $user_id,
                'courseID' => $course_id
            );
            $this->db->where($where);
            $query = $this->db->get('result');
            if ($query->num_rows() > 0) {
                $this->db->where($where);
                return $this->db->update('result', $data);
            }else
            {
                return $this->db->insert('result', $data);
            }
        }
    }
    
    /*
     Code by Wajahat 
     19 Jan 2017
     This function insert or update storyline answer
    */  
    
    function add_answer_detail($lesson_id,$course_id_num,$user_id_num,$course_name,$score3,$result,$verb)
    {
        if($result == true)
        {
            $status = '0';
        }else if($score3 == "0"){
            $status = '1';
        }else{
            $status = '1';
        }
        
        $data = array(
                    'userID' => $user_id_num,
                    'coursesID' => $course_id_num,
                    'lessonID' => $lesson_id,
                    'answerName' => $course_name,
                    'score' => $score3,
                    'status' => $status,
                    'verb' => $verb,
                    'createdDate' => date('Y-m-d H:i:s'),
                    'modifiedDate' => date('Y-m-d H:i:s'),
                    'answerDeleted' => '0',
                    'date' => date('Y-m-d')
                );
        if(!empty($user_id_num) && !empty($course_id_num) && !empty($lesson_id))
        {
            $this->db->select('*');
            $where = array(
                'userID' => $user_id_num,
                'coursesID' => $course_id_num,
                'lessonID' => $lesson_id,
                'answerName' => $course_name,
                'answerDeleted' => '0'
            );
            $this->db->where($where);
            $query = $this->db->get('storyline_answers');
            
            if ($query->num_rows() > 0) {
                $this->db->where($where);
                return $this->db->update('storyline_answers', $data);
            }else
            {
                return $this->db->insert('storyline_answers', $data);
            }
        }
    }

}