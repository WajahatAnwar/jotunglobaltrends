<?php

class Frontend_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

    }

   
    function get_courses_listing($category_id, $params = array())
    {

        $this->db->select('courses.coursesID,
        courses.coursesName,
        courses.displayName,
        category.categoryName,
        category.categoryDescription,
        users.userName,
        users.firstName,
        users.lastName,
        users.userID,
        courses.coursesDescription,
        courses.price,
        courses.ceCheck,
        courses.coursesImage,
        users.fileName,
        users.userType,
        users.userCompanyName,
        users.userPhoneNo,
        users.userEmail,
        users.bio,
        users.designation,
        users.country,
        users.ceCourseID,
        course_summary.totalAssign,
        course_summary.totalReviews,
        course_summary.averageRating,
        slugs.uri,
        slugs.title');
        if (!empty($category_id)) {
            if ($category_id > 0) {
                $this->db->join('course_category', 'courses.coursesID = course_category.courseID', 'left');
                $this->db->join('category', 'course_category.categoryID = category.categoryID', 'left');
                $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt');
                $this->db->join('users', 'courses.user_id = users.userID', 'LEFt');
				$this->db->join('course_summary', 'courses.coursesID = course_summary.coursesID', 'left');
                $where = array(
					'courses.coursesStatus' => 'publish',
                    'course_category.categoryID' => $category_id,
                    'courses.coursesDeleted' => '0'
                );
                $this->db->where($where);
            }
        } else {
				$this->db->join('course_category', 'courses.coursesID = course_category.courseID', 'LEFt');
                $this->db->join('category', 'course_category.categoryID = category.categoryID', 'LEFt');
                $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt');
				$this->db->join('users', 'courses.user_id = users.userID', 'LEFt');
				$this->db->join('course_summary', 'courses.coursesID = course_summary.coursesID', 'left');
				$where = array(
				'courses.coursesStatus' => 'publish',
					'courses.coursesDeleted' => '0'
				);
				$this->db->where($where);
        }
		$this->db->from('courses');
        if (!empty($category_id)){
            $this->db->order_by('courses.displayName', 'ASC');            
        }else{
    		$this->db->order_by('courses.coursesID', 'desc');            
        }
		$this->db->group_by('courses.coursesID');

        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit'], $params['start']);
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit']);
        }

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    
    function get_course_by_id($course_id)
    {
        $this->db->select('courses.coursesName,
        courses.coursesID,
        courses.displayName,
        courses.ceCheck,
        courses.disclosure_description,
        courses.content_disclousure,
        users.userName,
        users.userID,
        courses.coursesDescription,
        courses.price,
        courses.coursesImage,
        users.fileName,
        users.firstName,
        users.lastName,
        users.userType,
        users.userCompanyName,
        users.userPhoneNo,
        users.userEmail,
        users.bio,
        users.designation,
        users.country,
        users.ceCourseID,
        slugs.uri');
        $this->db->join('co_author', 'courses.coursesID = co_author.coursesID', 'LEFt');
        $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt'); 
        $this->db->join('users', 'co_author.userID = users.userID', 'LEFt');
        $where = array(
			    'courses.coursesID' => $course_id,
                'courses.coursesDeleted' => '0',
                'co_author.authorDeleted' => '0'
                );
        $this->db->where($where);
        $this->db->from('courses');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    function get_course_by_id_intro($course_id)
    {
        $this->db->select('courses.coursesImage,courses.coursesName');
        $where = array(
                'courses.coursesID' => $course_id,
                'courses.coursesDeleted' => '0'
                );
        $this->db->where($where);
        $this->db->from('courses');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    
    function get_course_meta($course_id)
    {
        $this->db->select('courses.coursesID,courses_meta.courseMetaKey,courses_meta.courseMetaValue');
        $this->db->join('courses_meta', 'courses.coursesID = courses_meta.courseID', 'LEFt');
         $where = array(
			     'courses.coursesID' => $course_id,
                 'courses.coursesDeleted' => '0'
                 );
        $this->db->where($where);
        $this->db->from('courses');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
        
    }
    
    function count_students($course_id)
    {
       $query = $this->db->query('select coursesID, count(employeeID) as TotalStudents from employee_courses where employee_courses.coursesID = "'.$course_id.'" group by coursesID');
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
        
    }
    
    function count_review($course_id)
    {
       $query = $this->db->query('select courseID, count(userID) as TotalReviews from courses_rating where courses_rating.courseID = "'.$course_id.'" group by courseID');
       return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
        
    }
    
    function count_all_students()
    {
       $query = $this->db->query('select coursesID, count(employeeID) as TotalStudents from employee_courses group by coursesID');
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
        
    }
    
    function count_all_reviews()
    {
       $query = $this->db->query('select courseID, count(userID) as TotalReviews from courses_rating group by courseID');
       return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
        
    }
    
    function get_course_review($course_id)
    {
        $this->db->select('*');
        $this->db->join('courses_rating', 'courses.coursesID = courses_rating.courseID', 'inner');
        $this->db->join('users', 'courses_rating.userID = users.userID', 'inner');
         $where = array(
			     'courses.coursesID' => $course_id,
                 'courses.coursesDeleted' => '0'
                 );
        $this->db->where($where);
        $this->db->from('courses');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    
    function rating($course_id,$rating_val,$userId,$review_description)
    {
        $this->db->select('*');
        $where = array(
			     'courses_rating.courseID' => $course_id,
                 'courses_rating.userID' => $userId,
                 'courses_rating.ratingDeleted' => '0'
                 );
         $this->db->where($where);
        $this->db->from('courses_rating');
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
           $data = array(
					'courseID' => $course_id,
					'userID' => $userId,
					'ratingValue' => $rating_val,
					'reviewDescription' => $review_description,
					'createdDate' => date('Y-m-d'),
					'modifiedDate' => date('Y-m-d H:i:s'),
					'ratingDeleted' => '0'
				); 
           $data = $this->security->xss_clean($data);
            $this->db->where($where);
            $result = $this->db->update('courses_rating', $data);
            return $result;
        }else{
            $data = array(
					'courseID' => $course_id,
					'userID' => $userId,
					'ratingValue' => $rating_val,
                    'reviewDescription' => $review_description,
					'createdDate' => date('Y-m-d'),
					'modifiedDate' => date('Y-m-d H:i:s'),
					'ratingDeleted' => '0'
				);
            $this->db->insert('courses_rating', $data);
            $last_id = $this->db->insert_id();
            return $last_id;
        }
        
        
    }
    
    function mycourses($userid)
    {
        $this->db->select('*');
        $this->db->join('employee_courses', 'courses.coursesID = employee_courses.coursesID', 'inner');
         $this->db->join('slugs', 'courses.coursesID = slugs.id', 'inner');
        $where = array(
			     'employee_courses.employeeID' => $userid,
                 'courses.coursesDeleted' => '0'
                 );
        $this->db->where($where);
        $this->db->from('courses');
		$this->db->order_by('courses.displayName', 'ASC');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

	function educator_courses($userid)
    {
        $this->db->select('*');
        $this->db->join('course_category', 'courses.coursesID = course_category.courseID', 'LEFt');
        $this->db->join('category', 'course_category.categoryID = category.categoryID', 'LEFt');
        $where = array(
			     'employee_courses.employeeID' => $userid,
                 'courses.coursesDeleted' => '0'
                 );
        $this->db->where($where);
        $this->db->from('courses');
        $this->db->group_by('courses.coursesID');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    
    function get_course_summary($course_id)
    {
        $query = $this->db->query("SELECT
            NULL as 'assigned_courses',
            `courses_rating`.`courseID` as 'review_courses',
            NULL as 'Total Assignee',
            count(`courses_rating`.`ratingValue`) as 'Total_Reviews',
            avg(`courses_rating`.`ratingValue`) as 'Average_Rating'

            FROM
            `courses_rating`
            WHERE courses_rating.courseID = '".$course_id."'
            GROUP BY courses_rating.courseID

            UNION

            SELECT
            `employee_courses`.`coursesID`,
            NULL,
            count(`employee_courses`.`employeeID`) as 'Total_Assignee',
            NULL,NULL

            FROM
            `employee_courses`
            WHERE employee_courses.coursesID = '".$course_id."'
            GROUP BY employee_courses.coursesID");
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    
    function update_total_assign($assgin_courses_id,$total_course_assign)
    {
        /*
        * Getting Authors and Minus them from
        * the Total Assign courses
        */
        $this->db->select('*');
        $where2 = array(
            "coursesID" => $assgin_courses_id,
            "authorDeleted" => '0'
        );
        $this->db->where($where2);
        $this->db->from('co_author');
        $query2 = $this->db->get();
        $author_num = $query2->num_rows();
        /*
            END of the count
        */
        if($total_course_assign > $author_num)
        {
            //  'totalAssign' => $total_course_assign - $author_num,
            $data = array(
                'coursesID' => $assgin_courses_id,
                'totalAssign' => $total_course_assign,
                'summaryDeleted'=> 0
            );
        }else
        {
            $data = array(
                'coursesID' => $assgin_courses_id,
                'totalAssign' => $total_course_assign,
                'summaryDeleted'=> 0
            );
        }
        
        
        $this->db->select('*');
        $where = array(
			     'course_summary.coursesID' => $assgin_courses_id
                 );
        $this->db->where($where);
        $this->db->from('course_summary');
        $query = $this->db->get();
        
        if($query->num_rows() > 0)
        {
            $this->db->where($where);
            $result = $this->db->update('course_summary', $data);
            return $result; 
        }
        else
        {
            $this->db->insert('course_summary', $data);
            $last_id = $this->db->insert_id();
            return $last_id; 
        }
    }
    
    function update_course_rating($review_course_id,$total_reviews_course,$average_rating_course)
    {
        $data = array(
            'coursesID' => $review_course_id,
            'totalReviews' => $total_reviews_course,
            'averageRating'=>$average_rating_course,
            'summaryDeleted'=> 0
        );
        
        $this->db->select('*');
        $where = array(
			     'course_summary.coursesID' => $review_course_id
                 );
        $this->db->where($where);
        $this->db->from('course_summary');
        $query = $this->db->get();
        
        if($query->num_rows() > 0)
        {
            $this->db->where($where);
            $result = $this->db->update('course_summary', $data);
            return $result; 
        }
        else
        {
            $this->db->insert('course_summary', $data);
            $last_id = $this->db->insert_id();
            return $last_id; 
        }
    }
	
	function course_assigned($user_id,$course_id)
	{
		$this->db->select('*');
        $where = array(
			     'employee_courses.employeeID' => $user_id,
			     'employee_courses.coursesID' => $course_id
                 );
        $this->db->where($where);
        $this->db->from('employee_courses');
        $query = $this->db->get();
		
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
	}
	
	function get_user_profile($user_id)
	{
		$this->db->select('*');
        $where = array(
			     'users.userID' => $user_id
                 );
        $this->db->where($where);
        $this->db->from('users');
        $query = $this->db->get();
        
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
	}
	
	function search($name)
	{
		$this->db->select('*');
		$this->db->like('displayName',$name,'both');
		$where = array(
			     'courses.coursesStatus' => 'publish',
                 'courses.coursesDeleted' => '0'
                 );
        $this->db->where($where);
		return $this->db->get('courses')->result();
	}
	
	function get_instructor($instructor_name)
	{
		$this->db->select('users.userID');
        $where = array(
			     'users.userName' => $instructor_name
                 );
        $this->db->where($where);
        $this->db->from('users');
        $query = $this->db->get();
        
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
	}
    
    function get_courses_search($course_name)
	{
		$this->db->select('courses.coursesID');
        $where = array(
			     'courses.displayName' => $course_name
                 );
        $this->db->where($where);
        $this->db->from('courses');
        $query = $this->db->get();
        
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
	}
	
	function latest_educator()
	{
		$this->db->select('*');
		 $where = array(
			     'users.userType' => 'instructor',
				 'users.userDeleted' => 0
                 );
        $this->db->where($where);
        $this->db->from('users');
		$this->db->limit(3); 
        $query = $this->db->get();
		
        
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
		
	}
    
    function get_selections($course_id)
	{
		$this->db->select('category.categoryName,course_category.categoryID');
        $this->db->join('category', 'course_category.categoryID = category.categoryID', 'LEFt');
        $where = array(
                 'course_category.courseID' => $course_id,
			     'course_category.deleted' => 0
                 );
        $this->db->where($where);
        $this->db->from('course_category');
        $query = $this->db->get();
		
        
		return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
		
	}
	
    function total_courses($userid)
    {
         $query = $this->db->query('SELECT
            co_author.coursesID,
            co_author.createdDate,
            co_author.modifiedDate,
            Count(co_author.userID) as total_courses,
            users.userID
            FROM
            users
            INNER JOIN co_author ON users.userID = co_author.userID
            INNER JOIN courses ON co_author.coursesID = courses.coursesID
            WHERE users.userID = "'.$userid.'" AND courses.coursesStatus = "publish" AND courses.coursesDeleted = "0"
            GROUP BY co_author.userID'
        );
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    
	function total_earnings($userid)
	{
		 $query = $this->db->query('SELECT

			Sum(educatorEarning.earnings) as total_earning
			FROM
			educatorEarning
			WHERE educatorEarning.authorID = "'.$userid.'"'
		);
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
	}
    
    function check_author($user_id,$course_id)
    {
         $this->db->select('courses.coursesName,
            courses.coursesID,
            courses.displayName,
            users.userName,
            users.userID,
            courses.coursesDescription,
            courses.price,
            courses.coursesImage,
            users.fileName,
            users.userType,
            users.userCompanyName,
            users.userPhoneNo,
            users.userEmail,
            users.bio,
            users.designation,
            users.country,
            users.ceCourseID');
         $this->db->join('co_author', 'courses.coursesID = co_author.coursesID', 'LEFt');
         $this->db->join('users', 'co_author.userID = users.userID', 'LEFt');
         $where = array(
			     'courses.coursesID' => $course_id,
			     'co_author.userID' => $user_id,
                 'courses.coursesDeleted' => '0'
                 );
         $this->db->where($where);
         $this->db->from('courses');

         $query = $this->db->get();
         return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    function get_user_id($user_name)
    {
        $this->db->select('*');
         $where = array(
                 'users.userName' => $user_name,
                 'users.userDeleted' => 0
                 );
        $this->db->where($where);
        $this->db->from('users');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    function get_course_lessons($courseid)
    {
        $this->db->select('*');
         $where = array(
                 'courses_lesson.courseID' => $courseid,
                 'courses_lesson.deleted'  => '0'
                 );
        $this->db->where($where);
        $this->db->from('courses_lesson');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;   
    }

    function get_course_id_by_slug($course_slug)
    {
        $this->db->select('*');
         $where = array(
                 'slugs.uri' => $course_slug
                 );
        $this->db->where($where);
        $this->db->from('slugs');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;  
    } 

    function get_user_meta($user_id)
    {
        $this->db->select('*');
        $this->db->join('users_meta', 'users.userID = users_meta.userID', 'INNER');
        $this->db->join('users_key', 'users_meta.userMetaKey = users_key.keyID', 'INNER');
        $where = array(
                'users.userID' => $user_id
            );
        $this->db->where($where);
        $this->db->order_by('users_meta.userMetaKey','ASC');
        $this->db->from('users');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;  

    }  

    function get_average_rating($course_id)
    {
        $this->db->select('*');
         $where = array(
                'course_summary.coursesID' => $course_id
            );
        $this->db->where($where);
        $this->db->from('course_summary');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;  

    }
    function get_assigned_courses_slp($user_id = null)
    {
        $this->db->select('employee_courses.coursesID');
        $where = array(
                "employee_courses.employeeID" =>  $user_id
        );
        $this->db->where($where);
        $this->db->from('employee_courses');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;  
    }
    function get_category_by_name($category_name)
    {
        $this->db->select('*');
        $where = array(
                "slug" => $category_name
        );
        $this->db->where($where);
        $this->db->from('category');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;  
    }

    function get_course_list()
    {
        $this->db->select('courses.coursesID,slugs.uri,courses.displayName');
        $this->db->join('slugs', 'courses.coursesID = slugs.id', 'INNER');
        $where = array(
                "coursesStatus" => "publish",
                "coursesDeleted" => 0
        );
        $this->db->where($where);
        $this->db->from('courses');
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    function sync()
    {
        $query = $this->db->query('SELECT
        `zoom_credentials`.`registrant_id`,
        `zoom_credentials`.`topic`,
        `employee_courses`.`coursesID`,
        `employee_courses`.`employeeID`,
        `users`.`userName`,
        `users`.`firstName`,
        `users`.`lastName`,
        `users`.`fileName`
        FROM
        `employee_courses`
        LEFT JOIN `zoom_credentials`
        ON `employee_courses`.`employeeID` = `zoom_credentials`.`userID` 
        LEFT JOIN `users`
        ON `employee_courses`.`employeeID` = `users`.`userID`
        WHERE
        `zoom_credentials`.`courseID` is null AND
        `users`.userName is not null');
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    function update_asha_summary($course_id, $offering_id, $data)
    {
        $this->db->select("*");
        $where = array(
            "offeringID" => $offering_id,
            "coursesID" => $course_id
        );
        $this->db->where($where);
        $this->db->from('course_offering');
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            $this->db->where($where);
            $this->db->update('course_offering', $data);
        }else
        {
            $this->db->insert('course_offering', $data);
        }
    }
}