<?php

class Quiz_model extends CI_Model {

	public function __construct() {
		parent::__construct();

	}

//	function get_courses()
//	{
//		$category = array();
//		$courses = array();
//		$this->db->select('categoryID, categoryName');
//		$this->db->from('category');
//		$this->db->order_by('categoryID','ASC');
//		$query = $this->db->get();
//		if($query->num_rows() >0)
//		{
//			$row = $query->result_array();
//			for($i = 0; $i<count($row); $i++)
//			{
//				$category["catId"] = $row[$i]["categoryID"];
//				$category["catName"] = $row[$i]["categoryName"];
//				$this->db->select('coursesID , categoryID , coursesName, displayName ');
//				$where = array (
//								'categoryID' => $row[$i]["categoryID"],
//						       );
//				$this->db->where($where);
//				$query = $this->db->get('courses');
//				$category["courseData"] = $query->result_array();
//				array_push($courses , $category);
//			}
//		}
//		return (count($courses) > 0)?$courses:FALSE;
//	}
    
    function get_courses($instructor_id)
    {
        if($instructor_id != null)
        {
            $this->db->select('*');
            $this->db->join('co_author', '`courses`.`coursesID` = `co_author`.`coursesID`','left');
            $this->db->join('course_category', '`courses`.`coursesID` = `course_category`.`courseID`','left');
            $this->db->join('quizzes', '`courses`.`coursesID` = `quizzes`.`courseID`','left');
            $where = array (
                                'co_author.userID' => $instructor_id,
								'courses.coursesDeleted' => 0
                            );
            $this->db->where($where);
			$this->db->from('courses');
            $this->db->group_by('courses.coursesID','ASC');
            $query = $this->db->get();
            $row = $query->result_array();
            return $row;
        }else{
            $this->db->select('*');
            $where = array(
                "coursesDeleted" => '0'
            );
            $this->db->where($where);
            $query = $this->db->get('courses');
            $row = $query->result_array();
            return $row;
        }
    }

	public function get_quizes_list($params = array())
	{
		$this->db->select('*');
		$this->db->from('quizzes');
				$where = array (
							'quizDeleted' => '0'
						);
		$this->db->where($where);
		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function add_quiz($data , $quiz_id)
	{
		$response = array();
		if($quiz_id > 0)
		{
			$this->db->where('quizID', $quiz_id);
			if($this->db->update('quizzes', $data))
			{
				$response['status'] = true;
			}
			else
			{
			 $response['status'] = false;
			}
			$response['record_id'] = $quiz_id;
		}
		else
		{
			$this->db->select('*');
			$this->db->from('quizzes');
			
			$where = array (
					'courseID' => $data['courseID'],
					'quizDeleted' => '0'
				);
			
			$this->db->where($where);
			
			$query = $this->db->get();
			$results = $query->result_array();
			
			if($query->num_rows() > 0)
			{
				$this->db->where('courseID', $data['courseID']);
				if($this->db->update('quizzes', $data))
				{
					$response['status'] = true;
				}
				else
				{
				 $response['status'] = false;
				}
				$response['record_id'] = $results[0]['quizID'];
			}else
			{
				if($this->db->insert('quizzes', $data))
				{
					$response['status'] = true;
					$response['record_id'] = $this->db->insert_id();
				}
				else
				{
				  $response['status'] = false;
				  $response['record_id'] = '';
				}
			}
		}
		return 	$response;
	}
	/////////////////////////////////////////Added By Mohsin On 4/9/2016 to add question for a specific quiz///////////////////////
	function add_question($data , $record_id)
	{
		$response = array();
		if($record_id > 0)
		{
			$this->db->where('questionID', $record_id);
			if($this->db->update('quizzes_question', $data))
			{
				$response['status'] = true;
			}
			else
			{
			 $response['status'] = false;
			}
			$response['record_id'] = $record_id;
		}
		else
		{
			$this->db->select('*');
			$where = array(
				"quizID" => $data['quizID'],
				"questionName" => $data['questionName'],
				"questionDeleted" => 0
				);
			$this->db->where($where);
			$this->db->from('quizzes_question');

			$query = $this->db->get();
			
			if($query->num_rows() <= 0)
			{
				if($this->db->insert('quizzes_question', $data))
				{
					$response['status'] = true;
					$response['record_id'] = $this->db->insert_id();
				}
				else
				{
				  $response['status'] = false;
				  $response['record_id'] = '';
				}
			}else
			{
				$response['status'] = false;
				$response['record_id'] = '';
			}
			
		}
		return 	$response;
	}

	/////////////////////////////////////////Added By Mohsin On 4/9/2016 to add answer for a specific question///////////////////////
	function add_answer($data , $record_id)
	{
		$response = array();
		if($record_id > 0)
		{
			$this->db->where('answerID', $record_id);
			if($this->db->update('quizzes_answer', $data))
			{
				$response['status'] = true;
			}
			else
			{
			 $response['status'] = false;
			}
			$response['record_id'] = $record_id;
		}
		else
		{
			if($this->db->insert('quizzes_answer', $data))
			{
				$response['status'] = true;
				$response['record_id'] = $this->db->insert_id();
				$questionsArray = array(
				"isAdded"=>1
				);
				$this->db->where('questionID', $data['questionID']);
				$this->db->update('quizzes_question', $questionsArray);
			}
			else
			{
			  $response['status'] = false;
			  $response['record_id'] = '';
			}
		}
		return 	$response;
	}


	/////////////////////////////////////////Added By Mohsin On 4/9/2016 to add answer for a specific question///////////////////////
	function save_quiz_ans($data,$courID,$userID,$quesID)
	{
		$response = array();
        
        $this->db->select('*');
        $where = array(
           'courseID' => $courID,
           'userID' => $userID,
           'questionID' => $quesID
        );
        $this->db->where($where);
        $query = $this->db->get('users_answers');
        if ($query->num_rows() > 0) {
                
                $row = $query->result_array();
                $this->db->where($where);
                $query1 = $this->db->update('users_answers', $data);
                if($query1)
                {
                    $response['status'] = true;
                    $response['record_id'] = $row[0]['userAnsId'];   
                }else
                {
                    $response['status'] = false;
                    $response['record_id'] = '';
                }
               
        } else{
                if($this->db->insert('users_answers', $data))
                {
                    $response['status'] = true;
                    $response['record_id'] = $this->db->insert_id();
                }
                else
                {
                  $response['status'] = false;
                  $response['record_id'] = '';
                }   
        }
		return 	$response;
	}
	/////////////////////////////////////////Added By Mohsin On 4/9/2016 to get questions///////////////////////

	function get_all_question($question_id = '' , $quiz_id = '' , $isAdded = '' , $isEdit = '')
	{
		$added = '';
		if($isAdded == 1)
		{
			$added = 1;
		}
		else
		{
			$added = 0;
		}

		$this->db->select('*');
		$this->db->from('quizzes_question');
		$where = '';
		if($question_id > 0)
		{
		$where = array (
							'questionDeleted' => '0',
							'questionID' => $question_id
						);
		}
		else
		{
			if($isEdit == 'yes')
			{
				$where = array (
						'questionDeleted' => '0',
						'quizID'=>$quiz_id
					);
			}
			else
			{
			$where = array (
							'questionDeleted' => '0',



							'quizID'=>$quiz_id
						);
			}
			///	'isAdded' => $added,
		}
		$this->db->where($where);
		$this->db->order_by('questionID','desc');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	/////////////////////////////////////////Added By Mohsin On 4/9/2016 to get questions///////////////////////
	function get_ans_detail($question_id = '' , $quiz_id)
	{
		$this->db->select('*');
		$this->db->from('quizzes_answer');
		$where = '';

		$where = array (
							'answerDeleted' => '0',
							'questionID' => $question_id,
							'quizIDAnswer'=>$quiz_id
						);

		$this->db->where($where);
		$this->db->order_by('questionID','desc');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	/////////////////////////////////////////Added By Mohsin On 4/9/2016 to get questions///////////////////////
	function delete_answer($ans_value , $ans_id , $selectedAns)
	{
		$this->db->select('*');
		$this->db->from('quizzes_answer');
		$where = '';
		$where = array (
							'answerID'=>$ans_id
						);
		$this->db->where($where);
		$query = $this->db->get();
		$result = $query->result_array();
		// if($result[0]['correctAnswers'] ==  $selectedAns)
		// {
		// 	return 'cant_delete';
		// }
		// else
		// {
			$answers = explode(",", $result[0]['answersName']);
			if(($key = array_search($ans_value, $answers)) !== false) {
				unset($answers[$key]);
			}
			$ans_string =  implode(",",$answers);

			$data = array(
			"answersName"=>$ans_string
			);
			$this->db->where('answerID', $ans_id);
			if($this->db->update('quizzes_answer', $data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		// }
	}


	/////////////////////////////////////////Added By Mohsin On 4/9/2016 to get questions///////////////////////
	function delete_question($questionId)
	{
		$data = array(
		"answerDeleted"=>1
		);
		$this->db->where('questionID', $questionId);
		if($this->db->update('quizzes_answer', $data))
		{
			$data = array(
			"questionDeleted"=>1
			);
			$this->db->where('questionID', $questionId);
			if($this->db->update('quizzes_question', $data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}

	/////////////////////////////////////////Added By Mohsin On 5/9/2016 to get questions Against quiz Id///////////////////////
	function get_questions_by_quiz_id($quiz_id = '')
	{
		$this->db->select('*');
		$this->db->from('quizzes_question');
		$where = '';
		$where = array (
							'questionDeleted' => '0',
							'quizID' => $quiz_id
						);

		$this->db->where($where);
		$this->db->order_by('questionID','desc');
		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}


	/////////////////////////////////////////Added By Mohsin On 5/9/2016 to get answer Against question Id///////////////////////
	function get_answer_by_question_id($question_id = '')
	{
		$this->db->select('*');
		$this->db->from('quizzes_answer');
		$where = '';
		$where = array (
							'answerDeleted' => '0',
							'questionID' => $question_id
						);

		$this->db->where($where);
		$this->db->order_by('answerID','desc');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}


	/////////////////////////////////////////Added By Mohsin On 5/9/2016 to get quizes///////////////////////
	function delete_quiz($quiz_id = '')
	{
		$questionsData = array(
			'questionDeleted' => '1'
		);
		$this->db->where('quizID', $quiz_id);
		$this->db->update('quizzes_question', $questionsData);

		$answersData = array(
			'answerDeleted' => '1'
		);

		$this->db->select('*');
		$this->db->from('quizzes_answer');
		$this->db->join('quizzes_question', 'quizzes_answer.questionID = quizzes_question.questionID');
		$where = '';
		$where = array (
							'questionDeleted' => '0',
							'answerDeleted' => '0',
							'quizID' => $quiz_id
						);

		$this->db->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			while($row = $query->result_array())
			{
				$this->db->where(questionID , $row['questionID']);
				$this->db->update('quizzes_answer', $answersData);
			}
		}



		$questionsData = array(
			'quizDeleted' => '1'
		);
		$this->db->where('quizID', $quiz_id);
		if($this->db->delete('quizzes'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	}


	/////////////////////////////////////////Added By Mohsin On 5/9/2016 to get quizes///////////////////////
	function delelte_quiz($quiz_id = '')
	{

		$names = array(4,5);
		$this->db->where_in('id', $names);
		$this->db->delete('mytable');
		 $this->db->where('id', $id);
  		 $this->db->delete('testimonials');
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	/////////////////////////////////////////Added By Mohsin On 5/9/2016 to get quizes///////////////////////
	function get_quizes($quiz_id = '',$user_id = '')
	{
		$this->db->select('quizzes.* , courses.coursesName, courses.displayName');
		$this->db->from('quizzes');
		$this->db->join('courses', 'quizzes.courseID = courses.coursesID','left');
		$this->db->join('co_author', 'courses.coursesID = co_author.coursesID','left');

		$where = '';
		if($quiz_id > 0)
		{
		$where = array (
							'quizDeleted' => '0',
							'quizID' => $quiz_id,
							'co_author.userID' => $user_id
						);
		}
		else
		{
			$where = array (
							'quizzes.quizDeleted' => '0',
                            'co_author.userID' => $user_id
						);
		}
		$this->db->where($where);
		$this->db->order_by('quizID','desc');

		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

		/////////////////////////////////////////Added By Mohsin On 5/9/2016 to get quizes///////////////////////
	function get_quizes_count()
	{
		$where = array (
					'quizDeleted' => 0
				);
		$this->db->where($where);
		$this->db->from('quizzes');
		return $this->db->count_all_results();
	}

	/////////////////////////////////////////Added By Mohsin On 5/9/2016 to get quizes///////////////////////
	function get_quize_by_course($quiz_id = '' , $course_id)
	{

		$this->db->select('quizzes.*');
		$this->db->from('quizzes');

		$where = '';
		if($quiz_id > 0)
		{
		$where = array (
							'quizDeleted' => '0',
							'quizID' => $quiz_id,
							'courseID' => $course_id
						);
		}
		else
		{
			$where = array (
							'quizDeleted' => '0'
						);
		}
		$this->db->where($where);
		// $this->db->order_by('quizID','desc');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}



	/////////////////////////////////////////Added By Mohsin On 5/9/2016 to get quizes///////////////////////
	function get_question_answer_by_quiz_id($quiz_id = '' , $order = '' , $limit='' , $question_id="")
	{
		//echo $quiz_id. $order. $limit .$question_id;
		$this->db->select('quizzes_question.questionName,quizzes_question.quizID, quizzes_question.questionScore, quizzes_question.questionType, quizzes_question.questionID, quizzes_question.questionDescription, quizzes_answer.answerID, quizzes_answer.quizIDAnswer, quizzes_answer.answersName, quizzes_answer.correctAnswers');
		$this->db->from('quizzes_question');
		$this->db->join('quizzes_answer', 'quizzes_question.questionID = quizzes_answer.questionID' , 'left');
		$where = '';
		if($question_id > 0)
		{
		$where = array (
							'quizID' => $quiz_id,
							'quizzes_question.questionID >' => $question_id,
							'quizzes_answer.answerDeleted' => '0'
						);
		}
		else
		{
			$where = array (
							'quizID' => $quiz_id,
							'quizzes_question.questionDeleted' => '0'
						);
		}
		$this->db->where($where);
		$order_by = '';
		if(isset($order) && $order != '')
		{
			$order_by = 'ASC';
		}
		else
		{
			$order_by = 'DESC';
		}

		$limit_offset = '';
		if(isset($limit) && $limit != '')
		{
			$limit_offset = $limit;
		}
		else
		{
			$limit_offset = '';
		}

		$this->db->order_by('quizzes_question.questionID',$order_by);
		$this->db->limit($limit_offset);
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function get_questions_count($quiz_id)
	{
		$where = array (
							'quizID' => $quiz_id,
							'quizzes_question.questionDeleted' => 0
						);

		$this->db->where($where);
		$this->db->from('quizzes_question');
		return $this->db->count_all_results();
	}


	function get_employee($params = array())
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->order_by('userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function get_employee_by_id($company_id)
	{
		$this->db->select('*');
		$where = array (
						'userID' => $company_id,
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('userID','desc');

			$this->db->limit(1);


		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function get_passing_criteria($course_id)
	{
		$this->db->select('passingCriteria');
		$where = array (
						'coursesID' => $course_id,
					);
		$this->db->where($where);
		$this->db->from('courses');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function show_report($user_id , $quiz_id , $course_id)
	{
		$this->db->select('*');
		$this->db->join('courses', 'courses.coursesID = users_answers.courseID' , 'left');
		$this->db->join('slugs', 'courses.coursesID = slugs.id', 'left');
		$this->db->join('category', 'category.categoryID = courses.categoryID' , 'left');
		$this->db->from('users_answers');
		$where = array (
							'quizID' => $quiz_id,
							'userID' => $user_id,
							'courseID' => $course_id
						);

		$this->db->where($where);
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
    
    function show_storyline_report($user_id , $course_id)
	{
		$this->db->select('*');
		$this->db->join('storyline_answers', 'courses.coursesID = storyline_answers.coursesID' , 'left');
		$this->db->from('courses');
		$where = array (
							'storyline_answers.userID' => $user_id,
							'storyline_answers.coursesID' => $course_id,
							'storyline_answers.answerDeleted' => '0'
						);

		$this->db->where($where);
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function check_lesson_quiz($course_id)
	{
		$this->db->select('*');
		$this->db->from('courses_lesson');
		$where = array (
							'courses_lesson.courseID' => $course_id,
							'courses_lesson.hasQuiz' => '1',
							'courses_lesson.deleted' => '0'
						);

		$this->db->where($where);
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}

	function check_user_quiz($user_id, $lesson_id)
	{
		$this->db->select('*');
		$this->db->from('storyline_answers');
		$where = array (
							'storyline_answers.userID' => $user_id,
							'storyline_answers.lessonID' => $lesson_id,
						);

		$this->db->where($where);
		$this->db->group_by('storyline_answers.lessonID','ASC');
		$query = $this->db->get();
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
    
    function get_total_marks($quiz_id)
    {
        $query = $this->db->query('SELECT
        sum(quizzes_question.questionScore) as totalScore
        FROM
        `quizzes`
        JOIN `quizzes_question`
        ON `quizzes`.`quizID` = `quizzes_question`.`quizID`
        where quizzes.quizID = "'.$quiz_id.'"');
        
        return $query->result_array();
    }
    
    function insert_report($data,$user_id,$quiz_id,$course_id)
    {
        $this->db->select('*');
        $where = array (
                        'quizId' => $quiz_id,
                        'userId' => $user_id,
                        'courseId' => $course_id
				);
        $this->db->where($where);
        $this->db->from('quizess_report');
        $query = $this->db->get();
        $for_raw_data = $query->result_array();
        if($query->num_rows() > 0)
        {
        	$check_result_highest = $query->result_array();
        	$highest_score = $check_result_highest[0]['obtainedPercentage'];
        	if($highest_score < $data['obtainedPercentage'])
        	{
        		$this->db->where($where);
	            $rows = $this->db->update('quizess_report', $data);
	            return $for_raw_data[0]['reportId'];
        	}else
        	{
        		return true;
        	}
        }else
        {
            $rows = $this->db->insert('quizess_report', $data);
            $last_insert_id = $this->db->insert_id();
            return $last_insert_id;
        }
    }
    
    function get_wrong_answers($course_id, $user_id, $quiz_id)
    {
    	$query = $this->db->query("SELECT
		`users`.`userID`,
		`users_answers`.`questionID`,
		`users_answers`.`selectedAns`,
		`quizzes_answer`.`quizIDAnswer`,
		`quizzes_answer`.`correctAnswers`,
		`users_answers`.`courseID`
		FROM
		`users`
		JOIN `users_answers`
		ON `users`.`userID` = `users_answers`.`userID` 
		JOIN `quizzes_answer`
		ON `users_answers`.`questionID` = `quizzes_answer`.`questionID`
		WHERE `users_answers`.`courseID` ='".$course_id."' AND users.userID = '".$user_id."' AND quizzes_answer.quizIDAnswer = '".$quiz_id."'");
		return $query->result_array();
    }

    function hide_quiz($quiz_id, $course_id)
    {
    	$this->db->select('*');
    	$where = array(
    		"quizID" => $quiz_id,
    		"courseID" => $course_id,
    	);
    	$this->db->where($where);
    	$this->db->from('quizzes');

    	$query2 = $this->db->get();

    	if($query2->num_rows() > 0)
    	{
    		$this->db->where($where);
    		$this->db->set('hideQuiz', '1');
    		$query = $this->db->update('quizzes');
    		return $query;
    	}else{
    		return false;
    	}
    }
	function unhide_quiz($quiz_id, $course_id)
	{
    	$this->db->select('*');
    	$where = array(
    		"quizID" => $quiz_id,
    		"courseID" => $course_id,
    	);
    	$this->db->where($where);
    	$this->db->from('quizzes');

    	$query2 = $this->db->get();

    	if($query2->num_rows() > 0)
    	{
    		$this->db->where($where);
    		$this->db->set('hideQuiz', '0');
    		$query = $this->db->update('quizzes');
    		return $query;
    	}else{
    		return false;
    	}
	}

	function get_date_from_quiz($user_id, $quiz_id, $course_id)
	{
		$this->db->select('*');
        $where = array (
                        'quizId' => $quiz_id,
                        'userId' => $user_id,
                        'courseId' => $course_id
				);
        $this->db->where($where);
        $this->db->from('quizess_report');
        $query = $this->db->get();

        return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
}

