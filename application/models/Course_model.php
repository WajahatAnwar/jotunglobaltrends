<?php

class Course_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

    }

    function create_course($data)
    {

            $this->db->insert('courses', $data);
            $last_id = $this->db->insert_id();
            
            // $this->db->insert_batch('courses_meta', $data3);
            return $last_id;

//        }

        // return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function insertion_course_meta($course_id, $data)
    {
        $this->db->select('*');
        $where = array(
            'courseID' =>  $course_id
            );
        $this->db->where($where);
        $query = $this->db->get('courses_meta');

        if($query->num_rows() > 0)
        {
            $this->db->where($where);
            return $this->db->update_batch('courses_meta', $data,'courseMetaKey');
        }else
        {
            $this->db->insert_batch('courses_meta', $data);
            return $this->db->insert_id();
        }
        
    }

    function update_course_info($course_id, $data)
    {
        $this->db->select('*');
        $where = array(
            'courseID' =>  $course_id,
            'courseMetaKey' => $data['courseMetaKey']
            );
        $this->db->where($where);
        $query = $this->db->get('courses_meta');
        
        if($query->num_rows() > 0)
        {
            $this->db->where($where);
            return $this->db->update('courses_meta', $data);
        }else{
            $this->db->insert("courses_meta", $data);
        }
    }

    function create_coauthor($co_author,$courseid,$author)
    {
        if(!empty($co_author) || $co_author != 0 )
        {
           $this->db->select('*');
            $where = array(
                'coursesID' => $courseid,
                'userID' => $co_author
            );
            $this->db->where($where);
            $query = $this->db->get('co_author');

            if ($query->num_rows() > 0) {
                $row = $query->result_array();
                return false;
            } else {
                $data   = array(
                                array(
                                        'coursesID' => $courseid,
                                        'userID' => $co_author,
                                        'createdDate'   => date('Y-m-d h:i:s'),
                                        'modifiedDate'  => date('Y-m-d h:i:s'),
                                        'authorDeleted' => 0
                                ),array(
                                        'coursesID' => $courseid,
                                        'userID' => $author,
                                        'createdDate'   => date('Y-m-d h:i:s'),
                                        'modifiedDate'  => date('Y-m-d h:i:s'),
                                        'authorDeleted' => 0
                                ),
                );

                $last_id = $this->db->insert_batch('co_author', $data);
                return $last_id;

            }
        }else
        {
           $this->db->select('*');
            $where = array(
                'coursesID' => $courseid,
                'userID' => $author
            );
            $this->db->where($where);
            $query = $this->db->get('co_author');

            if ($query->num_rows() > 0) {
                $row = $query->result_array();
                return false;
            } else {
                $data   = array(
                                        'coursesID' => $courseid,
                                        'userID' => $author,
                                        'createdDate'   => date('Y-m-d h:i:s'),
                                        'modifiedDate'  => date('Y-m-d h:i:s'),
                                        'authorDeleted' => 0
                                );

                $last_id = $this->db->insert('co_author', $data);
                return $last_id; 
            }
        }
    }
    
    function add_coauthor($user_id, $course_id)
    {
            $this->db->select('*');
            $where = array(
                'coursesID' => $course_id ,
                'userID' => $user_id
            );
            $this->db->where($where);
            $query = $this->db->get('co_author');

            if ($query->num_rows() > 0) {
                
                $this->db->where($where);
                $this->db->set('authorDeleted', 0);
                $query = $this->db->update('co_author');
                return true;
                
            } else {
                $data   = array(
                                'coursesID'     => $course_id,
                                'userID'        => $user_id,
                                'createdDate'   => date('Y-m-d h:i:s'),
                                'modifiedDate'  => date('Y-m-d h:i:s'),
                                'authorDeleted' => 0
                                );

                $last_id = $this->db->insert('co_author', $data);
                return $last_id; 
            }
    }
    
    function delete_author($user_id, $course_id)
    {       
            $where = array(
                'coursesID' => $course_id ,
                'userID' => $user_id
            );
            $this->db->where($where);
            $this->db->set('authorDeleted', 1);
            $query = $this->db->update('co_author');
            
            if($query)
            {
                return true;
            }else
            {
                return false;
            }

    }

    function show_course_connection($id)
    {
        $this->db->select('*');
        $this->db->join('courses', 'course_connection.coursesID = courses.coursesID', 'LEFT');
        $where = "course_connection.coursesID = '".$id."' OR course_connection.connectWithID = '".$id."' && course_connection.deleted = 0";
        $this->db->where($where);
        $query = $this->db->get('course_connection');
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

    function show_course_connection2($id)
    {
        $this->db->select('*');
        $this->db->join('courses', 'course_connection.connectWithID = courses.coursesID', 'LEFT');
        $where = "course_connection.connectWithID = '".$id."' && course_connection.deleted = 0";
        $this->db->where($where);
        $query = $this->db->get('course_connection');
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

    function get_connection_for_check($course_id)
    {
        $this->db->select('*');
        $where = array(
            'coursesID' => $course_id,
            'deleted' => 0
        );
        $this->db->where($where);
        $this->db->from('course_connection');
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

    function add_connection($course_id, $connect_with)
    {
            $this->db->select('*');
            $where = array(
                'coursesID' => $course_id ,
                'connectWithID' => $connect_with
            );
            $this->db->where($where);
            $query = $this->db->get('course_connection');

            if ($query->num_rows() > 0) {
                
                $this->db->where($where);
                $this->db->set('deleted', 0);
                $query = $this->db->update('course_connection');
                return true;
                
            } else {
                $data   = array(
                                'coursesID'     => $course_id,
                                'connectWithID' => $connect_with,
                                'createdDate'   => date('Y-m-d h:i:s'),
                                'modifiedDate'  => date('Y-m-d h:i:s'),
                                'deleted' => 0
                                );
                $last_id = $this->db->insert('course_connection', $data);
                return $last_id; 
            }
    }
    
    function get_course_name($course_id)
    {
        $this->db->select('coursesName');
        $where = array(
            'coursesID' => $course_id
        );
        $this->db->where($where);
        $this->db->from('courses');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $rows = $query->result_array();
            return $rows[0]['coursesName'];
        } else {
            return false;
        }
    }
    function add_lesson($data)
    {
        $this->db->select('*');
        $where = array(
            // 'userCompanyName' => $data['userCompanyName'],
            'lessonID' => $data['lessonName']
        );
        $this->db->where($where);
        $query = $this->db->get('courses_lesson');

        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {

            $this->db->insert('courses_lesson', $data);
            $last_id = $this->db->insert_id();
            
            return true;

        }

        // return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    function add_camtasia($data)
    {
        $this->db->select('*');
        $where = array(
            // 'userCompanyName' => $data['userCompanyName'],
            'lessonName' => $data['lessonName']
        );
        $this->db->where($where);
        $query = $this->db->get('courses_lesson');

        if ($query->num_rows() > 0) {
            // $row = $query->result_array();
            return false;
        } else {

            $this->db->insert('courses_lesson', $data);
            $last_id = $this->db->insert_id();
          
            return true;

        }
    }

    function add_youtube($data)
    {
        
        $this->db->insert('courses_lesson', $data);
        $last_id = $this->db->insert_id();
        return true;

    }

    function get_lesson_by_id($course_id, $lesson_type = null)
    {
        $this->db->select('*');
        if(empty($lesson_type) || $lesson_type == null)
        {
            $where = array(
                // 'userCompanyName' => $data['userCompanyName'],
                'courseID' => $course_id
            );
        }else
        {
            $where = array(
                'lessonType' => $lesson_type,
                'courseID' => $course_id
            );
        }
        $this->db->where($where);
        $query = $this->db->get('courses_lesson');

        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {

            // $this->db->insert('courses_lesson', $data);
            // $last_id = $this->db->insert_id();
            return false;

        }

        // return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    function get_course_by_id($course_id)
    {
        $this->db->select('*');
        $this->db->join('courses_meta', 'courses.coursesID = courses_meta.courseID', 'LEFt');
        $this->db->join('course_key', 'courses_meta.courseMetaKey = course_key.keyName', 'LEFt');
        $where = array(
            // 'userCompanyName' => $data['userCompanyName'],
            'courses.coursesID' => $course_id
        );
        $this->db->where($where);
        $this->db->from('courses');
        $this->db->order_by('course_key.ordering', 'ASC');
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {

            return false;

        }

    }

    function get_courses_listing($category_id, $employee_id, $instructor_id , $admin="", $course_status = "", $params = array())
    {
        if(empty($course_status))
        {
            $course_status = "publish";
        }
        if(!empty($instructor_id)){
            if(!empty($admin) && $admin = "assigned")
            {
                $this->db->select('courses.coursesID,
                courses.displayName,
                courses.coursesName,
                category.categoryName,
                category.categoryDescription,
                courses.coursesDescription,
                courses.price,
                courses.coursesImage,
                slugs.uri');
            }else
            {
                $this->db->select('courses.coursesID,
                courses.displayName,
                courses.coursesName,
                category.categoryName,
                category.categoryDescription,
                courses.coursesDescription,
                courses.price,
                courses.coursesImage,
                course_summary.totalAssign,
                course_summary.totalReviews,
                course_summary.averageRating,
                slugs.uri');
            }
        }else{
             $this->db->select('courses.coursesID,
                courses.displayName,
                courses.coursesName,
                category.categoryName,
                category.categoryDescription,
                courses.coursesDescription,
                courses.price,
                courses.coursesImage,
                slugs.uri');
        }
       
        if (!empty($employee_id)) {
            $this->db->join('employee_courses', 'courses.coursesID = employee_courses.coursesID', 'LEFt');
            $where = array(
                'employee_courses.employeeID' => $employee_id,
                'courses.coursesDeleted' => '0',
                'courses.coursesStatus' => $course_status
            );
            $this->db->where($where);
        }else if(!empty($instructor_id)){

                if(!empty($admin) && $admin = "assigned")
                {

                    $this->db->join('employee_courses', 'courses.coursesID = employee_courses.coursesID', 'LEFt');
                    $where = array(
                        'employee_courses.employeeID' => $instructor_id,
                        'courses.coursesDeleted' => '0',
                        'courses.coursesStatus' => $course_status
                    );
                    $this->db->where($where);

                }else
                {
                
                    $this->db->join('co_author', 'courses.coursesID = co_author.coursesID', 'LEFt');
                    $this->db->join('course_summary', 'courses.coursesID = course_summary.coursesID', 'left');
                    
                    $where = array(
                        'co_author.userID' => $instructor_id,
                        'courses.coursesDeleted' => '0'
                    );

                }
            
            $this->db->where($where);
        }

        if (!empty($category_id)) {
            if ($category_id > 0) {

                $this->db->join('course_category', 'courses.coursesID = course_category.courseID', 'left');
                $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt');
                $this->db->join('category', 'course_category.categoryID = category.categoryID', 'left');
                $where = array(
                    'courses.coursesStatus' => $course_status,
                    'category.categoryID' => $category_id,
                    'courses.coursesDeleted' => '0'
                );
                $this->db->where($where);
            }
        } else {
            
            $this->db->join('course_category', 'courses.coursesID = course_category.courseID', 'LEFt');
            $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt');
            $this->db->join('category', 'course_category.categoryID = category.categoryID', 'LEFt');
            
            $where = array(
            'courses.coursesStatus' => $course_status,
                'courses.coursesDeleted' => '0'
            );
            $this->db->where($where);
        }
        $this->db->from('courses');
        $this->db->order_by('courses.coursesID', 'desc');
        $this->db->group_by('courses.coursesID');

        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit'], $params['start']);
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit']);
        }

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /*
        * This query fetch the course which are
        * publish and are still in the drafts
        * return listing
    */

    function get_courses_drafts($category_id, $instructor_id, $course_status, $params = array())
    {
        if(!empty($instructor_id)){
               $this->db->select('courses.coursesID,
                courses.displayName,
                courses.coursesName,
                category.categoryName,
                category.categoryDescription,
                courses.coursesDescription,
                courses.price,
                courses.coursesImage,
                course_summary.totalAssign,
                course_summary.totalReviews,
                course_summary.averageRating,
                slugs.uri');
        }
       
        if(!empty($instructor_id)){
            $this->db->join('co_author', 'courses.coursesID = co_author.coursesID', 'LEFt');
            $this->db->join('course_summary', 'courses.coursesID = course_summary.coursesID', 'left');
            if($this->session->userdata('userType') == "admin")
            {
                $where = array(
                    'courses.coursesStatus' => $course_status,
                    'courses.coursesDeleted' => '0'
                );
            }else
            {
                $where = array(
                    'courses.coursesStatus' => $course_status,
                    'co_author.userID' => $instructor_id,
                    'courses.coursesDeleted' => '0'
                );
            }
            
            
            $this->db->where($where);
        }

        if (!empty($category_id)) {
            if ($category_id > 0) {

                $this->db->join('course_category', 'courses.coursesID = course_category.courseID', 'left');
                $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt');
                $this->db->join('category', 'course_category.categoryID = category.categoryID', 'left');
                $where = array(
                    'courses.coursesStatus' => $course_status,
                    'category.categoryID' => $category_id,
                    'courses.coursesDeleted' => '0'
                );
                $this->db->where($where);
            }
        } else {
            
            $this->db->join('course_category', 'courses.coursesID = course_category.courseID', 'LEFt');
            $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt');
            $this->db->join('category', 'course_category.categoryID = category.categoryID', 'LEFt');
            
            $where = array(
            'courses.coursesStatus' => $course_status,
                'courses.coursesDeleted' => '0'
            );
            $this->db->where($where);
        }
        $this->db->from('courses');
        $this->db->order_by('courses.coursesID', 'desc');
        $this->db->group_by('courses.coursesID');

        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit'], $params['start']);
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit']);
        }

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    
    
    /* 
    * this fucntion bring all the publish courses
    * @return all publish courses array
    * code by wajahat@ikarobar.com
    */

    function get_all_courses($category_id, $params = array())
    {

        $this->db->select('*');
        if (!empty($category_id)) {
            if ($category_id > 0) {

                $this->db->join('category', 'courses.categoryID = category.categoryID', 'LEFt');
                $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt');
                $where = array(
                    'courses.coursesStatus' => 'publish',
                    'courses.categoryID' => $category_id,
                    'courses.coursesDeleted' => '0'
                );
                $this->db->where($where);
            }
        } else {
            $this->db->join('category', 'courses.categoryID = category.categoryID', 'LEFt');
            $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt');
            $where = array(
            'courses.coursesStatus' => 'publish',
                'courses.coursesDeleted' => '0'
            );
            $this->db->where($where);
        }
        $this->db->from('courses');
        $this->db->order_by('courses.coursesID', 'desc');
        // $this->db->order_by('userID','desc');

        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit'], $params['start']);
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit']);
        }

        $query = $this->db->get();
        // print_r($query->result_array()); exit;
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    function get_other_courses()
    {
        $this->db->select('*');
        $employee_id = $this->session->userdata('userID');
        $user_type = $this->session->userdata('userType');
        if ($user_type == 'employee') {
            $this->db->join('employee_courses', 'courses.coursesID = employee_courses.coursesID', 'LEFt');
            $where = array(
                'employeeID' => $employee_id,
                'courses.coursesDeleted' => '0'
            );
            $this->db->where($where);
            $this->db->from('courses');
            $this->db->order_by('courses.coursesID', 'desc');
            $this->db->limit(3);
        }else{
            $this->db->from('courses');
            $this->db->order_by('courses.coursesID', 'desc');
            $this->db->limit(3);
        }
        $query = $this->db->get();
        // print_r($query->result_array()); exit;
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }


    function get_courses_detail($id)
    {

    $user_id = $this->session->userdata('userID');

        $this->db->select('`courses`.`coursesName`,
`courses`.`displayName`,
`courses`.`coursesID`,
`courses`.`categoryID`,
`courses`.`coursesDescription`,
`courses`.`financial_check`,
`courses`.`disclosure_description`,
`courses`.`price`,
`courses`.`coursesImage`,
`courses`.`ceCheck`,
`courses`.`createdDate` AS `createdDate_0`,
`courses`.`modifiedDate`,
`courses`.`coursesDeleted`,
`courses`.`user_id`,
`courses`.`start_time`,
`courses`.`liveCheck`,
`courses`.`coursesStatus`,
`courses_lesson`.`lessonName`,
`courses_lesson`.`displayName` AS `displayName_0`,
`courses_lesson`.`lessonFileName`,
`courses_lesson`.`lessonType`,
`courses_lesson`.`createdDate`,
`courses_lesson`.`modifiedDate` AS `modifiedDate_0`,
`courses_lesson`.`deleted`,
`courses_lesson`.`courseID`,
`courses_lesson`.`lessonID`,
`co_author`.`authorID`,
`co_author`.`coursesID` AS `coursesID_0`,
`co_author`.`userID`,
`co_author`.`createdDate` AS `createdDate_1`,
`co_author`.`modifiedDate` AS `modifiedDate_1`,
`co_author`.`authorDeleted`,
`slugs`.`uri`');
        $this->db->join('co_author', 'courses.coursesID = co_author.coursesID', 'LEFt');
        $this->db->join('courses_lesson', 'courses.coursesID = courses_lesson.courseID', 'LEFt');
        $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt');
        $where = array(
            'courses.coursesID' => $id,
            
        );
        $this->db->where($where);
        $this->db->from('courses');
        $this->db->group_by('courses_lesson.lessonID');


        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    function get_course_quiz($id)
    {
        $this->db->select('quizzes.quizID,quizzes.hideQuiz');
        // $this->db->join('quizzes', 'courses.coursesID = quizzes.courseID', 'LEFt');
        $where = array(
            'courseID' => $id,
            'quizDeleted' => '0'
        );
        $this->db->where($where);
        $this->db->from('quizzes');
       
        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    function get_courses_meta($id)
    {
        $this->db->select('*');
        $where = array(
            'courseID' => $id
        );
        $this->db->where($where);
        $this->db->from('courses_meta');


        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }


    function delete_users($userID)
    {
        $this->db->select('userDeleted');
        $where = array(
            'userID' => $userID
        );
        $this->db->where($where);
        $query = $this->db->get('users');

        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            if ($row[0]['userDeleted'] == 0) {
                $user_deleted = 1;
            } else {
                $user_deleted = 0;
            }
            $this->db->where(array(
                "userID" => $userID
            ));
            $this->db->set('userDeleted', $user_deleted);
            $this->db->update('users');

        }



    }
    function update_course($data, $course_id, $completion, $duration_count, $duration, $co_author)
    {

        $where = array(
            'coursesID' => $course_id
        );
        $where2 = array(
            'courseID' => $course_id,
            'courseMetaKey' =>'Completion'
        );
        $where3 = array(
            'courseID' => $course_id,
            'courseMetaKey' =>'Duration_count'
        );
        $where4 = array(
            'courseID' => $course_id,
            'courseMetaKey' =>'duration'
        );
        $where5 = array(
            'coursesID' => $course_id,
            'userID' => $co_author
        );
        $this->db->where($where);
        $result = $this->db->update('courses', $data);
            $data2   = array(
                    'courseMetaValue' => $completion
                );
                $data3 = array(
                    'courseMetaValue' => $duration_count
                );
                $data4 =  array(
                    'courseMetaValue' => $duration
                );
            $data5 =  array(
                    'coursesID' => $course_id,
                    'userID' => $co_author,
                    'modifiedDate' => date('Y-m-d')
                );
            $data6 =  array(
                    'coursesID' => $course_id,
                    'userID' => $co_author,
                    'createdDate' => date('Y-m-d'),
                    'modifiedDate' => date('Y-m-d'),
                    'authorDeleted' => '0'
                );
            $this->db->where($where2);
            $this->db->update('courses_meta', $data2);
            $this->db->where($where3);
            $this->db->update('courses_meta', $data3);
            $this->db->where($where4);
            $this->db->update('courses_meta', $data4);
            if(!empty($co_author) || $co_author != 0 )
            {
                $this->db->where($where5);
                $this->db->from('co_author');
                $query = $this->db->get();

                $row = $query->result_array();

                if(!empty($row))
                {
                    $this->db->where($where5);
                    $update = $this->db->update('co_author', $data5);
                }else
                {
                    $this->db->insert('co_author',$data6);

                }
            }
        if ($result) {

            return true;
        } else {

            return false;
        }
    }

    function delete_course($course_id)
    {
       /*  $this->db->where(array(
            "coursesID" => $course_id
        ));
        $this->db->set('coursesDeleted', '1');
        $this->db->update('courses');
        return true; */

        $this->db->where('coursesID', $course_id);
        $this->db->set('coursesDeleted', '1');
        $this->db->update('courses');
        return true;
        // $this->db->delete('courses');
    }

    function delete_lesson($lesson_id)
    {
        $this->db->where('lessonID', $lesson_id);
        $this->db->delete('courses_lesson');
        return true;
    }

    function update_storyline_answer($lesson_id)
    {
        $this->db->where('lessonID', $lesson_id);
        $this->db->set('answerDeleted', '1');
        $this->db->update('storyline_answers');
        return true;
    }

    function get_id_employee($user_id)
    {

    $this->db->select('membershipID');
        $where = array (
                        // 'userCompanyName' => $data['userCompanyName'],
                        'userID' => $user_id
                    );
        $this->db->where($where);
        $query = $this->db->get('users');
        $row = $query->result_array();
        return $row;

}
    function get_employee_limit($for_employee)
    {
        $this->db->select('employeeLimit');
            $where = array (
                            // 'userCompanyName' => $data['userCompanyName'],
                            'membershipID' => $for_employee
                        );
            $this->db->where($where);
            $query = $this->db->get('membership');
            $row = $query->result_array();
            return $row;
    }

    function check_schedules($employee_id,$course_id)
    {
        $this->db->select('*');
        $where = array (
                        'employeeID' => $employee_id,
                        'coursesID' => $course_id,
                    );
        $this->db->where($where);
        $this->db->from('employee_courses');
        $query = $this->db->get();

        $row = $query->result_array();

        return $row;

    }
    function delete_schedules($employee_id,$course_id)
    {
        $where = array (
                        'employeeID' => $employee_id,
                        'coursesID' => $course_id,
                    );
        $this->db->where($where);
        $query = $this->db->delete('employee_courses');
        return $query;

    }

    function start_course($user_id, $id)
    {
        $this->db->select('employee_courses.startCourse');
        $this->db->join('courses', 'employee_courses.coursesID = courses.coursesID', 'LEFt');
        $where = array (
                        'employeeID' => $user_id,
                        'employee_courses.coursesID' => $id,
                    );
        $this->db->where($where);
        $this->db->from('employee_courses');
        $query = $this->db->get();

        $row = $query->result_array();

        return $row;
    }
    
    function check_course_author($user_id, $id)
    {
        $this->db->select("*");
        $where = array(
                "userID" => $user_id,
                "coursesID" => $id
            );
        $this->db->where($where);
        
        $this->db->from("co_author");
        $query = $this->db->get();
        
        $row = $query->result_array();

        return $row;
    }

    function finish_course($user_id, $id)
    {
        $this->db->select('*');
        $this->db->join('courses', 'employee_courses.coursesID = courses.coursesID', 'LEFt');
        $where = array (
                        'employeeID' => $user_id,
                        'employee_courses.coursesID' => $id,
                    );
        $this->db->where($where);
        $this->db->from('employee_courses');
        $query = $this->db->get();

        $row = $query->result_array();

        return $row;
    }

    function course_start($user_id,$course_started)
    {

        $this->db->where(array(
                'employeeID' => $user_id,
                'coursesID' => $course_started
        ));
        $this->db->set('startCourse', '0');
        $this->db->update('employee_courses');
    }

    function course_end($user_id,$course_finish)
    {
        $this->db->select("*");
        $where = array(
            "employeeID" => $user_id,
            "finishCourse"  => '0',  
            "coursesID" => $course_finish
        );
        $this->db->where($where);
        $this->db->from("employee_courses");
        $query2 = $this->db->get();

        if (empty($query2->result_array())) 
        {
            $data = array(
                "finishCourse" => 0,
                "courseDate" =>date('Y-m-d')
            );
            $this->db->where(array(
                    'employeeID' => $user_id,
                    'coursesID' => $course_finish
            ));
            // $this->db->set('finishCourse', '0');
            $query = $this->db->update('employee_courses',$data);

            return $query;
        }else
        {
            return true;
        }
    }
    function delete_employee_course($course_id){

        $where = array (
                        'coursesID' => $course_id
                    );
        $this->db->where($where);
        $query = $this->db->delete('employee_courses');
        return $query;
    }

    function cat_verify($category_id){
        $this->db->select('verification');
        $where = array (
                        'categoryID' => $category_id,
                        'verification' => '1'
                    );
        $this->db->where($where);
        $this->db->from('category');
        $query = $this->db->get();

        $row = $query->result_array();
        
        if($query->num_rows() > 0){
            return $row;
        }
        
//        $query->num_rows()
//      return $row;
    }
    
    function get_pending_courses($params = array())
    {
        $this->db->select('*');
        $this->db->join('slugs', 'courses.coursesID = slugs.id', 'LEFt');
        $where = array (
                        'coursesStatus' => 'pending',
                    );
        $this->db->where($where);
        $this->db->from('courses');
        // $query = $this->db->get();
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit'],$params['start']);
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit']);
        }

        $query = $this->db->get();

        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function approve_courses($draft_id)
    {
        $this->db->where(array(
                'coursesID' => $draft_id
        ));
        $this->db->set('coursesStatus', 'publish');
        $query = $this->db->update('courses');
        return $query;
    }
    function unapprove_courses($draft_id)
    {
        $this->db->where(array(
                'coursesID' => $draft_id
        ));
        $this->db->set('coursesStatus', 'draft');
        $query = $this->db->update('courses');
        return $query;
    }
    
    function get_user_by_id($id)
    {
        $this->db->select('*');
        $this->db->join('users_meta', 'users.userID = users_meta.userID', 'LEFt');
        $where = array (
                        'users.userID' => $id,
                    );
        $this->db->where($where);
        $this->db->from('users');
        
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    function get_authors($userID)
    {
        $this->db->select('*');
        $where = array (
                        'users.userType' => "instructor",
                        'users.userID' => $userID,
                        'users.userDeleted' => 0
                    );
        $this->db->where($where);
        $this->db->from('users');
        
        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }
    
    function assign_author_a_course($user_id, $course_id)
    {
        $data = array(
            'employeeID'=>$user_id,
            'coursesID'=>$course_id,
            'startCourse'=>'0',
            'courseDate' => date('Y-m-d'),
            'createdDate' => date('Y-m-d h:i:s'),
            'modifiedDate' => date('Y-m-d h:i:s')
        );
        
        $this->db->select('*');
        $where = array (
            'employeeID'=>$user_id,
            'coursesID'=>$course_id,
        );
        $this->db->where($where);
        $this->db->from('employee_courses');
        $query = $this->db->get();

        $row = $query->result_array();
        
        if($query->num_rows() > 0)
        {
            $data = array(
                'employeeID'=>$user_id,
                'coursesID'=>$course_id,
                'startCourse'=>'0',
                'courseDate' => date('Y-m-d'),
                'modifiedDate' => date('Y-m-d h:i:s')
             );
            $this->db->where($where);
            $this->db->update('employee_courses', $data);
        }else
        {
            $this->db->insert('employee_courses', $data);
        }
    }
    
    function course_assigning_free($course_id,$user_id,$offering_id = null)
    {
        $this->db->select('*');
        $where = array (
                        'employee_courses.employeeID' => $user_id,
                        'employee_courses.coursesID' => $course_id
                    );
        $this->db->where($where);
        $query = $this->db->get('employee_courses');

        if ($query->num_rows() > 0) {
            
            return false;
            
        }else{
            $data = array(
            "employeeID" => $user_id,
            "coursesID" => $course_id,
            "offeringID" => $offering_id,
            "startCourse" => '0',
            "courseDate" => date('Y-m-d'),
            "modifiedDate" => date('Y-d-m')
            );
            $this->db->insert('employee_courses', $data);
            $last_id = $this->db->insert_id();
            return $last_id;
        }
    }
    
    function create_multicategory($categories,$course_id)
    {
        $this->db->select('*');
        $where = array (
                        'course_category.courseID' => $course_id,
                        'course_category.categoryID' => $categories
                    );
        $this->db->where($where);
        $query = $this->db->get('course_category');

        if ($query->num_rows() > 0) {
            
            $data = array(
            "course_category.courseID" => $course_id,
            "course_category.categoryID" => $categories,
            "course_category.createdDate" => date("Y/m/d"),
            "course_category.modifiedDate" => date("Y-m-d H:i:s"),
            "course_category.deleted" => '0'
                
            ); 
            
            $this->db->where($where);
            $this->db->update('course_category', $data);
           
        }else{
            $data = array(
            "course_category.courseID" => $course_id,
            "course_category.categoryID" => $categories,
            "course_category.createdDate" => date("Y/m/d"),
            "course_category.modifiedDate" => date("Y-m-d H:i:s"),
            "course_category.deleted" => '0'
            );
            $this->db->insert('course_category', $data);
            $last_id = $this->db->insert_id();
            return $last_id;
    }
    }
    
    function check_free($course_id)
    {
        $this->db->select('*');
        $where = array (
                        'courses.coursesID' => $course_id,
                        'courses.price' => '0'
                    );
        $this->db->where($where);
        $query = $this->db->get('courses');

        if ($query->num_rows() > 0) {
            
            return true;
            
        }
    }
    
    function banned($user_id)
    {
        $this->db->where(array(
                "userID" => $user_id
            ));
            $this->db->set('userStatus', '0');
            $this->db->update('users'); 
    }
    
    function earnings($course_id)
    {
        $this->db->select('courses.price,courses.ceCheck,co_author.userID,co_author.coursesID');
        $this->db->join('co_author', 'courses.coursesID = co_author.coursesID', 'LEFt');
        $where = array (
                        'courses.coursesID' => $course_id,
                        'co_author.authorDeleted' => 0
                    );
        $this->db->where($where);
        $query = $this->db->get('courses');
        return $query->result_array();
    }
    
    function author_earnings($author_id, $course_id, $final_earnings, $admin_commission, $price)
    {
        $user_id = $this->session->userdata('userID');
        
        $this->db->select('*');
        $where = array (
                        'educatorEarning.courseID' => $course_id,
                        'educatorEarning.usersID' => $user_id,
                        'educatorEarning.authorID' => $author_id,
                    );
        $this->db->where($where);
        $query = $this->db->get('educatorEarning');
        
        if ($query->num_rows() > 0) {
            
            $data = array(
            "educatorEarning.authorID" => $author_id,
            "educatorEarning.courseID" => $course_id,
            "educatorEarning.usersID" => $user_id,
            "educatorEarning.earnings" => $final_earnings,
            "educatorEarning.adminCommission" => $admin_commission,
            "educatorEarning.totalPrice" => $price,
            "educatorEarning.modifiedDate" => date("Y-m-d H:i:s"),
            "educatorEarning.deleted" => '0'
            );
            $this->db->where($where);
            $this->db->update('educatorEarning', $data);
            
        }else
        {
            $data = array(
            "educatorEarning.authorID" => $author_id,
            "educatorEarning.courseID" => $course_id,
            "educatorEarning.usersID" => $user_id,
            "educatorEarning.earnings" => $final_earnings,
            "educatorEarning.adminCommission" => $admin_commission,
            "educatorEarning.totalPrice" => $price,
            "educatorEarning.createdDate" => date("Y/m/d"),
            "educatorEarning.modifiedDate" => date("Y-m-d H:i:s"),
            "educatorEarning.deleted" => '0'
            );
            $this->db->insert('educatorEarning', $data);
            $last_id = $this->db->insert_id();
            return $last_id;
        }
    }
    
    /**
    * get show membership package  data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
    
    function get_author_name($phone_no)
    {

            $clientID = $this->session->userdata('userID');
            $query = $this->db->query("SELECT *
                    FROM `users`
                    WHERE firstName like '" .$phone_no. "%' OR userEmail like '".$phone_no."%' AND userDeleted = 0 AND userType='instructor' 
                    ORDER BY userID
                    LIMIT 0,10");
            $skillarryhold = '';
            if($query->num_rows() > 0){
                
                $rows     = $query->result();
                foreach ($rows as $row)
                {
                    $cashbackComission = 0;
                    $charityComission = 0;

                    $skillarryhold .= ' <ul id="name-list">
                    <li class="phone_click search_item" data="'.$row->userName.'" data2="'.$row->userID.'"  > <span class=""><i class="fa fa-phone" style="/*background: #e5e5e5;*/ padding: 12px 27px 12px 12px;margin-top: 0;" ></i></span><span class="">'.$row->firstName.' '.$row->lastName.'('.$row->userEmail.')</span></li></ul>';
                }
                return $skillarryhold;
                
            }
    }

    function get_author_course_search($course_id, $instructor_id)
    {

            $this->db->select('*');
            $this->db->join('courses', 'co_author.coursesID = courses.coursesID','inner');
            $this->db->join('slugs', 'courses.coursesID = slugs.id','inner');
            $where = "co_author.userID = '".$instructor_id."' && courses.coursesID = '".$course_id."'";
            $this->db->where($where);
            $this->db->from('co_author');

            $query = $this->db->get();
            if($query->num_rows() > 0){
                $course_name = '';
                $rows     = $query->result();
                foreach ($rows as $row)
                {
                    $course_name .= ' <ul id="name-list">
                    <li class="phone_click connection_iem" onclick="connection_term()" data-connect="'.$row->coursesID.'" data-user="'.$row->userID.'" data-course="'.$course_id.'"  > <span class=""><i class="fa fa-book" style="/*background: #e5e5e5;*/ padding: 12px 27px 12px 12px;margin-top: 0;" ></i></span><span class="">'.str_replace("_"," ",$row->displayName).'</span></li></ul>';
                }
                return $course_name;
                
            }
    }

    function show_course_authors($id)
    {
        $this->db->select('*');
        $this->db->join('users', 'co_author.userID = users.userID', 'LEFt');
        $where = array (
                        'co_author.coursesID' => $id,
                        'co_author.authorDeleted' => 0,
                    );
        $this->db->where($where);
        $query = $this->db->get('co_author');
        return $query->result_array();
    }

    function show_course_keys()
    {
        $this->db->select('*');
        $where = array (
                        'course_key.deleted' => '0',
                    );
        $this->db->where($where);
        $this->db->order_by('course_key.ordering', 'ASC');
        $query = $this->db->get('course_key');
        return $query->result_array();
    }
    function show_course_keys2()
    {
        $this->db->select('*');
        $where = array (
                        'course_key.access' => null,
                        'course_key.deleted' => '0',
                    );
        $this->db->where($where);
        $this->db->order_by('course_key.ordering', 'ASC');
        $query = $this->db->get('course_key');
        return $query->result_array();
    }

    function get_slug($id)
    {
        $this->db->select('*');
        $where = array (
                        'slugs.id' => $id,
                    );
        $this->db->where($where);
        $query = $this->db->get('slugs');
        return $query->result_array();
    }

    function check_lesson_has_quiz($course_id)
    {
        $this->db->select('*');
        $where = array (
                        'courses_lesson.courseID' => $course_id,
                        'courses_lesson.hasQuiz' => '1',
                        'courses_lesson.deleted' => '0'
                    );
        $this->db->where($where);
        $query = $this->db->get('courses_lesson');
        return $query->result_array();
    }

    function check_external_quiz($course_id)
    {
        $this->db->select('*');
        $where = array (
                        'quizzes.courseID' => $course_id,
                        'quizzes.quizDeleted' => '0'
                    );
        $this->db->where($where);
        $query = $this->db->get('quizzes');
        return $query->result_array();
    }

    function mark_complete($lesson_id, $course_id, $user_id, $data)
    {
        $this->db->select('*');
        $where = array (
                        'lesson_check.CcUserID' => $user_id,
                        'lesson_check.CcLessonID' => $lesson_id,
                        'lesson_check.CcCourseID' => $course_id
                    );
        $this->db->where($where);
        $query = $this->db->get('lesson_check');

        if ($query->num_rows() > 0) {

            $this->db->where($where);
            $this->db->update('lesson_check', $data);

        }else{

            $this->db->insert('lesson_check', $data);
            $last_id = $this->db->insert_id();
            return $last_id;

        }
    }

    function publish($course_id, $course_status)
    {
        $where = array(
            "courses.coursesID" => $course_id
            );
        $this->db->where($where);
        $this->db->set('coursesStatus', $course_status);
        $this->db->update('courses');
    }

    function retake_quiz($user_id, $course_id, $quiz_id)
    {
        $this->db->select('*');
        $where = array (
                        'quizess_report.userId' => $user_id,
                        'quizess_report.courseId' => $course_id,
                        'quizess_report.quizId' => $quiz_id
                    );
        $this->db->where($where);
        $query = $this->db->get('quizess_report'); 
        return ($query->num_rows() > 0)?$query->result_array():FALSE;  
    }

    function check_reviews($user_id , $quiz_id , $course_id)
    {
        $this->db->select('*');
        $where = array (
                        'courses_rating.userID' => $user_id,
                        'courses_rating.courseID' => $course_id
                    );
        $this->db->where($where);
        $query = $this->db->get('courses_rating'); 
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

    function webinar_list($user_id, $course_id)
    {
        $this->db->select('*');
        $where = array(
            "userID" =>$user_id,
            "courseID" =>  $course_id
        );
        $this->db->where($where);
        $this->db->from('zoom_credentials');

        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

    function delete_category_from_course($category_id, $course_id)
    {
        $this->db->select('*');
        $where = array (
                        'course_category.courseID' => $course_id,
                        'course_category.categoryID' => $category_id
                    );
        $this->db->where($where);
        $query = $this->db->get('course_category');

        if ($query->num_rows() > 0) {
            $this->db->where($where);
            $this->db->delete('course_category');
            return true;
        }
    }

    function get_course_offer($course_id)
    {
        $this->db->select('offeringID');
        $where = array(
            "coursesID" => $course_id,
            "coursesDeleted" => 0
        );
        $this->db->where($where);
        $this->db->from('courses');

        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

    function has_webinar($course_id)
    {
        $this->db->select("*");
        $where = array(
            "courseID" => $course_id,
            "lessonType" => "webinar"
        );
        $this->db->where($where);
        $this->db->from('courses_lesson');

        $query = $this->db->get();
        return ($query->num_rows() > 0)?$query->result_array():FALSE;        
    }

    function update_live_field($course_id)
    {
        $where = array(
            'coursesID' => $course_id,
            'coursesDeleted' => 0
        );
        $this->db->where($where);
        $this->db->set('liveCheck', 1);
        $query = $this->db->update('courses');
        if($query)
        {
            return $query;
        }else
        {
            return false;
        }
    }
}