<?php

class Orders_model extends CI_Model {
		public function __construct() {
		parent::__construct();
		$this->load->library('encrypt');

	}
	
	public function get_orders($params = array())
	{
		$this->db->select('orders.companyID,orders.courseID,orders.transactionID,orders.ordersPrice,orders.date,users.userName,users.userPaymentStatus,users.userStatus,courses.displayName,courses.coursesID,users.firstName, users.lastName');
		$this->db->join('users', 'orders.companyID = users.userID','inner');
		$this->db->join('courses','orders.courseID = courses.coursesID','inner');
		$this->db->from('orders');
		$this->db->order_by('userID','desc');
		
		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}
		
		$query = $this->db->get();
		
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	
	public function filter_by_date($starting_date,$ending_date,$payment_filter,$status_filter,$params = array())
	{
		$start_date = $starting_date;
		$end_date = $ending_date;
//		$start_date = date("Y-m-d", strtotime($original_start_Date));
//		$end_date = date("Y-m-d", strtotime($original_end_Date));
		
        
		$this->db->select('*');
		$this->db->join('orders', 'orders.companyID = users.userID','right');
		if($payment_filter =="" && $status_filter =="" && $starting_date != 0){
			$condition = "orders.date BETWEEN " . "'" . $start_date . "'" . " AND " . "'" . $end_date . "' AND users.userDeleted = '0' ";
		}else if($payment_filter =="" && $starting_date != 0){
			$condition = "orders.date BETWEEN " . "'" . $start_date . "'" . " AND " . "'" . $end_date . "' AND users.userStatus ='".$status_filter."' AND users.userDeleted = '0' ";
		}else if($status_filter =="" && $starting_date != 0){
			$condition = "orders.date BETWEEN " . "'" . $start_date . "'" . " AND " . "'" . $end_date . "' AND users.userPaymentStatus ='".$payment_filter."' AND users.userDeleted = '0' ";
		}else if ($starting_date == 0 && $payment_filter ==""){
			$condition = "users.userStatus ='".$status_filter."' AND users.userDeleted = '0' ";
		}else if ($starting_date == 0 && $status_filter ==""){
			$condition = "users.userPaymentStatus ='".$payment_filter."' AND users.userDeleted = '0' ";
		}else if($starting_date == 0){
			$condition = "users.userStatus ='".$status_filter."' AND users.userPaymentStatus ='".$payment_filter."' AND users.userDeleted = '0' ";
		}else{
			$condition = "orders.date BETWEEN " . "'" . $start_date . "'" . " AND " . "'" . $end_date . "' AND users.userPaymentStatus ='".$payment_filter."' AND users.userStatus ='".$status_filter."' AND users.userDeleted = '0' ";
		}
        
//        return $condition;
		
		$this->db->where($condition);
		$this->db->from('users');
		$this->db->order_by('userID','desc');
		
		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}
		
		$query = $this->db->get();
		
		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
	function order_report()
	{
				return $query = $this->db->query("SELECT * FROM `orders` ");
	}
    
    /**
    * get show membership package  data from  the database,
    * store it in a new array and return it to the controller
    * @return array
    */
	function get_order_phone($phone_no)
	{

			$clientID = $this->session->userdata('userID');
            $query = $this->db->query("
                            SELECT *
                    FROM `users`
                    JOIN `orders` ON `users`.`userID` = `orders`.`companyID`
                    WHERE userName like '" .$phone_no. "%' AND userDeleted = 0 
                    ORDER BY userID
                    LIMIT 0,10");

            $skillarryhold = '';
            if($query->num_rows() > 0){
                
			    $rows     = $query->result();
                foreach ($rows as $row)
                {
                    $cashbackComission = 0;
                    $charityComission = 0;

                    $skillarryhold .= ' <ul id="name-list">
                    <li class="phone_click" onClick="selectname('.$row->userID.');"><span class=""><i class="fa fa-phone" style="/*background: #e5e5e5;*/ padding: 12px 27px 12px 12px;margin-top: 0;" ></i></span><span class="">'.$row->userName.'</span></li></ul>';
                }
                return $skillarryhold;
                
            }
    }
    
    function search_order($company_id , $params = array())
	{
		$this->db->select('*');
        $this->db->join('orders', 'orders.companyID = users.userID','inner');
			$where = array (
						'users.userID' => $company_id,
						'users.userDeleted' => '0',
					);
		$this->db->where($where);
		$this->db->from('users');
		$this->db->order_by('users.userID','desc');

		if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit'],$params['start']);
		}elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
			$this->db->limit($params['limit']);
		}

		$query = $this->db->get();

		return ($query->num_rows() > 0)?$query->result_array():FALSE;
	}
    
}