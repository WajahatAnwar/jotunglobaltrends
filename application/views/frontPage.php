<main id="main" class="site-main">
    <header class="text-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
                    <h1 class="header">
                       <br>Cá tính
                    </h1>
                    <h3 class="sub-header">Bộ sưu tập màu sắc 2019</h3>
                    <p>Tạo không gian cá tính của bạn với màu sắc được thiết kế bởi Jotun.</p>
                </div>
            </div>
        </div>
    </header>

    <section class="section section--themes" id="themes">
        <div class="container card-container">
            <div class="row">
                <div class="card col-xs-12 col-sm-6 col-lg-4 ">
                    <div class="row">
                        <figure class="card__item card__item--figure col-xs-12" id="t1-image--card">
                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t1_0" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Lưu vào mục yêu thích</span>
                                </button>

                                <a href="<?php echo $this->config->item("calm_link"); ?>" tabindex="-1">
                                    <picture>
                                        <source data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_calm_a2_0531.webp" type="image/webp"
                                            srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_calm_a2_0531.webp">
                                        <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>/assets/dist/Jotun_calm_a2_0531.jpg"
                                            data-src="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_calm_a2_0531.jpg" data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_calm_a2_0531.jpg"
                                            alt="WALL: JOTUN 1877 WARM GREY" srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_calm_a2_0531.jpg">
                                    </picture>
                                </a>
                            </figure>
                        </figure>
                        <div class="card__item card__body col-xs-12">
                            <h2 class="card__header">
                                <a class="" href="<?php echo $this->config->item("calm_link"); ?>">Nhẹ nhàng</a>
                            </h2>
                            <p>Mềm mại, ánh sáng trung tính và ấm áp, tương phản tinh tế. <br class="visible-lg"><a href="<?php echo $this->config->item("calm_link"); ?>">Khám phá </a></p>
                        </div>
                    </div>
                </div>
                <div class="card col-xs-12 col-sm-6 col-lg-4 ">
                    <div class="row">
                        <figure class="card__item card__item--figure col-xs-12" id="t2-image--card">
                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t2_0" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Lưu vào mục yêu thích</span>
                                </button>

                                <a href="<?php echo $this->config->item("refined_link"); ?>" tabindex="-1">
                                    <picture>
                                        <source data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_moderne_acc1_0590.webp"
                                            type="image/webp" srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_moderne_acc1_0590.webp">
                                        <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>/assets/dist/Jotun_moderne_acc1_0590.jpg"
                                            data-src="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_moderne_acc1_0590.jpg"
                                            data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_moderne_acc1_0590.jpg" alt="CEILING: JOTUN PERFECTION 1453 COTTON BALL PANEL WALL: JOTUN SUPREME FINISH MATT 10580 SOFT SKIN WALL (IN FRONT): JOTUN PURE COLOR 20046 SAVANNA SUNSET SKIRTING: JOTUN SUPREME FINISH MATT 20046 SAVANNA SUNSET"
                                            srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_moderne_acc1_0590.jpg">
                                    </picture>
                                </a>
                            </figure>
                        </figure>
                        <div class="card__item card__body col-xs-12">
                            <h2 class="card__header">
                                <a class="" href="<?php echo $this->config->item("refined_link"); ?>">Tinh tế</a>
                            </h2>
                            <p>Màu xanh lá cây và màu vàng kích thích cảm giác, cho độ sâu với các họa tiết khác nhau. <br class="visible-lg"><a href="<?php echo $this->config->item("refined_link"); ?>">Khám phá </a></p>
                        </div>
                    </div>
                </div>
                <div class="card col-xs-12 col-sm-6 col-lg-4">
                    <div class="row">
                        <figure class="card__item card__item--figure col-xs-12" id="t3-image--card">
                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t3_0" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Lưu vào mục yêu thích</span>
                                </button>

                                <a href="<?php echo $this->config->item("raw_link"); ?>" tabindex="-1">
                                    <picture>
                                        <source data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_Raw_r4-ng4_0854.webp"
                                            type="image/webp" srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_Raw_r4-ng4_0854.webp">
                                        <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>/assets/dist/Jotun_Raw_r4-ng4_0854.jpg"
                                            data-src="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg"
                                            data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg" alt="WALL: JOTUN PURE COLOR 6350 SOFT TEAL"
                                            srcset="<?php echo base_url(); ?>/assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg">
                                    </picture>
                                </a>
                            </figure>
                        </figure>
                        <div class="card__item card__body col-xs-12">
                            <h2 class="card__header">
                                <a class="" href="<?php echo $this->config->item("raw_link"); ?>">Thô mộc</a>
                            </h2>
                            <p>Màu đỏ đất sâu, đào gợi cảm, màu xanh và màu trung tính đậm hơn. <br class="visible-lg"><a href="<?php echo $this->config->item("raw_link"); ?>">Khám phá </a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section--cta text-center js-section-video">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>Cách sử dụng bảng màu</h2>
                <p class="lead">Bảng màu này cho phép bạn tạo các kết hợp màu hài hòa. Xem cách sử dụng bảng màu mới.</p>

                <figure class="column__item column__item--video">
                    <div class="video-container keep-16-9">
                        <span class="video-overlay js-video-overlay" data-bg="<?php echo base_url(); ?>/assets/media/images/forside/gcc-frontpage-video.jpg"
                            data-bg-mobile="<?php echo base_url(); ?>/assets/media/images/forside/gcc-frontpage-video.jpg" style="background-image: url(&quot;<?php echo base_url(); ?>/assets/media/images/forside/gcc-frontpage-video.jpg&quot;);"></span>
                        <iframe class="video js-video" title="How to use the colour card" src="https://www.youtube.com/embed/1un_vVxFYw8?enablejsapi=1&amp;showinfo=0&amp;rel=0"
                            width="720" height="1280" frameborder="0" allowfullscreen="" id="widget2"></iframe>
                    </div>
                </figure>
            </div>
        </div>
    </section>

    <section class="section section--cta text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>Lưu vào mục yêu thích</h2>
                <p class="lead">Tạo một danh sách mong muốn của các màu sắc yêu thích của bạn, hình ảnh nghệ thuật và sản phẩm sơn Jotun bằng cách nhấn <i class="material-icons material-icons--first">&#xE87E;</i></p>
                <a class="btn btn--line" href="<?php echo $this->config->item("wishlist_link"); ?>">
                    <span class="btn__text">Yêu thích
                        <span class="btn--heart">
                            <i class="material-icons material-icons--second"></i>
                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </section>

    <section class="section text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>CÁ TÍNH VỚI MÀU SẮC</h2>
                <p class="lead">Hãy để màu sắc và sư phối hợp màu phản ánh cá tính của bạn - bản sắc màu riêng của bạn. Tạo bảng màu nội thất cho riêng bạn. Chọn một màu, khám phá các màu bổ sung và tạo bảng màu nội thất độc đáo của bạn.</p>
                <a href="<?php echo $this->config->item("palette_link"); ?>">
                    <picture>
                        <source data-srcset="<?php echo base_url(); ?>/assets/media/images/banners/calm_mobile.png" type="image/png"
                            srcset="<?php echo base_url(); ?>/assets/media/images/banners/calm_mobile.png">
                        <img class="img-responsive lazyload" src="<?php echo base_url(); ?>/<?php echo base_url(); ?>/assets/dist/calm_mobile.png" data-src="<?php echo base_url(); ?>/assets/media/images/banners/calm_mobile.png"
                            data-srcset="<?php echo base_url(); ?>/assets/media/images/banners/calm_mobile.png" alt="Express yourself with colours"
                            srcset="<?php echo base_url(); ?>/assets/media/images/banners/calm_mobile.png">
                    </picture>
                </a>
            </div>
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <a class="btn btn--line" href="<?php echo $this->config->item("palette_link"); ?>">
                    <span class="btn__text">Tạo bảng màu nội thất cho riêng bạn</span>
                </a>
            </div>
        </div>
    </section>

    <div class="section section--intro" id="intro">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-xl-offset-2">
                    <div class="row">
                        <figure class="card__item card__item--figure col-sm-6">
                            <figure class="card__image">
                                <picture>
                                    <source data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/_TK_8696.webp" type="image/webp"
                                        srcset="<?php echo base_url(); ?>/assets/media/images/forside/_TK_8696.webp">
                                    <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>/assets/dist/_TK_8696.jpg"
                                        data-src="<?php echo base_url(); ?>/assets/media/images/forside/_TK_8696.jpg" data-srcset="<?php echo base_url(); ?>/assets/media/images/forside/_TK_8696.jpg"
                                        alt="Lisbeth Larsen Global Colour Manager Jotun" srcset="<?php echo base_url(); ?>/assets/media/images/forside/_TK_8696.jpg">
                                </picture>
                            </figure>
                            <figcaption class="card__caption sr-only">Lisbeth Larsen Giám đốc màu sắc của Jotun toàn cầu</figcaption>
                        </figure>
                        <div class="col-sm-6">
                            <h3>HÃY NHỚ BẠN LÀ SAI</h3>
                            <p>Cuộc sống như vòng quay thời gian chuyển động nhanh và liên tục. Hàng 
triệu điều xảy ra mỗi ngày làm thay đổi cách chúng ta suy nghĩ và cảm 
nhận nhanh chóng. Theo thời gian, cá tính của chúng ta cũng dần thay đổi 
và bị ảnh hưởng – chúng ta, đôi khi, không còn nhận ra cá tính của mình 
hay lãng quên những điều mình thực sự yêu thích mà chạy theo trào lưu 
của xã hội.<br><br> Ngôi nhà lại là một điều hoàn toàn khác. Ngôi nhà là nơi mỗi buổi sáng 
đem lại tinh thần hứng khởi cho ta mỗi ngày mới và cũng là nơi mỗi buổi 
chiều muộn chào đón ta về nhà với vòng tay ấm áp. Ngôi nhà, do đó, cần 
phải thực sự thoải mái, là nơi ta quay về an trú, nhắc nhở ta về những điều 
hạnh phúc... Ngôi nhà là câu chuyện sống về cuộc đời, về những màu sắc 
và vật dụng trang trí mà chúng ta yêu thích. Vì vậy, hãy tô điểm cho không 
gian sống của bạn với những gam màu được thiết kế bởi các chuyên gia 
màu sắc Jotun.<br><br> Bộ sưu tập màu sắc năm nay của Jotun hi vọng sẽ giúp bạn viết mở đầu 
cho câu chuyện về không gian sống của mình. Bộ sưu tập màu sắc này sẽ 
là tổng hợp những ý tưởng và nguồn cảm hứng sáng tạo giúp bạn thể hiện 
cá tính của bản thân mình.</p>
                            <div class="endorsement-footer">
                                <span class="signature"></span>
                                <p>Lisbeth Larsen Giám đốc màu sắc của Jotun toàn cầu<br> Giám đốc màu sắc Jotun toàn cầu</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main><!-- .site-main -->