<header class="theme">
            <div class="theme__content">
                <div class="theme__column theme__column--image">
                    <figure class="theme__image lazyload" data-bg="assets/media/images/t1/Jotun_calm_a1_0621.jpg"
                        role="img" aria-label="WALL: JOTUN 10290 SOFT TOUCH" id="t1-image--1" style="background-image: url(&quot;assets/media/images/t1/Jotun_calm_a1_0621.jpg&quot;);">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="10290" data-image="assets/media/images/t1/Jotun_calm_a1_0621.jpg"
                            data-product="2">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figcaption class="card__caption card__caption--color js-card-caption">
                            <p>WALL: JOTUN 10290 <strong>SOFT TOUCH</strong></p>
                        </figcaption>
                    </figure>

                    <div class="theme__bar">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t1_1" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <div class="theme__caption">
                            <p>WALL: JOTUN 10290 <strong>SOFT TOUCH</strong></p>
                        </div>
                    </div>
                </div>

                <div class="theme__column theme__column--content">
                    <div class="theme__header">
                        <h1 class="theme__title">CALM</h1>
                    </div>

                    <div class="theme__video js-section-video" id="t1-video--1">
                        <div class="column__item column__item--video">
                            <div class="video-container keep-16-9">
                                <div class="column__caption js-video-caption">
                                    <h3>Khám phá màu sắc</h3>
                                </div>
                                <span class="video-overlay js-video-overlay" data-bg="assets/media/images/t1/t1-video-3.jpg"
                                    data-bg-mobile="assets/media/images/t1/t1-video-3.jpg" style="background-image: url(&quot;assets/media/images/t1/t1-video-3.jpg&quot;);"></span>
                                <iframe class="video js-video" src="https://www.youtube.com/embed/zidbfjRAMK8?enablejsapi=1&rel=0&showinfo=0" title="Explore the colours"
                                    width="720" height="1280" frameborder="0" allowfullscreen="" id="widget2"></iframe>
                            </div>
                        </div>

                        <div class="theme__text">
                            <p>Không gian tươi sáng, chân thực tương phản trong bộ sưu tập 
những tông màu trung tính, hiện đại, ấm áp, nhưng không kém 
phần tinh tế. Chính sự sắp xếp bất ngờ, ngẫu hứng nhưng đầy ý 
nghĩa này đã làm tỏa sáng không gian và các đồ nội thất trang trí.</p>
                        </div>
                    </div>
                </div>
            </div>
        </header>
<main id="main" class="site-main">

    <section class="section section--cta text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>LƯU MỤC YÊU THÍCH</h2>
                <p class="lead">Tạo danh sách các màu sắc yêu thích của bạn, hình ảnh và sản phẩm truyền cảm hứng từ Jotun bằng cách nhấn <i class="material-icons material-icons--first"></i></p>
                <a class="btn btn--line" href="<?php echo $this->config->item("wishlist_link"); ?>">
                    <span class="btn__text">My favorites
                        <span class="btn--heart">
                            <i class="material-icons material-icons--second"></i>
                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </section>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <figure class="card__item--figure col-xs-12 col-lg-8 image-more" id="t1-image--2">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="12083 1024" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_a2_0531.jpg"
                        data-product="2">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t1_2" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_a2_0531.webp" type="image/webp"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_a2_0531.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_calm_a2_0531.jpg" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_a2_0531.jpg"
                                data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_a2_0531.jpg" alt="WALL: JOTUN 12083 DEVINE WALL: JOTUN 1024 TIMELESS SKIRTING AND WINDOW: JOTUN 1024 TIMELESS"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_a2_0531.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col js-card-caption card__caption--color">
                        <p>WALL: JOTUN 12083 <strong>DEVINE</strong><br> WALL: JOTUN 1024 <strong>TIMELESS</strong><br>
                            SKIRTING AND WINDOW: JOTUN 1024 <strong>TIMELESS</strong></p>
                    </figcaption>
                </figure>

                <div class="col-lg-4 hidden-sm hidden-xs">
                    <div class="row">
                        <figure class="card__item--figure col-xs-12 col-md-6 col-lg-12 image-more" id="t1-image--3">
                            <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                                data-toggle="modal" data-target="#plus" data-colors="12078" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_8297.jpg"
                                data-product="2">
                                <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                            </button>

                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t1_3" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <picture>
                                    <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_8297.webp" type="image/webp"
                                        srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_8297.webp">
                                    <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_ng3_8297.jpg"
                                        data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_8297.jpg" data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_8297.jpg"
                                        alt="WALL: JOTUN 12078 COMFORT GREY" srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_8297.jpg">
                                </picture>
                            </figure>

                            <figcaption class="card__caption card__caption--col js-card-caption card__caption--color">
                                <p>WALL: JOTUN 12078 <strong>COMFORT GREY</strong></p>
                            </figcaption>
                        </figure>

                        <div class="color-group col-xs-12 col-md-6 col-lg-12">
                            <figure class="color-card  color-card--image js-color-card u-bg--12083 wow slideRightFade"
                                data-color-id="12083" style="visibility: visible; animation-name: i;">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12083 <strong>Devine</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour
                                            in fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12083" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>
                            <figure class="color-card  color-card--hex js-color-card u-bg--12078 wow slideRightFade"
                                data-color-id="12078" style="visibility: visible; animation-name: i;">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour
                                            in fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12078" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <small><a href="<?php echo $this->config->item("calm_link"); ?>#allcolors" class="js-btn-scroll">Xem tất cả các màu cùng nhau ở cuối trang</a></small>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p>Sơn nội thất cao cấp Majestic Đẹp & Chăm Sóc
Hoàn Hảo của Jotun đem lại cho bạn một 
không gian đẹp sang trọng với những 
sắc màu thời trang, tao nhã
của những bức tường láng mịn
và bền màu theo thời gian. Sản phẩm gần như không mùi và dễ thi công.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--product" id="t1-product--1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="product__body">
                        <h3 class="product__title">MAJESTIC PERFECT BEAUTY AND CARE</h3>
                        <figure class="product__image">
                            <picture>
                                <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png" type="image/webp" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png">
                                <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png" data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png"
                                    data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png" alt="MAJESTIC PERFECT BEAUTY AND CARE"
                                    srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint1.png">
                            </picture>

                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                aria-label="Add the product to your favourites" data-favorite="product_2" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the product to your favourites</span>
                            </button>
                        </figure>

                        <p class="product__tag">Màu sắc bền đẹp và an toàn cho sức khỏe, môi trường</p>

                        <div class="product__btn">
                            <a href="https://www.jotun.com/my/en/b2c/products/interior/majestic/majestic-perfect-beauty-and-care.aspx" class="btn btn--line">
                                <span class="btn__text">Tìm hiểu thêm về sản phẩm</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section--image visible-xs visible-sm">
        <div class="container">
            <div class="row">
                <figure class="card__item--figure col-xs-12 col-md-6 col-lg-12 image-more" id="t1-image--3-2">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="12078" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_8297.jpg"
                        data-product="2">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite animated" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t1_3" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_8297.webp" type="image/webp">
                            <img class="img-responsive lazyload" src="data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D&#39;http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg&#39; viewBox%3D&#39;0 0 600 765&#39;%2F%3E"
                                data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_8297.jpg" data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_8297.jpg"
                                alt="WALL: JOTUN 12078 COMFORT GREY">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col js-card-caption card__caption--color">
                        <p>WALL: JOTUN 12078 <strong>COMFORT GREY</strong></p>
                    </figcaption>
                </figure>

                <div class="color-group col-xs-12 col-md-6 col-lg-12">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12083 wow slideRightFade animated"
                        data-color-id="12083" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12083 <strong>Devine</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite animated" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12083" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                    <figure class="color-card  color-card--hex js-color-card u-bg--12078 wow slideRightFade animated"
                        data-color-id="12078" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite animated" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12078" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>

                    <small><a href="<?php echo $this->config->item("calm_link"); ?>#allcolors" class="js-btn-scroll">Xem tất cả các màu cùng nhau ở cuối trang</a></small>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-4">
                    <figure class="card__item--figure image-more" id="t1-image--4">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="12074" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0643.jpg"
                            data-product="2">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figure class="card__image">
                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                aria-label="Save your favourites" data-favorite="t1_4" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Save your favourites</span>
                            </button>

                            <picture>
                                <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0643.webp" type="image/webp"
                                    srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0643.webp">
                                <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_calm_acc2_0643.jpg"
                                    data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0643.jpg" data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0643.jpg"
                                    alt="WALL: JOTUN 12074 PEACHY TABLE: JOTUN 12074 PEACHY" srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0643.jpg">
                            </picture>
                        </figure>

                        <figcaption class="card__caption card__caption--color card__caption--col js-card-caption card__caption--nocol">
                            <p>WALL: JOTUN 12074 <strong>PEACHY</strong><br> TABLE: JOTUN 12074 <strong>PEACHY</strong></p>
                        </figcaption>
                    </figure>

                    <h3 class="product__header">Soothing harmony</h3>
                    <p>As life grows faster, busier and more complicated, we are drawn to slowness and
                        simplicity. We seek to design the possibility of peace and clarity into our homes,
                        creating areas of comfort and contemplation – the stillness of a Nordic landscape,
                        the calm of a Zen sanctuary.</p>

                    <div class="color-group">
                        <figure class="color-card  color-card--hex js-color-card u-bg--12074 wow slideRightFade"
                            data-color-id="12074" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12074 <strong>Peachy</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12074" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                        <figure class="color-card  color-card--hex js-color-card u-bg--12078 wow slideRightFade"
                            data-color-id="12078" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12078" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                    <small><a href="<?php echo $this->config->item("calm_link"); ?>#allcolors" class="js-btn-scroll">Xem tất cả các màu cùng nhau ở cuối trang</a></small>
                        <small><a href="<?php echo $this->config->item("calm_link"); ?>#allcolors" class="js-btn-scroll">Xem tất cả các màu cùng nhau ở cuối trang</a></small>
                    </div>
                </div>

                <figure class="card__item--figure col-xs-12 col-lg-8 image-more" id="t1-image--5">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="12078 1622" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ng3_0544.jpg"
                        data-product="2">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t1_5" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ng3_0544.webp" type="image/webp"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ng3_0544.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_calm_ng3_0544.jpg" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ng3_0544.jpg"
                                data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ng3_0544.jpg" alt="STAIRCASE WALL: JOTUN 12078 COMFORT GREY WALL: JOTUN 1622 EDELWEISS"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ng3_0544.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col js-card-caption card__caption--color">
                        <p>STAIRCASE WALL: JOTUN 12078 <strong>COMFORT GREY</strong><br> WALL: JOTUN 1622
                            <strong>EDELWEISS</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--video js-section-video" id="t1-video--2" style="display:none;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="column__item column__item--video">
                        <div class="video-container keep-16-9">
                            <div class="column__caption js-video-caption">
                                <h3>t1.video.2.title.1</h3>
                            </div>
                            <span class="video-overlay js-video-overlay" data-bg="<?php echo base_url();?>assets/media/images/t1/jotun-birk214210ret-AI-video.jpg"
                                data-bg-mobile="<?php echo base_url();?>assets/media/images/t1/jotun-birk214210ret-AI-video-mobil.jpg" style="background-image: url(&quot;<?php echo base_url();?>assets/media/images/t1/jotun-birk214210ret-AI-video.jpg&quot;);"></span>
                            <iframe class="video js-video" src="" title="t1.video.2.title.1"
                                width="720" height="1280" frameborder="0" allowfullscreen="" id="widget4"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p><strong>CAM KẾT MÀU CHÍNH XÁC</strong>Công nghệ chất tạo màu tiên tiến của Jotun đem lại độ chính xác màu cao. Với sự cam kết màu chính xác, bạn có thể chắc chắn rằng màu thực tế sẽ giống như màu bạn chọn trên bảng màu. </p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image section--selector">
        <div class="container">
            <div class="row">
                <figure class="selector card__item--figure col-xs-12" id="t1-selector--1">
                    <figure class="selector__inner card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite js-btn-selector-favorite col"
                            title="Save your favourites" aria-label="Save your favourites" data-show="1" data-favorite="t1_s_1_1"
                            style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite js-btn-selector-favorite col hidden animated"
                            title="Save your favourites" aria-label="Save your favourites" data-show="2" data-favorite="t1_s_1_2"
                            style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite js-btn-selector-favorite col hidden animated"
                            title="Save your favourites" aria-label="Save your favourites" data-show="3" data-favorite="t1_s_1_3"
                            style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <img class="selector__image img-responsive js-selector-img lazyload" src="http://jotun-gcc.com.dev02.allegro.no/assets/media/images/t1/Jotun_calm_ng1_0555.jpg"
                            data-show="1" data-src="http://jotun-gcc.com.dev02.allegro.no/assets/media/images/t1/Jotun_calm_ng1_0555.jpg" alt="CEILING: JOTUN 1622 EDELWEISS">

                        <img class="selector__image img-responsive lazyload js-selector-img hidden" src="data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D&#39;http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg&#39; viewBox%3D&#39;0 0 1600 2136&#39;%2F%3E"
                            data-show="2" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_y1_ng1_0564.jpg" alt="CEILING: JOTUN 12079 GLEAM">
                        <span class="selector__loader"></span>
                    </figure>
                    <div class="selector__wrapper">
                        <div class="selector__controls">
                            <button type="button" class="btn btn--square js-btn-selector u-bg--0394" title="Choose colour"
                                data-show="1" data-color="CEILING: JOTUN 1622 &lt;strong&gt;EDELWEISS&lt;/strong&gt;"></button>
                            <button type="button" class="btn btn--square js-btn-selector u-bg--9918" title="Choose colour"
                                data-show="2" data-color="CEILING: JOTUN 12079 &lt;strong&gt;GLEAM&lt;/strong&gt;"></button>
                            <p class="selector__helper js-selector-helper">Chọn màu</p>
                            <p class="selector__helper js-selector-caption">CEILING: JOTUN 1622 <strong>EDELWEISS</strong></p>
                        </div>
                    </div>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p>Sơn nội thất cao cấp Majestic Đẹp
Hoàn Hảo của Jotun đem lại cho bạn một 
không gian cổ điển sang trọng với những 
sắc màu thời trang, tao nhã
của những bức tường láng mịn
và bền màu theo thời gian..</p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--product" id="t1-product--2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="product__body">
                        <h3 class="product__title">MAJESTIC TRUE BEAUTY MATT</h3>
                        <figure class="product__image">
                            <picture>
                                <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png" type="image/png" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png">
                                <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png" data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png"
                                    data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png" alt="MAJESTIC TRUE BEAUTY MATT"
                                    srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png">
                            </picture>

                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                aria-label="Add the product to your favourites" data-favorite="product_4" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the product to your favourites</span>
                            </button>
                        </figure>

                        <p class="product__tag">Màu sắc bền đẹp và tinh tế theo thời gian</p>

                        <div class="product__btn">
                            <a href="https://www.jotun.com/my/en/b2c/products/interior/majestic/majestic-true-beauty-matt.aspx" class="btn btn--line">
                                <span class="btn__text">Tìm hiểu thêm về sản phẩm</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <figure class="card__item--figure col-xs-12 image-more" id="t1-image--6">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="12074" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0629.jpg"
                        data-product="2">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t1_6" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0629.webp" type="image/webp"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0629.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_calm_acc2_0629.jpg" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0629.jpg"
                                data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0629.jpg" alt="WALL: JOTUN 12074 PEACHY"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_acc2_0629.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col js-card-caption card__caption--color">
                        <p>WALL: JOTUN 12074 <strong>PEACHY</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p>Những màu trung tính sẽ luôn 
tạo ra một không gian đầy 
cảm hứng. Kết hợp gam màu 
trung tính với những màu đậm 
làm điểm nhấn sẽ tạo ra một 
hiệu ứng hoàn hảo với không 
gian thú vị và cá tính.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-4">
                    <figure class="card__item--figure image-more" id="t1-image--7">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="12078" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_7936.jpg"
                            data-product="2">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figure class="card__image">
                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                aria-label="Save your favourites" data-favorite="t1_7" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Save your favourites</span>
                            </button>

                            <picture>
                                <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_7936.webp" type="image/webp"
                                    srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_7936.webp">
                                <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_ng3_7936.jpg" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_7936.jpg"
                                    data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_7936.jpg" alt="WALL: JOTUN 12078 COMFORT GREY"
                                    srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_ng3_7936.jpg">
                            </picture>
                        </figure>

                        <figcaption class="card__caption card__caption--color card__caption--col js-card-caption card__caption--nocol">
                            <p>WALL: JOTUN 12078 <strong>COMFORT GREY</strong></p>
                        </figcaption>
                    </figure>

                    <h3 class="product__header">T R U N G   T Í N H</h3>
                    <p>Vẻ đẹp của sự đơn giản luôn có giá trị vượt thời gian. Nơi đó tâm trí của chúng ta hoàn 
toàn thư thái và tĩnh lặng. Nơi đó, không có gì là dư thừa, màu sắc, đồ trang trí đều thể 
hiện chất lượng, khéo léo, tinh tế và đầy cá tính. </p>

                    <div class="color-group">
                        <figure class="color-card  color-card--hex js-color-card u-bg--12078 wow slideRightFade"
                            data-color-id="12078" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12078" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                        <figure class="color-card  color-card--hex js-color-card u-bg--12075 wow slideRightFade"
                            data-color-id="12075" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12075 <strong>Soothing Beige</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12075" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                        <small><a href="<?php echo $this->config->item("calm_link"); ?>#allcolors" class="js-btn-scroll">Xem tất cả các màu cùng nhau ở cuối trang</a></small>
                    </div>
                </div>

                <figure class="card__item--figure col-xs-12 col-lg-8 image-more" id="t1-image--8">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="12075" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny3_0584.jpg"
                        data-product="4">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t1_8" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny3_0584.jpg" type="image/jpg"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny3_0584.jpg">
                            <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_calm_ny3_0584.jpg" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny3_0584.jpg"
                                data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny3_0584.jpg" alt="WALL: JOTUN 12075 SOOTHING BEIGE CEILING: JOTUN 12075 SOOTHING BEIGE SKIRTING: JOTUN SUPREME FINISH MATT 1024 TIMELESS"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny3_0584.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col js-card-caption card__caption--color">
                        <p>WALL: JOTUN 12075 <strong>SOOTHING BEIGE</strong><br> CEILING: JOTUN 12075 <strong>SOOTHING BEIGE</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section--cta">
        <section class="section text-center">
            <div class="container">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h2>Express yourself with colours</h2>
                    <p class="lead">Let the colours and combinations reflect and enhance who you are – your own
                        personal colour identity. Create your interior palette. Pick a colour, explore
                        complementary colours and create your unique interior palette.</p>
                    <a href="<?php echo $this->config->item("palette_link"); ?>">
                        <picture>
                            <source data-srcset="<?php echo base_url();?>assets/media/images/banners/calm_mobile.png" type="image/png"
                                srcset="<?php echo base_url();?>assets/media/images/banners/calm_mobile.png">
                            <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/calm_mobile.png"
                                data-src="<?php echo base_url();?>assets/media/images/banners/calm_mobile.png" data-srcset="<?php echo base_url();?>assets/media/images/banners/calm_mobile.png"
                                alt="Express yourself with colours" srcset="<?php echo base_url();?>assets/media/images/banners/calm_mobile.png">
                        </picture>
                    </a>
                </div>
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <a class="btn btn--line" href="<?php echo $this->config->item("palette_link"); ?>">
                        <span class="btn__text">Create your personal interior palette</span>
                    </a>
                </div>
            </div>
        </section>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p><strong>BẢNG MÀU TRUNG TÍNH</strong>  Bảng màu trung tính mới này tạo ra sự hài hòa cân bằng giữa tính gọn gàng của chủ nghĩa tối giản với năng lượng của màu sắc. Những thay đổi nhỏ trong không gian đơn sắc giúp điều chỉnh bầu không khí theo nhiệt độ, hướng tới sự ấm áp hoặc mát mẻ và tĩnh lặng nhẹ nhàng.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image section--selector">
        <div class="container">
            <div class="row">
                <figure class="selector card__item--figure col-xs-12" id="t1-selector--2">
                    <figure class="selector__inner card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite js-btn-selector-favorite col"
                            title="Save your favourites" aria-label="Save your favourites" data-show="1" data-favorite="t1_s_2_1"
                            style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite js-btn-selector-favorite col hidden animated"
                            title="Save your favourites" aria-label="Save your favourites" data-show="2" data-favorite="t1_s_2_2"
                            style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite js-btn-selector-favorite col hidden animated"
                            title="Save your favourites" aria-label="Save your favourites" data-show="3" data-favorite="t1_s_2_3"
                            style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite js-btn-selector-favorite col hidden animated"
                            title="Save your favourites" aria-label="Save your favourites" data-show="4" data-favorite="t1_s_2_4"
                            style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <img class="selector__image img-responsive js-selector-img lazyload" src="http://jotun-gcc.com.dev02.allegro.no/assets/media/images/t1/Jotun_calm_ny4_0521.jpg"
                            data-show="1" data-src="http://jotun-gcc.com.dev02.allegro.no/assets/media/images/t1/Jotun_calm_ny4_0521.jpg" alt="WALL: JOTUN 12076 MODERN BEIGE">

                        <img class="selector__image img-responsive lazyload js-selector-img hidden" src="data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D&#39;http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg&#39; viewBox%3D&#39;0 0 1600 2100&#39;%2F%3E"
                            data-show="2" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ng2_0638.jpg" alt="WALL: JOTUN 12077 SHEER GREY">

                        <img class="selector__image img-responsive lazyload js-selector-img hidden" src="data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D&#39;http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg&#39; viewBox%3D&#39;0 0 1600 2100&#39;%2F%3E"
                            data-show="3" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny2_0653.jpg" alt="WALL: JOTUN 12077 SHEER GREY ">

                        <img class="selector__image img-responsive lazyload js-selector-img hidden" src="data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D&#39;http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg&#39; viewBox%3D&#39;0 0 1600 2100&#39;%2F%3E"
                            data-show="4" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_thelab_ny2_1221.jpg" alt="TABLE: JOTUN 10678 SPACE">
                        <span class="selector__loader"></span>
                    </figure>
                    <div class="selector__wrapper">
                        <div class="selector__controls">

                            <button type="button" class="btn btn--square js-btn-selector u-bg--12076" title="Choose colour"
                                data-show="1" data-color="WALL: JOTUN 12076 &lt;strong&gt;MODERN BEIGE&lt;/strong&gt;"></button>
                            <button type="button" class="btn btn--square js-btn-selector u-bg--12077" title="Choose colour"
                                data-show="2" data-color="WALL: JOTUN 12077 &lt;strong&gt;SHEER GREY&lt;/strong&gt;"></button>
                            <button type="button" class="btn btn--square js-btn-selector u-bg--12077" title="Choose colour"
                                data-show="3" data-color="WALL: JOTUN 12077 &lt;strong&gt;SHEER GREY&lt;/strong&gt; "></button>
                            <button type="button" class="btn btn--square js-btn-selector u-bg--10678" title="Choose colour"
                                data-show="4" data-color="TABLE: JOTUN 10678 &lt;strong&gt;SPACE&lt;/strong&gt;"></button>

                            <p class="selector__helper js-selector-helper">Chọn màu</p>
                            <p class="selector__helper js-selector-caption">WALL: JOTUN 12076 <strong>MODERN
                                    BEIGE</strong></p>
                        </div>
                    </div>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p><strong>CAM KẾT MÀU CHÍNH XÁC</strong> Công nghệ chất tạo màu tiên tiến của Jotun đem lại độ chính xác màu cao. Với sự cam kết màu chính xác, bạn có thể chắc chắn rằng màu thực tế sẽ giống như màu bạn chọn trên bảng màu.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-4">
                    <figure class="card__item--figure image-more" id="t1-image--9">
                        <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                            data-toggle="modal" data-target="#plus" data-colors="12086" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_r2_0602.jpg"
                            data-product="2">
                            <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                        </button>

                        <figure class="card__image">
                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                aria-label="Save your favourites" data-favorite="t1_9" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Save your favourites</span>
                            </button>

                            <picture>
                                <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_r2_0602.webp" type="image/webp"
                                    srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_r2_0602.webp">
                                <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_calm_r2_0602.jpg"
                                    data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_r2_0602.jpg" data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_r2_0602.jpg"
                                    alt="TABLE: JOTUN 12086 RUSTIC PINK" srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_r2_0602.jpg">
                            </picture>
                        </figure>

                        <figcaption class="card__caption card__caption--color card__caption--col js-card-caption card__caption--nocol">
                            <p>TABLE: JOTUN 12086 <strong>RUSTIC PINK</strong></p>
                        </figcaption>
                    </figure>

                    <h3 class="product__header">Cáti1nh</h3>
                    <p>Thuộc tính và sắc độ pha trộn hài hòa trong sự tương phản nhẹ nhàng giữa các gam màu trung tính mềm mại. Các dải hoa văn kết hợp tạo điểm nhấn cho sự đơn giản của không gian, đem một hơi thở ấm áp vào thực tế vào ngôi nhà.</p>

                    <div class="color-group">
                        <figure class="color-card  color-card--hex js-color-card u-bg--12086 wow slideRightFade"
                            data-color-id="12086" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12086 <strong>Rustic Pink</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12086" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                        <figure class="color-card  color-card--hex js-color-card u-bg--12075-01 wow slideRightFade"
                            data-color-id="12075-01" style="visibility: visible; animation-name: i;">
                            <figcaption class="color-card__caption">
                                <p>JOTUN 12075 <strong>Soothing Beige</strong></p>
                            </figcaption>

                            <div class="color-card__helper">
                                <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                    <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                        fullscreen</span>
                                </button>
                            </div>

                            <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                data-favorite="12075-01" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the colour chip to your favourites</span>
                            </button>
                        </figure>
                        <small><a href="<?php echo $this->config->item("calm_link"); ?>#allcolors" class="js-btn-scroll">Xem tất cả các màu cùng nhau ở cuối trang</a></small>
                    </div>
                </div>

                <figure class="card__item--figure col-xs-12 col-lg-8 image-more" id="t1-image--10">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="10679 12075-01" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_thelab_seap1_1587_v2.jpg"
                        data-product="1 12">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t1_10" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_thelab_seap1_1587_v2.webp" type="image/webp"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_thelab_seap1_1587_v2.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_thelab_seap1_1587_v2.jpg"
                                data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_thelab_seap1_1587_v2.jpg" data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_thelab_seap1_1587_v2.jpg"
                                alt="WALL DESIGN: SHIMMERING CLOUD BASE COLOUR: JOTUN 10679 WASHED LINEN GLAZED OVER WITH MAJESTIC DESIGN PEARL 9958 BIANCO" srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_thelab_seap1_1587_v2.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col js-card-caption card__caption--color">
                        <p>WALL DESIGN: SHIMMERING <strong>CLOUD</strong><br> COLOUR: JOTUN 10679 <strong>WASHED LINEN</strong><br> GLAZED OVER WITH MAJESTIC DESIGN <strong>PEARL 9958 BIANCO</strong> </p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p>Phủ lên tường một lớp hiệu ứng ánh
ngọc trai mờ ảo mê hoặc với sơn hiệu ứng 
ánh ngọc trai Majestic Design Pearl.
Kết hợp sơn hiệu ứng này với bộ sưu tập 
màu sắc đa dạng của Jotun giúp cho
ngôi nhà của bạn đẹp ấn tượng hoàn hảo. </p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--product" id="t1-product--3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="product__body">
                        <h3 class="product__title">MAJESTIC DESIGN PEARL</h3>
                        <figure class="product__image">
                            <picture>
                                <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint7.png" type="image/png" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint7.png">
                                <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint7.png" data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint7.png"
                                    data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint7.png" alt="MAJESTIC DESIGN PEARL" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint7.png">
                            </picture>

                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                aria-label="Add the product to your favourites" data-favorite="product_1" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the product to your favourites</span>
                            </button>
                        </figure>

                        <p class="product__tag">Những màu hài hòa, trầm lắng và trung tính</p>

                        <div class="product__btn">
                            <a href="https://www.jotun.com/my/en/b2c/products/interior/majestic/majestic-design-pearl.aspx" class="btn btn--line">
                                <span class="btn__text">Tìm hiểu thêm về sản phẩm</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--gallery">
        <div class="container">
            <div class="row">
                <figure class="card__item--figure col-xs-12 col-md-6 image-more" id="t1-image--11">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="20119" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_Raw_r1_1013.jpg"
                        data-product="2">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t1_11" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_Raw_r1_1013.webp" type="image/webp"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_Raw_r1_1013.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_Raw_r1_1013.jpg" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_Raw_r1_1013.jpg"
                                data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_Raw_r1_1013.jpg" alt="WALL: JOTUN 20119 TRANSPARENT PINK"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_Raw_r1_1013.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col js-card-caption card__caption--color">
                        <p>WALL: JOTUN 20119 <strong>TRANSPARENT PINK</strong></p>
                    </figcaption>
                </figure>

                <figure class="card__item--figure col-xs-12 col-md-6 image-more" id="t1-image--12">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="12076 1024" data-image="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny4_0516.jpg"
                        data-product="2 4">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t1_12" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny4_0516.webp" type="image/webp"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny4_0516.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_calm_ny4_0516.jpg" data-src="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny4_0516.jpg"
                                data-srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny4_0516.jpg" alt="WALL: JOTUN 12076 MODERN BEIGE CEILING: JOTUN 1024 TIMELESS SKIRTING: JOTUN 1024 TIMELESS WINDOW: JOTUN 1024 TIMELESS"
                                srcset="<?php echo base_url();?>assets/media/images/t1/Jotun_calm_ny4_0516.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col js-card-caption card__caption--color">
                        <p>WALL: JOTUN 12076  <strong>Modern Beige</strong><br> CEILING & WINDOW: JOTUN 1024 <strong>TIMELESS</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <section class="section section--cta text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>Chọn màu cho không gian của riêng bạn</h2>
                <p class="lead">Chọn màu sắc cho ngôi nhà có vẻ như là một công việc 
không hề đơn giản và tốn nhiều thời gian. Tuy nhiên với ứng 
dụng phối màu Jotun Color Design sẽ giúp cho quy trình 
này trở nên đơn giản hơn. Download tại <a href="https://itunes.apple.com/no/app/jotun-colourdesign/id1312722535?mt=8">App Store</a> eller <a href="https://play.google.com/store/apps/details?id=com.jotun.colourdesign">Google Play.</a></p>
            </div>
        </div>
    </section>

    <div class="section section--video js-section-video" id="mood">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Get the Calm look</h2>
                    <p>Create your personal space with colours designed by Jotun. Get ideas and inspiration on
                        how to use and combine colours from the CALM colour theme.</p>

                    <div class="column__item column__item--video">
                        <div class="video-container keep-16-9">
                            <span class="video-overlay js-video-overlay" data-bg="<?php echo base_url();?>assets/media/images/t1/video-2-calm.jpg"
                                data-bg-mobile="<?php echo base_url();?>assets/media/images/t1/video-2-calm.jpg" style="background-image: url(&quot;<?php echo base_url();?>assets/media/images/t1/video-2-calm.jpg&quot;);"></span>
                            <iframe class="video js-video" src="https://www.youtube.com/embed/vzx5r3eCnuE?enablejsapi=1&rel=0&showinfo=0" title="Create your personal space with colours designed by Jotun. Get ideas and inspiration on how to use and combine colours from the CALM colour theme."
                                src="" width="720" height="1280" frameborder="0"
                                allowfullscreen="" id="widget6"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--colors" id="allcolors">
        <div class="container">
            <div class="row">
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--1024 wow slideRightFade"
                        data-color-id="1024" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 1024 <strong>TIMELESS</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="1024" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--10678 wow slideRightFade"
                        data-color-id="10678" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 10678 <strong>SPACE</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="10678" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12075 wow slideRightFade"
                        data-color-id="12075" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12075 <strong>Soothing Beige</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12075" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12076 wow slideRightFade"
                        data-color-id="12076" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12076 <strong>Modern Beige</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12076" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--1622 wow slideRightFade"
                        data-color-id="1622" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 1622 <strong>EDELWEISS</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="1622" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12077 wow slideRightFade"
                        data-color-id="12077" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12077 <strong>Sheer Grey</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12077" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12078 wow slideRightFade"
                        data-color-id="12078" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12078" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--0394 wow slideRightFade"
                        data-color-id="0394" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 0394 <strong>SOFT GREY</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="0394" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--10290 wow slideRightFade"
                        data-color-id="10290" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 10290 <strong>Soft Touch</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="10290" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12083 wow slideRightFade"
                        data-color-id="12083" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12083 <strong>Devine</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12083" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--20119 wow slideRightFade"
                        data-color-id="20119" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 20119 <strong>Transparent Pink</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="20119" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12086 wow slideRightFade"
                        data-color-id="12086" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12086 <strong>Rustic Pink</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12086" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12074 wow slideRightFade"
                        data-color-id="12074" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12074 <strong>Peachy</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12074" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
            </div>
            <div class="row text-center">
                <p>Những màu sơn trên đây gần với màu sơn thực tế trong kỹ thuật in hiện đại cho phép. Khi yêu cầu có độ chính xác cao về màu sắc, nên tiến hành thi công mẫu. 
Những màu sắc trên chỉ chính xác cho sản phẩm Majestic hoặc các sản phẩm khác của Jotun khi sử dụng máy pha màu Jotun Multicolor.</p>
            </div>
        </div>
    </div>

    <section class="section section--cta text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>Cách sử dụng bảng màu</h2>
                <p class="lead">Tìm hiểu thêm về các màu sắc thuộc gam màu Nhẹ nhàng.</p>
                <a class="btn btn--line" href="#colors-t1" data-toggle="modal"
                    data-target="#colors-t1">
                    <span class="btn__text">Nhẹ nhàng</span>
                </a>
                <a class="btn btn--line" href="<?php echo $this->config->item("colour_link");?>">
                    <span class="btn__text">Xem tất cả 28 màu</span>
                </a>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Thêm màu</h2>
                <div class="card col-xs-12 col-sm-6 col-lg-4 col-lg-offset-2 ">
                    <div class="row">
                        <figure class="card__item card__item--figure col-xs-12" id="t2-image--card">
                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t2_0" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <a href="<?php echo $this->config->item("refined_link"); ?>" tabindex="-1">
                                    <picture>
                                        <source data-srcset="<?php echo base_url();?>assets/media/images/forside/Jotun_moderne_acc1_0590.webp"
                                            type="image/webp" srcset="<?php echo base_url();?>assets/media/images/forside/Jotun_moderne_acc1_0590.webp">
                                        <img class="card__image img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_moderne_acc1_0590.jpg"
                                            data-src="<?php echo base_url();?>assets/media/images/forside/Jotun_moderne_acc1_0590.jpg"
                                            data-srcset="<?php echo base_url();?>assets/media/images/forside/Jotun_moderne_acc1_0590.jpg" alt="CEILING: JOTUN PERFECTION 1453 Vanilla PANEL WALL: JOTUN SUPREME FINISH MATT 10580 SOFT SKIN WALL (IN FRONT): JOTUN PURE COLOR 20046 SAVANNA SUNSET SKIRTING: JOTUN SUPREME FINISH MATT 20046 SAVANNA SUNSET"
                                            srcset="<?php echo base_url();?>assets/media/images/forside/Jotun_moderne_acc1_0590.jpg">
                                    </picture>
                                </a>
                            </figure>
                        </figure>
                        <div class="card__item card__body col-xs-12">
                            <h2 class="card__header">
                                <a class="" href="<?php echo $this->config->item("refined_link"); ?>">REFINED</a>
                            </h2>
                            <p>A vibrant palette of greens and yellows. <br class="visible-lg"><a href="<?php echo $this->config->item("refined_link"); ?>">Discover
                                    now</a></p>
                        </div>
                    </div>
                </div>
                <div class="card col-xs-12 col-sm-6 col-lg-4">
                    <div class="row">
                        <figure class="card__item card__item--figure col-xs-12" id="t3-image--card">
                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t3_0" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <a href="<?php echo $this->config->item("raw_link"); ?>" tabindex="-1">
                                    <picture>
                                        <source data-srcset="<?php echo base_url();?>assets/media/images/forside/Jotun_Raw_r4-ng4_0854.webp"
                                            type="image/webp" srcset="<?php echo base_url();?>assets/media/images/forside/Jotun_Raw_r4-ng4_0854.webp">
                                        <img class="card__image img-responsive lazyload" src="<?php echo base_url();?>assets/dist/Jotun_Raw_r4-ng4_0854.jpg"
                                            data-src="<?php echo base_url();?>assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg"
                                            data-srcset="<?php echo base_url();?>assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg" alt="WALL: JOTUN PURE COLOR 6350 SOFT TEAL"
                                            srcset="<?php echo base_url();?>assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg">
                                    </picture>
                                </a>
                            </figure>
                        </figure>
                        <div class="card__item card__body col-xs-12">
                            <h2 class="card__header">
                                <a class="" href="<?php echo $this->config->item("raw_link"); ?>">RAW</a>
                            </h2>
                            <p>Warm colours of soil and sand, peaches and earthy reds. <br class="visible-lg"><a href="<?php echo $this->config->item("raw_link"); ?>">Discover
                                    now</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal modal--plus fade" id="plus" tabindex="-1" role="dialog" aria-labelledby="modal-label">
        <div class="modal-dialog" role="document">
            <div class="modal-slider col-md-6">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <figure class="background-image modal-header lazyload js-modal-image" style="background-image: url('assets/media/images/t1/Jotun_calm_a1_0621.jpg');">
                    <div class="modal-caption js-modal-caption"></div>
                </figure>
            </div>
            <div class="modal-info">
                <i class="material-icons zoom js-zoom" tabindex="0"></i>
                <div class="js-modal-info"></div>
            </div>
            <div class="modal-content col-md-6">
                <div class="container">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 color-group js-modal-colors"></div>
                            <div class="col-xs-12 js-modal-products"></div>
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <div class="buttons js-modal-buttons">
                            <div class="btn-row">
                                <a class="btn btn--line" href="<?php echo $this->config->item("wishlist_link"); ?>">
                                    <span class="btn__text">Yêu thích <span class="btn--heart"><i class="material-icons material-icons--second"></i>
                                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span></span></span>
                                </a>
                                <a class="btn btn--line" href="http://jotun-gcc.com.dev02.allegro.no/products" style="display:none;">
                                    <span class="btn__text">Choose the right product</span>
                                </a>
                            </div>
                            <div class="btn-row">
                            <div class="btn btn--line btn--facebook js-btn-facebook fb-share-button" data-href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/calm" data-layout="button" data-size="large" data-mobile-iframe="true">
                                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com%2Fcalm&amp;src=sdkpreparse" ><i class="icon icon--facebook"></i> Share to Facebook</a>
                            </div>
                                <button class="btn btn--line btn--pinterest pinterest-pin-it"><i class="icon icon--pinterest"></i>Pin
                                    to Pinterest</button>
                            </div>
                            <div class="btn-row">
                                <button type="button" class="btn btn--line" data-dismiss="modal">
                                    <span class="btn__text">Back</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal--colors fade" id="colors-t1" tabindex="-1" role="dialog" aria-labelledby="modal-label">
        <div class="modal-dialog" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <div class="modal-content">
                <div class="modal-body">

                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-6 col-lg-offset-3">
                                <h2 class="text-center">Các sử dụng gam màu Nhẹ nhàng</h2>

                                <figure class="color-card color-card--hex js-color-card u-bg--1024" data-color-id="1024">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 1024 <strong>TIMELESS</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="1024">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A yellowy, grey white nuance. It is lightly
                                    coloured and will scarcely break against white details. Consider trying it
                                    with the new beige nuances 12075 Soothing Beige and 12076 Modern Beige.
                                    Timeless is also nice in combination with green nuances such as 8252 Green
                                    Harmony og 8469 Green Leaf, the new subdued green 7628 Treasure and 7629
                                    Antique Green. <br><strong>FIND THE RIGHT WHITE:</strong>1024 Timeless
                                    functions harmoniously with 1624 Skylight, 1001 Egg White and 1453 Cotton
                                    Ball. It may appear slightly greenish and golden against cold white shades
                                    such as 9918 Morning Fog or 7236 Jazz.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--10678" data-color-id="10678">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 10678 <strong>SPACE</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="10678">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A bright, subdued, slightly greyish beige
                                    nuance. It appears neither reddish nor too golden, - a perfect, calm beige
                                    nuance which combines well with many shades. If you are looking for a calm,
                                    harmonious atmosphere with beige and brown tones, the new 12075 Soothing
                                    Beige and 12076 Modern Beige will work beautifully against it. <br><strong>FIND
                                        THE RIGHT WHITE:</strong>10678 Space works well against 9918 Morning Fog, 1624 Skylight, 1001 Egg White and 1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12075" data-color-id="12075">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12075 <strong>Soothing Beige</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12075">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A golden beige nuance. This colour may
                                    appear similar to the popular 1140 Sand, but it will appear slightly more
                                    subdued - a faint blackish veil will spread across the colour. The colour
                                    is perfect against darker brown nuances such as 10965 Hipster Brown, 1623
                                    Marrakesh or 1929 Nutmeg. Among coloured tones, a subdued green will be a
                                    lovely combination, - check out 8252 Green Harmony, 8469 Green Leaf or 8494
                                    Organic Green. <br><strong>FIND THE RIGHT WHITE:</strong> 12075 Soothing
                                    Beige is nice against 9918 Morning Fog, 1624 Skylight, 1001 Egg White and
                                    1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12076" data-color-id="12076">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12076 <strong>Modern Beige</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12076">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued beige nuance. The colour is
                                    slightly darker than 12075 Soothing Beige and 1140 Sand, but brighter than
                                    our classic 1929 Nutmeg and 1623 Marrakesh. Moreover, these colours fit
                                    perfectly together. Burnt reddish brown nuances, such as 2859 Whispering
                                    Red and 20118 Amber Red, is also an attractive combination.<br><strong>FIND
                                        THE RIGHT WHITE:</strong> 12076 Modern Beige works well with 9918
                                    Classic White, 1624 Skylight, 1001 Egg White and 1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--1622" data-color-id="1622">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 1622 <strong>EDELWEISS</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="1622">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A slightly greyish white nuance. The colour
                                    has a reddish undertone, which will take many by surprise as it is barely
                                    visible at first sight. Soft and rustic reds, such as the new 12084 Dusky
                                    Peach, 12085 Rural, 20120 Organic Red and 2856 Warm Blush are great, warm
                                    combinations against 1622 Edelweiss. <br><strong>FIND THE RIGHT WHITE:</strong>
                                    1622 Edelweiss works well with 9918 Morning Fog, - but appears wonderful
                                    alone, both as wall and detail colour if so desired.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12077" data-color-id="12077">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12077 <strong>Sheer Grey</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12077">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A bright, warm grey nuance. It is neither
                                    beige nor bluish grey, - rather more like a bright, cool mole nuance. 12077
                                    Sheer Grey works perfectly against grey tones such as 10342 Sable Stone,
                                    but the reddish undertones also appear exciting for anyone wanting to
                                    pursue the red tones, in the form of the golden pink tones 20046 Savanna
                                    Sunset and 20047 Blushing Peach. Blue tones such as 4618 Evening Light,
                                    4638 Elegant Blue, 4477 Deco Blue og 4744 Sophisticated Blue are gorgeous
                                    combinations, as are the more reddish blue tones like 4109 Gustivian Blue.<br><strong>FIND
                                        THE RIGHT WHITE:</strong> 12077 Sheer Grey is at its most favourable
                                    with pure whites such as 7236 Jazz, 9918 Morning Fog or the warm greyish
                                    white 1622 Edelweiss. </p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12078" data-color-id="12078">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12078">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A warm grey nuance. 12078 Comfort Grey is a
                                    brighter version of the popular 0394 Soft Grey. This colour looks quite
                                    cool against contrasts such as the red tones 20120 Organic Red or 20118
                                    Amber Red, check it also out against the darker green tone 7629 Antique
                                    Green. Blue tones such as 4618 Evening Light, 4638 Elegant Blue, 4477 Deco
                                    Blue and 4744 Sophisticated Blue works harmoniously with this grey nuance,
                                    as do the warm greys 12077 Sheer Grey, 1352 Form, 10429 Discrete, 10853
                                    Velvet Grey and 10249 Sober. <br><strong>FIND THE RIGHT WHITE:</strong>
                                    12078 Comfort Grey works perfect with pure white colours 7236 Jazz, 9918
                                    Classic White or 1624 Skylight, but may also be combined with 1001 Egg
                                    White and 1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--0394" data-color-id="0394">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 0394 <strong>SOFT GREY</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="0394">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A warm grey nuance. This is a suitable grey
                                    tone if you are looking for warm grey nuances in your house. This has been
                                    a Jotun favourite among greys for many years! Works well with other grey
                                    nuances such as 12077 Sheer Grey, 1024 Timeless, 1352 Form, 10679 Washed
                                    Linen or 1376 Mist. Try it out with green tones such as 8494 Organic Green,
                                    8469 Green Leaf and 8252 Green Harmony, - it looks great! Against pink
                                    nuances such as 20046 Savanna Sunset, 20047 Blushing Peach, 2024 Senses or
                                    2771 Rustic Terracotta it will also appear quite cool. <br><strong>FIND THE
                                        RIGHT WHITE:</strong> 0394 Soft Grey functions well with 9918 Morning Fog , 1624 Skylight, 1001 Egg White and 1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--10290" data-color-id="10290">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 10290 <strong>Soft Touch</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="10290">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A faint peach nuance. The colour is subdued
                                    and greyish. Comparatively it is brighter, less pink and more golden than
                                    10580 Soft Skin, well known to many. It works beautifully with subdued
                                    white nuances such as 1622 Edelweiss, but also with the more golden
                                    whites, thanks to its yellow undertones. Combined with colours such as
                                    12083 Devine and 12084 Dusky Peach it forms a lovely peach coloured
                                    harmony. <br><strong>FIND THE RIGHT WHITE:</strong> 12090 Soft Touch
                                    functions well with 9918 Morning Fog, 1624 Skylight, 1001 Egg White and
                                    1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12083" data-color-id="12083">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12083 <strong>Devine</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12083">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued peach nuance. The colour is
                                    golden, subdued and pleasant. It is not really pink, it has more of a
                                    greyish apricot/peach nuance. 12083 Devine works well with golden bright
                                    nuances such as 1024 Timeless and 1376 Mist, or with other peach nuances
                                    such as 12084 Dusky Peach and 12085 Rural. It may surprise you in a
                                    positive way, when combined with green nuances such as 7628 Treasure. <br><strong>FIND
                                        THE RIGHT WHITE:</strong> 12083 Devine works well with 9918 Morning Fog, 1624 Skylight, 1001 Egg White and 1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--20119" data-color-id="20119">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 20119 <strong>Transparent Pink</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="20119">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued pink nuance. The colour is golden
                                    with grey undertones. Compared to ordinary pink nuances, it appears far
                                    more golden and subdued. The colour combines beautifully with the other
                                    pink tones 12086 Rustic Pink, 2024 Senses and 20120 Organic Red. Among the
                                    quite bright tones, both 1024 Timeless, 1376 Mist and 1622 Edelweiss look
                                    great as combinations. It works well with 0394 Soft Grey, 1352 Form, 1032
                                    Iron Grey to mention a few greys. Green nuances such as 7628 Treasure and
                                    7629 Antique Green may be exciting contrasts to the sweeter 20119
                                    Transparent Pink. <br><strong>FIND THE RIGHT WHITE:</strong> 20119
                                    Transparent Pink appears at its best with 9918 Morning Fog, 7236 Jazz and
                                    to some extent 1624 Skylight. 1001 Egg White and 1453 Vanilla may
                                    appear yellow when combined with this colour.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12086" data-color-id="12086">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12086 <strong>Rustic Pink</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12086">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued pink tone. The colour is somewhat
                                    golden. We may say that 10286 Rustic Pink is a brighter version of the
                                    well-known 2024 Senses - they are beautiful together. Also try it with
                                    20120 Organic Red or 20119 Transparent Pink, <br><strong>FIND THE RIGHT
                                        WHITE:</strong> 12086 Rustic Pink functions well with 9918 Morning Fog, 1624 Skylight, 1001 Egg White. 1453 Vanilla may turn in a golden
                                    direction when combined with 10286 Rustic Pink.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12074" data-color-id="12074">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12074 <strong>Peachy</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the
                                                colour in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12074">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued peach nuance. 12074 is a greyish
                                    peach tone, but it will appear fresh enough on your wall. It is brighter
                                    than 12085 Rural and slightly darker than 12084 Dusky Peach. 12074 Peachy
                                    creates a cool accent when combined with 7628 Treasure and 7629 Antique
                                    Green for the more daring, or merely as a fresh golden nuance among the
                                    beige tones such as 10678 Space, 12075 Soothing Beige, 12076 Modern Beige -
                                    or 1622 Edelweiss, 1024 Timeless, 10679 Washed Linen, 12077 Sheer Grey,
                                    12078 Comfort Grey and 0394 Soft Grey. The colour also combines well with
                                    12085 Rural. <br><strong>FIND THE RIGHT WHITE:</strong> 12074 Peachy works
                                    well with 9918 Morning Fog, 1624 Skylight, 1001 Egg White and 1453 Cotton
                                    Ball.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn--line" data-dismiss="modal">
                            <span class="btn__text">Back</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main><!-- .site-main -->

        <script>
            var params = {
        "badge_01" : {
            method      : "feed",
            link        : "http://localhost:7888/jotuntrends2/assets/media/images/t1/Jotun_calm_a2_0531.jpg>",
            picture     : "http://localhost:7888/jotuntrends2/assets/media/images/t1/Jotun_calm_a2_0531.jpg",
            name        : "image",
            caption     : "image",
            description : "image"
        },
        "badge_02" : {
            method      : "feed",
            link        : "http://localhost:7888/jotuntrends2/assets/media/images/t1/Jotun_calm_a2_0531.jpg",
            name        : "image",
            caption     : "iamge"
        }
    };
</script>

        <footer id="footer" class="footer section">
            <div class="container">
                <div class="btn-row">
                    <a class="btn btn--line" href="<?php echo base_url();?>wishlist">
                        <span class="btn__text">Yêu thích <span class="btn--heart"><i class="material-icons material-icons--second"></i>
                                <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span></span></span>
                    </a>
                </div>
                <div class="btn-row">
                    <a class="btn btn--line" href="<?php echo base_url();?>palettes">
                        <span class="btn__text">Create your colour palette</span>
                    </a>
                    <a class="btn btn--line" href="<?php echo base_url();?>products" style="display:none;">
                        <span class="btn__text">Choose the right product</span>
                    </a>
                </div>
                <div class="btn-row">
                <div class="btn btn--line btn--facebook js-btn-facebook fb-share-button" data-href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/" data-layout="button" data-size="large" data-mobile-iframe="true">
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com%2F&amp;src=sdkpreparse" ><i class="icon icon--facebook"></i> Share to Facebook</a>
                </div>
                    <button class="btn btn--line btn--pinterest pinterest-pin-it"><i class="icon icon--pinterest"></i>Pin
                        to Pinterest</button>
                </div>
                <a href="https://jotun.com/" class="footer__logo">
                    <img data-src="assets/media/logos/jotun.svg" class=" lazyload" alt="JOTUN" src="./assets/dist/jotun.svg">
                    <span class="sr-only">JOTUN</span>
                </a>
                <ul class="footer-links list-unstyled">

                    <li>
                        <a class="ladybloggen" href="https://www.jotun.com/no/en/corporate/Termsandconditionscorporate.aspx"
                            target="_blank">
                            Privacy, terms &amp; condition and cookie policy
                        </a>
                    </li>
                </ul>
            </div>
        </footer>
        <div class="fullscreencolor color-card--fullscreen js-color-card--fullscreen" role="dialog" aria-modal="true"
            aria-labelledby="fullscreenCaption" aria-describedby="colorinfo" tabindex="-1">
            <div class="fullscreencolor__color">

                <button type="button" class="close js-color-card-close" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                <button type="button" class="fullscreencolor__expand js-color-fullscreen-toggle" aria-label="View the colour in fullscreen"
                    aria-expanded="true" aria-controls="fullscreenContent">
                    <i class="material-icons">fullscreen</i>
                </button>


                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" aria-label="Add the colour chip to your favourites">
                    <i class="material-icons material-icons--first"></i>
                    <i class="material-icons material-icons--second"></i>
                    <span class="sr-only">Add the colour chip to your favourites</span>
                </button>

                <div class="fullscreencolor__caption fullscreencolor__caption--color js-color-card--caption" id="fullscreenCaption"></div>
            </div>

            <div class="fullscreencolor__content" id="fullscreenContent">
                <div class="fullscreencolor__caption js-color-card--caption"></div>

                <ul class="nav nav-tabs" role="tablist" id="colorTabs">
                    <li role="presentation" class="active">
                        <a href="<?php echo $this->config->item('calm_link'); ?>#colorinfo" aria-controls="colorinfo" role="tab"
                            data-toggle="tab">About the colour</a>
                    </li>
                    <li role="presentation">
                        <a href="<?php echo $this->config->item('calm_link'); ?>#colormatching" aria-controls="colormatching"
                            role="tab" data-toggle="tab">Matching colours</a>
                    </li>
                </ul>

                <div class="fullscreencolor__inner">
                    <div class="container">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="colorinfo">
                                <div class="fullscreencolor__info">
                                    <p class="js-disclaimer-color"></p>

                                    <p class="minerals-mix-text">modal.minerals.mix.text</p>
                                    <p class="ceiling-mix-text">modal.ceiling.mix.text</p>

                                           <p>
                                <strong>CAM KẾT MÀU CHÍNH XÁC</strong><br>
                                Công nghệ chất tạo màu tiên tiến của Jotun đem lại độ chính xác màu cao. Với sự cam kết màu chính xác, bạn có thể chắc chắn rằng màu thực tế sẽ giống như màu bạn chọn trên bảng màu
                            </p>

                                   <p>
                                    <strong>TÁI TẠO MÀU</strong><br>
                                    Xin lưu ý rằng màu sắc sẽ khác nhau tùy thuộc vào cài đặt màn hình và độ phân giải của bạn. Ghé thăm cửa hàng Jotun gần nhất để xem các màu mẫu chính xác.
                                </p>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="colormatching">
                                <div class="fullscreencolor__matching"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cookie js-cookie" role="alert">
            <div class="container">
                <div class="cookie__row">
                    <div class="cookie__left">
                        <p>
                            By using the Jotun sites, you are consenting to our use of cookies in accordance with this
                            Cookie Policy. If you do not agree to our use of cookies in this way, you should set your
                            browser settings accordingly or not use the Jotun Sites. If you disable the cookies that we
                            use, this may impact your user experience while on the Jotun Sites.
                        </p>
                    </div>
                    <div class="cookie__right">
                        <button type="button" class="cookie__close js-cookie-close">
                            Do not show again
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="assets/dist/messages.js"></script>
        <script type="text/javascript" src="assets/dist/jquery.min.js"></script>
        <script type="text/javascript" src="assets/dist/custom.js"></script>



        <script>
            $(window).load(function () {
                $('.js-btn-facebook').click(function () {
                    FB.ui(params[$(this).attr('rel')]);
                    return false;
                });
            });
        </script>

</body>

</html>