<main id="main" class="site-main">
    <section class="section text-center" id="palettes">
        <div class="palettes">
            <div class="palettes__content">
                <div class="palettes__area palettes__area--select is-active">
                    <div class="palettes__row">
                        <h1 class="palettes__title">Tạo bảng màu nội thất của riêng bạn </h1>

                        <div class="guide">
                            <div class="container">
                                <div class="row">
                                    <div class="guide__item col-xs-12 col-sm-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="88.66" height="93.03" viewBox="0 0 88.66 93.03"
                                            class="guide__svg">
                                            <title>pick-a-colour</title>
                                            <rect x="36.53" y="1.7" width="50.14" height="50.71" rx="1.61" ry="1.61"
                                                fill="#fff"></rect>
                                            <path d="M-607,325.17l-47.56-.3a1,1,0,0,1-1-1V275.71a1,1,0,0,1,1-1h48.21a1,1,0,0,1,1,1v48.21a1,1,0,0,1-1,1"
                                                transform="translate(692.04 -272.75)" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></path>
                                            <path d="M-669.17,334.35h-20.21a0.66,0.66,0,0,1-.66-0.66V300.47a0.66,0.66,0,0,1,.66-0.66h33.23a0.66,0.66,0,0,1,.66.66V333.7a0.66,0.66,0,0,1-.66.66"
                                                transform="translate(692.04 -272.75)" fill="#eae8e8"></path>
                                            <path d="M-674.37,334.35h18.21a0.66,0.66,0,0,0,.66-0.66V300.47a0.66,0.66,0,0,0-.66-0.66h-33.23a0.66,0.66,0,0,0-.66.66V333.7a0.66,0.66,0,0,0,.66.66"
                                                transform="translate(692.04 -272.75)" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></path>
                                            <line x1="11.87" y1="61.6" x2="10.87" y2="61.6" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                            <path d="M-627.93,344l7.63,6.37a7.87,7.87,0,0,1,7.87-7.87H-611a5.06,5.06,0,0,0,5.07-5.07,5.06,5.06,0,0,0-5.07-5.07h-12.88Z"
                                                transform="translate(692.04 -272.75)" fill="#fff" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></path>
                                            <path d="M-660.82,326v24.77a12.93,12.93,0,0,0,.54,3.71,13,13,0,0,0,12.48,9.31h12.16a15.33,15.33,0,0,0,15.34-15.34V308.07a5.07,5.07,0,0,0-5.07-5.07,5,5,0,0,0-3.58,1.49,5,5,0,0,0-1.49,3.58v11.4a5.06,5.06,0,0,0-5.07-5.07,5.07,5.07,0,0,0-5.07,5.07v3a5.06,5.06,0,0,0-5.07-5.07,5.07,5.07,0,0,0-5.07,5.07V326a5.06,5.06,0,0,0-5.07-5.07A5.07,5.07,0,0,0-660.82,326Z"
                                                transform="translate(692.04 -272.75)" fill="#fff" stroke="#555"
                                                stroke-miterlimit="10" stroke-width="4"></path>
                                            <line x1="61.6" y1="46.71" x2="61.6" y2="56.57" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                            <line x1="51.48" y1="46.71" x2="51.48" y2="56.57" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                            <line x1="41.35" y1="49.73" x2="41.35" y2="56.57" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                        </svg>
                                        <h3 class="guide__title">1. Chọn một màu</h3>
                                        <p class="guide__desc">Chọn một màu trong bảng màu</p>
                                    </div>
                                    <div class="guide__item col-xs-12 col-sm-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="109.25" height="93.71" viewBox="0 0 109.25 93.71"
                                            class="guide__svg">
                                            <title>see-combinations</title>
                                            <rect x="23.16" y="23.16" width="42.33" height="42.33" fill="#fff"></rect>
                                            <rect x="23.16" y="2" width="42.33" height="21.16" fill="#eae8e8"></rect>
                                            <rect x="23.16" y="65.49" width="42.83" height="21.57" fill="#eae8e8"></rect>
                                            <rect x="2" y="23.16" width="21.16" height="42.73" fill="#eae8e8"></rect>
                                            <rect x="66" y="23.16" width="20.66" height="42.73" fill="#eae8e8"></rect>
                                            <polyline points="65.5 2 65.5 23.16 23.16 23.16 23.16 2" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></polyline>
                                            <polyline points="23.16 87.06 23.16 65.5 65.5 65.5 65.5 87.06" fill="none"
                                                stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="4"></polyline>
                                            <polyline points="86.66 65.5 65.5 65.5 65.5 23.16 86.66 23.16" fill="none"
                                                stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="4"></polyline>
                                            <polyline points="23.16 44.07 23.16 23.16 2 23.16" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></polyline>
                                            <polyline points="2 65.5 23.16 65.5 23.16 62.78" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></polyline>
                                            <line x1="44.83" y1="44.33" x2="44.33" y2="44.33" fill="#fff" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                            <line x1="78.99" y1="44.33" x2="50.33" y2="44.33" fill="#fff" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                            <path d="M-781.32,343.91l-6.48,5.41a6.68,6.68,0,0,0-6.68-6.68h-1.19a4.3,4.3,0,0,1-4.3-4.3,4.29,4.29,0,0,1,4.3-4.3h10.94Z"
                                                transform="translate(860.64 -272.08)" fill="#fff" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></path>
                                            <path d="M-753.39,331.7v21a11,11,0,0,1-.46,3.15,11.05,11.05,0,0,1-10.6,7.9h-10.33a13,13,0,0,1-13-13v-34.3a4.31,4.31,0,0,1,4.3-4.3,4.25,4.25,0,0,1,3,1.26,4.25,4.25,0,0,1,1.26,3v9.68a4.3,4.3,0,0,1,4.3-4.3,4.31,4.31,0,0,1,4.3,4.3v2.56a4.3,4.3,0,0,1,4.3-4.3,4.31,4.31,0,0,1,4.3,4.3v3a4.3,4.3,0,0,1,4.3-4.3A4.31,4.31,0,0,1-753.39,331.7Z"
                                                transform="translate(860.64 -272.08)" fill="#fff" stroke="#555"
                                                stroke-miterlimit="10" stroke-width="4"></path>
                                            <line x1="81.45" y1="54.07" x2="81.45" y2="62.44" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                            <line x1="90.05" y1="54.07" x2="90.05" y2="62.44" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                            <line x1="98.64" y1="56.63" x2="98.64" y2="62.44" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                            <line x1="23.16" y1="57.23" x2="23.16" y2="55.1" fill="none" stroke="#555"
                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                        </svg>
                                        <h3 class="guide__title">2. Xem phương án phối màu</h3>
                                        <p class="guide__desc">Khám phá các màu hài hòa bổ sung cho màu cơ bản đã chọn của bạn</p>
                                    </div>
                                    <div class="guide__item col-xs-12 col-sm-4">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                            width="129.39" height="131.6" viewBox="0 0 129.39 131.6" class="guide__svg">
                                            <defs>
                                                <clippath id="a" transform="translate(266.78 26.33)">
                                                    <rect x="-266.78" y="-26.33" width="129.39" height="131.6" fill="none"></rect>
                                                </clippath>
                                            </defs>
                                            <title>get-the-right-white</title>
                                            <g clip-path="url(#a)">
                                                <path d="M-262.42,73.31l26.3-96.14,43.89,12-25.55,96.16a22.69,22.69,0,0,1-5.9,10.39c-4.17,4.11-11.19,7.75-22.2,4.74C-257.48,97.29-261.5,90-262.79,84.17a22.72,22.72,0,0,1,.37-10.87"
                                                    transform="translate(266.78 26.33)" fill="#fff"></path>
                                                <path d="M-262.42,73.31l26.3-96.14,43.89,12-25.55,96.16a22.69,22.69,0,0,1-5.9,10.39c-4.17,4.11-11.19,7.75-22.2,4.74C-257.48,97.29-261.5,90-262.79,84.17A22.72,22.72,0,0,1-262.42,73.31Z"
                                                    transform="translate(266.78 26.33)" fill="none" stroke="#555"
                                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="7"></path>
                                                <path d="M-241.67,78.25a1.9,1.9,0,0,1,2.33-1.33A1.89,1.89,0,0,1-238,79.25a1.9,1.9,0,0,1-2.33,1.33,1.89,1.89,0,0,1-1.33-2.33"
                                                    transform="translate(266.78 26.33)" fill="#fff"></path>
                                                <circle cx="-239.84" cy="78.74" r="1.9" transform="translate(14.24 -147.04) rotate(-74.71)"
                                                    fill="none" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                                    stroke-width="7"></circle>
                                                <path d="M-243.14,43.41l2,0.56" transform="translate(266.78 26.33)"
                                                    fill="#fff"></path>
                                                <path d="M-243.14,43.41l2,0.56" transform="translate(266.78 26.33)"
                                                    fill="none" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                                    stroke-width="7"></path>
                                                <path d="M-237.26,22c1.21,0.33,12.17,3,12.17,3" transform="translate(266.78 26.33)"
                                                    fill="#fff"></path>
                                                <path d="M-237.26,22c1.21,0.33,12.17,3,12.17,3" transform="translate(266.78 26.33)"
                                                    fill="none" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                                    stroke-width="7"></path>
                                                <path d="M-231.46.73l24.11,6.6" transform="translate(266.78 26.33)"
                                                    fill="#fff"></path>
                                                <path d="M-231.46.73l24.11,6.6" transform="translate(266.78 26.33)"
                                                    fill="none" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                                    stroke-width="7"></path>
                                                <path d="M-254.34,59.73L-175.11-.74l27.6,36.17-78.65,60.94a22.71,22.71,0,0,1-11,4.68c-5.81.74-13.6-.63-20.52-9.7-7.3-9.57-6.08-17.8-3.56-23.21a22.73,22.73,0,0,1,6.89-8.41"
                                                    transform="translate(266.78 26.33)" fill="#eaeaea"></path>
                                                <path d="M-254.34,59.73L-175.11-.74l27.6,36.17-78.65,60.94a22.71,22.71,0,0,1-11,4.68c-5.81.74-13.6-.63-20.52-9.7-7.3-9.57-6.08-17.8-3.56-23.21A22.73,22.73,0,0,1-254.34,59.73Z"
                                                    transform="translate(266.78 26.33)" fill="none" stroke="#555"
                                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="7"></path>
                                                <path d="M-240.85,76.24a1.89,1.89,0,0,1,2.66.36,1.9,1.9,0,0,1-.36,2.66,1.9,1.9,0,0,1-2.66-.36,1.9,1.9,0,0,1,.36-2.66"
                                                    transform="translate(266.78 26.33)" fill="#eaeaea"></path>
                                                <circle cx="-239.7" cy="77.74" r="1.9" transform="matrix(0.79, -0.61, 0.61, 0.79, 170.47, -103.14)"
                                                    fill="none" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                                    stroke-width="7"></circle>
                                                <line x1="45.9" y1="73.99" x2="46.84" y2="75.34" fill="#eaeaea"></line>
                                                <line x1="45.9" y1="73.99" x2="46.84" y2="75.34" fill="none" stroke="#555"
                                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="7"></line>
                                                <path d="M-203.19,34.16c0.76,1,9.33,11.85,9.33,11.85" transform="translate(266.78 26.33)"
                                                    fill="#eaeaea"></path>
                                                <path d="M-203.19,34.16c0.76,1,9.33,11.85,9.33,11.85" transform="translate(266.78 26.33)"
                                                    fill="none" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                                    stroke-width="7"></path>
                                                <path d="M-185.7,20.82l15.17,19.87" transform="translate(266.78 26.33)"
                                                    fill="#eaeaea"></path>
                                                <path d="M-185.7,20.82l15.17,19.87" transform="translate(266.78 26.33)"
                                                    fill="none" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                                    stroke-width="7"></path>
                                                <path d="M-183.4,101.31l-57,.46A22.71,22.71,0,0,1-252,98.82c-5.07-2.94-10.43-8.75-10.43-20.16,0-12,6-17.84,11.25-20.62a22.72,22.72,0,0,1,10.58-2.51h99.67V101l-13.27.1"
                                                    transform="translate(266.78 26.33)" fill="#fff"></path>
                                                <path d="M-183.4,101.31l-57,.46A22.71,22.71,0,0,1-252,98.82c-5.07-2.94-10.43-8.75-10.43-20.16,0-12,6-17.84,11.25-20.62a22.72,22.72,0,0,1,10.58-2.51h99.67V101l-13.27.1"
                                                    transform="translate(266.78 26.33)" fill="none" stroke="#555"
                                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="7"></path>
                                                <path d="M-239.84,76.85a1.9,1.9,0,0,1,1.9,1.9,1.9,1.9,0,0,1-1.9,1.9,1.9,1.9,0,0,1-1.9-1.9,1.9,1.9,0,0,1,1.9-1.9"
                                                    transform="translate(266.78 26.33)" fill="#fff"></path>
                                                <circle cx="26.94" cy="105.07" r="1.9" fill="none" stroke="#555"
                                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="7"></circle>
                                                <line x1="60.14" y1="92.58" x2="60.14" y2="117.58" fill="#fff"></line>
                                                <line x1="60.14" y1="92.58" x2="60.14" y2="117.58" fill="none" stroke="#555"
                                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="7"></line>
                                                <line x1="82.39" y1="92.58" x2="82.39" y2="117.58" fill="#fff"></line>
                                                <line x1="82.39" y1="92.58" x2="82.39" y2="117.58" fill="none" stroke="#555"
                                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="7"></line>
                                                <line x1="104.39" y1="92.58" x2="104.39" y2="117.58" fill="#fff"></line>
                                                <line x1="104.39" y1="92.58" x2="104.39" y2="117.58" fill="none" stroke="#555"
                                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="7"></line>
                                                <line x1="100.85" y1="127.5" x2="103.94" y2="127.5" fill="#fff"></line>
                                                <line x1="100.85" y1="127.5" x2="103.94" y2="127.5" fill="none" stroke="#555"
                                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="7"></line>
                                            </g>
                                        </svg>
                                        <h3 class="guide__title">3. Chọn màu trắng phù hợp</h3>
                                        <p class="guide__desc">Xem màu trắng hoàn hảo tương ứng phù hợp với các màu trong bảng màu của bạn</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="palettes__list">




                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--3377 js-palettes-color-card"
                                data-color-id="3377">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 3377 <strong>Slate Lavender</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12074 js-palettes-color-card"
                                data-color-id="12074">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12074 <strong>Peachy</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--20118 js-palettes-color-card"
                                data-color-id="20118">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 20118 <strong>Amber Red</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--7613 js-palettes-color-card"
                                data-color-id="7613">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 7613 <strong>Northern Mystic</strong></p>
                                </div>
                            </button>

                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--1024 js-palettes-color-card"
                                data-color-id="1024">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 1024 <strong>TIMELESS</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--10678 js-palettes-color-card"
                                data-color-id="10678">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 10678 <strong>SPACE</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12075 js-palettes-color-card"
                                data-color-id="12075">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12075 <strong>Soothing Beige</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12076 js-palettes-color-card"
                                data-color-id="12076">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12076 <strong>Modern Beige</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--1622 js-palettes-color-card"
                                data-color-id="1622">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 1622 <strong>Edelweiss</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12077 js-palettes-color-card"
                                data-color-id="12077">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12077 <strong>Sheer Grey</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12078 js-palettes-color-card"
                                data-color-id="12078">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12078 <strong>Comfort Grey</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--0394 js-palettes-color-card"
                                data-color-id="0394">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 0394 <strong>SOFT GREY</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--7626 js-palettes-color-card"
                                data-color-id="7626">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 7626 <strong>Airy Green</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--7627 js-palettes-color-card"
                                data-color-id="7627">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 7627 <strong>Refresh</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--7628 js-palettes-color-card"
                                data-color-id="7628">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 7628 <strong>Treasure</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--7629 js-palettes-color-card"
                                data-color-id="7629">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 7629 <strong>Antique Green</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12079 js-palettes-color-card"
                                data-color-id="12079">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12079 <strong>Gleam</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12080 js-palettes-color-card"
                                data-color-id="12080">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12080 <strong>Soft Radiance</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12081 js-palettes-color-card"
                                data-color-id="12081">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12081 <strong>Silky Yellow</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12082 js-palettes-color-card"
                                data-color-id="12082">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12082 <strong>Refined Yellow</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--10290 js-palettes-color-card"
                                data-color-id="10290">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 10290 <strong>Soft Touch</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12083 js-palettes-color-card"
                                data-color-id="12083">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12083 <strong>Devine</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12084 js-palettes-color-card"
                                data-color-id="12084">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12084 <strong>Dusky Peach</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12085 js-palettes-color-card"
                                data-color-id="12085">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12085 <strong>Rural</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--20119 js-palettes-color-card"
                                data-color-id="20119">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 20119 <strong>Transparent Pink</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--12086 js-palettes-color-card"
                                data-color-id="12086">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 12086 <strong>Rustic Pink</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--2024 js-palettes-color-card"
                                data-color-id="2024">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 2024 <strong>Senses</strong></p>
                                </div>
                            </button>
                            <button type="button" class="palettes__item color-card  color-card--hex u-bg--20120 js-palettes-color-card"
                                data-color-id="20120">
                                <div class="color-card__caption">
                                    <p>Select JOTUN 20120 <strong>Organic Red</strong></p>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="palettes__area palettes__area--result">
                    <div class="palettes__result">
                        <h2 class="palettes__title">Bảng màu nội thất của tôi</h2>
                        <p class="palettes__text">Các màu hài hòa bổ sung cho màu cơ sở đã chọn của bạn.</p>


                        <div class="palettes__colors">
                            <div class="palettes__base">
                                <h3 class="palettes__subheader">Màu nền bạn chọn</h3>
                                <div class="js-palette-base"></div>
                            </div>
                            <div class="palettes__groups">
                                <div class="palettes__section">
                                    <h3 class="palettes__subheader">Màu bổ sung</h3>
                                    <div class="palettes__group js-palette-group-1"></div>
                                </div>
                                <div class="palettes__section">
                                    <h3 class="palettes__subheader">Màu tương phản</h3>
                                    <div class="palettes__group js-palette-group-2"></div>
                                </div>
                                <div class="palettes__section">
                                    <h3 class="palettes__subheader">Màu trắng</h3>
                                    <div class="palettes__group js-palette-group-3"></div>
                                </div>
                            </div>
                        </div>

                        <div class="palettes__buttons">
                            <span class="palettes__button palettes__button--reset">
                                <button type="button" class="btn btn--line js-palettes-reset">
                                    <span class="btn__text"><i class="material-icons">replay</i>Tạo bảng màu mới</span>
                                </button>
                            </span>
                            <span class="palettes__button palettes__button--save">
                                <button type="button" class="btn btn--line" data-toggle="modal" data-target="#modal_save">
                                    <span class="btn__text"><i class="material-icons">save_alt</i> Lưu bảng màu</span>
                                </button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</main><!-- .site-main -->

<div class="modal modal--save fade" id="modal_save" tabindex="-1" role="dialog" aria-labelledby="save-label">
    <div class="modal-dialog" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <div class="modal-content">
            <div class="save">
                <div class="save__actions">
                    <div class="modal-body">
                        <div class="text-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="201.77" height="88.66" viewBox="0 0 201.77 88.66"
                                class="guide__svg guide__svg--save">
                                <title>save-group</title>
                                <line x1="72.39" y1="54.24" x2="73.39" y2="54.24" fill="none" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <polygon points="199.77 32.54 199.77 74.65 137.12 74.65 137.12 32.54 168.44 18.68 199.77 32.54"
                                    fill="#eee" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="4"></polygon>
                                <polyline points="168.44 14.01 148.74 14.01 148.74 65.35 188.14 65.35 188.14 14.01 182.53 14.01"
                                    fill="#eee" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="4"></polyline>
                                <line x1="177.13" y1="14.01" x2="177.84" y2="14.01" fill="none" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <polygon points="199.77 36.79 180.31 51.43 157.78 51.43 137.12 36.79 137.12 74.65 199.77 74.65 199.77 36.79"
                                    fill="#fff" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="4"></polygon>
                                <line x1="180.31" y1="51.43" x2="199.77" y2="74.65" fill="#eee" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <line x1="157.78" y1="51.43" x2="137.12" y2="74.65" fill="#eee" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <rect x="2" y="32.56" width="60.4" height="37.69" rx="5.13" ry="5.13" fill="#fff"
                                    stroke="#555" stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></rect>
                                <polyline points="32.2 14.13 12.58 14.13 12.58 32.59 51.82 32.59 51.82 14.13 46.23 14.13"
                                    fill="#eee" stroke="#555" stroke-linecap="round" stroke-linejoin="round"
                                    stroke-width="4"></polyline>
                                <rect x="12.58" y="58.12" width="39.24" height="16.41" fill="#eee" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></rect>
                                <line x1="40.85" y1="14.13" x2="41.56" y2="14.13" fill="none" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <line x1="12.22" y1="51.4" x2="12.94" y2="51.4" fill="none" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <line x1="17.93" y1="51.4" x2="18.65" y2="51.4" fill="none" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <line x1="51.46" y1="51.4" x2="52.18" y2="51.4" fill="none" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <line x1="80.39" y1="54.24" x2="81.39" y2="54.24" fill="none" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <line x1="127.39" y1="54.24" x2="128.39" y2="54.24" fill="none" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <path d="M-1042.52,274.26h-45.38a3.42,3.42,0,0,0-3.42,3.42v78.19a3.05,3.05,0,0,0,3.05,3.05h76.55a3.05,3.05,0,0,0,3.05-3.05v-67.2l-17.91-14.41"
                                    transform="translate(1150.38 -272.26)" fill="#eee" stroke="#555" stroke-linecap="round"
                                    stroke-linejoin="round" stroke-width="4"></path>
                                <line x1="113.72" y1="2" x2="114.82" y2="2" fill="none" stroke="#555" stroke-linecap="round"
                                    stroke-linejoin="round" stroke-width="4"></line>
                                <circle cx="100.39" cy="54.24" r="19.69" fill="#fff" stroke="#555" stroke-linecap="round"
                                    stroke-linejoin="round" stroke-width="4"></circle>
                                <rect x="70.38" y="2" width="28.03" height="20.74" fill="#fff" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></rect>
                                <line x1="78.72" y1="10.12" x2="78.72" y2="14.62" fill="#fff" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></line>
                                <polyline points="92.6 54.06 95.93 59.77 108.17 48.72" fill="none" stroke="#555"
                                    stroke-linecap="round" stroke-linejoin="round" stroke-width="4"></polyline>
                            </svg>
                            <p class="save__text" id="save-label">Tạo không gian cá tính của bạn với màu sắc và sản phẩm của JOTUN. Lưu bảng màu của bạn và ghé thăm các cửa hàng gần nhất để tìm màu theo sở thích của bạn.</p>
                        </div>
                    </div>
                    <div class="container">
                        <div class="modal-footer text-center">
                            <button type="button" class="save__button btn btn--transparent" id="palette_save">
                                <span class="btn__text"><i class="material-icons">save_alt</i> Tải</span>
                            </button>
                            <button type="button" class="save__button btn btn--transparent" id="palette_print">
                                <span class="btn__text"><i class="material-icons">print</i> In</span>
                            </button>
                            <button type="button" class="save__button btn btn--transparent" data-toggle="collapse"
                                data-target="#collapseForward" aria-expanded="false" aria-controls="collapseForward">
                                <span class="btn__text"><i class="material-icons">email</i> Chia sẻ</span>
                            </button>
                        </div>
                        <form action="h<?php echo $this->config->item("palette_link"); ?>" class="palettes__share" id="js-send-palette-email">
                            <div class="collapse" id="collapseForward">
                                <div class="palettes__share-group">
                                    <label class="form__group">
                                        <span class="sr-only">Enter your e-mail adress</span>
                                        <input class="form__input form-control js-validate-email" type="email" name="email"
                                            id="forward_email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
                                            placeholder="Nhập địa chỉ email của bạn">
                                        <span class="error error-invalid">Địa chỉ email không hợp lệ</span>
                                        <span class="error error-empty">Nhập địa chỉ email của bạn dưới đây</span>
                                    </label>

                                    <button class="btn btn--validate js-btn-share-email-finish" type="submit">Send
                                        my colour palette</button>
                                </div>

                                <div class="col-xs-12 js-share-email-fail hidden">
                                    <div class="alert alert-danger text-center">
                                        <p>palettes.share.error</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 js-share-email-success hidden">
                                <div class="alert alert-success text-center">
                                    <p>E-mail đã được gửi đến "<strong class="js-show-email-sent-to"></strong>"</p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="save__loading text-center">
                    <img class="lazyload guide__svg guide__svg--save" src="data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D&#39;http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg&#39; viewBox%3D&#39;0 0 273 120&#39;%2F%3E"
                        data-src="<?php echo base_url(); ?>/assets/icons/palettes-loading.png" alt="Please wait while your colour palette is being generated.">
                    <p class="save__text" id="save-label">Vui lòng chờ trong khi bảng màu của bạn đang được tạo.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal--colors fade" id="colors-t1" tabindex="-1" role="dialog" aria-labelledby="modal-label">
    <div class="modal-dialog" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <div class="modal-content">
            <div class="modal-body">

                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-6 col-lg-offset-3">
                            <h2 class="text-center">How to use the Calm colours</h2>

                            <figure class="color-card color-card--hex js-color-card u-bg--1024" data-color-id="1024">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 1024 <strong>TIMELESS</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="1024">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A yellowy, grey white nuance. It is lightly coloured
                                and will scarcely break against white details. Consider trying it with the new
                                beige nuances 12075 Soothing Beige and 12076 Modern Beige. Timeless is also nice in
                                combination with green nuances such as 8252 Green Harmony og 8469 Green Leaf, the
                                new subdued green 7628 Treasure and 7629 Antique Green. <br><strong>FIND THE RIGHT
                                    WHITE:</strong>1024 Timeless functions harmoniously with 1624 Skylight, 1001
                                Egg White and 1453 Cotton Ball. It may appear slightly greenish and golden against
                                cold white shades such as 9918 Classic White eller 7236 Jazz.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--10678" data-color-id="10678">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 10678 <strong>SPACE</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="10678">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A bright, subdued, slightly greyish beige nuance. It
                                appears neither reddish nor too golden, - a perfect, calm beige nuance which
                                combines well with many shades. If you are looking for a calm, harmonious
                                atmosphere with beige and brown tones, the new 12075 Soothing Beige and 12076
                                Modern Beige will work beautifully against it. <br><strong>FIND THE RIGHT WHITE:</strong>10678
                                Space works well against 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453
                                Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12075" data-color-id="12075">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12075 <strong>Soothing Beige</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12075">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A golden beige nuance. This colour may appear
                                similar to the popular 1140 Sand, but it will appear slightly more subdued - a
                                faint blackish veil will spread across the colour. The colour is perfect against
                                darker brown nuances such as 10965 Hipster Brown, 1623 Marrakesh or 1929 Nutmeg.
                                Among coloured tones, a subdued green will be a lovely combination, - check out
                                8252 Green Harmony, 8469 Green Leaf or 8494 Organic Green. <br><strong>FIND THE
                                    RIGHT WHITE:</strong> 12075 Soothing Beige is nice against 9918 Classic White,
                                1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12076" data-color-id="12076">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12076 <strong>Modern Beige</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12076">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued beige nuance. The colour is slightly
                                darker than 12075 Soothing Beige and 1140 Sand, but brighter than our classic 1929
                                Nutmeg and 1623 Marrakesh. Moreover, these colours fit perfectly together. Burnt
                                reddish brown nuances, such as 2859 Whispering Red and 20118 Amber Red, is also an
                                attractive combination.<br><strong>FIND THE RIGHT WHITE:</strong> 12076 Modern
                                Beige works well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453
                                Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--1622" data-color-id="1622">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 1622 <strong>Edelweiss</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="1622">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A slightly greyish white nuance. The colour has a
                                reddish undertone, which will take many by surprise as it is barely visible at
                                first sight. Soft and rustic reds, such as the new 12084 Dusky Peach, 12085 Rural,
                                20120 Organic Red and 2856 Warm Blush are great, warm combinations against 1622
                                Edelweiss. <br><strong>FIND THE RIGHT WHITE:</strong> 1622 Edelweiss works well
                                with 9918 Classic White, - but appears wonderful alone, both as wall and detail
                                colour if so desired.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12077" data-color-id="12077">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12077 <strong>Sheer Grey</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12077">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A bright, warm grey nuance. It is neither beige nor
                                bluish grey, - rather more like a bright, cool mole nuance. 12077 Sheer Grey works
                                perfectly against grey tones such as 10342 Sable Stone, but the reddish undertones
                                also appear exciting for anyone wanting to pursue the red tones, in the form of the
                                golden pink tones 20046 Savanna Sunset and 20047 Blushing Peach. Blue tones such as
                                4618 Evening Light, 4638 Elegant Blue, 4477 Deco Blue og 4744 Sophisticated Blue
                                are gorgeous combinations, as are the more reddish blue tones like 4109 Gustivian
                                Blue.<br><strong>FIND THE RIGHT WHITE:</strong> 12077 Sheer Grey is at its most
                                favourable with pure whites such as 7236 Jazz, 9918 Classic White or the warm
                                greyish white 1622 Edelweiss. </p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12078" data-color-id="12078">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12078">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A warm grey nuance. 12078 Comfort Grey is a brighter
                                version of the popular 0394 Soft Grey. This colour looks quite cool against
                                contrasts such as the red tones 20120 Organic Red or 20118 Amber Red, check it also
                                out against the darker green tone 7629 Antique Green. Blue tones such as 4618
                                Evening Light, 4638 Elegant Blue, 4477 Deco Blue and 4744 Sophisticated Blue works
                                harmoniously with this grey nuance, as do the warm greys 12077 Sheer Grey, 1352
                                Form, 10429 Discrete, 10853 Velvet Grey and 10249 Sober. <br><strong>FIND
                                    THE RIGHT WHITE:</strong> 12078 Comfort Grey works perfect with pure white
                                colours 7236 Jazz, 9918 Classic White or 1624 Skylight, but may also be combined
                                with 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--0394" data-color-id="0394">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 0394 <strong>SOFT GREY</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="0394">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A warm grey nuance. This is a suitable grey tone if
                                you are looking for warm grey nuances in your house. This has been a Jotun
                                favourite among greys for many years! Works well with other grey nuances such as
                                12077 Sheer Grey, 1024 Timeless, 1352 Form, 10679 Washed Linen or 1376 Mist. Try it
                                out with green tones such as 8494 Organic Green, 8469 Green Leaf and 8252 Green
                                Harmony, - it looks great! Against pink nuances such as 20046 Savanna Sunset, 20047
                                Blushing Peach, 2024 Senses or 2771 Rustic Terracotta it will also appear quite
                                cool. <br><strong>FIND THE RIGHT WHITE:</strong> 0394 Soft Grey functions well with
                                9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--10290" data-color-id="10290">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 10290 <strong>Soft Touch</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="10290">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A faint peach nuance. The colour is subdued and
                                greyish. Comparatively it is brighter, less pink and more golden than 10580 Soft
                                Skin, well known to many. It works beautifully with subdued white nuances such as
                                1622 Edelweiss, but also with the more golden whites, thanks to its yellow
                                undertones. Combined with colours such as 12083 Devine and 12084 Dusky Peach it
                                forms a lovely peach coloured harmony. <br><strong>FIND THE RIGHT WHITE:</strong>
                                12090 Soft Touch functions well with 9918 Classic White, 1624 Skylight, 1001 Egg
                                White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12083" data-color-id="12083">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12083 <strong>Devine</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12083">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued peach nuance. The colour is golden,
                                subdued and pleasant. It is not really pink, it has more of a greyish apricot/peach
                                nuance. 12083 Devine works well with golden bright nuances such as 1024 Timeless
                                and 1376 Mist, or with other peach nuances such as 12084 Dusky Peach and 12085
                                Rural. It may surprise you in a positive way, when combined with green nuances such
                                as 7628 Treasure. <br><strong>FIND THE RIGHT WHITE:</strong> 12083 Devine works
                                well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--20119" data-color-id="20119">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 20119 <strong>Transparent Pink</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="20119">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued pink nuance. The colour is golden with
                                grey undertones. Compared to ordinary pink nuances, it appears far more golden and
                                subdued. The colour combines beautifully with the other pink tones 12086 Rustic
                                Pink, 2024 Senses and 20120 Organic Red. Among the quite bright tones, both 1024
                                Timeless, 1376 Mist and 1622 Edelweiss look great as combinations. It works well
                                with 0394 Soft Grey, 1352 Form, 1032 Iron Grey to mention a few greys. Green
                                nuances such as 7628 Treasure and 7629 Antique Green may be exciting contrasts to
                                the sweeter 20119 Transparent Pink. <br><strong>FIND THE RIGHT WHITE:</strong>
                                20119 Transparent Pink appears at its best with 9918 Classic White, 7236 Jazz and
                                to some extent 1624 Skylight. 1001 Egg White and 1453 Cotton Ball may appear yellow
                                when combined with this colour.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12086" data-color-id="12086">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12086 <strong>Rustic Pink</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12086">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued pink tone. The colour is somewhat golden.
                                We may say that 10286 Rustic Pink is a brighter version of the well-known 2024
                                Senses - they are beautiful together. Also try it with 20120 Organic Red or 20119
                                Transparent Pink, <br><strong>FIND THE RIGHT WHITE:</strong> 12086 Rustic Pink
                                functions well with 9918 Classic White, 1624 Skylight, 1001 Egg White. 1453 Cotton
                                Ball may turn in a golden direction when combined with 10286 Rustic Pink.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12074" data-color-id="12074">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12074 <strong>Peachy</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12074">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued peach nuance. 12074 is a greyish peach
                                tone, but it will appear fresh enough on your wall. It is brighter than 12085 Rural
                                and slightly darker than 12084 Dusky Peach. 12074 Peachy creates a cool accent when
                                combined with 7628 Treasure and 7629 Antique Green for the more daring, or merely
                                as a fresh golden nuance among the beige tones such as 10678 Space, 12075 Soothing
                                Beige, 12076 Modern Beige - or 1622 Edelweiss, 1024 Timeless, 10679 Washed Linen,
                                12077 Sheer Grey, 12078 Comfort Grey and 0394 Soft Grey. The colour also combines
                                well with 12085 Rural. <br><strong>FIND THE RIGHT WHITE:</strong> 12074 Peachy
                                works well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton
                                Ball.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn--line" data-dismiss="modal">
                        <span class="btn__text">Back</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal--colors fade" id="colors-t2" tabindex="-1" role="dialog" aria-labelledby="modal-label">
    <div class="modal-dialog" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <div class="modal-content">
            <div class="modal-body">

                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                            <h2 class="text-center">How to use the Refined colours</h2>

                            <figure class="color-card color-card--hex js-color-card u-bg--7626" data-color-id="7626">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 7626 <strong>Airy Green</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="7626">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">color.7626.text</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--7627" data-color-id="7627">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 7627 <strong>Refresh</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="7627">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A fresh green nuance. This tone appears golden and
                                thus more greenish than the mint nuance 7555 Soft Mint. The colour mixes
                                beautifully with the darker greens 7628 Treasure and 7629 Antique Green. But it may
                                also surprise as a lovely pastel when combined with cooler pink tones such as 12086
                                Rustic Pink. <br><strong>FIND THE RIGHT WHITE:</strong> 7627 Refresh functions well
                                with 7236 Jazz and 9918 Classic White. 1624 Skylight, 1001 Egg White and 1453
                                Cotton Ball should be avoided, as they will appear yellow and somewhat dirty when
                                combined.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--7628" data-color-id="7628">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 7628 <strong>Treasure</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="7628">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued green nuance. The colour appears as more
                                golden and thus more greenish than 7163 Minty Breeze, which is known to many. It
                                combines beautifully with cool white tones such as 7236 Jazz or the greyish white
                                8394 White Poetry. Blue is a colour that may work well as a coloured contrast, and
                                the greenish blues 5030 St. Pauls Blue and 5180 Oslo are preferable. The colour
                                will appear smashing against bright peach nuances such as 12083 Devine, but it also
                                works well with the cooler pink shades. A trendy contrast may be achieved with a
                                quite dark green nuance such as 7613 Northern Mystic.<br><strong>FIND THE RIGHT
                                    WHITE:</strong> 7628 Treasure combines perfectly with the pure white nuances
                                7236 Jazz, 9918 Classic White or 1624 Skylight, but may also work well with 1001
                                Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--7629" data-color-id="7629">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 7629 <strong>Antique Green</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="7629">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued green nuance. It appears cooler and more
                                bluish than the golden greens 8252 Green Harmony or 8469 Green Leaf, known to many.
                                7629 Antique Green is lovely against the brighter versions 7628 Treasure and 7627
                                Refresh. Subdued greens such as this also combine harmoniously with this years´
                                peach and yellow tones - a more eccentric way of combining different colours. If
                                you like blue, 5180 Oslo fits nicely. <br><strong>FIND THE RIGHT WHITE:</strong>
                                7629 Antique Green appears at its best against pure white tones 7236 Jazz, 9918
                                Classic White or 1624 Skylight, but may function well against 1001 Egg White and
                                1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12079" data-color-id="12079">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12079 <strong>Gleam</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12079">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A bright yellow nuance. The colour is faintly yellow
                                and warm. It may appear similar to the yellow nuance 1442 Clear, but 12079 Gleam is
                                somewhat more subdued and greyish in expression. A good colour for anyone wanting a
                                yellow, but calm atmosphere. This colour works harmoniously with golden white
                                nuances, and the more coloured tones 12080 Soft Radiance, 12081 Silky Yellow and
                                10245 Ginseng. Some may like a combination of yellow and greens; the subdued
                                nuances 8252 Green Harmony and 8469 Green Leaf may function as dark contrasts.<br><strong>FIND
                                    THE RIGHT WHITE:</strong> 12079 Gleam functions well with 9918 Classic White,
                                1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12080" data-color-id="12080">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12080 <strong>Soft Radiance</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12080">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued yellow nuance. The colour is relaxed, warm
                                and has a slightly greyish appearance. 12080 Soft Radiance combines beautifully
                                with brighter yellow tones such as 1442 Clear, 12079 Gleam, but also with the
                                well-known white tones mentioned below, as well as 8395 White Comfort. If a grey
                                contrast is desired, we recommend a warm nuance, or the combination yellow and grey
                                may appear quite different than intended, - however, 0394 Soft Grey may work well.
                                Among the green nuances, 8252 Green Harmony and 8469 Green Leaf fine are dark
                                contrasts, but anyone who likes yellow brown tones may check out 1938 Tea Leaves.
                                <br><strong>FIND THE RIGHT WHITE:</strong> 12080 Soft Radiance functions well with
                                9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12081" data-color-id="12081">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12081 <strong>Silky Yellow</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12081">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued yellow nuance. The colour is soft and
                                pleasant. 12081 Silky Yellow is gorgeous combined with the brighter versions 1442
                                Clear, 12079 Gleam and 10280 Soft Radiance. It may appear as a soft and elegant
                                contrast to grey nuances such as 1877 Pebblestone, or the greyish brown 10965
                                Hipster Brown. <br><strong>FIND THE RIGHT WHITE:</strong> 12081 Silky Yellow
                                functions well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453
                                Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12082" data-color-id="12082">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12082 <strong>Refined Yellow</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12082">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued yellow nuance. The colour is pleasant and
                                relaxed. It works as an elegant contrast to bright, golden nuances such as 1024
                                Timeless and slightly golden tones such as 12080 Soft Radiance and 12082 Silky
                                Yellow, but also as an exciting contrast to deeper colours. It appears soft against
                                warm grey nuances such as 0394 Soft Grey and 1352 Form, but 1877 Pebblestone is
                                also a quite cool contrast. <br><strong>FIND THE RIGHT WHITE:</strong> 12082
                                Refined Yellow functions well with 9918 Classic White, 1624 Skylight 1001 Egg White
                                and 1453 Cotton Ball.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn--line" data-dismiss="modal">
                        <span class="btn__text">Back</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal--colors fade" id="colors-t3" tabindex="-1" role="dialog" aria-labelledby="modal-label">
    <div class="modal-dialog" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <div class="modal-content">
            <div class="modal-body">

                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                            <h2 class="text-center">How to use the Raw colours</h2>

                            <figure class="color-card color-card--hex js-color-card u-bg--12078" data-color-id="12078">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12078">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A warm grey nuance. 12078 Comfort Grey is a brighter
                                version of the popular 0394 Soft Grey. This colour looks quite cool against
                                contrasts such as the red tones 20120 Organic Red or 20118 Amber Red, check it also
                                out against the darker green tone 7629 Antique Green. Blue tones such as 4618
                                Evening Light, 4638 Elegant Blue, 4477 Deco Blue and 4744 Sophisticated Blue works
                                harmoniously with this grey nuance, as do the warm greys 12077 Sheer Grey, 1352
                                Form, 10429 Discrete, 10853 Velvet Grey and 10249 Sober. <br><strong>FIND
                                    THE RIGHT WHITE:</strong> 12078 Comfort Grey works perfect with pure white
                                colours 7236 Jazz, 9918 Classic White or 1624 Skylight, but may also be combined
                                with 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--0394" data-color-id="0394">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 0394 <strong>SOFT GREY</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="0394">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A warm grey nuance. This is a suitable grey tone if
                                you are looking for warm grey nuances in your house. This has been a Jotun
                                favourite among greys for many years! Works well with other grey nuances such as
                                12077 Sheer Grey, 1024 Timeless, 1352 Form, 10679 Washed Linen or 1376 Mist. Try it
                                out with green tones such as 8494 Organic Green, 8469 Green Leaf and 8252 Green
                                Harmony, - it looks great! Against pink nuances such as 20046 Savanna Sunset, 20047
                                Blushing Peach, 2024 Senses or 2771 Rustic Terracotta it will also appear quite
                                cool. <br><strong>FIND THE RIGHT WHITE:</strong> 0394 Soft Grey functions well with
                                9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12084" data-color-id="12084">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12084 <strong>Dusky Peach</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12084">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued peach nuance. The colour is golden, more
                                like an apricot/peach nuance and not a traditional pink. In many ways this is a
                                yellowy version of the colour 2024 Senses, known to many. 12084 Dusky Peach appears
                                new and exciting against the green nuance 7628 Treasure, and to anyone who enjoys a
                                fresh accent, 20118 Amber Red will be a fun element! The colour will appear soft
                                and warm when combined with the other peach nuances 12083 Devine og 12085 Rural. It
                                works well with brighter colours such as 1024 Timeless and 10679 Washed Linen. <br><strong>FIND
                                    THE RIGHT WHITE:</strong> 12084 Dusky Peach functions well with 9918 Classic
                                White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12085" data-color-id="12085">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12085 <strong>Rural</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12085">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A reddish brown peach nuance. The name implies
                                countryside colours, a mixture of reddish brown earth and golden, warm sunshine.
                                The colour is like a hybrid, somewhere in the middle between red, pink and orange.
                                For anyone wanting to pursue these nuances, it may be combined with like-minded
                                colours such as 12084 Dusky Peach, or with the stylish and pure version 12074
                                Peachy. Green nuances such as 7613 Northern Mystic and 8469 Green Leaf are exciting
                                combinations for the daring. A more neutral base, such as 12075 Soothing Beige,
                                10678 Space and 12076 Modern Beige is, however, also a lovely combination. <br><strong>FIND
                                    THE RIGHT WHITE:</strong> 2085 Rural functions well with 9918 Classic White,
                                1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--7628" data-color-id="7628">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 7628 <strong>Treasure</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="7628">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued green nuance. The colour appears as more
                                golden and thus more greenish than 7163 Minty Breeze, which is known to many. It
                                combines beautifully with cool white tones such as 7236 Jazz or the greyish white
                                8394 White Poetry. Blue is a colour that may work well as a coloured contrast, and
                                the greenish blues 5030 St. Pauls Blue and 5180 Oslo are preferable. The colour
                                will appear smashing against bright peach nuances such as 12083 Devine, but it also
                                works well with the cooler pink shades. A trendy contrast may be achieved with a
                                quite dark green nuance such as 7613 Northern Mystic.<br><strong>FIND THE RIGHT
                                    WHITE:</strong> 7628 Treasure combines perfectly with the pure white nuances
                                7236 Jazz, 9918 Classic White or 1624 Skylight, but may also work well with 1001
                                Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--7629" data-color-id="7629">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 7629 <strong>Antique Green</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="7629">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued green nuance. It appears cooler and more
                                bluish than the golden greens 8252 Green Harmony or 8469 Green Leaf, known to many.
                                7629 Antique Green is lovely against the brighter versions 7628 Treasure and 7627
                                Refresh. Subdued greens such as this also combine harmoniously with this years´
                                peach and yellow tones - a more eccentric way of combining different colours. If
                                you like blue, 5180 Oslo fits nicely. <br><strong>FIND THE RIGHT WHITE:</strong>
                                7629 Antique Green appears at its best against pure white tones 7236 Jazz, 9918
                                Classic White or 1624 Skylight, but may function well against 1001 Egg White and
                                1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12086" data-color-id="12086">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12086 <strong>Rustic Pink</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12086">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued pink tone. The colour is somewhat golden.
                                We may say that 10286 Rustic Pink is a brighter version of the well-known 2024
                                Senses - they are beautiful together. Also try it with 20120 Organic Red or 20119
                                Transparent Pink, <br><strong>FIND THE RIGHT WHITE:</strong> 12086 Rustic Pink
                                functions well with 9918 Classic White, 1624 Skylight, 1001 Egg White. 1453 Cotton
                                Ball may turn in a golden direction when combined with 10286 Rustic Pink.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--2024" data-color-id="2024">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 2024 <strong>Senses</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="2024">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A pink powder nuance. This is a golden pink nuance
                                that will appear as both delicate and subdued. It is lovely when combined with
                                brighter variations such as 10580 Soft Skin, 20119 Transparent Pink and 12086
                                Devine. This colour appears as quite cool when combined with burnt terracotta red
                                nuances such as 2771 Rustic Terracotta, 2859 Whispering Red or 2995 Dusty Red. It
                                is also gorgeous as a golden pink contrast to darker golden grey nuances such as
                                0394 Soft Grey and 1352 Form. <br><strong>FIND THE RIGHT WHITE:</strong> It
                                functions well with white nuances such as 9918 Classic White, 1624 Skylight, 1001
                                Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--20120" data-color-id="20120">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 20120 <strong>Organic Red</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="20120">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued reddish pink nuance. The colour appears
                                warm and burnt. You will find elements of red, pink and brown in this warm nuance.
                                Combined with brighter tones, such as 2024 Senses or 12086 Rustic Pink, the colours
                                will create a warm colour flow. Green is also an exciting combination, check out
                                7629 Antique Green, or more golden green shades such as 8252 Green Harmony and 8469
                                Green Leaf. Among the basic colours, nuances such as the beige 10678 Space, 12075
                                Soothing Beige, 12076 Modern Beige, 1140 Sand, and 1623 Marrakesh will work well.
                                <br><strong>FIND THE RIGHT WHITE:</strong> 20120 Organic Red functions well with
                                9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--7613" data-color-id="7613">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 7613 <strong>Northern Mystic</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="7613">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A dark, golden green nuance. The colour is deep
                                green, perfectly balanced in the sense that it is neither blue and cool, nor golden
                                and yellow. It works well as a beautiful contrast to a series of other beige, grey,
                                green, golden pink and peach nuances. It may work as a dark, cool contrast to
                                practically all the colours in this years´ colour card from LADY. Some favourites
                                are 7628 Treasure, 7629 Antique Green, 12078 Sheer Grey and 0394 Soft Grey. <br><strong>FIND
                                    THE RIGHT WHITE:</strong> 7613 Northern Mystic functions well with 7236 Jazz,
                                9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--12074" data-color-id="12074">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12074 <strong>Peachy</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12074">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A subdued peach nuance. 12074 is a greyish peach
                                tone, but it will appear fresh enough on your wall. It is brighter than 12085 Rural
                                and slightly darker than 12084 Dusky Peach. 12074 Peachy creates a cool accent when
                                combined with 7628 Treasure and 7629 Antique Green for the more daring, or merely
                                as a fresh golden nuance among the beige tones such as 10678 Space, 12075 Soothing
                                Beige, 12076 Modern Beige - or 1622 Edelweiss, 1024 Timeless, 10679 Washed Linen,
                                12077 Sheer Grey, 12078 Comfort Grey and 0394 Soft Grey. The colour also combines
                                well with 12085 Rural. <br><strong>FIND THE RIGHT WHITE:</strong> 12074 Peachy
                                works well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton
                                Ball.</p>
                            <figure class="color-card color-card--hex js-color-card u-bg--20118" data-color-id="20118">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 20118 <strong>Amber Red</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="20118">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <p class="color-card__description">A golden red nuance. The colour appears burnt and
                                golden. It will appear more lively and slightly more red than 2771 Rustic
                                Terracotta, but will appear more reddish brown when compared with 2859 Whispering
                                Red. It works well with grey and beige such as 10678 Space, 12075 Soothing Beige,
                                12076 Modern Beige, 1622 Edelweiss, 1024 Timeless, 10679 Washed Linen, 10429
                                Discrete, 12077 Sheer Grey, 12078 Comfort Grey, 0394 Soft Grey and 1973 Objective.
                                <br><strong>FIND THE RIGHT WHITE:</strong> 20118 Amber Red works well with 9918
                                Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn--line" data-dismiss="modal">
                        <span class="btn__text">Back</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fullscreencolor color-card--fullscreen js-color-card--fullscreen" role="dialog" aria-modal="true"
    aria-labelledby="fullscreenCaption" aria-describedby="colorinfo" tabindex="-1">
    <div class="fullscreencolor__color">

        <button type="button" class="close js-color-card-close" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>

        <button type="button" class="fullscreencolor__expand js-color-fullscreen-toggle" aria-label="View the colour in fullscreen"
            aria-expanded="true" aria-controls="fullscreenContent">
            <i class="material-icons">fullscreen</i>
        </button>


        <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" aria-label="Add the colour chip to your favourites">
            <i class="material-icons material-icons--first"></i>
            <i class="material-icons material-icons--second"></i>
            <span class="sr-only">Add the colour chip to your favourites</span>
        </button>

        <div class="fullscreencolor__caption fullscreencolor__caption--color js-color-card--caption" id="fullscreenCaption"></div>
    </div>

    <div class="fullscreencolor__content" id="fullscreenContent">
        <div class="fullscreencolor__caption js-color-card--caption"></div>

        <ul class="nav nav-tabs" role="tablist" id="colorTabs">
            <li role="presentation" class="active">
                <a href="<?php echo $this->config->item("palette_link"); ?>#colorinfo" aria-controls="colorinfo" role="tab"
                    data-toggle="tab">About the colour</a>
            </li>
            <li role="presentation">
                <a href="<?php echo $this->config->item("palette_link"); ?>#colormatching" aria-controls="colormatching"
                    role="tab" data-toggle="tab">Matching colours</a>
            </li>
        </ul>

        <div class="fullscreencolor__inner">
            <div class="container">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="colorinfo">
                        <div class="fullscreencolor__info">
                            <p class="js-disclaimer-color"></p>

                            <p class="minerals-mix-text">modal.minerals.mix.text</p>
                            <p class="ceiling-mix-text">modal.ceiling.mix.text</p>

                            <p>
                                <strong>CAM KẾT MÀU CHÍNH XÁC</strong><br>
                                Công nghệ chất tạo màu tiên tiến của Jotun đem lại độ chính xác màu cao. Với sự cam kết màu chính xác, bạn có thể chắc chắn rằng màu thực tế sẽ giống như màu bạn chọn trên bảng màu
                            </p>

                          <p>
                                    <strong>TÁI TẠO MÀU</strong><br>
                                    Xin lưu ý rằng màu sắc sẽ khác nhau tùy thuộc vào cài đặt màn hình và độ phân giải của bạn. Ghé thăm cửa hàng Jotun gần nhất để xem các màu mẫu chính xác.
                                </p>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="colormatching">
                        <div class="fullscreencolor__matching"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>