<!DOCTYPE html>
<!-- saved from url=(0045)http://jotun-gcc.com.dev02.allegro.no/colours -->
<html class="no-js " lang="en">
<!--<![endif]-->

<head data-lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php 
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
        if($actual_link = base_url()."colours")
        {
    ?>
            <meta property="og:url" content="">
            <meta property="og:type" content="website">
            <meta property="og:title" content="IDENTITY, COLOUR COLLECTION 2019">
            <meta property="og:description" content="Be inspired by the new colours, get tips on how to choose colours and combinations that reflect your own personal colour identity">
            <meta property="og:image" content="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/media/images/banners/facebook_banner.png">

    <?php
        }else         if($actual_link = base_url()."frontpage")
        {
    ?>
            <meta property="og:url" content="">
            <meta property="og:type" content="website">
            <meta property="og:title" content="IDENTITY, COLOUR COLLECTION 2019">
            <meta property="og:description" content="Be inspired by the new colours, get tips on how to choose colours and combinations that reflect your own personal colour identity">
            <meta property="og:image" content="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/media/images/banners/facebook_banner.png">

    <?php        
        }else         if($actual_link = base_url()."palette")
        {
    ?>
            <meta property="og:url" content="">
            <meta property="og:type" content="website">
            <meta property="og:title" content="IDENTITY, COLOUR COLLECTION 2019">
            <meta property="og:description" content="Be inspired by the new colours, get tips on how to choose colours and combinations that reflect your own personal colour identity">
            <meta property="og:image" content="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/media/images/banners/facebook_banner.png">

    <?php        
        }else         if($actual_link = base_url()."calm")
        {
    ?>
            <meta property="og:url" content="">
            <meta property="og:type" content="website">
            <meta property="og:title" content="IDENTITY, COLOUR COLLECTION 2019">
            <meta property="og:description" content="Be inspired by the new colours, get tips on how to choose colours and combinations that reflect your own personal colour identity">
            <meta property="og:image" content="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/media/images/banners/facebook_banner.png">

    <?php      
        }else         if($actual_link = base_url()."refined")
        {
    ?>
            <meta property="og:url" content="">
            <meta property="og:type" content="website">
            <meta property="og:title" content="IDENTITY, COLOUR COLLECTION 2019">
            <meta property="og:description" content="Be inspired by the new colours, get tips on how to choose colours and combinations that reflect your own personal colour identity">
            <meta property="og:image" content="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/media/images/banners/facebook_banner.png">

    <?php
        }else         if($actual_link = base_url()."raw")
        {
    ?>
            <meta property="og:url" content="">
            <meta property="og:type" content="website">
            <meta property="og:title" content="IDENTITY, COLOUR COLLECTION 2019">
            <meta property="og:description" content="Be inspired by the new colours, get tips on how to choose colours and combinations that reflect your own personal colour identity">
            <meta property="og:image" content="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/media/images/banners/facebook_banner.png">

    <?php
        }
    ?>
   
    <meta name="msapplication-TileColor" content="#fbfbfb">
    <meta name="theme-color" content="#fbfbfb">

    <title>Jotun Colour Collection</title>
    <meta name="description" content="facebook.general.text">

    <!-- <link rel="manifest" href="http://jotun-gcc.com.dev02.allegro.no/manifest.json"> -->
    <link id="main-css" href="<?php echo base_url();?>/assets/dist/styles.min.css" rel="stylesheet" type="text/css" media="all">

    <link rel="icon" type="image/png" href="http://jotun-gcc.com.dev02.allegro.no/favicon-16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="http://jotun-gcc.com.dev02.allegro.no/favicon-32.png" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="http://jotun-gcc.com.dev02.allegro.no/favicon-152.png">
    <meta name="msapplication-TileImage" content="favicon-478.png">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="<?php echo base_url();?>/assets/data.json"></script>
    <script>
        var $site_url = '';
        // Picture element HTML5 shiv
      document.createElement( "picture" );
    </script>
    <script src="<?php echo base_url();?>/assets/dist/picturefill.min.js" async=""></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WCWWSKH');</script>
    <!-- End Google Tag Manager -->
</head>

<body class="colours page lang-en is-ready">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=760992557367437&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WCWWSKH"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <a href="<?php echo base_url(); ?>colours#main" title="Skip to the main content" class="skip-navigation">To
        the main content</a>

    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu"
                    aria-expanded="false">
                    <span class="navbar__label">Menu</span>
                    <span class="navbar__icon">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </button>
                <a href="<?php echo base_url(); ?>" class="navbar__logo">
                    <img src="<?php echo base_url();?>/assets/dist/jotun.svg" alt="JOTUN">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="main-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo $this->config->item("calm_link"); ?>">Nhẹ nhàng</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("refined_link"); ?>">Tinh tế</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("raw_link"); ?>">Thô mộc</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("palette_link"); ?>">Tạo màu của riêng bạn</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("colour_link"); ?>">Xem tất cả các màu</a>
                    </li>
                    <li>
                        <a href="http://inspiration.jotun.my/"  target="_blank">Inspiration Blog</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->config->item("app_page"); ?>">Colour Design</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="favorite-nav js-favorite-nav is-visible">
        <a href="<?php echo $this->config->item("wishlist_link"); ?>" title="Go to my favourites" class="btn btn--transparent js-favorite-btn"
            aria-live="polite" aria-atomic="false">
            <i class="material-icons material-icons--second"></i>
            <p class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</p>
        </a>
    </div>