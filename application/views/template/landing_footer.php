


    <footer id="footer" class="footer section">
        <div class="container">
            <div class="btn-row">
                <a class="btn btn--line" href="http://jotun-gcc.com.dev02.allegro.no/wishlist">
                    <span class="btn__text">Yêu thích <span class="btn--heart"><i class="material-icons material-icons--second"></i>
                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span></span></span>
                </a>
            </div>
            <div class="btn-row">
                <a class="btn btn--line" href="<?php echo $this->config->item("palette_link"); ?>">
                    <span class="btn__text">Tạo màu của riêng bạn</span>
                </a>
            </div>
            <div class="btn-row">
            <div class="btn btn--line btn--facebook js-btn-facebook fb-share-button" data-href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/" data-layout="button" data-size="large" data-mobile-iframe="true">
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com%2F&amp;src=sdkpreparse" ><i class="icon icon--facebook"></i> Chia sẻ trên Facebook</a>
                </div>
                <!-- <button class="btn btn--line btn--pinterest pinterest-pin-it"><i class="icon icon--pinterest"></i>Pin
                    to Pinterest</button> -->
            </div>
            <a href="https://jotun.com/" class="footer__logo">
                <img data-src="assets/media/logos/jotun.svg" class=" lazyload" alt="JOTUN" src="<?php echo base_url(); ?>assets/dist/jotun.svg">
                <span class="sr-only">JOTUN</span>
            </a>
            <ul class="footer-links list-unstyled">

                <li>
                    <a class="ladybloggen" href="https://www.jotun.com/no/en/corporate/Termsandconditionscorporate.aspx"
                        target="_blank">
                        Privacy, terms &amp; condition and cookie policy
                    </a>
                </li>
            </ul>
        </div>
    </footer>
    
   
    
    <script>
        var params = {
        "badge_01" : {
            method      : "feed",
            link        : $site_url,
            picture     : $site_url+"assets/media/facebook.general.image",
            name        : "facebook.general.name",
            caption     : "facebook.general.caption",
            description : "facebook.general.text"
        }
    };
</script>
<!--
    <div class="cookie js-cookie" role="alert">
        <div class="container">
            <div class="cookie__row">
                <div class="cookie__left">
                    <p>
                        By using the Jotun sites, you are consenting to our use of cookies in accordance with this
                        Cookie Policy. If you do not agree to our use of cookies in this way, you should set your
                        browser settings accordingly or not use the Jotun Sites. If you disable the cookies that we
                        use, this may impact your user experience while on the Jotun Sites.
                    </p>
                </div>
                <div class="cookie__right">
                    <button type="button" class="cookie__close js-cookie-close">
                        Do not show again
                    </button>
                </div>
            </div>
        </div>
    </div>
-->

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/messages.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-modal-video.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/scripts.min.js"></script>


     <script>
        $(document).ready(function(){
 		
            var bottom = $(document).height() - $(window).height();
            var section_height = ($('.main').height())+100;
            
            
                $('.scroller').click(function(){
                   $('body,html').animate({ scrollTop: section_height }, 600);
                });
            
            $('.hand_phone_section').mouseenter(function(){
                $('.hand_phone').animate({ marginTop: 0 }, 600);
            });
            
            
//            $('.hand_phone_section').mouseleave(function(){
//                $('.hand_phone').animate({ marginTop: 60 }, 600);
//            });
            
            
            $('.animation_1').mouseenter(function(){
                $('.activate').trigger('click');
            });
            
             $('.animation_1').mouseleave(function(){
                $('.activate').trigger('click');
            });
            
             $('.animation_2').mouseenter(function(){
                $('.browse_products').trigger('click');
            });
            
             $('.animation_2').mouseleave(function(){
                $('.browse_products').trigger('click');
            });
            
            $('.ms-device-two').click(function(){
                $('.browse_products').trigger('click');
            });
                 
        });
       
    </script>

    <script>
        $(window).load(function () {
            $('.js-btn-facebook').click(function () {
                FB.ui(params[$(this).attr('rel')]);
                return false;
            });
        });
    </script>

    <script>
		$(".js-video-button").modalVideo({
			youtube:{
				controls:0,
				nocookie: true
			}
		});
	</script>
    
    <!-- speical phone animations -->
    <script src="<?php echo base_url(); ?>assets/js/appshowcase.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/classie.js"></script>
    
    <script src="<?php echo base_url(); ?>assets/js/phoneSlideshow.js"></script>
		<script>
			$(function() {
				AppShowcase.init();
                 
			});
		</script>
		
		
		
		<script src="<?php echo base_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/dynamics.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/classie-isometric.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
		<script>
		(function() {
			function getRandomInt(min, max) {
				return Math.floor(Math.random() * (max - min + 1)) + min;
			}

			new IsoGrid(document.querySelector('.isolayer--deco1'), {
				transform : 'translateX(33vw) translateY(-340px) rotateX(45deg) rotateZ(45deg)',
				stackItemsAnimation : {
					properties : function(pos) {
						return {
							translateZ: (pos+1) * 30,
							rotateZ: getRandomInt(-4, 4)
						};
					},
					options : function(pos, itemstotal) {
						return {
							type: dynamics.bezier,
							duration: 500,
							points: [{"x":0,"y":0,"cp":[{"x":0.2,"y":1}]},{"x":1,"y":1,"cp":[{"x":0.3,"y":1}]}],
							delay: (itemstotal-pos-1)*40
						};
					}
				}
			});
			
           
            
			
		})();
            
            
		</script>

</body>

</html>