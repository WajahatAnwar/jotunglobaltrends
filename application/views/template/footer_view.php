
    <footer id="footer" class="footer section">
        <div class="container">
            <div class="btn-row">
                <a class="btn btn--line" href="<?php $this->config->item("wishlist_link"); ?>">
                    <span class="btn__text">Yêu thích<span class="btn--heart"><i class="material-icons material-icons--second"></i>
                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span></span></span>
                </a>
                <a class="btn btn--line" href="http://jotun-gcc.com.dev02.allegro.no/products" style="display:none;">
                    <span class="btn__text">Chọn màu thích hợp</span>
                </a>
            </div>
            
            <div class="btn-row">
                <div class="btn btn--line btn--facebook js-btn-facebook fb-share-button" data-href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/" data-layout="button" data-size="large" data-mobile-iframe="true">
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com%2F&amp;src=sdkpreparse" ><i class="icon icon--facebook"></i> Chia sẻ trên Facebook</a>
                </div>
                <!-- <a data-href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com%2F&amp;src=sdkpreparse" class="btn btn--line btn--facebook js-btn-facebook fb-share-button"
                    rel="badge_01"><i class="icon icon--facebook"></i> Share to Facebook</a> -->
                <button class="btn btn--line btn--pinterest pinterest-pin-it"><i class="icon icon--pinterest"></i>Gắn vào Pinterest </button>
            </div>
            <a href="https://jotun.com/" class="footer__logo">
                <img data-src="<?php echo base_url(); ?>assets/media/logos/jotun.svg" class=" lazyload" alt="JOTUN" src="<?php echo base_url();?>/assets/dist/jotun.svg">
                <span class="sr-only">JOTUN</span>
            </a>
            <ul class="footer-links list-unstyled">

                <li>
                    <a class="ladybloggen" href="https://www.jotun.com/no/en/corporate/Termsandconditionscorporate.aspx"
                        target="_blank">
                        Privacy, terms &amp; condition and cookie policy
                    </a>
                </li>
            </ul>
        </div>
    </footer>

    <script>
        var params = {
        "badge_01" : {
            method      : "feed",
            link        : $site_url,
            picture     : $site_url+"assets/media/facebook.general.image",
            name        : "facebook.general.name",
            caption     : "facebook.general.caption",
            description : "facebook.general.text"
        }
    };
</script>
    <div class="cookie js-cookie" role="alert">
        <div class="container">
            <div class="cookie__row">
                <div class="cookie__left">
                    <p>
                        By using the Jotun sites, you are consenting to our use of cookies in accordance with this
                        Cookie Policy. If you do not agree to our use of cookies in this way, you should set your
                        browser settings accordingly or not use the Jotun Sites. If you disable the cookies that we
                        use, this may impact your user experience while on the Jotun Sites.
                    </p>
                </div>
                <div class="cookie__right">
                    <button type="button" class="cookie__close js-cookie-close">
                        Do not show again
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/messages.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/custom.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url();?>/assets/dist/scripts.min.js"></script> -->



    <script>
        $(window).load(function () {
            $('.js-btn-facebook').click(function () {
                FB.ui(params[$(this).attr('rel')]);
                return false;
            });
        });

        $(document).ready(function(){
            
        });
    </script>

</body>

</html>