<header class="theme">
        <div class="theme__content">
            <div class="theme__column theme__column--image">
                <figure class="theme__image lazyload" data-bg="assets/media/images/t2/Jotun_moderne_g3_0573.jpg" role="img"
                    aria-label="WALL: JOTUN 7628 TREASURE SKIRTING: JOTUN 7628 TREASURE CEILING: JOTUN 7236 JAZZ WHITE"
                    id="t2-image--1" style="background-image: url(&quot;assets/media/images/t2/Jotun_moderne_g3_0573.jpg&quot;);">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="7628" data-image="assets/media/images/t2/Jotun_moderne_g3_0573.jpg"
                        data-product="3">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figcaption class="card__caption card__caption--color js-card-caption">
                        <p>WALL & SKIRTING BOARD: JOTUN 7628 <strong>TREASURE</strong></p>
                    </figcaption>
                </figure>

                <div class="theme__bar">
                    <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                        aria-label="Save your favourites" data-favorite="t2_1" style="visibility: visible; animation-name: d;">
                        <i class="material-icons material-icons--first"></i>
                        <i class="material-icons material-icons--second"></i>
                        <span class="sr-only">Save your favourites</span>
                    </button>

                    <div class="theme__caption">
                        <p>WALL: JOTUN 7628 <strong>TREASURE</strong><br> SKIRTING: JOTUN 7628 <strong>TREASURE</strong></p>
                    </div>
                </div>
            </div>

            <div class="theme__column theme__column--content">
                <div class="theme__header">
                    <h1 class="theme__title">TINH TẾ</h1>
                </div>

                <div class="theme__video js-section-video" id="t2-video--1">
                    <div class="column__item column__item--video">
                        <div class="video-container keep-16-9">
                            <div class="column__caption js-video-caption">
                                <h3>Khám phá màu sắc</h3>
                            </div>
                            <span class="video-overlay js-video-overlay" data-bg="assets/media/images/t2/t2-video-2.jpg"
                                data-bg-mobile="assets/media/images/t2/t2-video-2.jpg" style="background-image: url(&quot;assets/media/images/t2/t2-video-2.jpg&quot;);"></span>
                            <iframe class="video js-video" src="https://www.youtube.com/embed/OnRcliJOtMw?enablejsapi=1&rel=0&showinfo=0" title="Explore the colours"
                                width="720" height="1280" frameborder="0" allowfullscreen="" id="widget2"></iframe>
                        </div>
                    </div>

                    <div class="theme__text">
                        <p>Quá khứ và tương lai, phá cách và sáng tạo trong thiết kế được lấy 
cảm hứng từ công nghệ của con người, tất cả kết hợp trong một 
bảng màu chân thật với gam màu chính là xanh và vàng. Vật liệu 
tương phản cùng kết cấu bề mặt - thô và mịn, mờ và bóng - tạo ra 
các khoảng cách hấp dẫn với không gian dịu nhẹ đầy lạc quan. </p>
                    </div>
                </div>
            </div>
        </div>
    </header>
<main id="main" class="site-main">

    <section class="section section--cta text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>Save your favourites</h2>
                <p class="lead">Create a wish list of your favourite colours, inspirational photos and paints from
                    Jotun by pressing <i class="material-icons material-icons--first"></i></p>
                <a class="btn btn--line" href="<?php echo $this->config->item("whislist_link"); ?>">
                    <span class="btn__text">My favorites
                        <span class="btn--heart">
                            <i class="material-icons material-icons--second"></i>
                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </section>

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-4">
                    <div class="row">
                        <figure class="card__item--figure col-xs-12 image-more" id="t2-image--2">
                            <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                                data-toggle="modal" data-target="#plus" data-colors="12081" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y3_0740.jpg"
                                data-product="5">
                                <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                            </button>

                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t2_2" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>


                                <picture>
                                    <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y3_0740.webp" type="image/webp"
                                        srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y3_0740.webp">
                                    <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_moderne_y3_0740.jpg"
                                        data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y3_0740.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y3_0740.jpg"
                                        alt="WALL: JOTUN 12081 SILKY YELLOW" srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y3_0740.jpg">
                                </picture>
                            </figure>

                            <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                                <p>WALL: JOTUN 12081 <strong>SILKY YELLOW</strong></p>
                            </figcaption>
                        </figure>

                        <div class="color-group col-xs-12">
                            <figure class="color-card  color-card--hex js-color-card u-bg--12081 wow slideRightFade"
                                data-color-id="12081" style="visibility: visible; animation-name: i;">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12081 <strong>Silky Yellow</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12081" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>
                            <figure class="color-card  color-card--hex js-color-card u-bg--7629 wow slideRightFade"
                                data-color-id="7629" style="visibility: visible; animation-name: i;">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 7629 <strong>Antique Green</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="7629" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <small><a href="<?php echo $this->config->item("refined_link"); ?>#allcolors" class="js-btn-scroll">Xem tất cả các màu cùng nhau ở cuối trang</a></small>
                        </div>
                    </div>
                </div>

                <figure class="card__item--figure col-xs-12 col-lg-8 image-more" id="t2-image--3">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="7629" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_model_g4_8601.jpg"
                        data-product="3">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t2_3" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_model_g4_8601.webp" type="image/webp"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_model_g4_8601.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_model_g4_8601.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_model_g4_8601.jpg"
                                data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_model_g4_8601.jpg" alt="WALL: JOTUN 7629 ANTIQUE GREEN"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_model_g4_8601.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                        <p>WALL: JOTUN 7629 <strong>ANTIQUE GREEN</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p>Majestic Đẹp Hoàn Hảo giúp ngôi nhà 
thêm rực rỡ với màng sơn ánh ngọc trai, 
màu sắc lung linh đa dạng, thanh lịch, bền 
màu và dễ lau chùi.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="section section--product" id="t2-product--1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="product__body">
                        <h3 class="product__title">MAJESTIC TRUE BEAUTY SHEEN</h3>
                        <figure class="product__image">
                            <picture>
                                <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png" type="image/webp" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png">
                                <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png " data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png"
                                    data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png" alt="Fenomastic My Home Rich Matt"
                                    srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png">
                            </picture>

                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                aria-label="Add the product to your favourites" data-favorite="product_2" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the product to your favourites</span>
                            </button>
                        </figure>

                        <p class="product__tag">Hoàn hảo và tỏa sáng</p>

                        <div class="product__btn">
                            <a href="https://www.jotun.com/my/en/b2c/products/interior/majestic/majestic-true-beauty-sheen.aspx" class="btn btn--line">
                                <span class="btn__text">Tìm hiểu thêm về sản phẩm</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <figure class="card__item--figure col-xs-12 image-more" id="t2-image--4">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="9918 3377 12078" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0590.jpg"
                        data-product="3 4">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t2_4" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0590.webp" type="image/webp"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0590.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_moderne_acc1_0590.jpg"
                                data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0590.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0590.jpg"
                                alt="CEILING: JOTUN 9918 Morning Fog WALL: JOTUN 3377 SLATE LAVENDER WALL IN FRONT: JOTUN 12078 COMFORT GREY SKIRTING: JOTUN 3377 SLATE LAVENDER"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0590.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                        <p>CEILING: JOTUN 9918 <strong>MORNING FOG</strong><br> WALL: JOTUN 3377 <strong>SLATE LAVENDER</strong><br> WALL IN FRONT: JOTUN 12078 <strong>COMFORT GREY</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p>Thay đổi về màu sắc tạo ra ngữ cảnh mới 
với những tâm trạng khác nhau. Câu chuyện 
được kể với bối cảnh là một cánh đồng hoa 
oải hương mang chiều sâu và cá tính được tạo 
ra bởi những đồ trang trí. Vẻ đẹp của phong 
cách này nhờ đó cũng nổi bật hơn.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-4 col-lg-push-8">
                    <div class="row">
                        <figure class="card__item--figure col-xs-12 col-md-6 col-lg-12 image-more" id="t2-image--6">
                            <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                                data-toggle="modal" data-target="#plus" data-colors="12082" data-image="http://jotun-gcc.com.dev02.allegro.no/assets/media/images/t2/Jotun_moderne_y4_0671.jpg"
                                data-product="2">
                                <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                            </button>

                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t2_6" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_moderne_y4_0671.jpg"
                                    data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y4_0671.jpg" alt="WALL: JOTUN 12082 REFINED YELLOW">
                            </figure>

                            <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                                <p>WALL: JOTUN 12082 <strong>REFINED YELLOW</strong></p>
                            </figcaption>
                        </figure>

                        <div class="color-group col-xs-12 col-md-6 col-lg-12">
                            <figure class="color-card  color-card--hex js-color-card u-bg--0394 wow slideRightFade"
                                data-color-id="0394" style="visibility: visible; animation-name: i;">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 0394 <strong>SOFT GREY</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="0394" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>
                            <figure class="color-card  color-card--hex js-color-card u-bg--12082 wow slideRightFade"
                                data-color-id="12082" style="visibility: visible; animation-name: i;">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12082 <strong>Refined Yellow</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12082" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <small><a href="<?php echo $this->config->item("refined_link"); ?>#allcolors" class="js-btn-scroll">Xem tất cả các màu cùng nhau ở cuối trang</a></small>
                        </div> <!-- /.color-group -->
                    </div>
                </div>

                <figure class="card__item--figure col-xs-12 col-md-12 col-lg-8 col-lg-pull-4 image-more" id="t2-image--5">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="10429" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_ng4_1706.jpg"
                        data-product="6">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t2_5" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_ng4_1706.jpg" type="image/webp"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_ng4_1706.jpg">
                            <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_thelab_ng4_1706.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_ng4_1706.jpg"
                                data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_ng4_1706.jpg" alt="WALL: JOTUN 0394 SOFT GREY"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_ng4_1706.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                        <p>WALL DESIGN: STARRY <strong>NIGHT </strong><br> BASE COLOUR: JOTUN 10429 DISCRETE<br> <strong> GLAZED OVER WITH 10607 DREAMS</strong> </p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p>Lấy nguồn cảm hứng từ bầu trời đêm 
rực rỡ ánh sao, sản phẩm sơn hiệu ứng 
nội thất cao cấp Jotun Majestic Design 
Diamond chắc chắn sẽ đem lại cho
ngôi nhà bạn một không gian huyền ảo 
như trong những câu chuyện cổ tích 
nhưng không kém phần hiện đại. </p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--product" id="t2-product--2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="product__body">
                        <h3 class="product__title">MAJESTIC DESIGN DIAMOND</h3>
                        <figure class="product__image">
                            <picture>
                                <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint5.png" type="image/webp" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint5.png">
                                <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint5.png" data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint5.png"
                                    data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint5.png" alt="Lady Design Romano" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint5.png">
                            </picture>

                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                aria-label="Add the product to your favourites" data-favorite="product_1" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the product to your favourites</span>
                            </button>
                        </figure>

                        <p class="product__tag">Bầu trời đêm đầy sao lấp lánh</p>

                        <div class="product__btn">
                            <a href="https://www.jotun.com/my/en/b2c/products/interior/majestic/majestic-design-landingpage.aspx" class="btn btn--line">
                                <span class="btn__text">Tìm hiểu thêm về sản phẩm</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--gallery">
        <div class="container">
            <div class="row">
                <figure class="card__item--figure col-sm-6 image-more" id="t2-image--7">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="7627" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g2_0700.jpg"
                        data-product="3">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t2_7" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g2_0700.webp" type="image/webp"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g2_0700.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_moderne_g2_0700.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g2_0700.jpg"
                                data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g2_0700.jpg" alt="WALL: JOTUN 7627 REFRESH TABLE / SHELF: JOTUN 7627 REFRESH"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g2_0700.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                        <p>WALL: JOTUN 7627 <strong>REFRESH</strong><br> TABLE / SHELF: JOTUN 7627 <strong>REFRESH</strong></p>
                    </figcaption>
                </figure>

                <figure class="card__item--figure col-sm-6 image-more" id="t2-image--8">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="7629" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_g4_1666.jpg"
                        data-product="3">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t2_8" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_g4_1666.webp" type="image/webp"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_g4_1666.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_thelab_g4_1666.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_g4_1666.jpg"
                                data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_g4_1666.jpg" alt="WALL: JOTUN 7629 ANTIQUE GREEN"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_thelab_g4_1666.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col card__caption--color">
                        <p>WALL: JOTUN 7629 <strong>ANTIQUE GREEN</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p>For rich matt colours engineered to withstand the rigours of real life, Majestic True Beauty Matt provides a pristine finish and smoothness that enriches walls and endures year after year.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="section section--product" id="t2-product--3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="product__body">
                        <h3 class="product__title">MAJESTIC TRUE BEAUTY MATT</h3>
                        <figure class="product__image">
                            <picture>
                                <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png" type="image/webp" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png">
                                <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png" data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png"
                                    data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png" alt="FENOMASTIC Wonderwall" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint3.png">
                            </picture>

                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                aria-label="Add the product to your favourites" data-favorite="product_3" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the product to your favourites</span>
                            </button>
                        </figure>

                        <p class="product__tag">True, deep colour designed to last</p>

                        <div class="product__btn">
                            <a href="https://www.jotun.com/my/en/b2c/products/interior/majestic/majestic-true-beauty-matt.aspx" class="btn btn--line">
                                <span class="btn__text">Tìm hiểu thêm về sản phẩm</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p><strong>CHÍNH XÁC</strong> Không gian của bạn thể hiện cá tính bạn rõ nét nhất. Phá vỡ với quy ước, tránh những luật lệ và thử nghiệm với sự kết hợp màu sắc mới đáng ngạc nhiên để phản ánh bản sắc của riêng bạn và kể câu chuyện của riêng bạn.</p>
                </div>
            </div>
        </div>
    </section>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <figure class="card__item--figure col-xs-12 image-more" id="t2-image--9">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="7629 7613" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g4_acc4_0630-1.jpg"
                        data-product="3 4">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t2_9" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g4_acc4_0630-1.webp" type="image/webp"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g4_acc4_0630-1.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_moderne_g4_acc4_0630-1.jpg"
                                data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g4_acc4_0630-1.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g4_acc4_0630-1.jpg"
                                alt="WALL AND DOOR: JOTUN 7629 ANTIQUE GREEN + JOTUN 7613 NORTHERN MYSTIC" srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g4_acc4_0630-1.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                        <p>WALL AND DOOR: JOTUN 7629 <strong>ANTIQUE GREEN</strong> + JOTUN 7613 <strong>NORTHERN
                                MYSTIC</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text" style="display:none;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p>t2.text.11</p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--product" style="display:none;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6" id="t2-product--4">
                    <div class="product__body">
                        <h3 class="product__title">MAJESTIC TRUE BEAUTY SHEEN</h3>
                        <figure class="product__image">
                            <picture>
                                <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png" type="image/webp" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png">
                                <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png" data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png"
                                    data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png" alt="Fenomastic My Home Rich Matt"
                                    srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png">
                            </picture>

                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                aria-label="Add the product to your favourites" data-favorite="product_2" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the product to your favourites</span>
                            </button>
                        </figure>

                        <p class="product__tag">Perfect practicality and state-of-the-art shine</p>

                        <div class="product__btn">
                            <a href="http://jotun-gcc.com.dev02.allegro.no/products" class="btn btn--line">
                                <span class="btn__text">Tìm hiểu thêm về sản phẩm</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6" id="t2-product--5">
                    <div class="product__body">
                        <h3 class="product__title">MAJESTIC TRUE BEAUTY SHEEN</h3>
                        <figure class="product__image">
                            <picture>
                                <source data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png" type="image/png" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png">
                                <img class="img-responsive lazyload" src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png" data-src="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png"
                                    data-srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png" alt="FENOMASTIC Wonderwall" srcset="http://colourtrends.jotun.my/jotun_assets/images_front/paint_bucket/JPaint2.png">
                            </picture>

                            <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Add the product to your favourites"
                                aria-label="Add the product to your favourites" data-favorite="product_3" style="visibility: visible; animation-name: d;">
                                <i class="material-icons material-icons--first"></i>
                                <i class="material-icons material-icons--second"></i>
                                <span class="sr-only">Add the product to your favourites</span>
                            </button>
                        </figure>

                        <p class="product__tag">Perfect practicality and state-of-the-art shine</p>

                        <div class="product__btn">
                            <a href="http://jotun-gcc.com.dev02.allegro.no/products" class="btn btn--line">
                                <span class="btn__text">Tìm hiểu thêm về sản phẩm</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <figure class="card__item--figure col-xs-12 image-more" id="t2-image--10">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="3377" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0620.jpg"
                        data-product="3">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t2_10" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0620.webp" type="image/webp"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0620.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_moderne_acc1_0620.jpg"
                                data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0620.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0620.jpg"
                                alt="WALL: JOTUN 3377 SLATE LAVENDER" srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_acc1_0620.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                        <p>WALL: JOTUN 3377 <strong>SLATE LAVENDER</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <p>
                                <strong>CAM KẾT MÀU CHÍNH XÁC</strong><br>
                                Công nghệ chất tạo màu tiên tiến của Jotun đem lại độ chính xác màu cao. Với sự cam kết màu chính xác, bạn có thể chắc chắn rằng màu thực tế sẽ giống như màu bạn chọn trên bảng màu
                            </p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-4 col-lg-push-8">
                    <div class="row">
                        <figure class="card__item--figure col-xs-12 col-md-6 col-lg-12 image-more" id="t2-image--12">
                            <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                                data-toggle="modal" data-target="#plus" data-colors="12076" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_ny4_0726.jpg"
                                data-product="3">
                                <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                            </button>

                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t2_12" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <picture>
                                    <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_ny4_0726.webp" type="image/webp"
                                        srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_ny4_0726.webp">
                                    <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_moderne_ny4_0726.jpg"
                                        data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_ny4_0726.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_ny4_0726.jpg"
                                        alt="WALL: JOTUN 12076 MODERN BEIGE" srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_ny4_0726.jpg">
                                </picture>
                            </figure>

                            <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                                <p>WALL: JOTUN 12076 <strong>MODERN BEIGE</strong></p>
                            </figcaption>
                        </figure>

                        <div class="color-group col-xs-12 col-md-6 col-lg-12">
                            <figure class="color-card  color-card--hex js-color-card u-bg--7628 wow slideRightFade"
                                data-color-id="7628" style="visibility: visible; animation-name: i;">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 7628 <strong>Treasure</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="7628" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>
                            <figure class="color-card  color-card--hex js-color-card u-bg--12084 wow slideRightFade"
                                data-color-id="12084" style="visibility: visible; animation-name: i;">
                                <figcaption class="color-card__caption">
                                    <p>JOTUN 12084 <strong>Dusky Peach</strong></p>
                                </figcaption>

                                <div class="color-card__helper">
                                    <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                        <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                            fullscreen</span>
                                    </button>
                                </div>

                                <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                    data-favorite="12084" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Add the colour chip to your favourites</span>
                                </button>
                            </figure>

                            <small><a href="<?php echo $this->config->item("refined_link"); ?>#allcolors" class="js-btn-scroll">Xem tất cả các màu cùng nhau ở cuối trang</a></small>
                        </div> <!-- /.color-group -->
                    </div>
                </div>

                <figure class="card__item--figure col-xs-12 col-md-12 col-lg-8 col-lg-pull-4 image-more" id="t2-image--11">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="7628 12084" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g3_a3_0715.jpg"
                        data-product="3">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t2_11" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g3_a3_0715.webp" type="image/webp"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g3_a3_0715.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_moderne_g3_a3_0715.jpg"
                                data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g3_a3_0715.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g3_a3_0715.jpg"
                                alt="WALL: JOTUN 7628 TREASURE PLATE: JOTUN 12084 DUSKY PEACH" srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_g3_a3_0715.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                        <p>WALL: JOTUN 7628 <strong>TREASURE</strong><br> BOARD: JOTUN 12084 <strong>DUSKY PEACH</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <div class="section section--text">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <p>Ngôi nhà giống như một kho báu thời 
gian, nơi lưu giữ các đồ vật lưu niệm, 
các mảnh ghép cuộc sống, các tác 
phẩm nghệ thuật theo không gian 
và thời gian khác nhau... Những vật 
phẩm mà chúng ta thu thập trong 
suốt cuộc hành trình của mình, được 
phản chiếu trong sự tương phản hài 
hòa, dung dị, như khi ta đối diện với 
chính mình, tự sự về câu chuyện của 
chính cuộc đời ta.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--image">
        <div class="container">
            <div class="row">
                <figure class="card__item--figure col-xs-12 image-more" id="t2-image--13">
                    <button type="button" class="btn--plus js-btn-plus" title="Read more" aria-label="Read more"
                        data-toggle="modal" data-target="#plus" data-colors="12082 1624" data-image="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y4_0655.jpg"
                        data-product="3 4">
                        <span class="btn__text">+</span> <span class="sr-only">Read more</span>
                    </button>

                    <figure class="card__image">
                        <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                            aria-label="Save your favourites" data-favorite="t2_13" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Save your favourites</span>
                        </button>

                        <picture>
                            <source data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y4_0655.webp" type="image/webp"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y4_0655.webp">
                            <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_moderne_y4_0655.jpg" data-src="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y4_0655.jpg"
                                data-srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y4_0655.jpg" alt="WALL: JOTUN 12082 REFINED YELLOW CEILING: JOTUN 1624 SKYLIGHT CORNICE: JOTUN 1624 SKYLIGHT SKIRTING: JOTUN 12082 REFINED YELLOW"
                                srcset="<?php echo base_url(); ?>assets/media/images/t2/Jotun_moderne_y4_0655.jpg">
                        </picture>
                    </figure>

                    <figcaption class="card__caption card__caption--col card__caption--color js-card-caption">
                        <p>WALL: JOTUN 12082 <strong>REFINED YELLOW</strong><br> CEILING: JOTUN 1624 <strong>SKYLIGHT</strong></p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>

    <section class="section section--cta text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>Colour your own space</h2>
                <p class="lead">Chọn màu sắc cho ngôi nhà có vẻ như là một công việc 
không hề đơn giản và tốn nhiều thời gian. Tuy nhiên với ứng 
dụng phối màu Jotun Color Design sẽ giúp cho quy trình 
này trở nên đơn giản hơn. Download tại <a href="https://itunes.apple.com/no/app/jotun-colourdesign/id1312722535?mt=8">App Store</a> eller <a href="https://play.google.com/store/apps/details?id=com.jotun.colourdesign">Google Play.</a></p>
            </div>
        </div>
    </section>

    <div class="section section--video js-section-video" id="mood">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-8 col-md-offset-2">
                    <h2>Tạo phong cách Tinh tế</h2>
                    <p>Tạo không gian cá tính của bạn với màu sắc được thiết kế bởi Jotun. Lấy ý tưởng và nguồn cảm hứng về cách sử dụng và kết hợp màu sắc từ chủ đề màu Tinh tế.</p>

                    <div class="column__item column__item--video">
                        <div class="video-container keep-16-9 js-video-container">
                            <span class="video-overlay js-video-overlay" data-bg="<?php echo base_url(); ?>assets/media/images/t2/video-2-refined.jpg"
                                data-bg-mobile="<?php echo base_url(); ?>assets/media/images/t2/video-2-refined.jpg" style="background-image: url(&quot;<?php echo base_url(); ?>assets/media/images/t2/video-2-refined.jpg&quot;);"></span>
                            <iframe class="video js-video" title="Create your personal space with colours designed by Jotun. Get ideas and inspiration on how to use and combine colours from the REFINED colour theme."
                                src="https://www.youtube.com/embed/4rUiwE57_UY?enablejsapi=1&rel=0&showinfo=0" width="720" height="1280" frameborder="0"
                                allowfullscreen="" id="widget4"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section section--colors" id="allcolors">
        <div class="container">
            <div class="row">
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--7626 wow slideRightFade"
                        data-color-id="7626" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 7626 <strong>Airy Green</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="7626" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--7627 wow slideRightFade"
                        data-color-id="7627" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 7627 <strong>Refresh</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="7627" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--7628 wow slideRightFade"
                        data-color-id="7628" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 7628 <strong>Treasure</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="7628" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--7629 wow slideRightFade"
                        data-color-id="7629" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 7629 <strong>Antique Green</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="7629" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12079 wow slideRightFade"
                        data-color-id="12079" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12079 <strong>Gleam</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12079" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12080 wow slideRightFade"
                        data-color-id="12080" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12080 <strong>Soft Radiance</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12080" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12081 wow slideRightFade"
                        data-color-id="12081" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12081 <strong>Silky Yellow</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12081" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
                <div class="color-group col-xs-12 col-md-6">
                    <figure class="color-card  color-card--hex js-color-card u-bg--12082 wow slideRightFade"
                        data-color-id="12082" style="visibility: visible; animation-name: i;">
                        <figcaption class="color-card__caption">
                            <p>JOTUN 12082 <strong>Refined Yellow</strong></p>
                        </figcaption>

                        <div class="color-card__helper">
                            <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                    fullscreen</span>
                            </button>
                        </div>

                        <button class="btn btn--favorite wow heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                            data-favorite="12082" style="visibility: visible; animation-name: d;">
                            <i class="material-icons material-icons--first"></i>
                            <i class="material-icons material-icons--second"></i>
                            <span class="sr-only">Add the colour chip to your favourites</span>
                        </button>
                    </figure>
                </div>
            </div>
            <div class="row text-center">
                <p>Những màu sơn trên đây gần với màu sơn thực tế trong kỹ thuật in hiện đại cho phép. Khi yêu cầu có độ chính xác cao về màu sắc, nên tiến hành thi công mẫu. 
Những màu sắc trên chỉ chính xác cho sản phẩm Majestic hoặc các sản phẩm khác của Jotun khi sử dụng máy pha màu Jotun Multicolor. </p>
            </div>
        </div>
    </div>

    <section class="section text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>Express yourself with colours</h2>
                <p class="lead">Let the colours and combinations reflect and enhance who you are – your own
                    personal colour identity. Create your interior palette. Pick a colour, explore complementary
                    colours and create your unique interior palette.</p>
                <a href="<?php echo $this->config->item("palette_link"); ?>">
                    <picture>
                        <source data-srcset="<?php echo base_url(); ?>assets/media/images/banners/palettes-mockup-refined-no-2.webp" type="image/webp"
                            srcset="<?php echo base_url(); ?>assets/media/images/banners/palettes-mockup-refined-no-2.webp">
                        <img class="img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/palettes-mockup-refined-no-2.png"
                            data-src="<?php echo base_url(); ?>assets/media/images/banners/palettes-mockup-refined-no-2.png" data-srcset="<?php echo base_url(); ?>assets/media/images/banners/palettes-mockup-refined-no-2.png"
                            alt="Express yourself with colours" srcset="<?php echo base_url(); ?>assets/media/images/banners/palettes-mockup-refined-no-2.png">
                    </picture>
                </a>
            </div>
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <a class="btn btn--line" href="<?php echo $this->config->item("palette_link"); ?>">
                    <span class="btn__text">Create your personal interior palette</span>
                </a>
            </div>
        </div>
    </section>

    <section class="section section--cta text-center">
        <div class="container">
            <div class="col-xs-12 col-md-8 col-md-offset-2">
                <h2>Cách sử dụng bảng màu</h2>
                <p class="lead">Tìm hiểu thêm về các màu sắc thuộc gam màu Tinh tế.</p>
                <a class="btn btn--line" href="<?php echo $this->config->item("refined_link"); ?>#colors-t2" data-toggle="modal"
                    data-target="#colors-t2">
                    <span class="btn__text">Tinh tế</span>
                </a>
                <a class="btn btn--line" href="<?php echo $this->config->item("colour_link"); ?>">
                    <span class="btn__text">Xem tất cả 28 màu</span>
                </a>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <h2 class="text-center">Thêm màu</h2>
                <div class="card col-xs-12 col-sm-6 col-lg-4 col-lg-offset-2 ">
                    <div class="row">
                        <figure class="card__item card__item--figure col-xs-12" id="t1-image--card">
                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t1_0" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <a href="<?php echo $this->config->item("calm_link"); ?>" tabindex="-1">
                                    <picture>
                                        <source data-srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_calm_a2_0531.webp" type="image/webp"
                                            srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_calm_a2_0531.webp">
                                        <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_calm_a2_0531.jpg"
                                            data-src="<?php echo base_url(); ?>assets/media/images/forside/Jotun_calm_a2_0531.jpg" data-srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_calm_a2_0531.jpg"
                                            alt="WALL: JOTUN 1877 WARM GREY" srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_calm_a2_0531.jpg">
                                    </picture>
                                </a>
                            </figure>
                        </figure>
                        <div class="card__item card__body col-xs-12">
                            <h2 class="card__header">
                                <a class="" href="<?php echo $this->config->item("calm_link"); ?>">CALM</a>
                            </h2>
                            <p>Soft neutral tones and warm, subtle contrasts. <br class="visible-lg"><a href="<?php echo $this->config->item("calm_link"); ?>">Discover
                                    now</a></p>
                        </div>
                    </div>
                </div>
                <div class="card col-xs-12 col-sm-6 col-lg-4">
                    <div class="row">
                        <figure class="card__item card__item--figure col-xs-12" id="t3-image--card">
                            <figure class="card__image">
                                <button type="button" class="btn btn--favorite wow heartBeat js-btn-favorite" title="Save your favourites"
                                    aria-label="Save your favourites" data-favorite="t3_0" style="visibility: visible; animation-name: d;">
                                    <i class="material-icons material-icons--first"></i>
                                    <i class="material-icons material-icons--second"></i>
                                    <span class="sr-only">Save your favourites</span>
                                </button>

                                <a href="<?php echo $this->config->item("raw_link"); ?>" tabindex="-1">
                                    <picture>
                                        <source data-srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_Raw_r4-ng4_0854.webp"
                                            type="image/webp" srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_Raw_r4-ng4_0854.webp">
                                        <img class="card__image img-responsive lazyload" src="<?php echo base_url(); ?>assets/dist/Jotun_Raw_r4-ng4_0854.jpg"
                                            data-src="<?php echo base_url(); ?>assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg"
                                            data-srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg" alt="WALL: JOTUN PURE COLOR 6350 SOFT TEAL"
                                            srcset="<?php echo base_url(); ?>assets/media/images/forside/Jotun_Raw_r4-ng4_0854.jpg">
                                    </picture>
                                </a>
                            </figure>
                        </figure>
                        <div class="card__item card__body col-xs-12">
                            <h2 class="card__header">
                                <a class="" href="<?php echo $this->config->item("raw_link"); ?>">RAW</a>
                            </h2>
                            <p>Warm colours of soil and sand, peaches and earthy reds. <br class="visible-lg"><a href="<?php echo $this->config->item("raw_link"); ?>">Discover
                                    now</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal modal--plus fade" id="plus" tabindex="-1" role="dialog" aria-labelledby="modal-label">
        <div class="modal-dialog" role="document">
            <div class="modal-slider col-md-6">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <figure class="background-image modal-header lazyload js-modal-image" style="background-image: url('assets/media/images/t2/Jotun_moderne_g3_0573.jpg');">
                    <div class="modal-caption js-modal-caption"></div>
                </figure>
            </div>
            <div class="modal-info">
                <i class="material-icons zoom js-zoom" tabindex="0"></i>
                <div class="js-modal-info"></div>
            </div>
            <div class="modal-content col-md-6">
                <div class="container">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 color-group js-modal-colors"></div>
                            <div class="col-xs-12 js-modal-products"></div>
                        </div>
                    </div>
                    <div class="modal-footer text-center">
                        <div class="buttons js-modal-buttons">
                            <div class="btn-row">
                                <a class="btn btn--line" href="<?php echo $this->config->item("whislist_link"); ?>">
                                    <span class="btn__text">My favourites <span class="btn--heart"><i class="material-icons material-icons--second"></i>
                                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span></span></span>
                                </a>
                                <a class="btn btn--line" href="http://jotun-gcc.com.dev02.allegro.no/products" style="display:none;">
                                    <span class="btn__text">Choose the right product</span>
                                </a>
                            </div>
                            <div class="btn-row">
                            <div class="btn btn--line btn--facebook js-btn-facebook fb-share-button" data-href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/refined" data-layout="button" data-size="large" data-mobile-iframe="true">
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com%2Frefined&amp;src=sdkpreparse" ><i class="icon icon--facebook"></i> Share to Facebook</a>
                </div>
                                <button class="btn btn--line btn--pinterest pinterest-pin-it"><i class="icon icon--pinterest"></i>Pin
                                    to Pinterest</button>
                            </div>
                            <div class="btn-row">
                                <button type="button" class="btn btn--line" data-dismiss="modal">
                                    <span class="btn__text">Back</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal--colors fade" id="colors-t2" tabindex="-1" role="dialog" aria-labelledby="modal-label">
        <div class="modal-dialog" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <div class="modal-content">
                <div class="modal-body">

                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                <h2 class="text-center">Các sử dụng gam màu Tinh tế</h2>

                                <figure class="color-card color-card--hex js-color-card u-bg--7626" data-color-id="7626">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7626 <strong>Airy Green</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7626">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <!-- <p class="color-card__description">color.7626.text</p> -->
                                <figure class="color-card color-card--hex js-color-card u-bg--7627" data-color-id="7627">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7627 <strong>Refresh</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7627">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A fresh green nuance. This tone appears golden
                                    and thus more greenish than the mint nuance 7555 Soft Mint. The colour mixes
                                    beautifully with the darker greens 7628 Treasure and 7629 Antique Green. But it
                                    may also surprise as a lovely pastel when combined with cooler pink tones such
                                    as 12086 Rustic Pink. <br><strong>FIND THE RIGHT WHITE:</strong> 7627 Refresh
                                    functions well with 7236 Jazz and 9918 Morning Fog. 1624 Skylight, 1001 Egg
                                    White and 1453 Vanilla should be avoided, as they will appear yellow and
                                    somewhat dirty when combined.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--7628" data-color-id="7628">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7628 <strong>Treasure</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7628">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued green nuance. The colour appears as
                                    more golden and thus more greenish than 7163 Minty Breeze, which is known to
                                    many. It combines beautifully with cool white tones such as 7236 Jazz or the
                                    greyish white 8394 White Poetry. Blue is a colour that may work well as a
                                    coloured contrast, and the greenish blues 5030 St. Pauls Blue and 5180 Oslo are
                                    preferable. The colour will appear smashing against bright peach nuances such
                                    as 12083 Devine, but it also works well with the cooler pink shades. A trendy
                                    contrast may be achieved with a quite dark green nuance such as 7613 Northern
                                    Mystic.<br><strong>FIND THE RIGHT WHITE:</strong> 7628 Treasure combines
                                    perfectly with the pure white nuances 7236 Jazz, 9918 Morning Fog or 1624
                                    Skylight, but may also work well with 1001 Egg White and 1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--7629" data-color-id="7629">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7629 <strong>Antique Green</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7629">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued green nuance. It appears cooler and
                                    more bluish than the golden greens 8252 Green Harmony or 8469 Green Leaf, known
                                    to many. 7629 Antique Green is lovely against the brighter versions 7628
                                    Treasure and 7627 Refresh. Subdued greens such as this also combine
                                    harmoniously with this years´ peach and yellow tones - a more eccentric way of
                                    combining different colours. If you like blue, 5180 Oslo fits nicely. <br><strong>FIND
                                        THE RIGHT WHITE:</strong> 7629 Antique Green appears at its best against
                                    pure white tones 7236 Jazz, 9918 Morning Fog or 1624 Skylight, but may
                                    function well against 1001 Egg White and 1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12079" data-color-id="12079">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12079 <strong>Gleam</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12079">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A bright yellow nuance. The colour is faintly
                                    yellow and warm. It may appear similar to the yellow nuance 1442 Clear, but
                                    12079 Gleam is somewhat more subdued and greyish in expression. A good colour
                                    for anyone wanting a yellow, but calm atmosphere. This colour works
                                    harmoniously with golden white nuances, and the more coloured tones 12080 Soft
                                    Radiance, 12081 Silky Yellow and 10245 Ginseng. Some may like a combination of
                                    yellow and greens; the subdued nuances 8252 Green Harmony and 8469 Green Leaf
                                    may function as dark contrasts.<br><strong>FIND THE RIGHT WHITE:</strong> 12079
                                    Gleam functions well with 9918 Morning Fog, 1624 Skylight, 1001 Egg White and
                                    1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12080" data-color-id="12080">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12080 <strong>Soft Radiance</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12080">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued yellow nuance. The colour is relaxed,
                                    warm and has a slightly greyish appearance. 12080 Soft Radiance combines
                                    beautifully with brighter yellow tones such as 1442 Clear, 12079 Gleam, but
                                    also with the well-known white tones mentioned below, as well as 8395 White
                                    Comfort. If a grey contrast is desired, we recommend a warm nuance, or the
                                    combination yellow and grey may appear quite different than intended, -
                                    however, 0394 Soft Grey may work well. Among the green nuances, 8252 Green
                                    Harmony and 8469 Green Leaf fine are dark contrasts, but anyone who likes
                                    yellow brown tones may check out 1938 Tea Leaves. <br><strong>FIND THE RIGHT
                                        WHITE:</strong> 12080 Soft Radiance functions well with 9918 Morning Fog,
                                    1624 Skylight, 1001 Egg White and 1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12081" data-color-id="12081">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12081 <strong>Silky Yellow</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12081">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued yellow nuance. The colour is soft and
                                    pleasant. 12081 Silky Yellow is gorgeous combined with the brighter versions
                                    1442 Clear, 12079 Gleam and 10280 Soft Radiance. It may appear as a soft and
                                    elegant contrast to grey nuances such as 1877 Pebblestone, or the greyish brown
                                    10965 Hipster Brown. <br><strong>FIND THE RIGHT WHITE:</strong> 12081 Silky
                                    Yellow functions well with 9918 Morning Fog, 1624 Skylight, 1001 Egg White
                                    and 1453 Vanilla.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12082" data-color-id="12082">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12082 <strong>Refined Yellow</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour
                                                in fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12082">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued yellow nuance. The colour is pleasant
                                    and relaxed. It works as an elegant contrast to bright, golden nuances such as
                                    1024 Timeless and slightly golden tones such as 12080 Soft Radiance and 12082
                                    Silky Yellow, but also as an exciting contrast to deeper colours. It appears
                                    soft against warm grey nuances such as 0394 Soft Grey and 1352 Form, but 1877
                                    Pebblestone is also a quite cool contrast. <br><strong>FIND THE RIGHT WHITE:</strong>
                                    12082 Refined Yellow functions well with 9918 Morning Fog, 1624 Skylight 1001
                                    Egg White and 1453 Vanilla.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn--line" data-dismiss="modal">
                            <span class="btn__text">Back</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</main><!-- .site-main -->

<script>
    var params = {
        "badge_01": {
            method: "feed",
            link: $site_url,
            picture: $root_url + "/<?php echo base_url(); ?>assets/media/facebook.t2.image",
            name: "facebook.t2.name",
            caption: "facebook.t2.caption",
            description: "facebook.t2.text"
        },
        "badge_02": {
            method: "feed",
            link: $site_url,
            name: "facebook.t2.name.2",
            caption: "facebook.t2.caption.2"
        }
    };
</script>
<footer id="footer" class="footer section">
        <div class="container">
            <div class="btn-row">
                <a class="btn btn--line" href="<?php echo $this->config->item("whislist_link"); ?>">
                    <span class="btn__text">My favourites <span class="btn--heart"><i class="material-icons material-icons--second"></i>
                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span></span></span>
                </a>
            </div>
            <div class="btn-row">
                <a class="btn btn--line" href="<?php echo $this->config->item("palette_link"); ?>">
                    <span class="btn__text">Create your colour palette</span>
                </a>
                <a class="btn btn--line" href="http://jotun-gcc.com.dev02.allegro.no/products" style="display:none;">
                    <span class="btn__text">Choose the right product</span>
                </a>
            </div>
            <div class="btn-row">
                <div class="btn btn--line btn--facebook js-btn-facebook fb-share-button" data-href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/refined" data-layout="button" data-size="large" data-mobile-iframe="true">
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com%2Frefined&amp;src=sdkpreparse" ><i class="icon icon--facebook"></i> Share to Facebook</a>
                </div>
                <button class="btn btn--line btn--pinterest pinterest-pin-it"><i class="icon icon--pinterest"></i>Pin
                    to Pinterest</button>
            </div>
            <a href="https://jotun.com/" class="footer__logo">
                <img data-src="assets/media/logos/jotun.svg" class=" lazyload" alt="JOTUN" src="./assets/dist/jotun.svg">
                <span class="sr-only">JOTUN</span>
            </a>
            <ul class="footer-links list-unstyled">

                <li>
                    <a class="ladybloggen" href="https://www.jotun.com/no/en/corporate/Termsandconditionscorporate.aspx"
                        target="_blank">
                        Privacy, terms &amp; condition and cookie policy
                    </a>
                </li>
            </ul>
        </div>
    </footer>
    <div class="fullscreencolor color-card--fullscreen js-color-card--fullscreen" role="dialog" aria-modal="true"
        aria-labelledby="fullscreenCaption" aria-describedby="colorinfo" tabindex="-1">
        <div class="fullscreencolor__color">

            <button type="button" class="close js-color-card-close" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>

            <button type="button" class="fullscreencolor__expand js-color-fullscreen-toggle" aria-label="View the colour in fullscreen"
                aria-expanded="true" aria-controls="fullscreenContent">
                <i class="material-icons">fullscreen</i>
            </button>


            <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" aria-label="Add the colour chip to your favourites">
                <i class="material-icons material-icons--first"></i>
                <i class="material-icons material-icons--second"></i>
                <span class="sr-only">Add the colour chip to your favourites</span>
            </button>

            <div class="fullscreencolor__caption fullscreencolor__caption--color js-color-card--caption" id="fullscreenCaption"></div>
        </div>

        <div class="fullscreencolor__content" id="fullscreenContent">
            <div class="fullscreencolor__caption js-color-card--caption"></div>

            <ul class="nav nav-tabs" role="tablist" id="colorTabs">
                <li role="presentation" class="active">
                    <a href="http://jotun-gcc.com.dev02.allegro.no/refined#colorinfo" aria-controls="colorinfo" role="tab"
                        data-toggle="tab">About the colour</a>
                </li>
                <li role="presentation">
                    <a href="http://jotun-gcc.com.dev02.allegro.no/refined#colormatching" aria-controls="colormatching"
                        role="tab" data-toggle="tab">Matching colours</a>
                </li>
            </ul>

            <div class="fullscreencolor__inner">
                <div class="container">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="colorinfo">
                            <div class="fullscreencolor__info">
                                <p class="js-disclaimer-color"></p>

                                <p class="minerals-mix-text">modal.minerals.mix.text</p>
                                <p class="ceiling-mix-text">modal.ceiling.mix.text</p>

                                <p>
                                    <strong>CAM KẾT MÀU CHÍNH XÁC</strong><br>
                                     Công nghệ chất tạo màu tiên tiến của Jotun đem lại độ chính xác màu cao. Với sự cam kết màu chính xác, bạn có thể chắc chắn rằng màu thực tế sẽ giống như màu bạn chọn trên bảng màu
                                </p>

                              <p>
                                    <strong>TÁI TẠO MÀU</strong><br>
                                    Xin lưu ý rằng màu sắc sẽ khác nhau tùy thuộc vào cài đặt màn hình và độ phân giải của bạn. Ghé thăm cửa hàng Jotun gần nhất để xem các màu mẫu chính xác.
                                </p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="colormatching">
                            <div class="fullscreencolor__matching"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="cookie js-cookie" role="alert">
        <div class="container">
            <div class="cookie__row">
                <div class="cookie__left">
                    <p>
                        By using the Jotun sites, you are consenting to our use of cookies in accordance with this
                        Cookie Policy. If you do not agree to our use of cookies in this way, you should set your
                        browser settings accordingly or not use the Jotun Sites. If you disable the cookies that we
                        use, this may impact your user experience while on the Jotun Sites.
                    </p>
                </div>
                <div class="cookie__right">
                    <button type="button" class="cookie__close js-cookie-close">
                        Do not show again
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="assets/dist/messages.js"></script>
    <script type="text/javascript" src="assets/dist/jquery.min.js"></script>
    <script type="text/javascript" src="assets/dist/custom.js"></script>



    <script>
        $(window).load(function () {
            $('.js-btn-facebook').click(function () {
                FB.ui(params[$(this).attr('rel')]);
                return false;
            });
        });
    </script>

</body>

</html>