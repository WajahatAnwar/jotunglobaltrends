<main id="main" class="site-main">

        <header class="text-header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
                        <h1 class="header">Yêu<br>thích</h1>
                        <h3 class="sub-header">Bộ sưu tập màu sắc 2019</h3>
                        <p class="lead js-visible-no-favorites hidden">Tạo một danh sách mong muốn của các màu sắc yêu thích của bạn, hình ảnh nghệ thuật và sản phẩm sơn Jotun bằng cách nhấn <i class="material-icons material-icons--first">&#xE87E;</i></p>
                        <p class="lead js-visible-favorites">Thêm sản phẩm vào mục yêu thích của bạn</p>
                    </div>
                </div>
            </div>
        </header>

        <form class="wishlist-continue js-wishlist-continue col-xs-12 col-md-10 col-lg-7 js-visible-favorites"
            id="wishlist-form" method="post">
            <input class="form__check" type="hidden" name="lang" id="langOption" value="en">

            <label class="form__group">
                <span class="sr-only">Tên</span>

                <input class="form__input form-control js-validate-name" type="text" name="name" id="signup_name"
                    placeholder="Tên của bạn *" maxlength="100" autocomplete="name" required="">

                <span class="error error-invalid">Viết tên của bạn, tối thiểu hai ký tự</span>
                <span class="error error-empty">Tên của bạn *</span>
            </label>

            <label class="form__group">
                <span class="sr-only">Địa chỉ e-mail</span>

                <input class="form__input form-control js-validate-email" type="email" name="email" id="signup_email"
                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Địa chỉ email của bạn *" autocomplete="email"
                    required="">

                <span class="error error-invalid">Địa chỉ email không hợp lệ</span>
                <span class="error error-empty">Địa chỉ email của bạn</span>
            </label>

            <div class="form-inputs">
                <!-- <label class="inline-group">
                    <input class="form__check" type="checkbox" name="mail" id="signup_newsletter">
                    <span class="form__text">wishlist.signup.3</span>
                </label> -->

                <label class="inline-group">
                    <input class="form__check" type="checkbox" name="mail" id="mailCheckBox">
                    <span class="form__text">Gửi yêu thích của tôi qua e-mail</span>
                </label>
            </div>

            <button type="submit" id="create-submissions" class="btn btn--validate js-btn-finish">Lưu mục yêu thích của bạn để sử dụng sau</button>
        </form>

        <div class="section section--wishlist">
            <div class="content">

                <div id="drawer" class="drawer js-select" aria-live="polite" aria-atomic="false" aria-busy="false">
                    <div class="container">

                        <div class="js-visible-no-favorites text-center hidden">
                            <p class="lead">Bạn chưa chọn thích bất kỳ màu sắc, sản phẩm sơn JOTUN hoặc những hình ảnh nghệ thuật nào.</p>
                            <a href="<?php echo base_url(); ?>" class="btn btn--line">
                                <span class="btn__text">Tới bảng màu</span>
                            </a>
                        </div>

                        <div class="drawer__container js-visible-favorites">
                            <section class="js-tab js-tab-color js-favorited-colors" id="farger">
                                <h2>My colours</h2>
                                <div class="section section--items js-section-items">
                                    <div class="drawer__row drawer__row--colors">
                                        <div class="drawer__item drawer__item--color js-select-item" data-type="color">
                                            <figure class="color-card js-color-card u-bg--12083" data-color-id="12083">
                                                <div class="color-card__helper"><button class="btn--plus" type="button"><span
                                                            class="btn__text">+</span> <span class="sr-only">See colour
                                                            in fullscreen</span></button></div><button type="button"
                                                    class="js-btn-favorite js-btn-remove btn--remove" data-favorite="12083"
                                                    aria-label="Remove from favourites"><i class="material-icons"></i></button>
                                                <div class="color-card__caption">
                                                    <p>JOTUN 12083 <strong>Devine</strong></p>
                                                </div>
                                            </figure>
                                        </div>
                                        <div class="drawer__item drawer__item--color js-select-item" data-type="color">
                                            <figure class="color-card js-color-card u-bg--12078" data-color-id="12078">
                                                <div class="color-card__helper"><button class="btn--plus" type="button"><span
                                                            class="btn__text">+</span> <span class="sr-only">See colour
                                                            in fullscreen</span></button></div><button type="button"
                                                    class="js-btn-favorite js-btn-remove btn--remove" data-favorite="12078"
                                                    aria-label="Remove from favourites"><i class="material-icons"></i></button>
                                                <div class="color-card__caption">
                                                    <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                                                </div>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="js-tab js-tab-product js-favorited-products" id="produkter">
                                <div class="section section--items js-section-items">
                                    <h2>Sản phẩm</h2><span class="alert alert-info">
                                        <h4>js.no_liked_products</h4>
                                        <p>js.find_products</p><a href="http://jotun-gcc.com.dev02.allegro.no/colorpicker"
                                            class="btn btn--line"><span class="btn__text">js.to_colorpicker</span></a>
                                    </span>
                                    <div class="drawer__row drawer__row--products">
                                        <div class="drawer__item drawer__item--product js-select-item" data-type="product">
                                            <div class="drawer__product"><button type="button" class="js-btn-favorite js-btn-remove btn--remove"
                                                    data-favorite="product_4" aria-label="Remove from favourites"><i
                                                        class="material-icons"></i></button><a href="<?php echo base_url(); ?>assets/dist/product.4.image.1"
                                                    class="drawer__product-wrapper js-product-lightbox" data-target="lightbox0"><img
                                                        class="drawer__image lazyload" src="<?php echo base_url(); ?>assets/dist/product.4.image.1"
                                                        data-src="<?php echo base_url(); ?>assets/media/product.4.image.1"></a>
                                                <div class="drawer__text">
                                                    <h3 class="product__title">Fenomastic My Home Rich Matt</h3>
                                                    <p class="product__tag">USP 4</p>
                                                </div>
                                            </div>
                                            <div class="js-product-recommended colorpicker__lightbox hidden" id="lightbox0"><button
                                                    type="button" class="close js-product-recommended-close" aria-label="Lukk"><span
                                                        aria-hidden="true">×</span></button>
                                                <h2 class="colorpicker-product__name">Fenomastic My Home Rich Matt</h2>
                                                <h3 class="colorpicker-product__role">USP 4</h3>
                                                <div class="colorpicker-product">
                                                    <div class="colorpicker-product__image"><img src="<?php echo base_url(); ?>assets/dist/product.4.image.1"></div>
                                                    <div class="colorpicker-product__content">
                                                        <ul>
                                                            <li class="colorpicker-product__prop">USP 1</li>
                                                            <li class="colorpicker-product__prop">USP 2</li>
                                                            <li class="colorpicker-product__prop">USP 3</li>
                                                            <li class="colorpicker-product__payoff">USP 4</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="drawer__item drawer__item--product js-select-item" data-type="product">
                                            <div class="drawer__product"><button type="button" class="js-btn-favorite js-btn-remove btn--remove"
                                                    data-favorite="product_1" aria-label="Remove from favourites"><i
                                                        class="material-icons"></i></button><a href="<?php echo base_url(); ?>assets/dist/product.1.image.1"
                                                    class="drawer__product-wrapper js-product-lightbox" data-target="lightbox1"><img
                                                        class="drawer__image lazyload" src="<?php echo base_url(); ?>assets/dist/product.1.image.1"
                                                        data-src="<?php echo base_url(); ?>assets/media/product.1.image.1"></a>
                                                <div class="drawer__text">
                                                    <h3 class="product__title">Lady Design Romano</h3>
                                                    <p class="product__tag">Product pay off</p>
                                                </div>
                                            </div>
                                            <div class="js-product-recommended colorpicker__lightbox hidden" id="lightbox1"><button
                                                    type="button" class="close js-product-recommended-close" aria-label="Lukk"><span
                                                        aria-hidden="true">×</span></button>
                                                <h2 class="colorpicker-product__name">Lady Design Romano</h2>
                                                <h3 class="colorpicker-product__role">Product pay off</h3>
                                                <div class="colorpicker-product">
                                                    <div class="colorpicker-product__image"><img src="<?php echo base_url(); ?>assets/dist/product.1.image.1"></div>
                                                    <div class="colorpicker-product__content">
                                                        <ul>
                                                            <li class="colorpicker-product__prop">USP 1</li>
                                                            <li class="colorpicker-product__prop">USP 2</li>
                                                            <li class="colorpicker-product__prop">USP 3</li>
                                                            <li class="colorpicker-product__payoff">Product pay off</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section class="js-tab js-tab-image js-favorited-images" id="bilder">
                                <div class="section section--items js-section-items">
                                    <h2>Photos</h2><span class="alert alert-info">
                                        <h4>js.no_liked_images</h4>
                                        <p>js.like_favorites</p><a href="<?php echo base_url(); ?>" class="btn btn--line"><span
                                                class="btn__text">js.to_global</span></a>
                                    </span>
                                    <div class="drawer__row drawer__row--images">
                                        <div class="drawer__item drawer__item--image js-select-item" data-type="image"
                                            data-slide-id="0">
                                            <div class="drawer__background"><button type="button" class="js-btn-favorite js-btn-remove btn--remove"
                                                    data-favorite="t1_3" aria-label="Remove from favourites"><i class="material-icons"></i></button><a
                                                    href="<?php echo base_url(); ?>assets/media/images/t1/Jotun_ng3_8297.jpg"
                                                    class="js-lightbox">
                                                    <figure class="background-image lazyload" data-bg="<?php echo base_url(); ?>assets/media/images/t1/Jotun_ng3_8297.jpg"
                                                        style="background-image: url(&quot;<?php echo base_url(); ?>assets/media/images/t1/Jotun_ng3_8297.jpg&quot;);">
                                                        <figcaption class="grid-caption js-image-caption sr-only">WALL:
                                                            JOTUN 12078 <strong>COMFORT GREY</strong></figcaption>
                                                    </figure>
                                                </a>
                                                <div class="drawer__text is-collapsed" id="drawerText0"><span class="drawer__text-inner">WALL:
                                                        JOTUN 12078 <strong>COMFORT GREY</strong></span><button type="button"
                                                        class="drawer__readall js-readall" data-active-text="Close"
                                                        data-default-text="See all" aria-expanded="false" aria-controls="drawerText0"
                                                        style="">See all</button></div>
                                            </div>
                                        </div>
                                        <div class="drawer__item drawer__item--image js-select-item" data-type="image"
                                            data-slide-id="1">
                                            <div class="drawer__background"><button type="button" class="js-btn-favorite js-btn-remove btn--remove"
                                                    data-favorite="t1_2" aria-label="Remove from favourites"><i class="material-icons"></i></button><a
                                                    href="<?php echo base_url(); ?>assets/media/images/t1/Jotun_calm_a2_0531.jpg"
                                                    class="js-lightbox">
                                                    <figure class="background-image lazyload" data-bg="<?php echo base_url(); ?>assets/media/images/t1/Jotun_calm_a2_0531.jpg"
                                                        style="background-image: url(&quot;<?php echo base_url(); ?>assets/media/images/t1/Jotun_calm_a2_0531.jpg&quot;);">
                                                        <figcaption class="grid-caption js-image-caption sr-only">WALL:
                                                            JOTUN 12083 <strong>DEVINE</strong><br> WALL: JOTUN 1024
                                                            <strong>TIMELESS</strong><br> SKIRTING AND WINDOW: JOTUN
                                                            1024 <strong>TIMELESS</strong></figcaption>
                                                    </figure>
                                                </a>
                                                <div class="drawer__text is-collapsed is-overflowing" id="drawerText1"
                                                    style="padding-right: 75px;"><span class="drawer__text-inner">WALL:
                                                        JOTUN 12083 <strong>DEVINE</strong><br> WALL: JOTUN 1024
                                                        <strong>TIMELESS</strong><br> SKIRTING AND WINDOW: JOTUN 1024
                                                        <strong>TIMELESS</strong></span><button type="button" class="drawer__readall js-readall"
                                                        data-active-text="Close" data-default-text="See all"
                                                        aria-expanded="false" aria-controls="drawerText1" style="">See
                                                        all</button></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>

            </div><!-- /content -->
        </div><!-- /container -->

        <div class="section text-center js-visible-favorites">
            <div class="container">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <p class="lead">Thêm danh sách mong muốn của các màu sắc yêu thích của bạn, hình ảnh nghệ thuật và sản phẩm sơn Jotun bằng cách nhấn <i class="material-icons material-icons--first">&#xE87E;</i></p>
                    <a class="btn btn--line" href="<?php echo base_url(); ?>">
                        <span class="btn__text">Tới bảng màu</span>
                    </a>
                </div>
            </div>
        </div>

    </main><!-- .site-main -->
    
    <div class="modal modal--colors fade" id="colors-t1" tabindex="-1" role="dialog" aria-labelledby="modal-label">
        <div class="modal-dialog" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <div class="modal-content">
                <div class="modal-body">

                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-6 col-lg-offset-3">
                                <h2 class="text-center">How to use the Calm colours</h2>

                                <figure class="color-card color-card--hex js-color-card u-bg--1024" data-color-id="1024">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 1024 <strong>TIMELESS</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="1024">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Thêm chip màu vào mục yêu thích của bạn</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A yellowy, grey white nuance. It is lightly coloured
                                    and will scarcely break against white details. Consider trying it with the new
                                    beige nuances 12075 Soothing Beige and 12076 Modern Beige. Timeless is also nice in
                                    combination with green nuances such as 8252 Green Harmony og 8469 Green Leaf, the
                                    new subdued green 7628 Treasure and 7629 Antique Green. <br><strong>FIND THE RIGHT
                                        WHITE:</strong>1024 Timeless functions harmoniously with 1624 Skylight, 1001
                                    Egg White and 1453 Cotton Ball. It may appear slightly greenish and golden against
                                    cold white shades such as 9918 Classic White eller 7236 Jazz.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--10678" data-color-id="10678">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 10678 <strong>SPACE</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="10678">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A bright, subdued, slightly greyish beige nuance. It
                                    appears neither reddish nor too golden, - a perfect, calm beige nuance which
                                    combines well with many shades. If you are looking for a calm, harmonious
                                    atmosphere with beige and brown tones, the new 12075 Soothing Beige and 12076
                                    Modern Beige will work beautifully against it. <br><strong>FIND THE RIGHT WHITE:</strong>10678
                                    Space works well against 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453
                                    Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12075" data-color-id="12075">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12075 <strong>Soothing Beige</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12075">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A golden beige nuance. This colour may appear
                                    similar to the popular 1140 Sand, but it will appear slightly more subdued - a
                                    faint blackish veil will spread across the colour. The colour is perfect against
                                    darker brown nuances such as 10965 Hipster Brown, 1623 Marrakesh or 1929 Nutmeg.
                                    Among coloured tones, a subdued green will be a lovely combination, - check out
                                    8252 Green Harmony, 8469 Green Leaf or 8494 Organic Green. <br><strong>FIND THE
                                        RIGHT WHITE:</strong> 12075 Soothing Beige is nice against 9918 Classic White,
                                    1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12076" data-color-id="12076">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12076 <strong>Modern Beige</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12076">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued beige nuance. The colour is slightly
                                    darker than 12075 Soothing Beige and 1140 Sand, but brighter than our classic 1929
                                    Nutmeg and 1623 Marrakesh. Moreover, these colours fit perfectly together. Burnt
                                    reddish brown nuances, such as 2859 Whispering Red and 20118 Amber Red, is also an
                                    attractive combination.<br><strong>FIND THE RIGHT WHITE:</strong> 12076 Modern
                                    Beige works well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453
                                    Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--1622" data-color-id="1622">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 1622 <strong>Reflection</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="1622">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A slightly greyish white nuance. The colour has a
                                    reddish undertone, which will take many by surprise as it is barely visible at
                                    first sight. Soft and rustic reds, such as the new 12084 Dusky Peach, 12085 Rural,
                                    20120 Organic Red and 2856 Warm Blush are great, warm combinations against 1622
                                    Reflection. <br><strong>FIND THE RIGHT WHITE:</strong> 1622 Reflection works well
                                    with 9918 Classic White, - but appears wonderful alone, both as wall and detail
                                    colour if so desired.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12077" data-color-id="12077">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12077 <strong>Sheer Grey</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12077">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A bright, warm grey nuance. It is neither beige nor
                                    bluish grey, - rather more like a bright, cool mole nuance. 12077 Sheer Grey works
                                    perfectly against grey tones such as 10342 Sable Stone, but the reddish undertones
                                    also appear exciting for anyone wanting to pursue the red tones, in the form of the
                                    golden pink tones 20046 Savanna Sunset and 20047 Blushing Peach. Blue tones such as
                                    4618 Evening Light, 4638 Elegant Blue, 4477 Deco Blue og 4744 Sophisticated Blue
                                    are gorgeous combinations, as are the more reddish blue tones like 4109 Gustivian
                                    Blue.<br><strong>FIND THE RIGHT WHITE:</strong> 12077 Sheer Grey is at its most
                                    favourable with pure whites such as 7236 Jazz, 9918 Classic White or the warm
                                    greyish white 1622 Reflection. </p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12078" data-color-id="12078">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite is-active" type="button"
                                        title="Add the colour chip to your favourites" data-favorite="12078">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A warm grey nuance. 12078 Comfort Grey is a brighter
                                    version of the popular 0394 Soft Grey. This colour looks quite cool against
                                    contrasts such as the red tones 20120 Organic Red or 20118 Amber Red, check it also
                                    out against the darker green tone 7629 Antique Green. Blue tones such as 4618
                                    Evening Light, 4638 Elegant Blue, 4477 Deco Blue and 4744 Sophisticated Blue works
                                    harmoniously with this grey nuance, as do the warm greys 12077 Sheer Grey, 1352
                                    Form, 10429 Discrete, 10853 Velvet Grey and 10249 Vandyke Brown. <br><strong>FIND
                                        THE RIGHT WHITE:</strong> 12078 Comfort Grey works perfect with pure white
                                    colours 7236 Jazz, 9918 Classic White or 1624 Skylight, but may also be combined
                                    with 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--0394" data-color-id="0394">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 0394 <strong>SOFT GREY</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="0394">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A warm grey nuance. This is a suitable grey tone if
                                    you are looking for warm grey nuances in your house. This has been a Jotun
                                    favourite among greys for many years! Works well with other grey nuances such as
                                    12077 Sheer Grey, 1024 Timeless, 1352 Form, 10679 Washed Linen or 1376 Mist. Try it
                                    out with green tones such as 8494 Organic Green, 8469 Green Leaf and 8252 Green
                                    Harmony, - it looks great! Against pink nuances such as 20046 Savanna Sunset, 20047
                                    Blushing Peach, 2024 Senses or 2771 Rustic Terracotta it will also appear quite
                                    cool. <br><strong>FIND THE RIGHT WHITE:</strong> 0394 Soft Grey functions well with
                                    9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--10290" data-color-id="10290">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 10290 <strong>Soft Touch</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="10290">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A faint peach nuance. The colour is subdued and
                                    greyish. Comparatively it is brighter, less pink and more golden than 10580 Soft
                                    Skin, well known to many. It works beautifully with subdued white nuances such as
                                    1622 Reflection, but also with the more golden whites, thanks to its yellow
                                    undertones. Combined with colours such as 12083 Devine and 12084 Dusky Peach it
                                    forms a lovely peach coloured harmony. <br><strong>FIND THE RIGHT WHITE:</strong>
                                    12090 Soft Touch functions well with 9918 Classic White, 1624 Skylight, 1001 Egg
                                    White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12083" data-color-id="12083">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12083 <strong>Devine</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite is-active" type="button"
                                        title="Add the colour chip to your favourites" data-favorite="12083">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued peach nuance. The colour is golden,
                                    subdued and pleasant. It is not really pink, it has more of a greyish apricot/peach
                                    nuance. 12083 Devine works well with golden bright nuances such as 1024 Timeless
                                    and 1376 Mist, or with other peach nuances such as 12084 Dusky Peach and 12085
                                    Rural. It may surprise you in a positive way, when combined with green nuances such
                                    as 7628 Treasure. <br><strong>FIND THE RIGHT WHITE:</strong> 12083 Devine works
                                    well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--20119" data-color-id="20119">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 20119 <strong>Transparent Pink</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="20119">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued pink nuance. The colour is golden with
                                    grey undertones. Compared to ordinary pink nuances, it appears far more golden and
                                    subdued. The colour combines beautifully with the other pink tones 12086 Rustic
                                    Pink, 2024 Senses and 20120 Organic Red. Among the quite bright tones, both 1024
                                    Timeless, 1376 Mist and 1622 Reflection look great as combinations. It works well
                                    with 0394 Soft Grey, 1352 Form, 1032 Iron Grey to mention a few greys. Green
                                    nuances such as 7628 Treasure and 7629 Antique Green may be exciting contrasts to
                                    the sweeter 20119 Transparent Pink. <br><strong>FIND THE RIGHT WHITE:</strong>
                                    20119 Transparent Pink appears at its best with 9918 Classic White, 7236 Jazz and
                                    to some extent 1624 Skylight. 1001 Egg White and 1453 Cotton Ball may appear yellow
                                    when combined with this colour.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12086" data-color-id="12086">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12086 <strong>Rustic Pink</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12086">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued pink tone. The colour is somewhat golden.
                                    We may say that 10286 Rustic Pink is a brighter version of the well-known 2024
                                    Senses - they are beautiful together. Also try it with 20120 Organic Red or 20119
                                    Transparent Pink, <br><strong>FIND THE RIGHT WHITE:</strong> 12086 Rustic Pink
                                    functions well with 9918 Classic White, 1624 Skylight, 1001 Egg White. 1453 Cotton
                                    Ball may turn in a golden direction when combined with 10286 Rustic Pink.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12074" data-color-id="12074">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12074 <strong>Peachy</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12074">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued peach nuance. 12074 is a greyish peach
                                    tone, but it will appear fresh enough on your wall. It is brighter than 12085 Rural
                                    and slightly darker than 12084 Dusky Peach. 12074 Peachy creates a cool accent when
                                    combined with 7628 Treasure and 7629 Antique Green for the more daring, or merely
                                    as a fresh golden nuance among the beige tones such as 10678 Space, 12075 Soothing
                                    Beige, 12076 Modern Beige - or 1622 Reflection, 1024 Timeless, 10679 Washed Linen,
                                    12077 Sheer Grey, 12078 Comfort Grey and 0394 Soft Grey. The colour also combines
                                    well with 12085 Rural. <br><strong>FIND THE RIGHT WHITE:</strong> 12074 Peachy
                                    works well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton
                                    Ball.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn--line" data-dismiss="modal">
                            <span class="btn__text">Back</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal--colors fade" id="colors-t2" tabindex="-1" role="dialog" aria-labelledby="modal-label">
        <div class="modal-dialog" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <div class="modal-content">
                <div class="modal-body">

                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                <h2 class="text-center">How to use the Refined colours</h2>

                                <figure class="color-card color-card--hex js-color-card u-bg--7626" data-color-id="7626">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7626 <strong>Airy Green</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7626">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">color.7626.text</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--7627" data-color-id="7627">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7627 <strong>Refresh</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7627">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A fresh green nuance. This tone appears golden and
                                    thus more greenish than the mint nuance 7555 Soft Mint. The colour mixes
                                    beautifully with the darker greens 7628 Treasure and 7629 Antique Green. But it may
                                    also surprise as a lovely pastel when combined with cooler pink tones such as 12086
                                    Rustic Pink. <br><strong>FIND THE RIGHT WHITE:</strong> 7627 Refresh functions well
                                    with 7236 Jazz and 9918 Classic White. 1624 Skylight, 1001 Egg White and 1453
                                    Cotton Ball should be avoided, as they will appear yellow and somewhat dirty when
                                    combined.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--7628" data-color-id="7628">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7628 <strong>Treasure</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7628">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued green nuance. The colour appears as more
                                    golden and thus more greenish than 7163 Minty Breeze, which is known to many. It
                                    combines beautifully with cool white tones such as 7236 Jazz or the greyish white
                                    8394 White Poetry. Blue is a colour that may work well as a coloured contrast, and
                                    the greenish blues 5030 St. Pauls Blue and 5180 Oslo are preferable. The colour
                                    will appear smashing against bright peach nuances such as 12083 Devine, but it also
                                    works well with the cooler pink shades. A trendy contrast may be achieved with a
                                    quite dark green nuance such as 7613 Northern Mystic.<br><strong>FIND THE RIGHT
                                        WHITE:</strong> 7628 Treasure combines perfectly with the pure white nuances
                                    7236 Jazz, 9918 Classic White or 1624 Skylight, but may also work well with 1001
                                    Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--7629" data-color-id="7629">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7629 <strong>Antique Green</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7629">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued green nuance. It appears cooler and more
                                    bluish than the golden greens 8252 Green Harmony or 8469 Green Leaf, known to many.
                                    7629 Antique Green is lovely against the brighter versions 7628 Treasure and 7627
                                    Refresh. Subdued greens such as this also combine harmoniously with this years´
                                    peach and yellow tones - a more eccentric way of combining different colours. If
                                    you like blue, 5180 Oslo fits nicely. <br><strong>FIND THE RIGHT WHITE:</strong>
                                    7629 Antique Green appears at its best against pure white tones 7236 Jazz, 9918
                                    Classic White or 1624 Skylight, but may function well against 1001 Egg White and
                                    1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12079" data-color-id="12079">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12079 <strong>Gleam</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12079">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A bright yellow nuance. The colour is faintly yellow
                                    and warm. It may appear similar to the yellow nuance 1442 Clear, but 12079 Gleam is
                                    somewhat more subdued and greyish in expression. A good colour for anyone wanting a
                                    yellow, but calm atmosphere. This colour works harmoniously with golden white
                                    nuances, and the more coloured tones 12080 Soft Radiance, 12081 Silky Yellow and
                                    10245 Ginseng. Some may like a combination of yellow and greens; the subdued
                                    nuances 8252 Green Harmony and 8469 Green Leaf may function as dark contrasts.<br><strong>FIND
                                        THE RIGHT WHITE:</strong> 12079 Gleam functions well with 9918 Classic White,
                                    1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12080" data-color-id="12080">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12080 <strong>Soft Radiance</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12080">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued yellow nuance. The colour is relaxed, warm
                                    and has a slightly greyish appearance. 12080 Soft Radiance combines beautifully
                                    with brighter yellow tones such as 1442 Clear, 12079 Gleam, but also with the
                                    well-known white tones mentioned below, as well as 8395 White Comfort. If a grey
                                    contrast is desired, we recommend a warm nuance, or the combination yellow and grey
                                    may appear quite different than intended, - however, 0394 Soft Grey may work well.
                                    Among the green nuances, 8252 Green Harmony and 8469 Green Leaf fine are dark
                                    contrasts, but anyone who likes yellow brown tones may check out 1938 Tea Leaves.
                                    <br><strong>FIND THE RIGHT WHITE:</strong> 12080 Soft Radiance functions well with
                                    9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12081" data-color-id="12081">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12081 <strong>Silky Yellow</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12081">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued yellow nuance. The colour is soft and
                                    pleasant. 12081 Silky Yellow is gorgeous combined with the brighter versions 1442
                                    Clear, 12079 Gleam and 10280 Soft Radiance. It may appear as a soft and elegant
                                    contrast to grey nuances such as 1877 Pebblestone, or the greyish brown 10965
                                    Hipster Brown. <br><strong>FIND THE RIGHT WHITE:</strong> 12081 Silky Yellow
                                    functions well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453
                                    Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12082" data-color-id="12082">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12082 <strong>Refined Yellow</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12082">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued yellow nuance. The colour is pleasant and
                                    relaxed. It works as an elegant contrast to bright, golden nuances such as 1024
                                    Timeless and slightly golden tones such as 12080 Soft Radiance and 12082 Silky
                                    Yellow, but also as an exciting contrast to deeper colours. It appears soft against
                                    warm grey nuances such as 0394 Soft Grey and 1352 Form, but 1877 Pebblestone is
                                    also a quite cool contrast. <br><strong>FIND THE RIGHT WHITE:</strong> 12082
                                    Refined Yellow functions well with 9918 Classic White, 1624 Skylight 1001 Egg White
                                    and 1453 Cotton Ball.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn--line" data-dismiss="modal">
                            <span class="btn__text">Back</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal--colors fade" id="colors-t3" tabindex="-1" role="dialog" aria-labelledby="modal-label">
        <div class="modal-dialog" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <div class="modal-content">
                <div class="modal-body">

                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                <h2 class="text-center">How to use the Raw colours</h2>

                                <figure class="color-card color-card--hex js-color-card u-bg--12078" data-color-id="12078">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12078 <strong>Comfort Grey</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite is-active" type="button"
                                        title="Add the colour chip to your favourites" data-favorite="12078">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A warm grey nuance. 12078 Comfort Grey is a brighter
                                    version of the popular 0394 Soft Grey. This colour looks quite cool against
                                    contrasts such as the red tones 20120 Organic Red or 20118 Amber Red, check it also
                                    out against the darker green tone 7629 Antique Green. Blue tones such as 4618
                                    Evening Light, 4638 Elegant Blue, 4477 Deco Blue and 4744 Sophisticated Blue works
                                    harmoniously with this grey nuance, as do the warm greys 12077 Sheer Grey, 1352
                                    Form, 10429 Discrete, 10853 Velvet Grey and 10249 Vandyke Brown. <br><strong>FIND
                                        THE RIGHT WHITE:</strong> 12078 Comfort Grey works perfect with pure white
                                    colours 7236 Jazz, 9918 Classic White or 1624 Skylight, but may also be combined
                                    with 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--0394" data-color-id="0394">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 0394 <strong>SOFT GREY</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="0394">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A warm grey nuance. This is a suitable grey tone if
                                    you are looking for warm grey nuances in your house. This has been a Jotun
                                    favourite among greys for many years! Works well with other grey nuances such as
                                    12077 Sheer Grey, 1024 Timeless, 1352 Form, 10679 Washed Linen or 1376 Mist. Try it
                                    out with green tones such as 8494 Organic Green, 8469 Green Leaf and 8252 Green
                                    Harmony, - it looks great! Against pink nuances such as 20046 Savanna Sunset, 20047
                                    Blushing Peach, 2024 Senses or 2771 Rustic Terracotta it will also appear quite
                                    cool. <br><strong>FIND THE RIGHT WHITE:</strong> 0394 Soft Grey functions well with
                                    9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12084" data-color-id="12084">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12084 <strong>Dusky Peach</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12084">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued peach nuance. The colour is golden, more
                                    like an apricot/peach nuance and not a traditional pink. In many ways this is a
                                    yellowy version of the colour 2024 Senses, known to many. 12084 Dusky Peach appears
                                    new and exciting against the green nuance 7628 Treasure, and to anyone who enjoys a
                                    fresh accent, 20118 Amber Red will be a fun element! The colour will appear soft
                                    and warm when combined with the other peach nuances 12083 Devine og 12085 Rural. It
                                    works well with brighter colours such as 1024 Timeless and 10679 Washed Linen. <br><strong>FIND
                                        THE RIGHT WHITE:</strong> 12084 Dusky Peach functions well with 9918 Classic
                                    White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12085" data-color-id="12085">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12085 <strong>Rural</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12085">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A reddish brown peach nuance. The name implies
                                    countryside colours, a mixture of reddish brown earth and golden, warm sunshine.
                                    The colour is like a hybrid, somewhere in the middle between red, pink and orange.
                                    For anyone wanting to pursue these nuances, it may be combined with like-minded
                                    colours such as 12084 Dusky Peach, or with the stylish and pure version 12074
                                    Peachy. Green nuances such as 7613 Northern Mystic and 8469 Green Leaf are exciting
                                    combinations for the daring. A more neutral base, such as 12075 Soothing Beige,
                                    10678 Space and 12076 Modern Beige is, however, also a lovely combination. <br><strong>FIND
                                        THE RIGHT WHITE:</strong> 2085 Rural functions well with 9918 Classic White,
                                    1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--7628" data-color-id="7628">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7628 <strong>Treasure</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7628">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued green nuance. The colour appears as more
                                    golden and thus more greenish than 7163 Minty Breeze, which is known to many. It
                                    combines beautifully with cool white tones such as 7236 Jazz or the greyish white
                                    8394 White Poetry. Blue is a colour that may work well as a coloured contrast, and
                                    the greenish blues 5030 St. Pauls Blue and 5180 Oslo are preferable. The colour
                                    will appear smashing against bright peach nuances such as 12083 Devine, but it also
                                    works well with the cooler pink shades. A trendy contrast may be achieved with a
                                    quite dark green nuance such as 7613 Northern Mystic.<br><strong>FIND THE RIGHT
                                        WHITE:</strong> 7628 Treasure combines perfectly with the pure white nuances
                                    7236 Jazz, 9918 Classic White or 1624 Skylight, but may also work well with 1001
                                    Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--7629" data-color-id="7629">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7629 <strong>Antique Green</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7629">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued green nuance. It appears cooler and more
                                    bluish than the golden greens 8252 Green Harmony or 8469 Green Leaf, known to many.
                                    7629 Antique Green is lovely against the brighter versions 7628 Treasure and 7627
                                    Refresh. Subdued greens such as this also combine harmoniously with this years´
                                    peach and yellow tones - a more eccentric way of combining different colours. If
                                    you like blue, 5180 Oslo fits nicely. <br><strong>FIND THE RIGHT WHITE:</strong>
                                    7629 Antique Green appears at its best against pure white tones 7236 Jazz, 9918
                                    Classic White or 1624 Skylight, but may function well against 1001 Egg White and
                                    1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12086" data-color-id="12086">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12086 <strong>Rustic Pink</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12086">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued pink tone. The colour is somewhat golden.
                                    We may say that 10286 Rustic Pink is a brighter version of the well-known 2024
                                    Senses - they are beautiful together. Also try it with 20120 Organic Red or 20119
                                    Transparent Pink, <br><strong>FIND THE RIGHT WHITE:</strong> 12086 Rustic Pink
                                    functions well with 9918 Classic White, 1624 Skylight, 1001 Egg White. 1453 Cotton
                                    Ball may turn in a golden direction when combined with 10286 Rustic Pink.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--2024" data-color-id="2024">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 2024 <strong>Senses</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="2024">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A pink powder nuance. This is a golden pink nuance
                                    that will appear as both delicate and subdued. It is lovely when combined with
                                    brighter variations such as 10580 Soft Skin, 20119 Transparent Pink and 12086
                                    Devine. This colour appears as quite cool when combined with burnt terracotta red
                                    nuances such as 2771 Rustic Terracotta, 2859 Whispering Red or 2995 Dusty Red. It
                                    is also gorgeous as a golden pink contrast to darker golden grey nuances such as
                                    0394 Soft Grey and 1352 Form. <br><strong>FIND THE RIGHT WHITE:</strong> It
                                    functions well with white nuances such as 9918 Classic White, 1624 Skylight, 1001
                                    Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--20120" data-color-id="20120">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 20120 <strong>Organic Red</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="20120">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued reddish pink nuance. The colour appears
                                    warm and burnt. You will find elements of red, pink and brown in this warm nuance.
                                    Combined with brighter tones, such as 2024 Senses or 12086 Rustic Pink, the colours
                                    will create a warm colour flow. Green is also an exciting combination, check out
                                    7629 Antique Green, or more golden green shades such as 8252 Green Harmony and 8469
                                    Green Leaf. Among the basic colours, nuances such as the beige 10678 Space, 12075
                                    Soothing Beige, 12076 Modern Beige, 1140 Sand, and 1623 Marrakesh will work well.
                                    <br><strong>FIND THE RIGHT WHITE:</strong> 20120 Organic Red functions well with
                                    9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--7613" data-color-id="7613">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 7613 <strong>Northern Mystic</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="7613">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A dark, golden green nuance. The colour is deep
                                    green, perfectly balanced in the sense that it is neither blue and cool, nor golden
                                    and yellow. It works well as a beautiful contrast to a series of other beige, grey,
                                    green, golden pink and peach nuances. It may work as a dark, cool contrast to
                                    practically all the colours in this years´ colour card from LADY. Some favourites
                                    are 7628 Treasure, 7629 Antique Green, 12078 Sheer Grey and 0394 Soft Grey. <br><strong>FIND
                                        THE RIGHT WHITE:</strong> 7613 Northern Mystic functions well with 7236 Jazz,
                                    9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--12074" data-color-id="12074">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 12074 <strong>Peachy</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="12074">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A subdued peach nuance. 12074 is a greyish peach
                                    tone, but it will appear fresh enough on your wall. It is brighter than 12085 Rural
                                    and slightly darker than 12084 Dusky Peach. 12074 Peachy creates a cool accent when
                                    combined with 7628 Treasure and 7629 Antique Green for the more daring, or merely
                                    as a fresh golden nuance among the beige tones such as 10678 Space, 12075 Soothing
                                    Beige, 12076 Modern Beige - or 1622 Reflection, 1024 Timeless, 10679 Washed Linen,
                                    12077 Sheer Grey, 12078 Comfort Grey and 0394 Soft Grey. The colour also combines
                                    well with 12085 Rural. <br><strong>FIND THE RIGHT WHITE:</strong> 12074 Peachy
                                    works well with 9918 Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton
                                    Ball.</p>
                                <figure class="color-card color-card--hex js-color-card u-bg--20118" data-color-id="20118">
                                    <figcaption class="color-card__caption">
                                        <p>JOTUN 20118 <strong>Amber Red</strong></p>
                                    </figcaption>

                                    <div class="color-card__helper">
                                        <button class="btn--plus" type="button" title="View the colour in fullscreen">
                                            <span class="btn__text">+</span> <span class="sr-only">View the colour in
                                                fullscreen</span>
                                        </button>
                                    </div>

                                    <button class="btn btn--favorite heartBeat js-btn-favorite" type="button" title="Add the colour chip to your favourites"
                                        data-favorite="20118">
                                        <i class="material-icons material-icons--first"></i>
                                        <i class="material-icons material-icons--second"></i>
                                        <span class="sr-only">Add the colour chip to your favourites</span>
                                    </button>
                                </figure>

                                <p class="color-card__description">A golden red nuance. The colour appears burnt and
                                    golden. It will appear more lively and slightly more red than 2771 Rustic
                                    Terracotta, but will appear more reddish brown when compared with 2859 Whispering
                                    Red. It works well with grey and beige such as 10678 Space, 12075 Soothing Beige,
                                    12076 Modern Beige, 1622 Reflection, 1024 Timeless, 10679 Washed Linen, 10429
                                    Discrete, 12077 Sheer Grey, 12078 Comfort Grey, 0394 Soft Grey and 1973 Objective.
                                    <br><strong>FIND THE RIGHT WHITE:</strong> 20118 Amber Red works well with 9918
                                    Classic White, 1624 Skylight, 1001 Egg White and 1453 Cotton Ball.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn--line" data-dismiss="modal">
                            <span class="btn__text">Back</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fullscreencolor color-card--fullscreen js-color-card--fullscreen" role="dialog" aria-modal="true"
        aria-labelledby="fullscreenCaption" aria-describedby="colorinfo" tabindex="-1">
        <div class="fullscreencolor__color">

            <button type="button" class="close js-color-card-close" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>

            <button type="button" class="fullscreencolor__expand js-color-fullscreen-toggle" aria-label="View the colour in fullscreen"
                aria-expanded="true" aria-controls="fullscreenContent">
                <i class="material-icons">fullscreen</i>
            </button>



            <div class="fullscreencolor__caption fullscreencolor__caption--color js-color-card--caption" id="fullscreenCaption"></div>
        </div>

        <div class="fullscreencolor__content" id="fullscreenContent">
            <div class="fullscreencolor__caption js-color-card--caption"></div>

            <ul class="nav nav-tabs" role="tablist" id="colorTabs">
                <li role="presentation" class="active">
                    <a href="http://jotun-gcc.com.dev02.allegro.no/wishlist#colorinfo" aria-controls="colorinfo" role="tab"
                        data-toggle="tab">About the colour</a>
                </li>
                <li role="presentation">
                    <a href="http://jotun-gcc.com.dev02.allegro.no/wishlist#colormatching" aria-controls="colormatching"
                        role="tab" data-toggle="tab">Matching colours</a>
                </li>
            </ul>

            <div class="fullscreencolor__inner">
                <div class="container">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="colorinfo">
                            <div class="fullscreencolor__info">
                                <p class="js-disclaimer-color"></p>

                                <p class="minerals-mix-text">modal.minerals.mix.text</p>
                                <p class="ceiling-mix-text">modal.ceiling.mix.text</p>

                                 <p>
                                <strong>CAM KẾT MÀU CHÍNH XÁC</strong><br>
                                Công nghệ chất tạo màu tiên tiến của Jotun đem lại độ chính xác màu cao. Với sự cam kết màu chính xác, bạn có thể chắc chắn rằng màu thực tế sẽ giống như màu bạn chọn trên bảng màu
                            </p>

                             <p>
                                    <strong>TÁI TẠO MÀU</strong><br>
                                    Xin lưu ý rằng màu sắc sẽ khác nhau tùy thuộc vào cài đặt màn hình và độ phân giải của bạn. Ghé thăm cửa hàng Jotun gần nhất để xem các màu mẫu chính xác.
                                </p>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="colormatching">
                            <div class="fullscreencolor__matching"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <script>
        var params = {
        "badge_01" : {
            method      : "feed",
            link        : "<?php echo base_url(); ?>",
            picture     : "<?php echo base_url(); ?>assets/media/facebook.wishlist.image",
            name        : "facebook.wishlist.name",
            caption     : "facebook.wishlist.caption",
            description : "facebook.wishlist.text"
        }
    };
</script> -->
    <div class="cookie js-cookie" role="alert">
        <div class="container">
            <div class="cookie__row">
                <div class="cookie__left">
                    <p>
                        By using the Jotun sites, you are consenting to our use of cookies in accordance with this
                        Cookie Policy. If you do not agree to our use of cookies in this way, you should set your
                        browser settings accordingly or not use the Jotun Sites. If you disable the cookies that we
                        use, this may impact your user experience while on the Jotun Sites.
                    </p>
                </div>
                <div class="cookie__right">
                    <button type="button" class="cookie__close js-cookie-close">
                        Do not show again
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/messages.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/custom.js"></script>


    <script type="text/javascript" src="<?php echo base_url(); ?>assets/dist/wishlist.js"></script>

    <script>
        $(window).load(function () {
            $('.js-btn-facebook').click(function () {
                FB.ui(params[$(this).attr('rel')]);
                return false;
            });
        });
    </script>

</body>

</html>