<!DOCTYPE html>
<!-- saved from url=(0038)http://jotun-gcc.com.dev02.allegro.no/ -->
<html class="no-js " lang="en">
<!--<![endif]-->

<head data-lang="en">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:url" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="facebook.general.name">
    <meta property="og:description" content="facebook.general.text">
    <meta property="og:image" content="assets/media/facebook.general.image">

    <meta name="msapplication-TileColor" content="#fbfbfb">
    <meta name="theme-color" content="#fbfbfb">

    <title>Jotun Colour Design app – Now available for iOS/Android</title>
    <meta name="description" content="facebook.general.text">

    <link rel="manifest" href="http://jotun-gcc.com.dev02.allegro.no/manifest.json">
    <link id="main-css" href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/dist/styles.min.css" rel="stylesheet" type="text/css" media="all">

    <link rel="icon" type="image/png" href="http://jotun-gcc.com.dev02.allegro.no/favicon-16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="http://jotun-gcc.com.dev02.allegro.no/favicon-32.png" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="http://jotun-gcc.com.dev02.allegro.no/favicon-152.png">
    <meta name="msapplication-TileImage" content="favicon-478.png">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/data.json"></script>
    <script>
        var $site_url = '';
        // Picture element HTML5 shiv
      document.createElement( "picture" );
    </script>
    <script src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/dist/picturefill.min.js" async=""></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <!-- phone animation -->
    <link rel="stylesheet" type="text/css" href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/css/component2.css" />
    <link rel="stylesheet" type="text/css" href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/css/component.css" />
    
    <!-- isometric gallery -->
    
    <link rel="stylesheet" type="text/css" href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/css/isometric-demo.css" />
    <link rel="stylesheet" type="text/css" href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/css/isometric.css" />
    <link rel="stylesheet" type="text/css" href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/css/isometric.css" />
    <link rel="stylesheet" href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/css/modal-video.min.css">
    <script src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/js/modernizr.custom.js"></script>
    

</head>

<body class="home page lang-en is-ready">
<div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=760992557367437&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <a href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/" title="Skip to the main content" class="skip-navigation">To
        the main content</a>


    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu"
                    aria-expanded="false">
                    <span class="navbar__label">Menu</span>
                    <span class="navbar__icon">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </button>
                <a href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/colour-design-mobile-app" class="navbar__logo">
                    <img src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/dist/jotun.svg" alt="JOTUN">
                </a>
            </div>

                <div class="collapse navbar-collapse" id="main-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/calm">Calm</a>
                    </li>
                    <li>
                        <a href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/refined">Refined</a>
                    </li>
                    <li>
                        <a href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/raw">Raw</a>
                    </li>
                    <li>
                        <a href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/palette">Create your colour palette</a>
                    </li>
                    <li>
                        <a href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/colours">See all colours</a>
                    </li>
                    <li>
                        <a href="http://inspiration.jotun.my/" target="_blank">Inspiration Blog</a>
                    </li>
                    <li>
                        <a href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/colour-design-mobile-app">Colour Design</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="favorite-nav js-favorite-nav is-visible">
        <a href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/wishlist" title="Go to my favourites" class="btn btn--transparent js-favorite-btn"
            aria-live="polite" aria-atomic="false">
            <i class="material-icons material-icons--second"></i>
            <p class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</p>
        </a>
    </div>
    
    


    <main id="main" class="site-main">
       
        <section class="main row" style="background: #eeee; padding-top:2%;">
        
<!--        style="background:#f8f8f8;padding-top: 2%;padding-left: 20%;margin: 0 auto;width: 100%;"-->
        
            <div class="content_section col-lg-6 col-md-6 col-sm-12 text-right" style="padding-top:5%; ">
                <div class="app-title col-sm-12">
                   
                    <img src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/colour-design-logo.png" alt="colour design logo" style="width: 350px;">
                    
                </div>
                
                <p class="col-sm-12">

                   Finding your perfect colour is as simple as a tap of the screen. <br>
    Download Jotun Colour Design App to Experiment <br>With Colours Like Never Before.

                </p>

                <div class="download-buttons col-sm-12">

                    <a href="https://itunes.apple.com/us/app/jotun-colourdesign/id1312722535?ls=1&mt=8" target="_blank">
                        <img style="width:140px;" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/appstore.png" alt="">
                    </a>

                    <a href="https://play.google.com/store/apps/details?id=com.jotun.colourdesign&referrer=utm_source%3Dcolour_trends_microsite_mea%26utm_medium%3Dreferral%26utm_campaign%3Dct_mea_microsite_referral_traffic" target="_blank">
                        <img style="width:140px;" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/googlestore.png" alt="">
                    </a>

                </div>

                <div class="col-sm-12"> 
                    <button class="js-video-button btn btn--line" data-video-id='nBDy_yuFapk' style="margin-right: 0px;">Watch Now</button>
                </div>
                <br>
            </div>
     
        <div id="ac-wrapper" class="ac-wrapper col-lg-6 col-md-6 col-sm-12 ">




            <div class="ac-device">
                <a href="#"><img style="       width: 240px;
margin: 0 auto;" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/1-colourdesign/colour-design-1.png"/></a>
                <h3></h3>
            </div>
            <div class="ac-grid">
                <a href="#"><img src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/1-colourdesign/colour-design-1.png"/><span></span></a>
                <a href="#"><img src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/1-colourdesign/colour-design-2.png"/><span></span></a>
                <a href="#"><img src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/1-colourdesign/colour-design-3.png"/><span></span></a>
<!--                    <a href="#"><img src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/1-colourdesign/colour-design-1.png"/><span></span></a>-->
            </div>

            <div class="scroll-below" style="text-align:center;cursor:">

                    <img class="scroller" style="cursor:pointer;width: 70px;margin-top: -20px;" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/gif-bounce-arrow.gif" alt="">

            </div>	
        </div>	
				
        </section>
        
        
        
<!--			<section class="animation isomatric section--intro text-center">-->
			<section class="text-center row" style="padding-top:5%; padding-bottom:3%;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center slideanim">
                            
                            <h2 class="app-title" style="margin-bottom:0px;color:#777;">
                                <span class="">Get</span> 
                            </h2>
                            <h1 class="app-title" style="margin-top:0px;">  
                                <span class="">Inspired</span>							
                            </h1>
                            
                            <p class="col-sm-12">
                                Discover the latest Global Colour Trends of 2019 and be inspired by a vast gallery<br> of colour collections for interior and exterior paint.
                            </p>
                    </div>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                        <img class="row" style="width:70%" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/2-getinspired/getinspired.png" alt="">
                    </div>
                    

			</section><!--/get inspired-->
		
        <section class=" row text-center " id="" style="background-color: #f1f1f1;padding-bottom:0px;background-size: contain;    background-repeat: no-repeat; min-height:200px;">

                <div class="row">
                   
                    <div class=" col-xs-12 col-sm-12 col-lg-12" style="">
                            <div class="col-lg-6 col-sm-12">
                                <img style="width:100%;" class="col-sm-12" src="getexperimentalv2.png" alt="">
                            </div>
                            <div class="col-lg-6  col-sm-12 col-xs-12 text-left">


                                <div class="title-area col-sm-12" style="    margin-top: 20%;
    margin-bottom: 20%;">
                                    <h2 class="app-title col-sm-12" style="margin-bottom:0px;color:#777;">
                                        <span class="">Get</span>

                                    </h2>

                                   <h1 class="app-title col-sm-12" style="margin-top:0px;">  
                                        <span class="">EXPERIMENTAL</span>							
                                    </h1>
                                    
                                    <p class="col-sm-12">
                                        Watch your walls come alive with vibrant colours at the tap of your finger. Be bold and experiment with various combinations, upload your own photo and share the final creations with friends and family.
                                    </p>
                                </div>

                                    

                            </div>

                    </div>
                </div>

        </section>


        <section class="row hand_phone_section  text-center " style=" background-size: cover;    padding-bottom: 5%;padding-top:5%">
            <div class="">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  text-right slideanim" style="    margin-top: 10%;">
                            
                            <div class="title-area col-sm-12">
                               
                                <h2 class="app-title" style="margin-bottom:0px;color:#777;">
                                    <span class="">Browse</span>
                                </h2>

                                <h1 class="app-title" style="margin-top:0px;">  
                                    <span class="">Products</span>							
                                </h1>
                            </div>
                            
                            <p class="col-sm-12">
                                Browse and discover the most suited Jotun <br>products for your tastes and needs.
                            </p>
                        </div>
                        <div  class="col-lg-6 col-md-6 col-sm-12 col-xs-12  text-left slideanim">
                            <img class="col-lg-6 col-md-6 col-sm-12 col-xs-12   hand_phone"  style="margin-top: 90px;  " src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/4-browseproducts/browseproduct.png" alt="">
                        </div>
                        
                </div>
            </div>
        </section>
        
        
			
        <section class=" animation_2 section text-center" >
            <div class="container">
                <div class="row ">
                  
                  
                  <div class="title-area col-sm-12">
                               
                                <h2 class="app-title" style="margin-bottom:0px;color:#777;">
                                    <span class="">Find a Multicolor Centre</span>
                                </h2>

                                <h1 class="app-title" style="margin-top:0px;">  
                                    <span class="">Near You</span>							
                                </h1>
                            </div>
                            
                            <p class="col-sm-12">
                                Identify the nearest Jotun Dealer near you. Obtain
professional advice, <br>quality products and be ready to craft
your own space.
                            </p>
                   
                </div>
                
                <div class="row">
                   <div class="col-sm-4"></div>
                   <div class="col-sm-4 slideanim">
                       <img src="find-near-you.jpg" style="margin-top:15px;width:100%; " class="col-sm-12" alt="">
                   </div>
                   <div class="col-sm-4"></div>
                    
                </div>
                
<!--
                <div class="col-xs-12 col-md-8 col-sm-12" style="margin-top:30px;">
                         <div class="ms-wrapper ms-effect-2">
                           
                            <div class="ms-perspective">
                                <div class="ms-device ms-device-two">
                                    <div class="ms-object">
                                        <div class="ms-front"></div>
                                        <div class="ms-back"></div>
                                        <div class="ms-left ms-side"></div>
                                        <div class="ms-right ms-side"></div>
                                        <div class="ms-top ms-side"></div>
                                        <div class="ms-bottom ms-side"></div>
                                    </div>
                                    <div class="ms-screens">
                                        <a class="ms-screen-1" style="background: url(http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/5-nearestdealer/1.png) no-repeat center center;background-size: contain;"></a>
                                        <a class="ms-screen-2" style="background: url(http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/5-nearestdealer/2.png) no-repeat center center;background-size: contain;"></a>


                                    </div>
                                </div>
                             </div>
                             </div>
                        
                    </div>
-->
                      
            </div>
        </section>

        <section class="section section--cta text-center" style="background:url(http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/download-section.jpg)">
            <div class="container">
               
               
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <h2> 
 
  <span>DOWNLOAD THE COLOUR DESIGN APP NOW</span>

                    <p style="font-size:16px;" class="ac-title">Finding your perfect colour is as simple as a tap of the screen.</p>
                     

                     <div class="download-buttons">
                        <a href="https://itunes.apple.com/us/app/jotun-colourdesign/id1312722535" target="_blank">
                            <img style="width:140px;" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/appstore.png" alt="">
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.jotun.colourdesign" target="_blank">
                            <img style="width:140px;" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/images/googlestore.png" alt="">
                        </a>
                    </div>

                </h2>
                </div>
            </div>
        </section>
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <iframe width="860" height="515" src="https://www.youtube.com/embed/nBDy_yuFapk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>


    <footer id="footer" class="footer section">
        <div class="container">
            <div class="btn-row">
                <a class="btn btn--line" href="http://jotun-gcc.com.dev02.allegro.no/wishlist">
                    <span class="btn__text">My favourites <span class="btn--heart"><i class="material-icons material-icons--second"></i>
                            <span class="favorite-nav__count js-favorite-nav__count" aria-label="Number of favourites">0</span></span></span>
                </a>
            </div>
            <div class="btn-row">
                <a class="btn btn--line" href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/palette">
                    <span class="btn__text">Create your colour palette</span>
                </a>
            </div>
            <div class="btn-row">
            <div class="btn btn--line btn--facebook js-btn-facebook fb-share-button" data-href="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/" data-layout="button" data-size="large" data-mobile-iframe="true">
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com%2F&amp;src=sdkpreparse" ><i class="icon icon--facebook"></i> Share to Facebook</a>
                </div>
                <!-- <button class="btn btn--line btn--pinterest pinterest-pin-it"><i class="icon icon--pinterest"></i>Pin
                    to Pinterest</button> -->
            </div>
            <a href="https://jotun.com/" class="footer__logo">
                <img data-src="assets/media/logos/jotun.svg" class=" lazyload" alt="JOTUN" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/dist/jotun.svg">
                <span class="sr-only">JOTUN</span>
            </a>
            <ul class="footer-links list-unstyled">

                <li>
                    <a class="ladybloggen" href="https://www.jotun.com/no/en/corporate/Termsandconditionscorporate.aspx"
                        target="_blank">
                        Privacy, terms &amp; condition and cookie policy
                    </a>
                </li>
            </ul>
        </div>
    </footer>
    
   
    
    <script>
        var params = {
        "badge_01" : {
            method      : "feed",
            link        : $site_url,
            picture     : $site_url+"assets/media/facebook.general.image",
            name        : "facebook.general.name",
            caption     : "facebook.general.caption",
            description : "facebook.general.text"
        }
    };
</script>
<!--
    <div class="cookie js-cookie" role="alert">
        <div class="container">
            <div class="cookie__row">
                <div class="cookie__left">
                    <p>
                        By using the Jotun sites, you are consenting to our use of cookies in accordance with this
                        Cookie Policy. If you do not agree to our use of cookies in this way, you should set your
                        browser settings accordingly or not use the Jotun Sites. If you disable the cookies that we
                        use, this may impact your user experience while on the Jotun Sites.
                    </p>
                </div>
                <div class="cookie__right">
                    <button type="button" class="cookie__close js-cookie-close">
                        Do not show again
                    </button>
                </div>
            </div>
        </div>
    </div>
-->

    <script type="text/javascript" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/dist/messages.js"></script>
    <script type="text/javascript" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/dist/jquery.min.js"></script>
    <script type="text/javascript" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/js/jquery-modal-video.min.js"></script>
<!--    <script type="text/javascript" src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/dist/scripts.min.js"></script>-->


     <script>
        $(document).ready(function(){
 		
            var bottom = $(document).height() - $(window).height();
            var section_height = ($('.main').height())+100;
            
            
                $('.scroller').click(function(){
                   $('body,html').animate({ scrollTop: section_height }, 600);
                });
            
            $('.hand_phone_section').mouseenter(function(){
                $('.hand_phone').animate({ marginTop: 0 }, 600);
            });
            
            
//            $('.hand_phone_section').mouseleave(function(){
//                $('.hand_phone').animate({ marginTop: 60 }, 600);
//            });
            
            
            $('.animation_1').mouseenter(function(){
                $('.activate').trigger('click');
            });
            
             $('.animation_1').mouseleave(function(){
                $('.activate').trigger('click');
            });
            
             $('.animation_2').mouseenter(function(){
                $('.browse_products').trigger('click');
            });
            
             $('.animation_2').mouseleave(function(){
                $('.browse_products').trigger('click');
            });
            
            $('.ms-device-two').click(function(){
                $('.browse_products').trigger('click');
            });
                 
        });
       
    </script>

    <script>
        $(window).load(function () {
            $('.js-btn-facebook').click(function () {
                FB.ui(params[$(this).attr('rel')]);
                return false;
            });
        });
    </script>

    <script>
		$(".js-video-button").modalVideo({
			youtube:{
				controls:0,
				nocookie: true
			}
		});
	</script>
    
    <!-- speical phone animations -->
    <script src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/js/appshowcase.js"></script>
    <script src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/js/classie.js"></script>
    
    <script src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/js/phoneSlideshow.js"></script>
		<script>
			$(function() {
				AppShowcase.init();
                 
			});
		</script>
		
		
		
		<script src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/js/imagesloaded.pkgd.min.js"></script>
		<script src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/js/masonry.pkgd.min.js"></script>
		<script src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/js/dynamics.min.js"></script>
		<script src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/js/classie-isometric.js"></script>
		<script src="http://ec2-13-251-222-74.ap-southeast-1.compute.amazonaws.com/assets/js/main.js"></script>
		<script>
		(function() {
			function getRandomInt(min, max) {
				return Math.floor(Math.random() * (max - min + 1)) + min;
			}

			new IsoGrid(document.querySelector('.isolayer--deco1'), {
				transform : 'translateX(33vw) translateY(-340px) rotateX(45deg) rotateZ(45deg)',
				stackItemsAnimation : {
					properties : function(pos) {
						return {
							translateZ: (pos+1) * 30,
							rotateZ: getRandomInt(-4, 4)
						};
					},
					options : function(pos, itemstotal) {
						return {
							type: dynamics.bezier,
							duration: 500,
							points: [{"x":0,"y":0,"cp":[{"x":0.2,"y":1}]},{"x":1,"y":1,"cp":[{"x":0.3,"y":1}]}],
							delay: (itemstotal-pos-1)*40
						};
					}
				}
			});
			
           
            
			
		})();
            
            
		</script>

</body>

</html>