<style>
    h1,h2,h3,h4,h5,h6{font-family:frutiger, Trajan Pro,Georgia,Times New Roman,Times,serif !important;color:#575756 !important}
</style>
<main id="main" class="site-main">
       
       <section class="main row" style="background: #eeee; padding-top:2%;">
       
<!--        style="background:#f8f8f8;padding-top: 2%;padding-left: 20%;margin: 0 auto;width: 100%;"-->
       
           <div class="hidden-xs content_section col-lg-6 col-md-6 col-sm-12 text-right" style="padding-top:5%; ">
               <div class="app-title col-sm-12">
                  
                   <img src="<?php echo base_url(); ?>/assets/images/colour-design-logo.png" alt="colour design logo" style="width: 350px;">
                   
               </div>
               
               <p class="col-sm-12">

                  Finding your perfect colour is as simple as a tap of the screen.<br>
   Download Jotun Colour Design App to Experiment<br>With Colours Like Never Before.

               </p>

               <div class="download-buttons col-sm-12">

                   <a href="https://itunes.apple.com/us/app/jotun-colourdesign/id1312722535?ls=1&mt=8" target="_blank">
                       <img style="width:140px;" src="<?php echo base_url(); ?>/assets/images/appstore.png" alt="">
                   </a>

                   <a href="https://play.google.com/store/apps/details?id=com.jotun.colourdesign&referrer=utm_source%3Dcolour_trends_microsite_mea%26utm_medium%3Dreferral%26utm_campaign%3Dct_mea_microsite_referral_traffic" target="_blank">
                       <img style="width:140px;" src="<?php echo base_url(); ?>/assets/images/googlestore.png" alt="">
                   </a>

               </div>

               <div class="col-sm-12"> 
                   <button class="js-video-button btn btn--line" data-video-id='45QyAuLmAMw' style="margin-right: 0px;">Watch Now</button>
               </div>
               <br>
           </div>
    
           <div class="visible-xs content_section col-lg-6 col-md-6 col-sm-12 text-center" style="padding-top:5%; ">
               <div class="app-title col-sm-12">
                  
                   <img src="<?php echo base_url(); ?>/assets/images/colour-design-logo.png" alt="colour design logo" style="width: 350px;">
                   
               </div>
               
               <p class="col-sm-12">

                  Finding your perfect colour is as simple as a tap of the screen. 
   Download Jotun Colour Design App to Experiment With Colours Like Never Before.

               </p>

               <div class="download-buttons col-sm-12">

                   <a href="https://itunes.apple.com/us/app/jotun-colourdesign/id1312722535?ls=1&mt=8" target="_blank">
                       <img style="width:140px;" src="<?php echo base_url(); ?>/assets/images/appstore.png" alt="">
                   </a>

                   <a href="https://play.google.com/store/apps/details?id=com.jotun.colourdesign&referrer=utm_source%3Dcolour_trends_microsite_mea%26utm_medium%3Dreferral%26utm_campaign%3Dct_mea_microsite_referral_traffic" target="_blank">
                       <img style="width:140px;" src="<?php echo base_url(); ?>/assets/images/googlestore.png" alt="">
                   </a>

               </div>

               <div class="col-sm-12"> 
                   <button class="js-video-button btn btn--line" data-video-id='nBDy_yuFapk' style="margin-right: 0px;">Watch Now</button>
               </div>
               <br>
           </div>
    
       <div id="ac-wrapper" class="ac-wrapper col-lg-6 col-md-6 col-sm-12 " style="padding:0px;    padding-bottom: 15px;">




           <div class="ac-device">
               <a href="#"><img style="       width: 240px;
margin: 0 auto;" src="<?php echo base_url(); ?>/assets/images/1-colourdesign/colour-design-1.png"/></a>
               <h3></h3>
           </div>
           <div class="ac-grid">
               <a href="#"><img src="<?php echo base_url(); ?>/assets/images/1-colourdesign/colour-design-1.png"/><span></span></a>
               <a href="#"><img src="<?php echo base_url(); ?>/assets/images/1-colourdesign/colour-design-2.png"/><span></span></a>
               <a href="#"><img src="<?php echo base_url(); ?>/assets/images/1-colourdesign/colour-design-3.png"/><span></span></a>
<!--                    <a href="#"><img src="<?php echo base_url(); ?>/assets/images/1-colourdesign/colour-design-1.png"/><span></span></a>-->
           </div>

           <div class="scroll-below" style="text-align:center;cursor:">

                   <img class="scroller" style="cursor:pointer;width: 70px;margin-top: -20px;" src="<?php echo base_url(); ?>/assets/images/gif-bounce-arrow.gif" alt="">

           </div>	
       </div>	
               
       </section>
       
       
       
<!--			<section class="animation isomatric section--intro text-center">-->
           <section class="text-center row" style="padding-top:5%; padding-bottom:3%;">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center slideanim">
                           
                           <h2 class="app-title" style="margin-bottom:0px;color:#777;">
                               <span class="">Get</span> 
                           </h2>
                           <h1 class="app-title" style="margin-top:0px;">  
                               <span class="">Inspired</span>							
                           </h1>
                           
                           <p class="col-sm-12 col-lg-6 col-lg-push-3">
                               Discover the latest Colour Collection 2019 and be inspired by a vast gallery of colour collections for interior and exterior paint.
                           </p>
                   </div>
                   
                   <div class="col-lg-12 col-md-12 col-sm-12 text-center ">
                       <img class="row hidden-xs" style="width:50%;    margin: 0 auto;" src="<?php echo base_url(); ?>/assets/images/2-getinspired/getinspired.png" alt="">
                       
                       <img class="row visible-xs" style="width:90%;     margin: 0 auto;" src="<?php echo base_url(); ?>/assets/images/2-getinspired/getinspired.png" alt="">
                   </div>
                   

           </section><!--/get inspired-->
       
       <section class=" row text-center " id="" style="background-color: #f1f1f1;padding-bottom:0px;background-size: contain;    background-repeat: no-repeat; min-height:200px;">

               <div class="row">
                  
                   <div class=" col-xs-12 col-sm-12 col-lg-12" style="">
                           
                           <div class="col-lg-6  col-sm-12 col-xs-12 text-center col-lg-push-1" style="margin-top: 5%; margin-bottom: 5%;">
                              <div class="space row hidden-xs" style="margin-top:10%"></div>
                               <div class="title-area col-sm-12 text-center" >
                                   <h2 class="app-title col-sm-12" style="margin-bottom:0px;color:#777;">
                                       <span class="">Get</span>

                                   </h2>

                                  <h1 class="app-title col-sm-12" style="margin-top:0px;">  
                                       <span class="">EXPERIMENTAL</span>							
                                   </h1>
                                   
                                   <p class="col-sm-12 col-lg-10 col-lg-push-1">
                                       Watch your walls come alive with vibrant colours at the tap of your finger. Be bold and experiment with various combinations, upload your own photo and share the final creations with friends and family.
                                   </p>
                               </div>
                           </div>
                           
                           <div class="col-lg-6 col-sm-12">
                               <img style="width:100%;" class="col-sm-12" src="<?php echo base_url(); ?>/assets/images/3-getexperimental/getexperimentalv2.png" alt="">
                           </div>

                   </div>
               </div>

       </section>


       <section class="row hand_phone_section  text-center " style="padding-bottom: 5%;padding-top:5%; background:white;">

               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 
<!--                       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  text-center col-lg-push-1 slideanim" style="    padding-top: 5%;">-->
                       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-push-1 text-center  slideanim" style="    padding-top: 5%;">
                           <div class="space row hidden-xs" style="margin-top:10%"></div>
                           <div class="title-area col-sm-12">
                              
                               <h2 class="app-title" style="margin-bottom:0px;color:#777;">
                                   <span class="">Browse</span>
                               </h2>

                               <h1 class="app-title" style="margin-top:0px;">  
                                   <span class="">Products</span>							
                               </h1>
                           </div>
                           
                           <p class="col-sm-12 col-lg-10 col-lg-push-1">
                               Browse and discover the most suited Jotun <br>products for your tastes and needs.
                           </p>
                       </div>
<!--slideanim-->

                        <div  class="col-lg-5  text-left hidden-xs">
                           <img class="hand_phone col-lg-6 hidden-xs"  style="margin-top: 90px; " src="<?php echo base_url(); ?>/assets/images/4-browseproducts/browseproduct.png" alt="">
                           
                           
                        </div>
                        
                        <div  class="col-lg-5  text-center visible-xs">
                           <img class=""  style="margin-top: 15px; width:60%; " src="<?php echo base_url(); ?>/assets/images/4-browseproducts/browseproduct.png" alt="">
                        </div>
                        
            </div>
                       
               

       </section>
       
       
       <section class="  text-center" style="padding-top:5%;padding-bottom:5%;" >
           <div class="">
               <div class="row ">
                 
                <div class="title-area col-sm-12">       
                   <h2 class="app-title" style="margin-bottom:0px;color:#777;">
                       <span class="">Find a Multicolor Centre</span>
                   </h2>

                   <h1 class="app-title" style="margin-top:0px;">  
                       <span class="">Near You</span>							
                   </h1>
                </div>
                           
                <p class="col-sm-6 col-sm-push-3">
                   Identify the nearest Jotun Dealer near you. Obtain professional advice, quality products and be ready to craft your own space.
                </p>
                  
               </div>
               
               <div class="row">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-4 slideanim text-center hidden-xs">
                      <img src="<?php echo base_url(); ?>/assets/images/5-nearestdealer/find-near-you.jpg" style="margin-top:15px;width:70%;margin-left:15%; " class="col-sm-12" alt="">
                  </div>
                  <div class="col-sm-4 slideanim text-center visible-xs">
                      <img src="<?php echo base_url(); ?>/assets/images/5-nearestdealer/find-near-you.jpg" style="margin-top:15px;width:80%; " class="col-sm-12" alt="">
                  </div>
                  <div class="col-sm-4"></div>
                   
               </div>
               
<!--
               <div class="col-xs-12 col-md-8 col-sm-12" style="margin-top:30px;">
                        <div class="ms-wrapper ms-effect-2">
                          
                           <div class="ms-perspective">
                               <div class="ms-device ms-device-two">
                                   <div class="ms-object">
                                       <div class="ms-front"></div>
                                       <div class="ms-back"></div>
                                       <div class="ms-left ms-side"></div>
                                       <div class="ms-right ms-side"></div>
                                       <div class="ms-top ms-side"></div>
                                       <div class="ms-bottom ms-side"></div>
                                   </div>
                                   <div class="ms-screens">
                                       <a class="ms-screen-1" style="background: url(<?php echo base_url(); ?>/assets/images/5-nearestdealer/1.png) no-repeat center center;background-size: contain;"></a>
                                       <a class="ms-screen-2" style="background: url(<?php echo base_url(); ?>/assets/images/5-nearestdealer/2.png) no-repeat center center;background-size: contain;"></a>


                                   </div>
                               </div>
                            </div>
                            </div>
                       
                   </div>
-->
                     
           </div>
       </section>

       <section class="section section--cta text-center" style="background:url(<?php echo base_url(); ?>/assets/images/download-section.jpg)">
           <div class="container">
              
              
               <div class="col-xs-12 col-md-8 col-md-offset-2">
                   <h2> 

 <span>DOWNLOAD THE COLOUR DESIGN APP NOW</span>

                   <p style="font-size:16px;" class="ac-title">Finding your perfect colour is as simple as a tap of the screen.</p>
                    

                    <div class="download-buttons">
                       <a href="https://itunes.apple.com/us/app/jotun-colourdesign/id1312722535" target="_blank">
                           <img style="width:140px;" src="<?php echo base_url(); ?>/assets/images/appstore.png" alt="">
                       </a>
                       <a href="https://play.google.com/store/apps/details?id=com.jotun.colourdesign" target="_blank">
                           <img style="width:140px;" src="<?php echo base_url(); ?>/assets/images/googlestore.png" alt="">
                       </a>
                   </div>

               </h2>
               </div>
           </div>
       </section>
       <!-- Modal -->
       <div class="modal fade" id="myModal" role="dialog">
           <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-body">
                       <iframe width="860" height="515" src="https://www.youtube.com/embed/nBDy_yuFapk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                   </div>
               </div>
           </div>
       </div>
