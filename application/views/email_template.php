<table style="max-width:600px;margin:0 auto" cellpadding="0" cellspacing="0" border="0" align="center" width="600">
   <tbody>
      <tr>
         <td id="container">
            <table bgcolor="f7f7f7" cellpadding="0" cellspacing="0" border="0" align="center" class="table" style="padding:20px;background-color:#f7f7f7" width="600">
               <tbody>
                  <tr>
                     <td style="width:100%;border-collapse:collapse;display:block;margin:0 auto" align="center">
                        <img src="<?php echo base_url(); ?>/assets/images/lady-email-header-en.png" alt="JOTUN IDENTITY - Colour Collection 2019" width="400" style="display:block;margin:0 auto;max-width:100%;height:auto" tabindex="0">
                        <div class="a6S" dir="ltr" style="opacity: 0.01; left: 668px; top: 214px;">
                           <div id=":i3" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Download" role="button" tabindex="0" aria-label="Download attachment " data-tooltip-class="a1V">
                              <div class="aSK J-J5-Ji aYr"></div>
                           </div>
                        </div>
                        <br>
                        <br>
                     </td>
                  </tr>
               </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#f7f7f7" style="border-collapse:collapse;background-color:#f7f7f7" width="600">
               <tbody>
                  <tr>
                     <td align="center" valign="top" style="border-collapse:collapse">
                        <table bgcolor="ffffff" cellpadding="0" cellspacing="0" border="0" align="center" class="table" style="border-collapse:collapse;padding:20px;border-style:solid;border-color:#ebebeb;border-top-width:1px;border-left-width:1px;border-right-width:1px;border-bottom-width:0px" width="600">
                           <tbody>
                              <tr>
                                 <td align="center" valign="top" style="width:100%;display:block;border-collapse:collapse;padding:36px 0">
                                    <h1 class="secondary" style="font-family:Georgia,serif;text-align:center;line-height:1">
                                       <strong style="font-family:Georgia,serif;text-align:center;line-height:1">Yêu thích</strong>
                                    </h1>
                                    <p style="font-family:Arial,sans-serif;text-align:center;padding:0 20px">These are my favourite colours, JOTUN-paints and inspirational photos.</p>
                                    <p style="font-family:Arial,sans-serif;word-break:break-all;text-align:center;opacity:0;height:0;padding:0;margin:0;overflow:hidden;color:#ffffff">..............................<wbr>..............................<wbr>..............................<wbr>..............................<wbr>.......</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <div>
                            <table align="center" bgcolor="ffffff" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; padding: 20px; border-style: solid; border-color: rgb(235, 235, 235); border-top-width: 1px; border-left-width: 1px; border-right-width: 1px; border-bottom-width: 0px" width="600">
                                <tbody>
                                    <tr>
                                        <td align="center" colspan="1" rowspan="1" valign="top" style="display: block; border-collapse: collapse; width: 100%">
                                        <h4 style="font-family: Georgia, serif; text-align: center; text-transform: uppercase; color: rgb(85, 85, 85); margin-top: 15px; margin-bottom: 15px">
                                            <strong style="font-family: Georgia, serif"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LADY paints</font></font></strong>
                                        </h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php 
                                if(!empty($color_product_array))
                                {
                                    $size = count($color_product_array);
                                    // array1 = products
                                    // array2 = names
                                    // $color_image_array;
                                    if($size > 1) {
                                        $size = $size -1;
                                    }

                                    for($i = 0; $i < $size; $i+2)
                                    {
                            ?>
                            <table align="center" bgcolor="ffffff" cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; border-style: solid; border-color: rgb(235, 235, 235); border-top-width: 1px; border-left-width: 1px; border-right-width: 1px; border-bottom-width: 0px; margin: 0; padding: 0" width="600">
                                <tbody>
                                    <tr>
                                        <td colspan="1" rowspan="1" valign="top" bgcolor="#ffffff" style="vertical-align: top;border-collapse: collapse;padding: 0px;margin: 0px;width: 50%;text-align: center;">
                                        <img border="0" id="1541142765970100001_imgsrc_url_7" alt="<?php echo $color_product_array[$i]; ?>" style="width: 200px" src="<?php echo $color_image_array[$i]; ?>">
                                        <table width="100%" cellpadding="20" cellspacing="0" border="0" style="border-collapse: collapse;text-align: center;">
                                            <tbody>
                                                <tr>
                                                    <td colspan="1" rowspan="1" bgcolor="#ffffff" style="border-collapse: collapse">
                                                    <p style="font-family: Arial, sans-serif; text-transform: uppercase"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><?php echo $color_product_array[$i]; ?></font></font></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </td>
                                        <?php if($size != 1){ ?>
                                        <td colspan="1" rowspan="1" valign="top" bgcolor="#ffffff" style="vertical-align: top;border-collapse: collapse;padding: 0px;margin: 0px;width: 50%;text-align: center;">
                                        <img border="0" id="1541142765970100001_imgsrc_url_7" alt="<?php echo $color_product_array[$i+1]; ?>" style="width: 200px" src="<?php echo $color_image_array[$i+1]; ?>">
                                        <table width="100%" cellpadding="20" cellspacing="0" border="0" style="border-collapse: collapse;text-align: center;">
                                            <tbody>
                                                <tr>
                                                    <td colspan="1" rowspan="1" bgcolor="#ffffff" style="border-collapse: collapse">
                                                    <p style="font-family: Arial, sans-serif; text-transform: uppercase"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><?php echo $color_product_array[$i+1]; ?></font></font></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </td>
                                    <?php } ?>
                                    </tr>
                                </tbody>
                            </table>
                            <?php
                            $i++;
                                    }
                                }
                            ?>
                        </div>
                  <tr>
                     <td align="center" valign="top">
                        <table bgcolor="ffffff" cellpadding="0" cellspacing="0" border="0" align="center" class="table" style="border-collapse:collapse;padding:20px;border-style:solid;border-color:#ebebeb;border-top-width:1px;border-left-width:1px;border-right-width:1px;border-bottom-width:0px" width="600">
                           <tbody>
                              <tr>
                                 <td align="center" valign="top" style="width:100%;display:block;border-collapse:collapse">
                                    <h4 class="secondary" style="font-family:Georgia,serif;text-align:center;text-transform:uppercase;color:#555555;margin-top:15px;margin-bottom:15px">
                                       <strong style="font-family:Georgia,serif">Màu sắc</strong>
                                    </h4>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <table bgcolor="ffffff" cellpadding="0" cellspacing="0" border="0" align="center" class="table" style="border-collapse:collapse;border-style:solid;border-color:#ebebeb;border-top-width:1px;border-left-width:1px;border-right-width:1px;border-bottom-width:0px;margin:0;padding:0" width="600">
                           <tbody>
                              <tr>
                                 <td bgcolor="#ffffff" class="column first" valign="top" style="vertical-align:top;border-collapse:collapse;padding:0px;margin:0px;width:50%">
                                    <?php 
                                    if(!empty($color_code_array))
                                    {
                                        $size = count($color_code_array);
                                        // array1 = products
                                        // array2 = names
                                        // $color_image_array;
                                        if($size > 1) {
                                            $size = $size -1;
                                        }
                                        // print_r($size);
                                        // die();
                                        for($i = 0; $i < $size; $i++)
                                        {
                                    ?>
                                            <table class="color u-bg--1024" width="300" height="150" border="0" style="border-collapse:collapse;background-color:<?php echo $color_code_array[$i]; ?>">
                                                <tbody>
                                                    <tr>
                                                        <td class="u-bg--1024" height="300" style="height:300px;border-collapse:collapse;background-color:<?php echo $color_code_array[$i]; ?>"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table width="300" height="150" cellpadding="20" cellspacing="0" border="0" style="border-collapse:collapse; text-align:center;">
                                                <tbody>
                                                    <tr>
                                                        <td bgcolor="#ffffff" class="contentblock" style="border-collapse:collapse">
                                                            <p style="font-family:Arial,sans-serif;text-transform:uppercase"><?php echo $color_name_array[$i]; ?></p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <?php if($size != 1){ ?>
                                            <table class="color u-bg--1024" width="300" height="150" border="0" style="border-collapse:collapse;background-color:<?php echo $color_code_array[$i+1]; ?>">
                                                <tbody>
                                                    <tr>
                                                        <td class="u-bg--1024" height="300" style="height:300px;border-collapse:collapse;background-color:<?php echo $color_code_array[$i+1]; ?>"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table width="300" height="150" cellpadding="20" cellspacing="0" border="0" style="border-collapse:collapse; text-align:center;">
                                                <tbody>
                                                    <tr>
                                                        <td bgcolor="#ffffff" class="contentblock" style="border-collapse:collapse">
                                                            <p style="font-family:Arial,sans-serif;text-transform:uppercase"><?php echo $color_name_array[$i+1]; ?></p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                    <?php 
                                            }
                                        $i++;
                                        } 
                                    }
                                    ?>
                                 </td>
                              </tr>
                           </tbody>
                        </table>

                        <table cellpadding="0" bgcolor="ffffff" cellspacing="0" border="0" align="center" class="table" style="border-collapse:collapse;border-style:solid;border-color:#ebebeb;border-top-width:1px;border-left-width:1px;border-right-width:1px;border-bottom-width:1px" width="600">
                           <tbody>
                              <tr>
                                 <td align="center" valign="top" style="display:block;width:100%;border-collapse:collapse">
                                    <p class="btn" style="margin-left:20px;margin-right:20px;margin-top:20px;margin-bottom:20px;text-align:center">
                                       <a href="<?php echo base_url(); ?>" target="_blank">Tới bảng màu</a>
                                    </p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <table cellpadding="20" bgcolor="f7f7f7" cellspacing="0" border="0" align="center" class="table" style="border-collapse:collapse;background-color:#f7f7f7" width="600">
                           <tbody>
                              <tr>
                                 <td style="display:block;border-collapse:collapse">
                                    <p style="color:#555555;font-size:12px;font-family:Arial,sans-serif;margin-top:0;margin-bottom:15px;padding-top:0;padding-bottom:0;line-height:18px;text-align:center" class="reminder">You are receiving this email because you saved your favourites at <a href="<?php echo base_url(); ?>" style="color:#555555;text-decoration:underline" target="_blank" >CÁ TÍNH - Bộ sưu tập màu sắc 2019</a>.</p>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>