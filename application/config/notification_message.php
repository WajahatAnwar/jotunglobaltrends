<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['wishlist_link'] = "http://colourtrends.jotun.my/wishlist";
$config['wishlist_saved'] = "http://colourtrends.jotun.my/wishlist_saved";
$config['calm_link'] = "http://colourtrends.jotun.my/calm";
$config['refined_link'] = "http://colourtrends.jotun.my/refined";
$config['raw_link'] = "http://colourtrends.jotun.my/raw";
$config['palette_link'] = "http://colourtrends.jotun.my/palette";
$config['colour_link'] = "http://colourtrends.jotun.my/colours";
$config['app_page'] = "http://colourtrends.jotun.my/colour-design-mobile-app";
$config['landing'] = "http://colourtrends.jotun.my/";

